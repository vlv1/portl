# system
import uuid

# django
from django.db import IntegrityError

# third party
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

# portl
from projectme.notifications.utils import register_device, unregister_device
from projectme.utils import ErrorResponse


class RegisterDeviceView(APIView):

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='token',
                in_=openapi.IN_FORM,
                type=openapi.TYPE_STRING,
                description='Device token',
                required=True
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(description='no data')
        }
    )
    def post(self, request, platform):
        try:
            self.register_device_token(request, request.data.get('token'), platform)
        except IntegrityError as e:
            return ErrorResponse.build_text_error(
                'Failed to register device',
                status.HTTP_400_BAD_REQUEST
            )
        return Response({}, status=status.HTTP_200_OK)

    def register_device_token(self, request, token, platform):
        register_device(request.user, platform, token, uuid.uuid4().hex)


class DeregisterDeviceView(APIView):

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='token',
                in_=openapi.IN_FORM,
                type=openapi.TYPE_STRING,
                description='Device token',
                required=True
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(description='no data')
        }
    )
    def post(self, request, format=None):
        unregister_device(request.user, request.data.get('token', None))
        return Response({}, status=status.HTTP_200_OK)
