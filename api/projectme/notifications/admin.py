from django import forms
from django.contrib import admin
from markdownx.widgets import AdminMarkdownxWidget

from projectme.notifications.models import EmailNotificationConfiguration, \
    PushNotificationConfiguration, SMSNotificationConfiguration, \
    OnboardingNotification, SearchNotification, ProfileNotification, \
    TransactionNotification, FeedbackNotification


class EmailNotificationConfigurationAdmin(admin.ModelAdmin):
    list_display = ['text', 'notification_type']

    def get_form(self, request, obj=None, **kwargs):
        form = super(EmailNotificationConfigurationAdmin, self).get_form(
            request, obj, **kwargs
        )

        if obj and len([
            c for c in EmailNotificationConfiguration.TYPES
            if c[0] == obj.notification_type
        ]) == 1:
            form.base_fields['notification_type'] = forms.ChoiceField(
                choices=[c for c in EmailNotificationConfiguration.TYPES
                         if c[0] == obj.notification_type]
            )
        else:
            form.base_fields['notification_type'] = forms.ChoiceField(
                choices=[
                    c for c in EmailNotificationConfiguration.TYPES
                    if c[0] not in EmailNotificationConfiguration.objects.values_list(
                        'notification_type', flat=True
                    )
                ]
            )

        form.base_fields['text'].widget = AdminMarkdownxWidget()
        return form


class PushNotificationConfigurationAdmin(admin.ModelAdmin):
    list_display = ['text','notification_type']

    def get_form(self, request, obj=None, **kwargs):
        form = super(PushNotificationConfigurationAdmin, self).get_form(request, obj, **kwargs)

        if obj and len([
            c for c in PushNotificationConfiguration.TYPES
            if c[0] == obj.notification_type
        ]) == 1:
            form.base_fields['notification_type'] = forms.ChoiceField(
                choices=[
                    c for c in PushNotificationConfiguration.TYPES
                    if c[0] == obj.notification_type]
            )
        else:
            form.base_fields['notification_type'] = forms.ChoiceField(
                choices=[
                    c for c in PushNotificationConfiguration.TYPES
                    if c[0] not in PushNotificationConfiguration.objects.values_list(
                        'notification_type', flat=True
                    )
                ]
            )
        return form


class SMSNotificationConfigurationAdmin(admin.ModelAdmin):
    list_display = ['text','notification_type']

    def get_form(self, request, obj=None, **kwargs):
        form = super(SMSNotificationConfigurationAdmin, self).get_form(
            request, obj, **kwargs
        )

        if obj and len([
            c for c in SMSNotificationConfiguration.TYPES
            if c[0] == obj.notification_type
        ]) == 1:
            form.base_fields['notification_type'] = forms.ChoiceField(
                choices=[c for c in SMSNotificationConfiguration.TYPES
                         if c[0] == obj.notification_type])
        else:
            form.base_fields['notification_type'] = forms.ChoiceField(
                choices=[
                    c for c in SMSNotificationConfiguration.TYPES
                    if c[0] not in SMSNotificationConfiguration.objects.values_list(
                        'notification_type', flat=True
                    )
                ]
            )
        return form


class NotificationAdmin(admin.ModelAdmin):
    list_display = [
        'user', 'notification_type', 'sms_status', 'email_status',
        'push_status'
    ]
    readonly_fields = ('attributes', 'sms_text', 'email_text', 'push_text')

    def sms_text(self, instance):
        try:
            configuration = SMSNotificationConfiguration.objects.get(
                notification_type=instance.notification_type
            )
            return configuration.replace_attributes(
                configuration.text, instance
            )
        except SMSNotificationConfiguration.DoesNotExist:
            pass

        return 'Not Applicable'

    def email_text(self, instance):
        try:
            configuration = EmailNotificationConfiguration.objects.get(
                notification_type=instance.notification_type
            )
            return configuration.replace_attributes(
                configuration.text, instance
            )
        except EmailNotificationConfiguration.DoesNotExist:
            pass

        return 'Not Applicable'

    def push_text(self, instance):
        try:
            configuration = PushNotificationConfiguration.objects.get(
                notification_type=instance.notification_type
            )
            return configuration.replace_attributes(
                configuration.text, instance
            )
        except PushNotificationConfiguration.DoesNotExist:
            pass
        return 'Not Applicable'


admin.site.register(
    SMSNotificationConfiguration,
    SMSNotificationConfigurationAdmin
)
admin.site.register(
    EmailNotificationConfiguration,
    EmailNotificationConfigurationAdmin
)
admin.site.register(
    PushNotificationConfiguration,
    PushNotificationConfigurationAdmin
)

admin.site.register(OnboardingNotification, NotificationAdmin)
admin.site.register(SearchNotification, NotificationAdmin)
admin.site.register(ProfileNotification, NotificationAdmin)
admin.site.register(TransactionNotification, NotificationAdmin)
admin.site.register(FeedbackNotification, NotificationAdmin)
