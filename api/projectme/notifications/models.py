from django.conf import settings
from django.contrib.gis.db import models
from django.core.mail import EmailMessage
from django.apps import apps

from twilio.rest import TwilioRestClient
from model_utils.managers import InheritanceManager
from picklefield.fields import PickledObjectField
from markdownx.utils import markdownify

from projectme.models import TimestampedModel
from projectme.notifications.utils import change_for_initials, send_notification


class NotificationConfiguration(TimestampedModel):
    text = models.TextField(blank=True, null=True)

    # Onboarding - 1000
    MOBILE_VERIFICATION_CODE = 1001
    RESET_PASSWORD = 1002
    WELCOME_FREELANCER = 1003
    WELCOME_CLIENT = 1004
    VERIFY_EMAIL = 1005

    # Search - 2000

    # Profile - 3000
    AVAILABILITY_REQUEST = 3001
    AVAILABILITY_UPDATED = 3002
    UNLOCK_PROFILE = 3003

    # Transactions - 4000
    PURCHASE_CONFIRMATION = 4001

    # Feedback - 5000
    ONBOARDING_TYPES = (
        (MOBILE_VERIFICATION_CODE, 'Mobile Verification Code'),
        (RESET_PASSWORD, 'Reset Password'),
        (WELCOME_FREELANCER, 'Welcome to Portl - Freelancer'),
        (WELCOME_CLIENT, 'Welcome to Portl - Client'),
        (VERIFY_EMAIL, 'Verify Email')
    )

    SEARCH_TYPES = (

    )

    PROFILE_TYPES = (
        (AVAILABILITY_REQUEST, 'Availability Request'),
        (AVAILABILITY_UPDATED, 'Availability Updated'),
        (UNLOCK_PROFILE, 'Unlock Profile'),
    )

    TRANSACTION_TYPES = (
        (PURCHASE_CONFIRMATION, 'Purchase Confirmation'),
    )

    FEEDBACK_TYPES = (

    )

    TYPES = ONBOARDING_TYPES + SEARCH_TYPES + PROFILE_TYPES + \
            TRANSACTION_TYPES + FEEDBACK_TYPES

    objects = InheritanceManager()

    def replace_attributes(self, text, notification):
        attributes = dict(notification.attributes)

        text = u"{}".format(text)

        if notification.user is not None \
                and attributes.get("USER", None) is None:
            attributes["USER"] = notification.user.get_full_name()

        if notification.user is not None and \
                attributes.get("USER_FIRSTNAME", None) is None:
            attributes["USER_FIRSTNAME"] = notification.user.first_name

        if notification.related_user is not None \
                and attributes.get("RELATED_USER", None) is None:
            if notification.related_user.profile is not None \
                    and notification.related_user.profile.company is not None:
                attributes["RELATED_USER"] = u"{}".format(
                    notification.related_user.profile.company
                )
            else:
                attributes["RELATED_USER"] = \
                    notification.related_user.get_full_name()

        if notification.related_user is not None \
                and attributes.get("RELATED_USER_FIRSTNAME",None) is None:
            attributes["RELATED_USER_FIRSTNAME"] = \
                notification.related_user.first_name

        change_for_initials(notification, attributes)

        for attribute in sorted(attributes, key=len, reverse=True):
            try:
                replace_with = u"{}".format(attributes[attribute])
                text = text.replace(attribute,replace_with)
            except Exception as e:
                pass

        return text

    def send(self, notification):
        pass


class SMSNotificationConfiguration(NotificationConfiguration):
    TYPES = (
        (
            NotificationConfiguration.MOBILE_VERIFICATION_CODE,
            'Mobile Verification Code'
        ),
    )

    notification_type = models.IntegerField(choices=TYPES)

    def send(self, notification):
        # Twilio
        notification.sms_status = Notification.SENDING
        try:
            if notification.attributes.get('MOBILE', None) is None:
                mobile = notification.user.mobile.as_e164
            else:
                mobile = notification.attributes["MOBILE"]

            client = TwilioRestClient(
                settings.TWILIO_ACCOUNT_SID,
                settings.TWILIO_AUTH_TOKEN
            )

            to = mobile
            from_ = settings.TWILIO_DEFAULT_CALLERID
            body = self.replace_attributes(self.text, notification)

            client.messages.create(to=to, from_=from_, body=body)
            notification.sms_status = Notification.SENT
        except Exception as e:
            notification.sms_status = Notification.FAILED

        notification.save()


class PushNotificationConfiguration(NotificationConfiguration):

    TYPES = (
        (
            NotificationConfiguration.AVAILABILITY_REQUEST,
            'Availability Request'
        ),
        (
            NotificationConfiguration.AVAILABILITY_UPDATED,
            'Availability Updated'
        ),
        (
            NotificationConfiguration.UNLOCK_PROFILE,
            'Unlock Profile'
        ),
    )

    notification_type = models.IntegerField(choices=TYPES)

    def send(self,notification):
        # Scarface
        notification.push_status = Notification.SENDING

        try:
            alert = self.replace_attributes(self.text,notification)
            devices = notification.user.devices.all()

            badge_count = 1

            FREELANCER = 1
            if notification.user.user_type == FREELANCER:
                ClientProfile = apps.get_model("profile", "ClientProfile")
                requests = ClientProfile.objects.filter(
                    user__requests__profile__user=notification.user,
                    user__requests__updated=False
                ).distinct()
                unlocks = ClientProfile.objects.filter(
                    user__unlocked_profiles__profile__user=notification.user,
                    user__unlocked_profiles__seen=False
                ).distinct()

                badge_count = requests.count() + unlocks.count()

                if badge_count < 1:
                    badge_count = 1

            send_notification(
                devices, alert, notification.attributes, badge_count
            )
        except Exception as e:
            notification.push_status = Notification.FAILED
            notification.save()

        notification.push_status = Notification.SENT
        notification.save()


class EmailNotificationConfiguration(NotificationConfiguration):
    TYPES = (
        (
            NotificationConfiguration.RESET_PASSWORD,
            'Reset Password'
        ),
        (
            NotificationConfiguration.WELCOME_FREELANCER,
            'Welcome to Portl - Freelancer'
        ),
        (
            NotificationConfiguration.WELCOME_CLIENT,
            'Welcome to Portl - Client'
        ),
        (
            NotificationConfiguration.AVAILABILITY_REQUEST,
            'Availability Request'
        ),
        (
            NotificationConfiguration.PURCHASE_CONFIRMATION,
            'Purchase Confirmation'),
        (
            NotificationConfiguration.VERIFY_EMAIL,
            'Verify Email'
        )
    )

    notification_type = models.IntegerField(choices=TYPES)

    email_subject = models.CharField(max_length=255)
    email_template = models.CharField(max_length=255)

    def send(self,notification):
        # Mandrill
        notification.email_status = Notification.SENDING

        subject = self.replace_attributes(
            self.email_subject,
            notification
        )
        body = self.replace_attributes(
            self.text, notification
        )

        merge_vars = dict(notification.attributes)

        merge_vars["MESSAGE"] = markdownify(body)
        merge_vars["URL"] = notification.full_url()

        try:
            if notification.user:
                email = notification.user.email
            else:
                email = notification.attributes["EMAIL"]

            msg = EmailMessage(subject=subject, to=[email])
            msg.template_name = self.email_template  # A Mandrill template name
            msg.global_merge_vars = merge_vars  # Message dictionary
            msg.send()
            notification.email_status = Notification.SENT
        except Exception as e:
            notification.email_status = Notification.FAILED
        notification.save()


class Notification(TimestampedModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="notifications",
        blank=True, null=True,
        on_delete=models.CASCADE
    )
    related_user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="related_notifications",
        blank=True, null=True,
        on_delete=models.CASCADE
    )
    url = models.TextField(
        blank=True, null=True,
        help_text="The URL that is displayed in emails/sms and used to "
                  "link to certain areas of the app when clicking "
                  "the notification in app, its relative to the "
                  "frontend domain"
    )
    attributes = PickledObjectField(default={})

    FAILED = -2
    NOT_APPLICABLE = -1
    SENDING = 0
    SENT = 1

    STATUSES = (
        (FAILED, 'Failed'),
        (NOT_APPLICABLE, 'N/A'),
        (SENDING, 'Sending'),
        (SENT, 'Sent')
    )
    sms_status = models.IntegerField(choices=STATUSES,default=NOT_APPLICABLE)
    email_status = models.IntegerField(choices=STATUSES,default=NOT_APPLICABLE)
    push_status = models.IntegerField(choices=STATUSES,default=NOT_APPLICABLE)

    objects = InheritanceManager()

    def save(self, *args, **kwargs):
        created = False
        if self.pk is None:
            created = True

        super(Notification, self).save(*args, **kwargs)

        if created is True or self.email_status == Notification.SENDING:
            try:
                EmailNotificationConfiguration.objects.get(
                    notification_type=self.notification_type
                ).send(self)
            except EmailNotificationConfiguration.DoesNotExist:
                pass

        if created is True or self.sms_status == Notification.SENDING:
            try:
                SMSNotificationConfiguration.objects.get(
                    notification_type=self.notification_type
                ).send(self)
            except SMSNotificationConfiguration.DoesNotExist:
                pass

        if created is True or self.push_status == Notification.SENDING:
            try:
                PushNotificationConfiguration.objects.get(
                    notification_type=self.notification_type
                ).send(self)
            except PushNotificationConfiguration.DoesNotExist:
                pass

    def full_url(self):
        if self.url is not None:
            return "{}{}".format(settings.BASE_URL, self.url)
        return None


class OnboardingNotification(Notification):
    notification_type = models.IntegerField(
        choices=NotificationConfiguration.ONBOARDING_TYPES
    )


class SearchNotification(Notification):
    notification_type = models.IntegerField(
        choices=NotificationConfiguration.SEARCH_TYPES
    )
    search = models.ForeignKey('search.Search', on_delete=models.CASCADE)


class ProfileNotification(Notification):
    notification_type = models.IntegerField(
        choices=NotificationConfiguration.PROFILE_TYPES
    )
    profile = models.ForeignKey('profile.Profile', on_delete=models.CASCADE)


class TransactionNotification(Notification):
    notification_type = models.IntegerField(
        choices=NotificationConfiguration.TRANSACTION_TYPES
    )
    transaction = models.ForeignKey(
        'transactions.Transaction',
        on_delete=models.CASCADE
    )


class FeedbackNotification(Notification):
    notification_type = models.IntegerField(
        choices=NotificationConfiguration.FEEDBACK_TYPES
    )
    feedback = models.ForeignKey('feedback.Feedback', on_delete=models.CASCADE)
