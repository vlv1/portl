import json
from django.conf import settings
from django.apps import apps

from scarface.models import Device, Application, PushMessage, Platform


def get_or_setup_apns_application():
    app, created = Application.objects.get_or_create(
        name=settings.SCARFACE_APNS_APPLICATION_NAME
    )
    return app

def get_or_setup_gcm_application():
    app, created = Application.objects.get_or_create(
        name=settings.SCARFACE_GCM_APPLICATION_NAME
    )
    return app

def get_or_setup_apns_platform():
    app = get_or_setup_apns_application()
    apns_platform, created = Platform.objects.get_or_create(
        platform=settings.SCARFACE_APNS_PLATFORM,
        application=app,
        arn=settings.SCARFACE_APNS_ARN
    )
    apns_platform.is_registered_or_register()
    return apns_platform

def get_or_setup_gcm_platform():
    app = get_or_setup_gcm_application()
    gcm_platform, created = Platform.objects.get_or_create(
        platform=settings.SCARFACE_GCM_PLATFORM,
        application=app,
        arn=settings.SCARFACE_GCM_ARN
    )
    gcm_platform.is_registered_or_register()
    return gcm_platform

def register_device(user, platform, token, uuid):

    if platform == 'apns':
        platform = get_or_setup_apns_platform()

    elif platform == 'gcm':
        platform = get_or_setup_gcm_platform()

    device, created = Device.objects.get_or_create(
        push_token=token,
        platform=platform
    )
    device.device_id = uuid
    device.save()
    device.is_registered_or_register()
    device.users.clear()
    user.devices.add(device)


def unregister_device(user, token):
    Device.objects.filter(push_token=token).delete()

def send_message_to_user(user_id, alert, context, badge_count=1):

    devices = Device.objects.filter(users__id=user_id)

    message = PushMessage(
        message=alert,
        sound="default",
        badge_count=badge_count,
        context=json.dumps(context),
    )
    for device in devices:
        device.update()
        device.send(message)

def send_apns_message_to_devices(devices, alert, context, badge_count):
    # TODO: remove this line?
    apns_platform = get_or_setup_apns_platform()

    message = PushMessage(
        message=alert,
        sound="default",
        badge_count=badge_count,
        context=json.dumps(context),
    )
    for device in devices:
        device.update()
        device.send(message)


def send_notification(devices, alert,context, badge_count):
    send_apns_message_to_devices(devices, alert, context, badge_count)


def change_for_initials(notification, attributes):
    FREELANCER = 1
    FreelancerProfile = apps.get_model("profile", "FreelancerProfile")

    if notification.related_user is not None and \
            notification.related_user.user_type == FREELANCER:
        try:
            profile = FreelancerProfile.objects.get(
                user=notification.related_user
            )
            if profile.unlocked(notification.user) is False:
                initials = notification.related_user.initials()
                attributes["RELATED_USER"] = initials
                attributes["RELATED_USER_FIRSTNAME"] = initials
        except FreelancerProfile.DoesNotExist:
            pass
