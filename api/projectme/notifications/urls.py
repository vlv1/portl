from django.urls import re_path

from projectme.notifications.api import RegisterDeviceView, \
    DeregisterDeviceView


urlpatterns = [
    re_path(
        r'^register_gcm/$',
        RegisterDeviceView.as_view(),
        {'platform': 'gcm'},
        name="register-device"
    ),
    re_path(
        r'^register_apns/$',
        RegisterDeviceView.as_view(),
        {'platform': 'apns'},
        name="register-device"
    ),
    re_path(
        r'^deregister/$',
        DeregisterDeviceView.as_view(),
        name="deregister-device"
    ),
]
