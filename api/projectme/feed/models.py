from django.db import models

from projectme.onboarding.models import User

from projectme.profile.models import UpdateAvailabilityRequest, Profile


def get_next_sequence_id(user_id):
    try:
        fe = FeedEvent.objects.filter(user_id=user_id).latest('id')

    except:
        fe = None

    return fe.seq_id + 1 if fe else 1

def get_default_message(type):
    if type == FeedEvent.EVENT_TYPE_COMPLETE_PROFILE:
        message = "Please complete your profile"

    elif type == FeedEvent.EVENT_TYPE_CONFIRM_EMAIL:
        message = "Please confirm your email"

    elif type == FeedEvent.EVENT_TYPE_UNLOCKED:
        message = "You were unlocked"

    elif type == FeedEvent.EVENT_TYPE_AVAILABILITY_REQUEST:
        message = "You have an availability request"

    elif type == FeedEvent.EVENT_TYPE_RATED:
        message = "You were rated"

    elif type == FeedEvent.EVENT_TYPE_VETTED:
        message = "You were vetted"

    else:
        message = 'Not implemented'

    return message

class FeedEvent(models.Model):

    EVENT_TYPE_COMPLETE_PROFILE = 0
    EVENT_TYPE_CONFIRM_EMAIL = 1
    EVENT_TYPE_UNLOCKED = 2
    EVENT_TYPE_AVAILABILITY_REQUEST = 3
    EVENT_TYPE_VETTED = 4
    EVENT_TYPE_RATED = 5

    EVENT_TYPES = (
        (EVENT_TYPE_COMPLETE_PROFILE, 'Complete profile'),
        (EVENT_TYPE_CONFIRM_EMAIL, 'Confirm email'),
        (EVENT_TYPE_UNLOCKED, 'Profile unlocked'),
        (EVENT_TYPE_AVAILABILITY_REQUEST, 'Availability request'),
        (EVENT_TYPE_VETTED, 'Vetted'),
        (EVENT_TYPE_RATED, 'Rated'),
    )

    user = models.ForeignKey(
        User,
        blank=True, null=True,
        on_delete=models.CASCADE
    )

    seq_id = models.IntegerField()

    type = models.IntegerField(choices=EVENT_TYPES, default=EVENT_TYPE_COMPLETE_PROFILE)

    message = models.CharField(max_length=255)

    read = models.BooleanField(default=False, blank=True)

    availability_request = models.ForeignKey(
        UpdateAvailabilityRequest,
        blank=True, null=True,
        on_delete=models.CASCADE
    )

    client_profile = models.ForeignKey(
        Profile,
        blank=True, null=True,
        related_name='feed_event',
        on_delete=models.CASCADE
    )

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s - %s' % (self.seq_id, self.user)

    def get_json(self):

        data = {
                'type': self.type,
                'message': self.message,
                'date': self.created_at.isoformat(),
                'read': self.read
            }

        if self.availability_request_id:
            data.update({'availability_request': self.availability_request_id})

        if self.client_profile_id:
            data.update({
                'client_id': self.client_profile_id,
                })

        return {
            'user': self.user.id,
            'id': self.seq_id,
            'data': data
        }
