import boto3

from boto3.dynamodb.conditions import Key, Attr

from django.conf import settings
from django.utils import timezone

from projectme.notifications.utils import send_message_to_user

from .models import FeedEvent, get_default_message, get_next_sequence_id


def add_feed_event(user, type, send_push=True, date=timezone.now(), message=None, **kwargs):

    if not message:
        message = get_default_message(type)

    data = {
            'type': type,
            'message': message,
            'date': date.isoformat(),
            'read': False
        }

    client_profile = kwargs.pop('client_profile', None)
    if client_profile:
        kwargs['client_id'] = client_profile.id
        kwargs['client_name'] = client_profile.company.company_name

    data.update(kwargs)

    event_data = {
        'user': user.id,
        'data': data
    }

    if settings.SAVE_TO_DYNAMO:
        seq_id = store_event_to_dynamodb(event_data)

    else:
        seq_id = get_next_sequence_id(user.id)

    event_data['id'] = seq_id

    if send_push:
        send_push_notification(event_data)

    fe = FeedEvent(
        user=user,
        seq_id=seq_id,
        type=type,
        message=message,
        created_at=date
    )

    for key, value in kwargs.items():
        if key == 'read':
            fe.read = value

        elif key == 'availability_request':
            fe.availability_request_id = value

    if client_profile:
        fe.client_profile = client_profile

    fe.save()


def store_event_to_dynamodb(event_data):

    user_id = event_data['user']
    data = event_data['data']

    dynamodb = boto3.resource(
        'dynamodb',
        aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        region_name=settings.AWS_DYNAMODB_REGION_NAME,
        #endpoint_url="http://localhost:8000"
    )

    table = dynamodb.Table(settings.AWS_DYNAMODB_NAME)

    response = table.query(
        KeyConditionExpression=Key('user').eq(user_id),
        ScanIndexForward=False,
        Limit=1
    )

    try:
        item = response['Items'][0]

        seq_id = item['id']

    except:
        seq_id = 0

    seq_id = seq_id + 1

    response = table.put_item(
        Item={
            'user': user_id,
            'id': seq_id,
            'data': data
        }
    )

    return seq_id


def send_push_notification(event_data):

    user_id = event_data['user']
    data = event_data['data']
    message = data['message']

    send_message_to_user(user_id, message, data, badge_count=1)


def unread_event_count(user_id):

    if settings.SAVE_TO_DYNAMO:

        dynamodb = boto3.resource(
            'dynamodb',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            region_name=settings.AWS_DYNAMODB_REGION_NAME,
            # endpoint_url='http://localhost:8000'
        )

        table = dynamodb.Table(settings.AWS_DYNAMODB_NAME)

        response = table.query(
            KeyConditionExpression=Key('user').eq(user_id),
            FilterExpression=Attr('data.read').eq(False)
        )

        try:
            items = response['Items']

        except:
            items = []

        return len(items)

    else:

        return FeedEvent.objects.filter(
            user_id=user_id,
            read=False,
        ).count()

def read_feed_events(user_id, from_id=None):

    if settings.SAVE_TO_DYNAMO:

        dynamodb = boto3.resource(
            'dynamodb',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            region_name=settings.AWS_DYNAMODB_REGION_NAME,
            #endpoint_url='http://localhost:8000'
        )

        table = dynamodb.Table(settings.AWS_DYNAMODB_NAME)

        #   query last item for this user
        response = table.query(
            KeyConditionExpression=Key('user').eq(user_id), ScanIndexForward=False, Limit=1
        )

        try:
            item = response['Items'][0]

            last_id = item['id']

        except:
            last_id = 0

        if not last_id or (last_id and last_id == from_id):
            return []

        limit = 0
        #   load data starting from_id, not including
        if from_id:
            limit = last_id - from_id

        if limit > 1:
            response = table.query(
                KeyConditionExpression=Key('user').eq(user_id), ScanIndexForward=False, Limit=limit
            )

        try:
            items = response['Items']

        except:
            items = []

    #   read from PostgreSQL
    else:
        events = FeedEvent.objects.filter(user_id=user_id).order_by('-seq_id')

        if from_id:
            events = events.filter(seq_id__gt=from_id)

        items = [e.get_json() for e in events]

    return items

def set_read_availability_events(user_id):
    set_read_events_by_type(user_id, FeedEvent.EVENT_TYPE_AVAILABILITY_REQUEST)

def set_read_complete_profile_event(user_id):
    set_read_events_by_type(user_id, FeedEvent.EVENT_TYPE_COMPLETE_PROFILE)

def set_read_confirm_email_event(user_id):
    set_read_events_by_type(user_id, FeedEvent.EVENT_TYPE_CONFIRM_EMAIL)

def set_read_events_by_type(user_id, type):

    if settings.SAVE_TO_DYNAMO:

        dynamodb = boto3.resource(
            'dynamodb',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            region_name=settings.AWS_DYNAMODB_REGION_NAME,
            #endpoint_url='http://localhost:8000'
        )

        table = dynamodb.Table(settings.AWS_DYNAMODB_NAME)

        response = table.query(
            KeyConditionExpression=Key('user').eq(user_id),
            FilterExpression=Attr('data.read').eq(False) & Attr('data.type').eq(type)
        )

        try:
            items = response['Items']

        except:
            items = []

        for item in items:
            item['data']['read'] = True
            response = table.put_item(Item=item)

    FeedEvent.objects.filter(
        user_id=user_id,
        read=False,
        type=type
    ).update(read=True)

def set_read_feed_event(user_id, event_id):

    if settings.SAVE_TO_DYNAMO:

        dynamodb = boto3.resource(
            'dynamodb',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            region_name=settings.AWS_DYNAMODB_REGION_NAME,
            #endpoint_url='http://localhost:8000'
        )

        table = dynamodb.Table(settings.AWS_DYNAMODB_NAME)

        response = table.query(
            KeyConditionExpression=Key('user').eq(user_id),
            FilterExpression=Attr('data.read').eq(False) & Attr('data.type').eq(FeedEvent.EVENT_TYPE_AVAILABILITY_REQUEST)
        )

        try:
            items = response['Items']

        except:
            items = []

        for item in items:
            item['data']['read'] = True
            response = table.put_item(Item=item)

    FeedEvent.objects.filter(
        user_id=user_id,
        seq_id=event_id,
        read=False,
    ).update(read=True)
