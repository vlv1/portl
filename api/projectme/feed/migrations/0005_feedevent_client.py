# Generated by Django 2.1.1 on 2018-11-19 15:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('feed', '0004_auto_20181114_1307'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedevent',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='feed_event', to=settings.AUTH_USER_MODEL),
        ),
    ]
