from django.urls import re_path

from projectme.feed.api import FeedView, ReadFeedEventView

urlpatterns = [
    re_path(
        r'^read/$',
        FeedView.as_view(),
        name='feed_read'
    ),
    re_path(
        r'^read_event/(?P<id>[0-9]+)/$',
        ReadFeedEventView.as_view(),
        name="set-read-feed-event"
    ),
]