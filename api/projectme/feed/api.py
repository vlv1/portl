from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from projectme.core.models import Action
from projectme.onboarding.models import User
from projectme.profile.models import Unlock, Profile, UpdateAvailabilityRequest

from projectme.utils import ErrorResponse

from .utils import read_feed_events, set_read_feed_event, unread_event_count
from .serializers import FeedEventSerializer


class FeedView(APIView):
    """
    Returns feed events for a freelancer
    """
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                'from_id',
                openapi.IN_QUERY,
                "(optional) load events after event with this id",
                type=openapi.TYPE_INTEGER
            ),
        ],
        responses={
            200: openapi.Response('response description', FeedEventSerializer),
        }
    )
    def get(self, request):
        user = request.user
        if user.user_type == User.CLIENT:
            return ErrorResponse.build_text_error(
                "Only available for freelancer",
                status.HTTP_400_BAD_REQUEST
            )

        from_id = request.query_params.get('from_id', None)
        if from_id:
            try:
                from_id = int(from_id)
            except:
                from_id = None

        events = read_feed_events(request.user.id, from_id)
        profile = Profile.objects.get_subclass(user=request.user)
        unlock_count = Unlock.objects.filter(profile=profile).count()
        request_count = UpdateAvailabilityRequest.objects.filter(profile=profile).count()
        unread_count = unread_event_count(request.user.id)

        # track action
        Action.track(user=user, verb='viewed notifications')

        return Response(
            {
                "events": events,
                "unread_count": unread_count,
                "unlock_count": unlock_count,
                "request_count": request_count
            }
        )


class ReadFeedEventView(APIView):

    def post(self, request, id):
        if request.user.user_type == User.CLIENT:
            return ErrorResponse.build_text_error(
                "Only available for freelancer",
                status.HTTP_400_BAD_REQUEST
            )

        set_read_feed_event(request.user.id, id)

        return Response({}, status=status.HTTP_200_OK)
