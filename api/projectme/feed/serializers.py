from rest_framework import serializers

from .models import FeedEvent


class FeedDataSerializer(serializers.Serializer):
    type = serializers.IntegerField(help_text="""
        EVENT_TYPE_COMPLETE_PROFILE = 0
        EVENT_TYPE_CONFIRM_EMAIL = 1
        EVENT_TYPE_UNLOCKED = 2
        EVENT_TYPE_AVAILABILITY_REQUEST = 3
        EVENT_TYPE_VETTED = 4
        EVENT_TYPE_RATED = 5
        """)
    date = serializers.CharField(help_text="a date in isoformat: '2018-11-06T12:56:41.974478+00:00'")
    message = serializers.CharField()


class FeedEventSerializer(serializers.Serializer):
    user = serializers.IntegerField()
    id = serializers.IntegerField()
    data = FeedDataSerializer()
