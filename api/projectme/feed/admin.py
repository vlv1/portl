# django
from django.contrib import admin

# portl
from .models import FeedEvent


class FeedEventAdmin(admin.ModelAdmin):
    list_display = (
        'user', 'seq_id', 'created_at',
    )

    class Meta:
        model = FeedEvent
        fields = (
            'user', 'seq_id', 'created_at'
        )


admin.site.register(FeedEvent, FeedEventAdmin)
