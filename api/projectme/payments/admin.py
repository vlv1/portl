# django
from django.contrib import admin

# portl
from projectme.payments.models import Payment, CreditPackage


class PaymentAdmin(admin.ModelAdmin):
    list_display = (
        'status', 'currency', 'amount',
        'type_verbose', 'status_verbose'
    )

    class Meta:
        model = Payment
        fields = (
            'status', 'amount', 'type_verbose', 'status_verbose'
        )


class CreditPackageAdmin(admin.ModelAdmin):
    list_display = (
        'amount', 'price', 'currency'
    )

    class Meta:
        model = CreditPackage
        fields = (
            'amount', 'price', 'currency'
        )


admin.site.register(Payment, PaymentAdmin)
admin.site.register(CreditPackage, CreditPackageAdmin)
