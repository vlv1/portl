# system
import json

# django
from django.http import JsonResponse
from django.conf import settings

# third party
import stripe
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

# portl
from stripe.error import StripeError

from projectme.payments.models import Payment, PaymentStatus, PaymentType, \
    CreditPackage
from projectme.payments.serializers import PaymentSerializer, \
    StripePaySerializer, CreditPackageSerializer


@method_decorator(name='list', decorator=swagger_auto_schema())
class PaymentViewSet(ModelViewSet):
    serializer_class = PaymentSerializer

    def get_queryset(self):
        return Payment.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


@method_decorator(name='list', decorator=swagger_auto_schema())
class CreditPackageViewSet(ListAPIView):
    queryset = CreditPackage.objects.all()
    serializer_class = CreditPackageSerializer


class StripePayView(APIView):
    serializer_class = StripePaySerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        vd = serializer.validated_data

        payment = Payment.objects.create(
            user=request.user,
            currency=vd['currency'],
            amount=vd['amount'],
            type=PaymentType.STRIPE,
            credits=vd.get('credits', 0)
        )

        stripe.api_key = settings.STRIPE_SECRET_KEY
        try:
            charge = stripe.Charge.create(
                amount=round(vd['amount'] * 100),
                currency=vd['currency'],
                description='',
                source=vd['token'],
                metadata={'payment_id': payment.id}
            )
            charge_status = charge['status']
            response_data = charge
            if charge_status != 'succeeded':
                response_status = status.HTTP_400_BAD_REQUEST
                payment_status = PaymentStatus.FAILED
            else:
                response_status = status.HTTP_202_ACCEPTED
                payment_status = PaymentStatus.PENDING

        except StripeError as e:
            response_status = status.HTTP_400_BAD_REQUEST
            payment_status = PaymentStatus.FAILED
            response_data = {'Error': str(e)}

        payment.status = payment_status
        payment.response = json.dumps(response_data)
        payment.save()
        return JsonResponse(response_data, status=response_status)
