from django.core.management.base import BaseCommand

from projectme.payments.models import CreditPackage


class Command(BaseCommand):
    help = 'Generates default credit packages'

    def handle(self, *args, **options):
        CreditPackage.objects.create(amount=1, price=4.99, currency='GBP')
        CreditPackage.objects.create(amount=5, price=23.99, currency='GBP')
        CreditPackage.objects.create(amount=10, price=43.99, currency='GBP')
        CreditPackage.objects.create(amount=20, price=79.99, currency='GBP')
        CreditPackage.objects.create(amount=50, price=169.99, currency='GBP')
        CreditPackage.objects.create(amount=100, price=299.99, currency='GBP')
