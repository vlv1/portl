# third party
from django.urls import re_path
from rest_framework import routers

# portl
from projectme.payments.api import PaymentViewSet, StripePayView, \
    CreditPackageViewSet


router = routers.SimpleRouter()
router.register(r'payment', PaymentViewSet, 'payment')


urlpatterns = router.urls

urlpatterns += (
    re_path(
        r'^stripe_pay/$',
        StripePayView.as_view(),
        name='stripe-pay'
    ),
    re_path(
        r'^credit_packages/$',
        CreditPackageViewSet.as_view(),
        name='credit-package'
    ),
)
