# Generated by Django 2.1.1 on 2018-10-02 09:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0006_auto_20181001_1848'),
    ]

    operations = [
        migrations.AlterField(
            model_name='billingaddress',
            name='ext',
            field=models.CharField(blank=True, max_length=9, verbose_name='Phone extension'),
        ),
        migrations.AlterField(
            model_name='billingaddress',
            name='zip_code',
            field=models.CharField(blank=True, help_text='Required if country is: ARG, AUS, BGR, CAN, CHN, CYP, EGY, FRA, IND, IDN, ITA, JPN, MYS, MEX, NLD, PAN, PHL, POL, ROU, RUS, SRB, SGP, ZAF, ESP, SWE, THA, TUR, GBR, USA', max_length=16, verbose_name='Zip'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='status',
            field=models.CharField(choices=[('new', 'New'), ('success', 'Success'), ('failed', 'Failed')], default='new', max_length=50, verbose_name='status'),
        ),
    ]
