# Generated by Django 2.1.1 on 2018-10-01 14:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0003_billingaddress_payment'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='billing_address',
            field=models.OneToOneField(default=None, on_delete=django.db.models.deletion.CASCADE, to='payments.BillingAddress', verbose_name='billing address'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='billingaddress',
            name='address_2',
            field=models.CharField(blank=True, help_text='Required if country is: CHN, JPN, RUS', max_length=64, verbose_name="Card holder's street address line 2"),
        ),
        migrations.AlterField(
            model_name='billingaddress',
            name='state',
            field=models.CharField(blank=True, help_text='Required if country is: ARG, AUS, BGR, CAN, CHN, CYP, EGY, FRA, IND, IDN, ITA, JPN, MYS, MEX, NLD, PAN, PHL, POL, ROU, RUS, SRB, SGP, ZAF, ESP, SWE, THA, TUR, GBR, USA', max_length=64, verbose_name="Card holder's state"),
        ),
    ]
