# django
from rest_framework import serializers

# third party
# ..

# portl
from projectme.payments.models import Payment, CreditPackage


class PaymentSerializer(serializers.ModelSerializer):

    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    class Meta:
        model = Payment
        exclude = ('response', 'webhook_data')


class CreditPackageSerializer(serializers.ModelSerializer):

    class Meta:
        model = CreditPackage
        fields = '__all__'


class StripePaySerializer(serializers.Serializer):

    token = serializers.CharField(required=True)
    amount = serializers.DecimalField(
        required=True,
        max_digits=10,
        decimal_places=2
    )
    currency = serializers.CharField(required=True)
    credits = serializers.IntegerField(required=False)

    class Meta:
        fields = ('token', 'amount', 'currency', 'credits')
