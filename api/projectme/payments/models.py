# django
from django.db import models

# portl
from projectme.core.models import Action
from projectme.models import TimestampedModel
from projectme.onboarding.models import User
from projectme.profile.models import Profile


class CreditPackage(models.Model):
    amount = models.PositiveIntegerField(verbose_name='credits amount')
    price = models.DecimalField(
        verbose_name='price',
        max_digits=8, decimal_places=2
    )
    currency = models.CharField(
        verbose_name='currency',
        max_length=3,
        default='GBP'
    )

    def __str__(self):
        return '{} credit(s) - {} {}'.format(
            self.amount, self.price, self.currency
        )


class PaymentType:
    STRIPE = 'stripe'
    PAYPAL = 'paypal'
    CHOICES = (
        (STRIPE, 'Stripe'),
        (PAYPAL, 'PayPal')
    )


class PaymentStatus:
    NEW = 'new'
    PENDING = 'pending'
    SUCCESS = 'success'
    FAILED = 'failed'
    CHOICES = (
        (NEW, 'New'),
        (PENDING, 'Pending'),
        (SUCCESS, 'Success'),
        (FAILED, 'Failed')
    )


class Payment(TimestampedModel):
    """
    Stored payment info
    """
    user = models.ForeignKey(
        User,
        verbose_name='user',
        on_delete=models.CASCADE
    )
    status = models.CharField(
        verbose_name='status',
        max_length=50,
        choices=PaymentStatus.CHOICES,
        default=PaymentStatus.NEW
    )
    currency = models.CharField(
        verbose_name='currency',
        max_length=5
    )
    amount = models.DecimalField(
        verbose_name='amount',
        max_digits=10,
        decimal_places=2
    )
    type = models.CharField(
        verbose_name='payment type',
        max_length=30,
        choices=PaymentType.CHOICES
    )
    credits = models.IntegerField(verbose_name='credits', default=0)
    response = models.TextField(
        verbose_name='response',
        blank=True
    )
    webhook_data = models.TextField(
        verbose_name='webhook data',
        blank=True
    )

    def __str__(self):
        return 'Payment via {} on {} {}. Status: {}. User: {}'.format(
            self.type_verbose, self.amount, self.currency, self.status,
            self.user
        )

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        created = self.pk is None
        super().save(force_insert, force_update, using, update_fields)

        if created and self.status == PaymentStatus.SUCCESS:
            profile = Profile.objects.get_subclass(user=self.user)
            profile.add_credits(self.credits, description='credits bought')

            # track action
            Action.track(
                user=self.user,
                verb='bought {} credits'.format(self.credits)
            )

    @property
    def status_verbose(self):
        return dict(PaymentStatus.CHOICES)[self.status]

    @property
    def type_verbose(self):
        return dict(PaymentType.CHOICES)[self.type]
