# django
from django.conf import settings


MAX_BODY_LENGTH = 3000  # log no more than 3k bytes of content
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'onboarding.User')


def chunked_to_max(msg):
    if len(msg) > MAX_BODY_LENGTH:
        return "{0}\n...\n".format(msg[0:MAX_BODY_LENGTH])
    else:
        return msg
