# system
import re
from _socket import gethostbyaddr

# django
from django.utils.six import text_type
from django.utils.timesince import timesince as djtimesince
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.db import models
from django.conf import settings

# portl
from django.utils.translation import ugettext as _
from projectme.core.constants import HTTP_STATUS_CODES, BROWSERS, ENGINES
from projectme.core.utils import chunked_to_max
from projectme.onboarding.models import User


class Request(models.Model):
    # Response information
    response = models.SmallIntegerField(
        'Response',
        choices=HTTP_STATUS_CODES,
        default=200
    )

    # Request information
    method = models.CharField('method', default='GET', max_length=7)
    path = models.CharField('path', max_length=255)
    time = models.DateTimeField('time', auto_now_add=True)

    is_secure = models.BooleanField('is secure', default=False)
    is_ajax = models.BooleanField(
        'is ajax',
        default=False,
        help_text='Whether this request was used via javascript.'
    )

    # POST body data
    data = models.TextField(null=True, blank=True)

    # json response content
    response_content = models.TextField(null=True, blank=True)

    # User information
    ip = models.GenericIPAddressField('ip address')
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True, null=True,
        verbose_name='user',
        on_delete=models.CASCADE
    )
    referer = models.URLField('referer', max_length=255, blank=True, null=True)
    user_agent = models.CharField(
        'User agent', max_length=255,
        blank=True, null=True
    )

    class Meta:
        verbose_name = 'request'
        verbose_name_plural = 'requests'
        ordering = ('-time',)

    def __str__(self):
        return '[%s] %s %s %s' % (
            self.time, self.method, self.path, self.response
        )

    def get_user(self):
        return get_user_model().objects.get(pk=self.user.id)

    def from_http_request(self, request, response=None, commit=True):
        # Request information
        self.method = request.method
        self.path = request.path[:255]

        self.is_secure = request.is_secure()
        self.is_ajax = request.is_ajax()

        if re.match(
                '^application/json',
                response.get('Content-Type', ''),
                re.I
        ):
            self.response_content = response.content

        # User information
        self.ip = request.META.get('REMOTE_ADDR', '')
        self.referer = request.META.get('HTTP_REFERER', '')[:255]
        self.user_agent = request.META.get('HTTP_USER_AGENT', '')[:255]
        self.language = request.META.get('HTTP_ACCEPT_LANGUAGE', '')[:255]

        if request.method in ('POST', 'PUT'):
            # if request.body is not empty
            # if there's an image in data, it might have
            # non-utf chars and break the server on self.save() below
            # we remove those
            try:
                self.data = chunked_to_max(
                    request._body_to_log
                ).decode('utf-8', 'ignore').encode('utf-8')
            except:
                self.data = ''

        if getattr(request, 'user', False):
            if request.user.is_authenticated:
                self.user = request.user

        if response:
            self.response = response.status_code

            if (response.status_code == 301) or (response.status_code == 302):
                self.redirect = response['Location']

        if commit:
            self.save()

    @property
    def browser(self):
        if not self.user_agent:
            return

        if not hasattr(self, '_browser'):
            self._browser = BROWSERS.resolve(self.user_agent)
        return self._browser[0]

    @property
    def keywords(self):
        if not self.referer:
            return

        if not hasattr(self, '_keywords'):
            self._keywords = ENGINES.resolve(self.referer)
        if self._keywords:
            return ' '.join(self._keywords[1]['keywords'].split('+'))

    @property
    def hostname(self):
        try:
            return gethostbyaddr(self.ip)[0]
        except Exception:  # socket.gaierror, socket.herror, etc
            return self.ip


class Action(models.Model):
    """
    Stores action data
    """
    user = models.ForeignKey(
        User,
        verbose_name='user',
        related_name='actions',
        on_delete=models.CASCADE,
        blank=True, null=True
    )
    verb = models.CharField(
        max_length=60,
        verbose_name='Verb',
        blank=True
    )
    description = models.TextField(
        verbose_name='description',
        blank=True
    )

    target_content_type = models.ForeignKey(
        ContentType,
        related_name='action_target',
        blank=True, null=True,
        on_delete=models.CASCADE
    )
    target_object_id = models.CharField(max_length=255, blank=True)
    target = GenericForeignKey('target_content_type', 'target_object_id')

    action_object_content_type = models.ForeignKey(
        ContentType,
        blank=True, null=True,
        related_name='action_action_object',
        on_delete=models.CASCADE
    )
    action_object_object_id = models.CharField(
        max_length=255,
        blank=True
    )
    action_object = GenericForeignKey(
        'action_object_content_type',
        'action_object_object_id'
    )
    timestamp = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ('-timestamp',)

    def __str__(self):
        ctx = {
            'actor': self.user if self.user else 'Anonymous user',
            'verb': self.verb,
            'action_object': self.action_object,
            'target': self.target,
            'timesince': self.timesince()
        }
        if self.target:
            if self.action_object:
                return _(
                    '%(actor)s %(verb)s %(action_object)s on '
                    '%(target)s %(timesince)s ago') % ctx

            return _('%(actor)s %(verb)s %(target)s %(timesince)s ago') % ctx

        if self.action_object:
            return _(
                '%(actor)s %(verb)s %(action_object)s %(timesince)s ago') % ctx

        return _('%(actor)s %(verb)s %(timesince)s ago') % ctx

    def timesince(self, now=None):
        """
        Shortcut for the ``django.utils.timesince.timesince`` function of the
        current timestamp.
        """
        return djtimesince(self.timestamp, now).encode('utf8').replace(
            b'\xc2\xa0', b' '
        ).decode('utf8')

    @classmethod
    def track(cls, user, verb='', description='',
              action_object=None, target=None):
        user = None if user.is_anonymous else user
        action = cls(
            user=user,
            verb=text_type(verb),
            description=description,
            timestamp=timezone.now()
        )

        if target:
            action.target = target

        if action_object:
            action.action_object = action_object

        action.save()
