# django
from django.contrib import admin
from django.contrib.admin import ModelAdmin

# portl
from projectme.core.models import Request, Action


class RequestAdmin(ModelAdmin):
    list_display = (
        'id', 'response', 'method', 'path', 'time', 'is_secure', 'is_ajax',
        'ip', 'user', 'referer', 'user_agent'
    )
    search_fields = ('path', 'ip', 'referer')


class ActionAdmin(ModelAdmin):
    list_display = ('__str__', 'user', 'timestamp')
    search_fields = ('user', )


admin.site.register(Request, RequestAdmin)
admin.site.register(Action, ActionAdmin)
