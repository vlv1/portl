from django.contrib import admin

from projectme.transactions.models import Product, Transaction


class TransactionAdmin(admin.ModelAdmin):
    list_display = ['user', 'product', 'status']


admin.site.register(Product)
admin.site.register(Transaction, TransactionAdmin)
