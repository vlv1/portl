from django.urls import re_path

from projectme.transactions.api import VerifyTransaction, ProductsListView


urlpatterns = [
    re_path(r'^$', VerifyTransaction.as_view(), name="verify-transaction"),
    re_path(r'^products/$', ProductsListView.as_view(), name="products"),
]
