from django.contrib.gis.db import models
from django.conf import settings

from projectme.models import TimestampedModel
from projectme.notifications.models import NotificationConfiguration, \
    TransactionNotification


class Product(TimestampedModel):
    title = models.CharField(max_length=255)
    credits = models.IntegerField()
    product_id = models.CharField(max_length=255)

    def __str__(self):
        return self.title


class Transaction(TimestampedModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    receipt = models.TextField()

    INVALID = -1
    PENDING = 0
    VERIFIED = 1

    STATUSES = (
        (INVALID, 'Invalid'),
        (PENDING, 'Pending'),
        (VERIFIED, 'Verified')
    )

    status = models.IntegerField(default=PENDING, choices=STATUSES)
    product = models.ForeignKey(
        Product,
        related_name="transactions",
        blank=True, null=True,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return "{} - {}".format(self.user, self.product)

    def save(self, *args, **kwargs):
        created = self.pk is None
        super(Transaction, self).save(*args, **kwargs)

        if created:
            TransactionNotification.objects.create(
                user=self.user,
                transaction=self,
                attributes={},
                notification_type=NotificationConfiguration.PURCHASE_CONFIRMATION
            )
