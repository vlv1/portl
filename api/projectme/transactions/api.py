from django.conf import settings

import itunesiap
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from projectme.transactions.models import Transaction, Product
from projectme.utils import ErrorResponse
from projectme.profile.serializers import ClientProfileSerializer
from projectme.profile.models import ClientProfile
from projectme.onboarding.models import User


# TODO: to be removed?
class VerifyTransaction(APIView):

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='receipt',
                in_=openapi.IN_FORM,
                type=openapi.TYPE_STRING,
                description="Transaction receipt",
                required=True
            ),
        ],
        responses={
            status.HTTP_202_ACCEPTED: ClientProfileSerializer,
            status.HTTP_400_BAD_REQUEST:
                'Transaction already submitted / No receipt sent / '
                'Only a client can buy credits'
        }
    )
    def post(self, request):

        if request.user.user_type == User.CLIENT:

            receipt = request.data.get('receipt', None)

            if receipt is not None:
                transaction, created = Transaction.objects.get_or_create(
                    user=request.user,
                    receipt=receipt
                )
                if created:
                    try:
                        if settings.DEBUG:
                            itunesiap.env.sandbox.push()
                        else:
                            itunesiap.env.production.push()

                        response = itunesiap.verify(receipt)
                    except itunesiap.exc.InvalidReceipt as e:
                        transaction.status = Transaction.INVALID
                        transaction.save()
                        return ErrorResponse.build_text_error(
                            "Transaction was invalid, if you believe there "
                            "to be an error please contact Portl.",
                            status.HTTP_400_BAD_REQUEST
                        )

                    try:
                        receipt = response.receipt
                        in_app = receipt.in_app
                        last_in_app = receipt.last_in_app
                        product = Product.objects.get(
                            product_id=receipt.last_in_app.product_id
                        )
                    except Product.DoesNotExist:
                        return ErrorResponse.build_text_error(
                            "Product isn't available, we can't asssign "
                            "you the credits, please contact Portl.",
                            status.HTTP_404_NOT_FOUND
                        )

                    transaction.product = product
                    transaction.status = Transaction.VERIFIED
                    transaction.save()

                    profile = ClientProfile.objects.get(user=request.user)
                    profile.credits = profile.credits + product.credits
                    profile.save()

                    serializer = ClientProfileSerializer(
                        profile,
                        context={"request": request}
                    )

                    return Response(
                        serializer.data,
                        status=status.HTTP_202_ACCEPTED
                    )
                return ErrorResponse.build_text_error(
                    "Transaction already submitted",
                    status.HTTP_400_BAD_REQUEST
                )
            return ErrorResponse.build_text_error(
                "No receipt sent",
                status.HTTP_400_BAD_REQUEST
            )
        return ErrorResponse.build_text_error(
            "Only a client can buy credits",
            status.HTTP_400_BAD_REQUEST
        )


class ProductsListView(APIView):

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: 'Products ids list'
        }
    )
    def get(self, request):
        products = Product.objects.all().order_by('credits')
        return Response(products.values_list('product_id', flat=True))
