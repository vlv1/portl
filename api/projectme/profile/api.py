# system
import base64
import datetime

# django
from django.core.paginator import Paginator
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.core.files.base import ContentFile
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.conf import settings

# third party
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework import status
from rest_framework.generics import ListAPIView, UpdateAPIView, CreateAPIView
from rest_framework.viewsets import ModelViewSet

# portl
from projectme.core.models import Action
from projectme.onboarding.models import User, ReferralCode
from projectme.profile.api_schemas import freelancer_profile_request_schema, \
    freelancer_profile_response_schema, profile_detail_schema, \
    referral_code_response_schema, client_profile_response_schema, \
    client_profile_request_schema
from projectme.profile.models import Experience, JobTitle, KeySkill, Sector, \
    Qualification, Language, ClientProfile, FreelancerProfile, Profile, \
    UpdateAvailabilityRequest, Unlock, Favourite, UmbrellaCompanyOption
from projectme.profile.serializers import JobTitleSerializer, \
    KeySkillSerializer, SectorSerializer, QualificationSerializer, \
    LanguageSerializer, ClientProfileSerializer, FreelancerProfileSerializer, \
    UpdateClientProfileSerializer, UpdateFreelancerProfileSerializer, \
    UmbrellaCompanyOptionsSerializer, ProfileOptionsSerializer, \
    FreelancerProfileInfoSerializer, UpdateAvailabilityRequestSerializer, \
    ChangePasswordSerializer, VettingRequestSerializer, \
    ClientStatisticsSerializer, FreelancerStatisticsSerializer, \
    RateProfileSerializer, ContestRatingSerializer, \
    FreelancerProfileViewSerializer, ReferralCodeSerializer, \
    LimitedCompanySerializer, CompanySerializer, SoleTraderCompanySerializer, \
    UmbrellaCompanySerializer
from projectme.utils import ErrorResponse
from projectme.onboarding.serializers import MyUserSerializer
from projectme.feed.utils import add_feed_event, \
    set_read_availability_events, set_read_complete_profile_event
from projectme.feed.models import FeedEvent


def update_profile_detail(profile, detail, details, detail_class):
    if details is not None:
        added_details = []

        if details.__class__.__name__ not in ('list', ):
            details = [details, ]

        for d in details:
            new_d, created = detail_class.objects.get_or_create(
                title=d["title"]
            )

            if created:
                new_d.accepted = False
                new_d.creator = profile.user
                new_d.save()

            added_details.append(new_d)

            if d.get('experience', None) is not None:
                experience, created = Experience.objects.get_or_create(
                    detail=new_d,
                    profile=profile
                )
                experience.years = d["experience"]
                experience.save()

        detail.set(added_details)


class MeProfileView(APIView):

    def get_serializer_class(self, profile):
        if isinstance(profile, ClientProfile):
            return ClientProfileSerializer
        elif isinstance(profile, FreelancerProfile):
            return FreelancerProfileSerializer

        return None

    def get_update_serializer_class(self, profile):
        if isinstance(profile, ClientProfile):
            return UpdateClientProfileSerializer
        elif isinstance(profile, FreelancerProfile):
            return UpdateFreelancerProfileSerializer

        return None

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: 'Endpoint returns different data '
                                'depending of the user type who sent request. '
                                'For clients - see /profile/me-client/. '
                                'For freelancers - see /profile/me-freelancer/.',
            status.HTTP_400_BAD_REQUEST: 'Error'
        }
    )
    def get(self, request):
        profile = Profile.objects.get_subclass(user=request.user)

        serializer_class = self.get_serializer_class(profile)
        if serializer_class is None:
            return ErrorResponse.build_text_error(
                "Error",
                status.HTTP_400_BAD_REQUEST
            )

        serializer = serializer_class(profile, context={"request": request})

        return Response(serializer.data)

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: 'Endpoint takes and returns different data '
                                'depending of the user type who sent request. '
                                'For clients - see /profile/me-client/. '
                                'For freelancers - see /profile/me-freelancer/.',
            status.HTTP_400_BAD_REQUEST: 'Error'
        }
    )
    def patch(self, request):
        profile = Profile.objects.get_subclass(user=request.user)

        serializer_class = self.get_update_serializer_class(profile)
        if serializer_class is None:
            return ErrorResponse.build_text_error(
                "Error",
                status.HTTP_400_BAD_REQUEST
            )

        data = request.data

        company = data.pop('company', None)
        job_title = data.pop('job_title', None)

        # get company data
        company_serializer = None
        if company:
            if isinstance(profile, ClientProfile):
                company_serializer = CompanySerializer(data=company)
            elif isinstance(profile, FreelancerProfile):
                company_type = company.get('company_type')
                if company_type == 'limited_company':
                    company_serializer = LimitedCompanySerializer(data=company)
                elif company_type == 'sole_trader':
                    company_serializer = SoleTraderCompanySerializer(
                        data=company
                    )
                elif company_type == 'umbrella_company':
                    company_serializer = UmbrellaCompanySerializer(data=company)
                else:
                    company_serializer = CompanySerializer(data=company)

        if isinstance(profile, FreelancerProfile):
            key_skills = data.pop('key_skills', None)
            update_profile_detail(
                profile, profile.key_skills, key_skills, KeySkill
            )

            sectors = data.pop('sectors', None)
            update_profile_detail(profile, profile.sectors, sectors, Sector)

            qualifications = data.pop('qualifications', None)
            update_profile_detail(
                profile, profile.qualifications, qualifications, Qualification
            )

            languages = data.pop('languages', None)
            update_profile_detail(profile, profile.languages, languages, Language)

            unavailable_dates = data.pop('unavailable_dates', None)
            if unavailable_dates is not None:
                profile.unavailable_dates = unavailable_dates

                for update_request in UpdateAvailabilityRequest.objects.filter(
                        profile=profile,
                        updated=False
                ):
                    update_request.updated = True
                    update_request.save()

                set_read_availability_events(request.user.id)

            set_read_complete_profile_event(request.user.id)

        # Update Company
        old_company = None
        if company_serializer:
            if not company_serializer.is_valid():
                return ErrorResponse.build_serializer_error(
                    company_serializer,
                    status.HTTP_400_BAD_REQUEST
                )

            if company is not None:
                old_company = profile.company
                new_company = company_serializer.save()
                profile.company = new_company

        # Update Job Title
        if job_title is not None:
            job_titles = JobTitle.objects.filter(title=job_title).order_by(
                '-accepted', 'created_at'
            )
            if job_titles.count() > 0:
                job_title = job_titles[0]
            else:
                job_title = JobTitle.objects.create(
                    title=job_title,
                    accepted=False,
                    creator=request.user
                )
            profile.job_title = job_title

        user = data.pop('user', None)

        if user is not None:
            current_password = user.pop('current_password', None)

            # update user avatar
            avatar = user.pop('avatar', False)
            if avatar is not None and avatar is not False:
                try:
                    format, imgstr = avatar.split(';base64,')
                    ext = format.split('/')[-1]

                    user['avatar'] = ContentFile(
                        base64.b64decode(imgstr), name='temp.' + ext
                    )
                except Exception as e:
                    user['avatar'] = None
            elif avatar is None:
                user['avatar'] = None

            # change password
            if user.get('password', None) is not None:
                if current_password is None or profile.user.check_password(
                        current_password
                ) is False:
                    return ErrorResponse.build_text_error(
                        "Incorrect current password",
                        status.HTTP_400_BAD_REQUEST
                    )

            serializer = MyUserSerializer(
                profile.user,
                data=user,
                partial=True,
                context={"request": request}
            )

            if not serializer.is_valid():
                return ErrorResponse.build_serializer_error(
                    serializer,
                    status.HTTP_400_BAD_REQUEST
                )

            serializer.save()

        # update profile
        serializer = serializer_class(
            profile, data=data, partial=True, context={'request': request}
        )

        if not serializer.is_valid():
            return ErrorResponse.build_serializer_error(
                serializer,
                status.HTTP_400_BAD_REQUEST
            )

        serializer.save()

        if old_company:
            old_company.delete()

        if isinstance(profile, FreelancerProfile):
            Action.track(user=request.user, verb='edited profile')

        # generate response data
        serializer = self.get_serializer_class(profile)(
            profile,
            context={'request': request}
        )

        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    def delete(self, request):
        user = request.user
        user.is_active = False
        user.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class MeFreelancerProfileView(MeProfileView):

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: openapi.Response(
                schema=freelancer_profile_response_schema,
                description=''
            )
        }
    )
    def get(self, request):
        # TODO: restrict access for clients
        response = super().get(request)
        return response

    @swagger_auto_schema(
        request_body=freelancer_profile_request_schema,
        responses={
            status.HTTP_200_OK: freelancer_profile_response_schema,
        }
    )
    def patch(self, request):
        # TODO: restrict access for clients
        response = super().patch(request)
        return response


class MeClientProfileView(MeProfileView):

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: client_profile_response_schema
        }
    )
    def get(self, request):
        # TODO: restrict access for freelancers
        response = super().get(request)
        return response

    @swagger_auto_schema(
        request_body=client_profile_request_schema,
        responses={
            status.HTTP_200_OK: client_profile_response_schema,
            status.HTTP_400_BAD_REQUEST: 'Error'
        }
    )
    def patch(self, request):
        # TODO: restrict access for freelancers
        response = super().patch(request)
        return response


class FreelancerProfileViewedView(CreateAPIView):
    serializer_class = FreelancerProfileViewSerializer

    def get_object(self, pk):
        try:
            return Profile.objects.get_subclass(pk=pk)
        except Exception as e:
            raise Http404

    def get_serializer_context(self, pk=None):
        freelancer_profile = self.get_object(pk)
        return {'freelancer_profile': freelancer_profile}

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={}
        ),
        responses={
            # TODO: add schema here
            status.HTTP_201_CREATED: 'None',
        }
    )
    def post(self, request, pk, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data,
            context=self.get_serializer_context(pk)
        )

        serializer.is_valid(raise_exception=True)
        serializer.save()

        # track viewed action
        freelancer_profile = self.get_object(pk)
        Action.generate(
            user=request.user,
            verb='viewed',
            target=freelancer_profile
        )

        return Response(
            serializer.data,
            status=status.HTTP_201_CREATED
        )


class ProfileStatisticsView(APIView):

    def get_serializer_class(self, profile):
        if isinstance(profile, ClientProfile):
            return ClientStatisticsSerializer
        elif isinstance(profile, FreelancerProfile):
            return FreelancerStatisticsSerializer

        return None

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK:
                'Endpoint returns different data depending of the user '
                'type who sent request. '
                'For clients - see /profile/statistics-client/. '
                'For freelancers - see /profile/statistics-freelancer/.',
            status.HTTP_400_BAD_REQUEST:
                'Unable to serialize current user statistics'
        }
    )
    def get(self, request):
        profile = Profile.objects.get_subclass(user=request.user)
        serializer_class = self.get_serializer_class(profile)
        if serializer_class is None:
            return ErrorResponse.build_text_error(
                'Unable to serialize current user statistics',
                status.HTTP_400_BAD_REQUEST
            )

        serializer_class = self.get_serializer_class(profile)
        serializer = serializer_class(instance=profile)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ProfileClientStatisticsView(ProfileStatisticsView):
    """
    Is used just for swagger
    """
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: ClientStatisticsSerializer,
            status.HTTP_400_BAD_REQUEST:
                'Unable to serialize current user statistics'
        }
    )
    def get(self, request):
        return super().get(request)


class ProfileFreelancerStatisticsView(ProfileStatisticsView):
    """
    Is used just for swagger
    """
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: FreelancerStatisticsSerializer,
            status.HTTP_400_BAD_REQUEST:
                'Unable to serialize current user statistics'
        }
    )
    def get(self, request):
        return super().get(request)


class DashboardView(APIView):

    @swagger_auto_schema(
        responses={
            # use custom schema
            # status.HTTP_200_OK: DashboardSerializer,
            status.HTTP_400_BAD_REQUEST: 'Only available for freelancer'
        }
    )
    def get(self, request):
        user = request.user
        if user.user_type == User.CLIENT:
            return ErrorResponse.build_text_error(
                "Only available for freelancer",
                status.HTTP_400_BAD_REQUEST
            )

        requests = UpdateAvailabilityRequest.objects.filter(
            profile__user=user,
            updated=False
        ).distinct('user')
        unlocks = Unlock.objects.filter(profile__user=user)

        profile = Profile.objects.get_subclass(user=user)

        avatar = None
        if user.avatar:
            avatar = user.avatar.storage.url(user.avatar.name)

        return Response({
            "requests_count": requests.count(),
            "unlocks_count": unlocks.count(),
            "views_count": 0,
            "email": user.email,
            "avatar": avatar,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "rating": profile.rating,
            "job_title": profile.job_title.title,
            "rate": profile.rate
        })


class VettingRequestView(APIView):

    def get_serializer_context(self):
        return {'request': self.request}

    def post(self, request):
        user = request.user
        if user.user_type == User.CLIENT:
            return ErrorResponse.build_text_error(
                'Only available for freelancer',
                status.HTTP_400_BAD_REQUEST
            )

        serializer = VettingRequestSerializer(
            data=request.data,
            context=self.get_serializer_context()
        )

        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            serializer.data,
            status=status.HTTP_201_CREATED
        )


class FreelancerProfileInfoView(APIView):

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: FreelancerProfileInfoSerializer,
            status.HTTP_400_BAD_REQUEST: 'Only available for freelancer'
        }
    )
    def get(self, request):
        user = request.user
        if user.user_type == User.CLIENT:
            return ErrorResponse.build_text_error(
                "Only available for freelancer",
                status.HTTP_400_BAD_REQUEST
            )

        profile = Profile.objects.get_subclass(user=user)
        serializer = FreelancerProfileInfoSerializer(profile)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ProfileDetailsSearchView(APIView):

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='query',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description='Query',
                required=False,
            ),
            openapi.Parameter(
                name='detail_type',
                in_=openapi.IN_PATH,
                type=openapi.TYPE_STRING,
                description='Profile detail type',
                required=True,
                enum=[
                    'key_skill', 'language', 'sector',
                    'qualification', 'job_title'
                ],
            )
        ],
        responses={
            status.HTTP_200_OK: openapi.Schema(
                type=openapi.TYPE_ARRAY,
                items=profile_detail_schema
            ),
            status.HTTP_400_BAD_REQUEST: openapi.Response(
                'Unknown profile detail type'
            )
        }
    )
    def get(self, request, detail_type):
        query = request.GET.get('query', '')
        if detail_type == 'key_skill':
            qset = KeySkill.objects.filter(title__icontains=query)
            serializer = KeySkillSerializer(qset, many=True)
        elif detail_type == 'language':
            qset = Language.objects.filter(title__icontains=query)
            serializer = LanguageSerializer(qset, many=True)
        elif detail_type == 'sector':
            qset = Sector.objects.filter(title__icontains=query)
            serializer = SectorSerializer(qset, many=True)
        elif detail_type == 'qualification':
            qset = Qualification.objects.filter(title__icontains=query)
            serializer = QualificationSerializer(qset, many=True)
        elif detail_type == 'job_title':
            qset = JobTitle.objects.filter(title__icontains=query)
            serializer = JobTitleSerializer(qset, many=True)
        else:
            return ErrorResponse.build_text_error(
                'Unknown profile detail type',
                status.HTTP_400_BAD_REQUEST
            )

        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)


class ProfileChangePasswordView(UpdateAPIView):

    serializer_class = ChangePasswordSerializer
    model = User

    def get_object(self, queryset=None):
        return self.request.user

    def get_serializer_context(self):
        return {'request': self.request}

    @swagger_auto_schema(
        request_body=ChangePasswordSerializer,
        responses={
            status.HTTP_200_OK: openapi.Response('None'),
            status.HTTP_400_BAD_REQUEST:
                openapi.Response(
                    description=
                    'Current password is not correct or Passwords do not match'
                )
        }
    )
    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            self.object.set_password(serializer.data['new_password'])
            self.object.save()
            return Response({}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RateProfileView(APIView):

    serializer = RateProfileSerializer

    def get_object(self, pk):
        try:
            return Profile.objects.get_subclass(pk=pk)
        except Exception as e:
            raise Http404

    def get_serializer_class(self, profile):
        if isinstance(profile, ClientProfile):
            return ClientProfileSerializer
        elif isinstance(profile, FreelancerProfile):
            return FreelancerProfileSerializer

        return None

    def get_serializer_context(self, pk=None):
        rated_profile = self.get_object(pk)
        profile = self.request.user.profile
        return {'profile': profile, 'rated_profile': rated_profile}

    @swagger_auto_schema(
        request_body=RateProfileSerializer,
        responses={
            status.HTTP_202_ACCEPTED: FreelancerProfileSerializer
        }
    )
    def post(self, request, pk):
        profile = request.user.profile
        rated_profile = self.get_object(pk)

        serializer = self.serializer(
            data=request.data,
            context=self.get_serializer_context(pk)
        )
        serializer.is_valid(raise_exception=True)

        # client cannot rate client and freelancer cannot rate freelancer
        if type(profile) == type(rated_profile):
            return ErrorResponse.build_text_error(
                'Profiles should differ by user type',
                status.HTTP_400_BAD_REQUEST
            )

        # if client tries to rate freelancer -
        # check if this freelancer was previously unlocked by client
        if isinstance(profile, ClientProfile):
            if not rated_profile.is_unlocked_by(profile.user):
                return ErrorResponse.build_text_error(
                    'Freelancer must be unlocked first',
                    status.HTTP_400_BAD_REQUEST
                )

        # if freelancer tries to rate client -
        # check if this client previously rated this freelancer
        if isinstance(profile, FreelancerProfile):
            if not profile.is_rated_by(profile):
                return ErrorResponse.build_text_error(
                    'You should be rated by this client first',
                    status.HTTP_400_BAD_REQUEST
                )

        serializer.save()
        rated_profile.refresh_from_db()

        if isinstance(rated_profile, FreelancerProfile):
            add_feed_event(
                rated_profile.user,
                type=FeedEvent.EVENT_TYPE_RATED,
                message='You were rated by %s' % profile.company.company_name,
                client_profile=profile
            )

        # generate response data
        serializer_class = self.get_serializer_class(rated_profile)
        response_serializer = serializer_class(
            rated_profile,
            context={'request': request}
        )
        return Response(
            response_serializer.data,
            status=status.HTTP_202_ACCEPTED
        )


class RatingContestView(APIView):

    serializer_class = ContestRatingSerializer

    def get_object(self, pk):
        try:
            return Profile.objects.get_subclass(pk=pk)
        except Exception as e:
            raise Http404

    def get_serializer_context(self, pk=None):
        client_profile = self.get_object(pk)
        freelancer_profile = Profile.objects.get_subclass(
            pk=self.request.user.profile.pk
        )
        return {
            'client_profile': client_profile,
            'freelancer_profile': freelancer_profile
        }

    @swagger_auto_schema(
        request_body=ContestRatingSerializer,
        responses={
            status.HTTP_201_CREATED: ContestRatingSerializer
        }
    )
    def post(self, request, pk):
        freelancer_profile = Profile.objects.get_subclass(
            pk=request.user.profile.pk
        )

        # only freelancer can create rating contest
        if isinstance(freelancer_profile, ClientProfile):
            return ErrorResponse.build_text_error(
                'Only freelancer can create contest',
                status.HTTP_400_BAD_REQUEST
            )

        serializer = self.serializer_class(
            data=request.data,
            context=self.get_serializer_context(pk)
        )
        serializer.is_valid(raise_exception=True)
        contest_rating = serializer.save()

        # generate response data
        response_serializer = self.serializer_class(
            contest_rating,
            context={'request': request}
        )
        return Response(
            response_serializer.data,
            status=status.HTTP_201_CREATED
        )


class ConfirmAvailabilityView(APIView):

    def get_object(self):
        try:
            return Profile.objects.get_subclass(user=self.request.user)
        except Exception as e:
            raise Http404

    def get_serializer_context(self, pk=None):
        profile = self.get_object()
        return {'freelancer_profile': profile}

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={}
        ),
        responses={
            status.HTTP_202_ACCEPTED: 'None'
        }
    )
    def post(self, request):
        profile = self.get_object()

        # only freelancer can update availability
        if isinstance(profile, ClientProfile):
            return ErrorResponse.build_text_error(
                'Only freelancer can confirm availability',
                status.HTTP_400_BAD_REQUEST
            )

        # mark requests as updated for the freelancer
        UpdateAvailabilityRequest.objects.filter(
            profile=profile,
            updated=False
        ).update(updated=True)

        #   mark events read
        set_read_availability_events(request.user.id)

        # track action
        Action.track(
            user=request.user,
            verb='updated availability (without availability request)'
        )
        return Response({}, status=status.HTTP_202_ACCEPTED)


class ReferralCodeViewSet(ModelViewSet):

    queryset = ReferralCode.objects.all()
    serializer_class = ReferralCodeSerializer

    def get_queryset(self):
        return ReferralCode.objects.filter(referrer=self.request.user)

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: referral_code_response_schema
        }
    )
    def list(self, request, *args, **kwargs):
        profile = Profile.objects.get_subclass(user=request.user)
        if isinstance(profile, FreelancerProfile):
            return ErrorResponse.build_text_error(
                'Can be used only by clients',
                status.HTTP_400_BAD_REQUEST
            )
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={}
        ),
        responses={
            status.HTTP_201_CREATED: referral_code_response_schema
        }
    )
    def post(self, request):
        profile = Profile.objects.get_subclass(user=request.user)
        if isinstance(profile, FreelancerProfile):
            return ErrorResponse.build_text_error(
                'Can be used only by clients',
                status.HTTP_400_BAD_REQUEST
            )
        return super().post(request)


class UpdateAvailabilityRequestDetailsView(APIView):

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: openapi.Response(
                schema=openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'client': openapi.Schema(
                            type=openapi.TYPE_STRING,
                            description='User\'s company name'
                        ),
                        'updated': openapi.Schema(
                            type=openapi.TYPE_BOOLEAN,
                        ),
                    }
                ),
                description=''
            )
        }
    )
    def get(self, request, request_pk):
        user = request.user
        if user.user_type == User.CLIENT:
            return ErrorResponse.build_text_error(
                'Only available for freelancer',
                status.HTTP_400_BAD_REQUEST
            )

        profile = Profile.objects.get_subclass(user=user)
        availability_requests = UpdateAvailabilityRequest.objects.filter(
            profile=profile
        )
        availability_request = get_object_or_404(
            availability_requests, pk=request_pk
        )
        serializer = UpdateAvailabilityRequestSerializer(
            availability_request,
            context={'request': request}
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'updated': openapi.Schema(
                    type=openapi.TYPE_BOOLEAN,
                ),
            }
        ),
        responses={
            status.HTTP_200_OK: openapi.Response('None')
        }
    )
    def patch(self, request, request_pk):
        user = request.user
        if user.user_type == User.CLIENT:
            return ErrorResponse.build_text_error(
                "Only available for freelancer",
                status.HTTP_400_BAD_REQUEST
            )

        profile = Profile.objects.get_subclass(user=user)
        availability_requests = UpdateAvailabilityRequest.objects.filter(
            profile=profile
        )
        availability_request = get_object_or_404(
            availability_requests, pk=request_pk
        )
        serializer = UpdateAvailabilityRequestSerializer(
            availability_request, data=request.data, partial=True,
            context={'request': request}
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status=status.HTTP_200_OK)


class UpdateAvailabilityRequestCreateView(APIView):

    def get_object(self, pk):
        try:
            return Profile.objects.get_subclass(pk=pk)
        except Exception as e:
            raise Http404

    @swagger_auto_schema(
        responses={
            status.HTTP_201_CREATED: openapi.Response('None')
        }
    )
    def post(self, request, pk):
        profile = self.get_object(pk)

        recent_request_exists = UpdateAvailabilityRequest.objects.filter(
            user=request.user,
            profile=profile,
            created_at__gte=timezone.now()-datetime.timedelta(
                seconds=settings.CLIENT_REQUEST_DELAY
            )
        ).exists()

        if not recent_request_exists:
            availability_request = UpdateAvailabilityRequest.objects.create(
                user=request.user,
                profile=profile,
                updated=False
            )

            add_feed_event(
                profile.user,
                type=FeedEvent.EVENT_TYPE_AVAILABILITY_REQUEST,
                availability_request=availability_request.id
            )

        return Response({}, status=status.HTTP_201_CREATED)


class ProfileOptionsView(APIView):
    permission_classes = (AllowAny, )

    @swagger_auto_schema(
        responses={
            status.HTTP_201_CREATED: ProfileOptionsSerializer
        }
    )
    def get(self, request):
        job_titles = JobTitle.objects.filter(accepted=True).order_by(
            '-priority', 'title'
        )
        key_skills = KeySkill.objects.filter(accepted=True).order_by(
            '-priority', 'title'
        )
        sectors = Sector.objects.filter(accepted=True).order_by(
            '-priority', 'title'
        )
        qualifications = Qualification.objects.filter(accepted=True).order_by(
            '-priority', 'title'
        )
        languages = Language.objects.filter(accepted=True).order_by(
            '-priority', 'title'
        )

        return Response(
            {
                "job_titles": JobTitleSerializer(
                    job_titles,
                    many=True,
                    context={"request": request}
                ).data,
                "key_skills": KeySkillSerializer(
                    key_skills,
                    many=True,
                    context={"request": request}
                ).data,
                "sectors": SectorSerializer(
                    sectors,
                    many=True,
                    context={"request": request}
                ).data,
                "qualifications": QualificationSerializer(
                    qualifications,
                    many=True,
                    context={"request": request}
                ).data,
                "languages": LanguageSerializer(
                    languages,
                    many=True,
                    context={"request": request}
                ).data
            }
        )


class MeUnlockedProfilesView(ListAPIView):
    serializer_class = ClientProfileSerializer

    def get_context(self):
        return {"request": self.request}

    def get_queryset(self):
        Unlock.objects.filter(
            profile__user=self.request.user,
            seen=False
        ).update(seen=True)
        return ClientProfile.objects.filter(
            user__unlocked_profiles__profile__user=self.request.user
        ).distinct().order_by('-user__unlocked_profiles__created_at')

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='page',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description='Page number. Default: 1.',
                required=False,
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(
                schema=openapi.Schema(
                    type=openapi.TYPE_ARRAY,
                    items=client_profile_response_schema
                ),
                description=''
            )
        }
    )
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page_number = request.GET.get('page', 1)
        paginator = Paginator(queryset, settings.COMMON_PAGE_SIZE)
        page = paginator.get_page(page_number)
        queryset = page.object_list
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@method_decorator(name='list', decorator=swagger_auto_schema())
class MeRequestProfilesView(ListAPIView):
    serializer_class = ClientProfileSerializer

    def get_context(self):
        return {"request": self.request}

    def get_queryset(self):
        return ClientProfile.objects.filter(
            user__requests__profile__user=self.request.user,
            user__requests__updated=False
        ).distinct().order_by('-user__requests__created_at')


class MeCountsProfilesView(APIView):

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: '{}',
        }
    )
    def get(self, request):
        requests = ClientProfile.objects.filter(
            user__requests__profile__user=request.user,
            user__requests__updated=False
        ).distinct().order_by('-user__requests__created_at')
        unlocks = ClientProfile.objects.filter(
            user__unlocked_profiles__profile__user=request.user,
            user__unlocked_profiles__seen=False
        ).distinct().order_by('-user__unlocked_profiles__created_at')

        return Response(
            {"requests": requests.count(), "unlocks": unlocks.count()}
        )


class UnlockedProfilesView(ListAPIView):
    serializer_class = FreelancerProfileSerializer

    def get_context(self):
        return {"request": self.request}

    def get_queryset(self):
        return FreelancerProfile.objects.filter(
            unlocks__user=self.request.user
        ).distinct().order_by('-unlocks__created_at')

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='page',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description='Page number. Default: 1.',
                required=False,
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(
                schema=openapi.Schema(
                    type=openapi.TYPE_ARRAY,
                    items=freelancer_profile_response_schema
                ),
                description=''
            )
        }
    )
    def get(self, *args, **kwargs):
        return super().get(*args, *kwargs)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page_number = request.GET.get('page', 1)
        paginator = Paginator(queryset, settings.COMMON_PAGE_SIZE)
        page = paginator.get_page(page_number)
        queryset = page.object_list
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@method_decorator(name='list', decorator=swagger_auto_schema())
class FavouriteProfilesView(ListAPIView):
    serializer_class = FreelancerProfileSerializer

    def get_context(self):
        return {"request": self.request}

    def get_queryset(self):
        return FreelancerProfile.objects.filter(
            favourites__user=self.request.user
        ).distinct().order_by('-favourites__created_at')


@method_decorator(name='list', decorator=swagger_auto_schema())
class RequestProfilesView(ListAPIView):
    serializer_class = FreelancerProfileSerializer

    def get_context(self):
        return {"request": self.request}

    def get_queryset(self):
        requests = UpdateAvailabilityRequest.objects.filter(
            user=self.request.user
        ).select_related('profile').distinct('profile')
        return FreelancerProfile.objects.filter(
            requests__in=requests
        ).distinct().order_by('-requests__created_at')


class ProfileView(APIView):
    serializer_class = FreelancerProfileSerializer

    def get_object(self, pk):
        try:
            return Profile.objects.get_subclass(pk=pk)
        except Exception as e:
            raise Http404

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: FreelancerProfileSerializer
        }
    )
    def get(self, request, pk):
        profile = self.get_object(pk)

        serializer = self.serializer_class(
            profile,
            context={"request": request}
        )
        return Response(serializer.data)


class ProfileDetailsView(ProfileView):
    permission_classes = (AllowAny, )

    pass


class FavouriteProfileView(ProfileView):

    @swagger_auto_schema(
        request_body=FreelancerProfileSerializer,
        responses={
            status.HTTP_201_CREATED: FreelancerProfileSerializer
        }
    )
    def post(self, request, pk):
        profile = self.get_object(pk)

        Favourite.objects.get_or_create(user=request.user, profile=profile)

        serializer = self.serializer_class(
            profile,
            context={"request": request}
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(
        request_body=FreelancerProfileSerializer,
        responses={
            status.HTTP_202_ACCEPTED: FreelancerProfileSerializer
        }
    )
    def delete(self, request, pk):
        profile = self.get_object(pk)
        get_object_or_404(
            Favourite,
            user=request.user,
            profile=profile
        ).delete()
        seriaizer = FreelancerProfileSerializer(
            profile,
            context={'request': request}
        )
        return Response(seriaizer.data, status=status.HTTP_202_ACCEPTED)


class UnlockProfileView(ProfileView):

    @swagger_auto_schema(
        request_body=FreelancerProfileSerializer,
        responses={
            status.HTTP_201_CREATED: FreelancerProfileSerializer,
            status.HTTP_400_BAD_REQUEST: 'Not enough credits'
        }
    )
    def post(self, request, pk):
        profile = self.get_object(pk)

        client_profile = get_object_or_404(ClientProfile, user=request.user)
        credits = client_profile.credits

        if credits <= 0:
            return ErrorResponse.build_text_error(
                "Not enough credits",
                status.HTTP_400_BAD_REQUEST
            )

        # temporary disable unlock, to test feed
        unlock, created = Unlock.objects.get_or_create(
            user=request.user,
            profile=profile
        )

        add_feed_event(
            profile.user,
            type=FeedEvent.EVENT_TYPE_UNLOCKED,
            message='You were unlocked by {}'.format(
                client_profile.company.company_name
            )
        )

        if created is True:
            if credits > 0:
                client_profile.take_credits(
                    credits_amount=settings.UNLOCK_PRICE,
                    description='for profile unlock'
                )

        # track event
        Action.track(
            user=self.request.user,
            verb='unlocked profile',
            target=profile
        )

        serializer = self.serializer_class(
            profile,
            context={"request": request}
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)


@method_decorator(name='list', decorator=swagger_auto_schema())
class UmbrellaCompanyOptionsListView(ListAPIView):
    serializer_class = UmbrellaCompanyOptionsSerializer
    queryset = UmbrellaCompanyOption.objects.all()
