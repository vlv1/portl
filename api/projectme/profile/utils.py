# portl
# ..

# third party
from elasticsearch_dsl import Search, Q


def generate_query(query_type, field, query):
    return Q(query_type, **{field: query})


def generate_nested_query(path, field, query):
    return Q(
        'nested',
        path=path,
        query=Q(
            'bool',
            must=[
                Q('regexp', **{'{}__{}'.format(path, field): query})
            ]
        )
    )


def start_search(query):

    s = Search(index='freelancer-profile')
    job_title_query = generate_query('match', 'job_title', query)
    key_skills_query = generate_nested_query('key_skills', 'title', query)
    qualifications_query = generate_nested_query('qualifications', 'title', query)
    languages_query = generate_nested_query('languages', 'title', query)
    sectors_query = generate_nested_query('sectors', 'title', query)

    # make additional filter by location if it is specified
    if query:
        s = s.query(
            Q(
                job_title_query |
                key_skills_query |
                qualifications_query |
                sectors_query |
                languages_query
            )
        )

    response = s.execute()
    profile_ids_score = []

    for hit in response:
        profile_ids_score.append((hit.meta.id, hit.meta.score))
        # print('Job title: {}'.format(hit.job_title))
        # print('Key skills: {}'.format(', '.join([k['title'] for k in hit.key_skills])))
        # print('Qualifications: {}'.format(', '.join([q['title'] for q in hit.qualifications])))
        # print('Sectors: {}'.format(', '.join([s['title'] for s in hit.sectors])))
        # print('Locations: {}'.format(', '.join([l['name'] for l in hit.locations])))
        # print('Score - {}'.format(hit.meta.score))
        # print('--------------------------')

    return profile_ids_score
