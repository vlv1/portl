from django.core.management.base import BaseCommand

from projectme.profile.models import KeySkill


KEY_SKILLS = [
    '5S',
    'A3',
    'Agile',
    'Andon',
    'Bottleneck Analysis',
    'Business Case Development',
    'Business Process Reengineering (BPR)',
    'Change Management',
    'Coaching',
    'Commercial Excellence',
    'Continuous Flow',
    'Continuous Improvement (CI)',
    'DFSS',
    'DMAIC',
    'Gemba',
    'Heijunka',
    'Hoshin Kanri',
    'Jidoka',
    'Just-In-Time (JIT)',
    'Kaizen',
    'Kanban',
    'Key Performance Indicators (KPI)',
    'Lean',
    'Lean Manufacturing',
    'Lean Office',
    'Lean Sigma',
    'Operational Excellence',
    'Organisational Design',
    'Overall Equipment Effectiveness (OEE)',
    'PDCA (Plan, Do, Check, Act)',
    'Performance Management',
    'Poke-Yoke',
    'Process Design',
    'Process Excellence',
    'Process Improvement',
    'Process Management',
    'Process Mapping',
    'Project Management',
    'Root Cause Analysis',
    'Single Minute Exchange of Die (SMED)',
    'Six Sigma',
    'Standard Work',
    'Supplier Development',
    'Supply Chain Management (SCM)',
    'Takt Time',
    'Target Operating Model (TOM)',
    'Total Productive Maintenance (TPM)',
    'Toyota Production System (TPS)',
    'Training',
    'Value Stream Mapping (VSM)',
    'Visual Management'
]


class Command(BaseCommand):
    help = 'Create key skills'

    def handle(self, *args, **options):
        for key_skill in KEY_SKILLS:
            KeySkill.objects.get_or_create(title=key_skill)
