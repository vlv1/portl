from django.core.management.base import BaseCommand

from projectme.profile.models import Qualification


QUALIFICATIONS = [
    'Accountant (FAIA)',
    'APM Introductory Certificate',
    'APMP',
    'APM Practitioner Qualification (PQ)',
    "Bachelor's degree",
    'CAPM Certification (Certified Associate of Project Management)',
    'Certified Management Consultant (CMC)',
    'Certified Project Manager (CPM)',
    'Chartered Accountant (ACA)',
    'Chartered Accountant (CA)',
    'Chartered Certified Accountant (ACCA)',
    'Chartered Management Accountant (ACMA & FCMA)',
    'Chartered Public Finance Accountant (CPFA)',
    'Doctorate',
    'LCS Level 1a',
    'LCS Level 1b',
    'LCS Level 1c',
    'LCS Level 2a',
    'LCS Level 2b',
    'LCS Level 3a',
    'LCS Level 3c',
    'Lean Six Sigma Black Belt Certified',
    'Lean Six Sigma Black Belt Trained',
    'Lean Six Sigma Green Belt Certified',
    'Lean Six Sigma Green Belt Trained',
    'Lean Six Sigma Master Black Belt Certified',
    'Lean Six Sigma Master Black Belt Trained',
    'Lean Six Sigma Yellow Belt Certified',
    'Lean Six Sigma Yellow Belt Trained',
    "Master's degree",
    'MBA',
    'MSc in Lean Enterprise (University of Buckingham)',
    'MSc in Lean Systems (Cardiff University)',
    'MSP (Managing Successful Programmes) Advanced',
    'MSP (Managing Successful Programmes) Foundation',
    'MSP (Managing Successful Programmes) Practitioner',
    'National Vocational Qualification (NVQ)',
    'Operations Excellence MSc (Cranfield University)',
    'PgMP (Programme Management Professional)',
    'PMP (Project Management Professional)',
    'Prince2 Foundation',
    'Prince2 Practitioner'
]


class Command(BaseCommand):
    help = 'Create qualifications'

    def handle(self, *args, **options):
        for qualification in QUALIFICATIONS:
            Qualification.objects.get_or_create(title=qualification)
