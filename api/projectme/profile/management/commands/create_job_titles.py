from django.core.management.base import BaseCommand


from projectme.profile.models import JobTitle


JOB_TITLES = [
    'BPR Consultant',
    'Business Analyst',
    'Business Consultant',
    'Business Improvement Consultant',
    'Business Improvement Specialist',
    'Business Process Consultant',
    'Change Agent',
    'Change Consultant',
    'Change Manager',
    'Consultant',
    'Continuous Improvement Consultant',
    'Continuous Improvement Manager',
    'Deployment Leader',
    'Interim Manager',
    'Lean Champion',
    'Lean Coach',
    'Lean Consultant',
    'Lean Expert',
    'Lean Leader',
    'Lean Master',
    'Lean Sensei',
    'Lean Sigma Black Belt',
    'Lean Sigma Green Belt',
    'Lean Sigma Master Black Belt',
    'Lean Sigma Trainer',
    'Lean Trainer',
    'Management Consultant',
    'OpEx Consultant',
    'Process Analyst',
    'Process Architect',
    'Process Consultant',
    'Process Improvement Consultant',
    'Programme Manager',
    'Project Director',
    'Project Manager',
    'Six Sigma Black Belt',
    'Six Sigma Consultant',
    'Six Sigma Green Belt',
    'Six Sigma Master Black Belt',
    'Six Sigma Trainer',
    'Transformation Consultant'
]


class Command(BaseCommand):
    help = 'Create job titles'

    def handle(self, *args, **options):
        for job_title in JOB_TITLES:
            JobTitle.objects.get_or_create(title=job_title)
