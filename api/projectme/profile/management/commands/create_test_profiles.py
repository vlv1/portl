from django.core.management.base import BaseCommand

import random
import names

from django.conf import settings
from django.db import IntegrityError

from projectme.onboarding.models import User
from projectme.profile.models import JobTitle, KeySkill, Sector, \
    Qualification, Language, Location, FreelancerProfile
from projectme.profile.constants import RateType


class Command(BaseCommand):
    help = 'Generates N random profiles'

    def add_arguments(self, parser):
        parser.add_argument(
            '-n', '--number',
            action='store',
            dest='number',
            type=int,
            help='Profiles number to generate'
        )

    def _get_job_title(self):
        """
        Get random job title
        """
        return JobTitle.objects.order_by('?').first()

    def _get_key_skills(self):
        """
        Get random key skills (from 1 to 5)
        """
        return KeySkill.objects.order_by('?')[:random.randint(1, 6)]

    def _get_sectors(self):
        """
        Get random sectors (from 1 to 5)
        """
        return Sector.objects.order_by('?')[:random.randint(1, 6)]

    def _get_qualifications(self):
        """
        Get random qualifications (from 1 to 5)
        """
        return Qualification.objects.order_by('?')[:random.randint(1, 6)]

    def _get_languages(self):
        """
        Get random languages (from 1 to 5)
        """
        return Language.objects.order_by('?')[:random.randint(1, 6)]

    def _get_location(self):
        """
        Get random location
        """
        return Location.objects.order_by('?').first()

    def _generate_rate(self, rate_type=RateType.DAILY):
        """
        Generate specified type rate for the freelancer
        """
        return random.randint(3, 8) * settings.HOURS_IN_A_DAY,\
            random.randint(9, 16) * settings.HOURS_IN_A_DAY,

    def handle(self, *args, **options):
        profiles_number = options.get('number', 1) or 1

        for i in range(profiles_number):
            fn = names.get_first_name()
            ln = names.get_last_name()
            email = '{}.{}@example.com'.format(fn.lower(), ln.lower())
            try:
                user = User.objects.create_user(
                    first_name=fn.lower(),
                    last_name=ln.lower(),
                    email=email
                )
            except IntegrityError:
                user = User.objects.get(email=email)

            print('User - {}'.format(user.email))

            min_rate, max_rate = self._generate_rate()
            fp, _ = FreelancerProfile.objects.get_or_create(
                user=user,
                rate_minimum=min_rate,
                rate_maximum=max_rate,
                rate_type=RateType.DAILY,
                rating=random.randint(100, 500) / 100
            )
            fp.job_title = self._get_job_title()
            fp.key_skills.add(*self._get_key_skills())
            fp.sectors.add(*self._get_sectors())
            fp.qualifications.add(*self._get_qualifications())
            fp.languages.add(*self._get_languages())
            fp.location = self._get_location()
            fp.save()
            print('Created profiles: {}'.format(i + 1))
