from django.db.models import Sum
from rest_framework import serializers

from projectme.core.models import Action
from projectme.feed.utils import set_read_availability_events
from projectme.onboarding.models import User, ReferralCode
from projectme.onboarding.serializers import UserSerializer, \
    LockedUserSerializer, MyUserSerializer
from projectme.profile.constants import CashFlowType
from projectme.profile.models import JobTitle, Company, LimitedCompany, \
    UmbrellaCompany, SoleTraderCompany, Experience, ProfileDetail, \
    KeySkill, Sector, Qualification, Language, Availability, \
    ClientProfile, UpdateAvailabilityRequest, \
    FreelancerProfile, Profile, BioApproval, UmbrellaCompanyOption, Location, \
    VettingRequest, ProfileRating, RatingContest, ProfileView


class JobTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobTitle
        fields = ('id', 'title')


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'


class LimitedCompanySerializer(serializers.ModelSerializer):
    company_type = serializers.SerializerMethodField()

    def get_company_type(self, obj):
        return 'Limited Company'

    class Meta:
        model = LimitedCompany
        fields = (
            'id', 'company_name', 'company_type', 'postcode',
            'registration_number', 'created_at', 'updated_at'
        )
        read_only_fields = ('id', 'company_type', 'created_at', 'updated_at')


class UmbrellaCompanySerializer(serializers.ModelSerializer):
    company_type = serializers.SerializerMethodField()

    def get_company_type(self, obj):
        return 'Umbrella Company'

    class Meta:
        model = UmbrellaCompany
        fields = (
            'id', 'company_name', 'company_type', 'created_at', 'updated_at'
        )
        read_only_fields = ('id', 'company_type', 'created_at', 'updated_at')


class SoleTraderCompanySerializer(serializers.ModelSerializer):
    company_type = serializers.SerializerMethodField()

    def get_company_type(self, obj):
        return 'Sole Trader'

    class Meta:
        model = SoleTraderCompany
        fields = (
            'id', 'company_name', 'company_type', 'created_at', 'updated_at'
        )
        read_only_fields = ('id', 'company_type', 'created_at', 'updated_at')


class LocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Location
        fields = '__all__'


class ProfileDetailSerializer(serializers.ModelSerializer):
    experience = serializers.SerializerMethodField()
    match = serializers.SerializerMethodField()

    def get_experience(self, obj):
        try:
            profile = self.context['profile']

            return Experience.objects.get(profile=profile, detail=obj).years
        except Exception as e:
            pass

        return 0

    def get_match(self, obj):
        search = self.context.get('search', None)
        if search:
            return obj.in_search(search)
        return False

    class Meta:
        model = ProfileDetail
        fields = ('id', 'title', 'experience', 'accepted', 'match')


class KeySkillSerializer(ProfileDetailSerializer):
    class Meta:
        model = KeySkill
        fields = ('id', 'title', 'experience', 'accepted', 'match')


class SectorSerializer(ProfileDetailSerializer):
    class Meta:
        model = Sector
        fields = ('id', 'title', 'experience', 'accepted', 'match')


class QualificationSerializer(ProfileDetailSerializer):
    class Meta:
        model = Qualification
        fields = ('id', 'title', 'experience', 'accepted', 'match')


class LanguageSerializer(ProfileDetailSerializer):
    class Meta:
        model = Language
        fields = ('id', 'title', 'experience', 'accepted', 'match')


class AvailabilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Availability
        fields = ('start_date', 'end_date')


class UpdateClientProfileSerializer(serializers.ModelSerializer):
    def update(self, instance, validated_data):
        return super().update(instance, validated_data)

    class Meta:
        model = ClientProfile
        fields = '__all__'


class UpdateFreelancerProfileSerializer(serializers.ModelSerializer):
    def update(self, instance, validated_data):
        instance.unavailable_weekends = validated_data.get(
            'unavailable_weekends',
            instance.unavailable_weekends
        )
        instance.always_available = validated_data.get(
            'always_available',
            instance.always_available
        )
        instance.bio = validated_data.get('bio', instance.bio)
        instance.on_hold = validated_data.get('on_hold', instance.on_hold)

        if instance.always_available is True:
            instance.unavailable_dates = []

        if validated_data.get('always_available', None) is not None:
            for update_request in UpdateAvailabilityRequest.objects.filter(
                    profile=instance,
                    updated=False
            ):
                update_request.updated = True
                update_request.save()

        instance.rate_minimum = validated_data.get(
            'rate_minimum', instance.rate_minimum
        )
        instance.rate_maximum = validated_data.get(
            'rate_maximum', instance.rate_minimum
        )
        instance.rate_type = validated_data.get(
            'rate_type', instance.rate_type
        )
        instance.save()
        return instance

    class Meta:
        model = FreelancerProfile
        fields = '__all__'


class ProfileSerializer(serializers.ModelSerializer):
    company = serializers.SerializerMethodField()
    job_title = JobTitleSerializer()
    user = serializers.SerializerMethodField()

    def get_company(self, obj):
        try:
            company = Company.objects.get_subclass(profile=obj)
        except Exception as e:
            return None

        if isinstance(company, LimitedCompany):
            return LimitedCompanySerializer(company, context=self.context).data
        if isinstance(company, UmbrellaCompany):
            return UmbrellaCompanySerializer(company, context=self.context).data
        if isinstance(company, SoleTraderCompany):
            return SoleTraderCompanySerializer(company, context=self.context).data
        return CompanySerializer(company, context=self.context).data

    def get_user(self, obj):
        if obj.user == self.context['request'].user:
            return MyUserSerializer(obj.user, context=self.context).data
        return UserSerializer(obj.user, context=self.context).data

    class Meta:
        model = Profile
        fields = '__all__'


class ClientProfileSerializer(ProfileSerializer):

    unlocked_profiles = serializers.SerializerMethodField()

    def get_unlocked_profiles(self, obj):
        return obj.user.unlocked_profiles.count()

    class Meta:
        model = ClientProfile
        fields = '__all__'


class FreelancerProfileSerializer(ProfileSerializer):
    key_skills = serializers.SerializerMethodField()
    sectors = serializers.SerializerMethodField()
    qualifications = serializers.SerializerMethodField()
    languages = serializers.SerializerMethodField()
    location = serializers.SerializerMethodField()

    unlocked = serializers.SerializerMethodField()
    favourite = serializers.SerializerMethodField()
    unlocked_at = serializers.SerializerMethodField()

    availability_updated = serializers.SerializerMethodField()
    availability_requested_at = serializers.SerializerMethodField()
    availability_updated_at = serializers.SerializerMethodField()

    can_favourite = serializers.SerializerMethodField()

    rating = serializers.SerializerMethodField()
    bio = serializers.SerializerMethodField()

    last_login = serializers.SerializerMethodField()

    def get_last_login(self, obj):
        return obj.user.last_login

    def get_bio(self, obj):
        user = self.context['request'].user

        if obj.user == user:
            return obj.bio

        try:
            BioApproval.objects.get(
                profile=obj,
                bio=obj.bio,
                status=BioApproval.ACCEPTED
            )
            return obj.bio
        except BioApproval.DoesNotExist:
            return None

    def get_key_skills(self, obj):
        key_skills = obj.key_skills.all()
        search = self.context.get('search', None)
        if search is not None:
            key_skills = sorted(
                key_skills,
                key=lambda x: x.in_search(search),
                reverse=True
            )
        return KeySkillSerializer(
            key_skills,
            many=True,
            context={'profile': obj, 'search': search}
        ).data

    def get_sectors(self, obj):
        sectors = obj.sectors.all()
        search = self.context.get('search', None)
        if search is not None:
            sectors = sorted(
                sectors,
                key=lambda x: x.in_search(search),
                reverse=True
            )
        return SectorSerializer(
            sectors,
            many=True,
            context={'profile': obj, 'search': search}
        ).data

    def get_qualifications(self, obj):
        qualifications = obj.qualifications.all()
        search = self.context.get('search', None)
        if search is not None:
            qualifications = sorted(
                qualifications,
                key=lambda x: x.in_search(search),
                reverse=True
            )
        return QualificationSerializer(
            qualifications,
            many=True,
            context={'profile': obj, 'search': search}
        ).data

    def get_languages(self, obj):
        languages = obj.languages.all()
        search = self.context.get('search',None)
        if search is not None:
            languages = sorted(
                languages, key=lambda x: x.in_search(search),
                reverse=True
            )
        return LanguageSerializer(
            languages,
            many=True,
            context={'profile': obj, 'search': search}
        ).data

    def get_location(self, obj):
        location = obj.location
        return LocationSerializer(location).data if obj.location else None

    def get_user(self, obj):
        user = self.context['request'].user

        if obj.user == user:
            return MyUserSerializer(obj.user, context=self.context).data

        if obj.unlocked(user) is False:
            return LockedUserSerializer(obj.user, context=self.context).data

        return UserSerializer(obj.user, context=self.context).data

    def get_unlocked(self, obj):
        user = self.context['request'].user

        return obj.unlocked(user)

    def get_favourite(self, obj):
        user = self.context['request'].user

        return obj.favourite(user)

    def get_unlocked_at(self, obj):
        user = self.context['request'].user

        return obj.unlocked_at(user)

    def get_availability_updated(self, obj):
        user = self.context['request'].user

        if user.is_anonymous is False and user.user_type == User.CLIENT:
            try:
                return UpdateAvailabilityRequest.objects.filter(
                    user=user,
                    profile=obj
                ).order_by('-updated_at')[0].updated
            except Exception as e:
                pass

        return False

    def get_availability_requested_at(self, obj):
        user = self.context['request'].user

        if user.is_anonymous is False and user.user_type == User.CLIENT:
            try:
                return UpdateAvailabilityRequest.objects.filter(
                    user=user,
                    profile=obj
                ).order_by('-updated_at')[0].created_at
            except Exception as e:
                pass
        return None

    def get_availability_updated_at(self, obj):
        try:
            return UpdateAvailabilityRequest.objects.filter(
                profile=obj,
                updated=True
            ).order_by('-updated_at')[0].updated_at
        except Exception as e:
            pass

        return obj.updated_at

    def get_can_favourite(self, obj):
        user = self.context['request'].user

        if user.is_anonymous is False and user != obj.user:
            if obj.unlocked(user):
                return True

            if UpdateAvailabilityRequest.objects.filter(
                    user=user,
                    profile=obj
            ).count() > 0:
                return True

        return False

    def get_rating(self, obj):
        return obj.rating

    class Meta:
        model = FreelancerProfile
        fields = '__all__'


class ClientStatisticsSerializer(serializers.ModelSerializer):
    profiles_unlocked = serializers.SerializerMethodField()
    credits_bought = serializers.SerializerMethodField()
    credits_spent = serializers.SerializerMethodField()
    credits_left = serializers.SerializerMethodField()

    class Meta:
        model = ClientProfile
        fields = (
            'profiles_unlocked', 'credits_bought',
            'credits_spent', 'credits_left'
        )

    def get_profiles_unlocked(self, obj):
        return obj.unlocked_profiles.count()

    def get_credits_bought(self, obj):
        return obj.cash_flows.filter(
            flow_type=CashFlowType.INCREASE
        ).aggregate(total_amount=Sum('amount'))['total_amount']

    def get_credits_spent(self, obj):
        return obj.cash_flows.filter(
            flow_type=CashFlowType.DECREASE
        ).aggregate(total_amount=Sum('amount'))['total_amount']

    def get_credits_left(self, obj):
        return obj.credits


class FreelancerStatisticsSerializer(serializers.Serializer):
    appeared_in_search_results = serializers.SerializerMethodField()
    unlocked_count = serializers.SerializerMethodField()
    viewed_count = serializers.SerializerMethodField()

    class Meta:
        model = FreelancerProfile
        fields = (
            'appeared_in_search_results', 'unlocked_count', 'viewed_count'
        )

    def get_appeared_in_search_results(self, obj):
        return obj.search_matches.count()

    def get_unlocked_count(self, obj):
        return obj.unlocks.count()

    def get_viewed_count(self, obj):
        return obj.views.count()


class FreelancerProfileInfoSerializer(serializers.ModelSerializer):
    key_skills = KeySkillSerializer(many=True)
    sectors = SectorSerializer(many=True)
    qualifications = QualificationSerializer(many=True)
    languages = LanguageSerializer(many=True)

    class Meta:
        model = FreelancerProfile
        fields = (
            'key_skills', 'sectors', 'qualifications', 'languages'
        )


class VettingRequestSerializer(serializers.ModelSerializer):

    class Meta:
        model = VettingRequest
        fields = ()

    def save(self, **kwargs):
        user = self.context['request'].user
        profile = FreelancerProfile.objects.get(user=user)

        vetting_request = VettingRequest(profile=profile)
        vetting_request.save()

        profile.vetting_in_progress = True
        profile.save()

        return vetting_request


class FreelancerProfileViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProfileView
        fields = ()

    def save(self, **kwargs):
        profile = self.context['freelancer_profile']
        profile_view = ProfileView(profile=profile)
        profile_view.save()
        return profile_view


class ChangePasswordSerializer(serializers.Serializer):

    current_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)
    confirm_password = serializers.CharField(required=True)

    def validate(self, data):
        new_password = data['new_password']
        confirm_password = data['confirm_password']

        if new_password != confirm_password:
            raise serializers.ValidationError('Passwords do not match')

        return data

    def validate_current_password(self, current_password):
        user = self.context.get('request').user
        if not user.check_password(current_password):
            raise serializers.ValidationError(
                'Current password is not correct'
            )

        return current_password


class RateProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProfileRating
        fields = ('work_score', )

    def save(self, **kwargs):
        vd = self.validated_data

        profile = self.context['profile']
        rated_profile = self.context['rated_profile']

        # create or update rating
        try:
            profile_rating = ProfileRating.objects.get(
                profile=profile,
                rated_profile=rated_profile,

            )
            profile_rating.work_score = vd['work_score']
        except ProfileRating.DoesNotExist:
            profile_rating = ProfileRating(
                profile=profile,
                rated_profile=rated_profile,
                work_score=vd['work_score']
            )

        profile_rating.save()
        return profile_rating


class ContestRatingSerializer(serializers.ModelSerializer):

    class Meta:
        model = RatingContest
        fields = ('comment', )

    def save(self, **kwargs):
        vd = self.validated_data

        freelancer_profile = self.context['freelancer_profile']
        client_profile = self.context['client_profile']

        # create or update contest
        try:
            contest_rating = RatingContest.objects.get(
                freelancer_profile=freelancer_profile,
                client_profile=client_profile

            )
            contest_rating.comment = vd['comment']
        except RatingContest.DoesNotExist:
            contest_rating = RatingContest(
                freelancer_profile=freelancer_profile,
                client_profile=client_profile,
                comment=vd['comment']
            )

        contest_rating.save()
        return contest_rating


class ReferralCodeSerializer(serializers.ModelSerializer):

    referrer = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )
    referred = serializers.SerializerMethodField()
    url = serializers.SerializerMethodField()

    class Meta:
        model = ReferralCode
        fields = '__all__'
        read_only_fields = (
            'code', 'url', 'referred', 'created_at', 'updated_at', 'expired_at'
        )
        write_only_fields = ('referrer', )

    def get_url(self, obj):
        return obj.url

    def get_referred(self, obj):
        return obj.referred.email if obj.referred else None


class UpdateAvailabilityRequestSerializer(serializers.ModelSerializer):
    client = serializers.SerializerMethodField()

    class Meta:
        model = UpdateAvailabilityRequest
        fields = ('client', 'updated')

    def get_client(self, obj):
        full_name = obj.user.get_full_name()
        if hasattr(obj.user, 'profile'):
            profile = obj.user.profile
            if profile.company:
                return obj.user.profile.company.company_name

        return full_name

    def save(self, **kwargs):
        availability_request = super().save(**kwargs)
        availability_requests = UpdateAvailabilityRequest.objects.filter(
                profile=availability_request.profile,
                updated=False
        )
        availability_requests.update(updated=True)

        # track event
        Action.track(
            user=self.context['request'].user,
            verb='updated availability (responding to availability request)'
        )

        set_read_availability_events(availability_request.profile.user.id)
        return availability_request


class UmbrellaCompanyOptionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UmbrellaCompanyOption
        fields = ('company_name', 'id')


class ProfileOptionsSerializer(serializers.Serializer):
    job_titles = JobTitleSerializer(many=True)
    key_skills = KeySkillSerializer(many=True)
    sectors = SectorSerializer(many=True)
    qualifications = QualificationSerializer(many=True)
    languages = LanguageSerializer(many=True)
