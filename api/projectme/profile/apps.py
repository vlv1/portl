# django
from django.apps import AppConfig
from django.conf import settings
from elasticsearch_dsl.connections import connections

from projectme.profile.signals import freelancer_index_remove, recalculate_rate


class ProfileConfig(AppConfig):
    name = 'projectme.profile'

    def ready(self):
        from django.db.models.signals import post_save, pre_delete
        from projectme.profile.models import ProfileRating
        from projectme.profile.models import FreelancerProfile
        from projectme.profile.signals import freelancer_index
        post_save.connect(
            recalculate_rate,
            sender=ProfileRating,
            dispatch_uid='recalculate_rate'
        )
        post_save.connect(
            freelancer_index,
            sender=FreelancerProfile,
            dispatch_uid='freelancer_index'
        )
        pre_delete.connect(
            freelancer_index_remove,
            sender=FreelancerProfile,
            dispatch_uid='freelancer_index_remove'
        )

        connections.configure(**settings.ES_CONNECTIONS)
