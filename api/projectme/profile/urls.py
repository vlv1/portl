from django.urls import re_path

from projectme.profile.api import ProfileOptionsView, MeProfileView, \
    DashboardView, MeUnlockedProfilesView, MeRequestProfilesView, \
    MeCountsProfilesView, UnlockedProfilesView, FavouriteProfilesView, \
    RequestProfilesView, UnlockProfileView, FavouriteProfileView, \
    UmbrellaCompanyOptionsListView, ProfileDetailsView, \
    MeClientProfileView, MeFreelancerProfileView, FreelancerProfileInfoView, \
    UpdateAvailabilityRequestCreateView, UpdateAvailabilityRequestDetailsView, \
    ProfileDetailsSearchView, ProfileChangePasswordView, VettingRequestView, \
    ProfileClientStatisticsView, ProfileStatisticsView, \
    ProfileFreelancerStatisticsView, RateProfileView, RatingContestView, \
    FreelancerProfileViewedView, ReferralCodeViewSet, ConfirmAvailabilityView

urlpatterns = [
    re_path(
        r'^me/$',
        MeProfileView.as_view(),
        name='me'
    ),
    re_path(
        r'^me-client/$',
        MeClientProfileView.as_view(),
        name='me-client'
    ),
    re_path(
        r'^me-freelancer/$',
        MeFreelancerProfileView.as_view(),
        name='me-freelancer'
    ),
    re_path(
        r'^statistics/$',
        ProfileStatisticsView.as_view(),
        name='statistics'
    ),
    re_path(
        r'^statistics-client/$',
        ProfileClientStatisticsView.as_view(),
        name='statistics-client'
    ),
    re_path(
        r'^statistics-freelancer/$',
        ProfileFreelancerStatisticsView.as_view(),
        name='statistics-freelancer'
    ),
    re_path(
        r'^dashboard/$',
        DashboardView.as_view(),
        name='dashboard'
    ),
    re_path(
        r'^vetting/$',
        VettingRequestView.as_view(),
        name='vetting'
    ),
    re_path(
        r'^info/$',
        FreelancerProfileInfoView.as_view(),
        name='info'
    ),
    re_path(
        r'^(?P<pk>[0-9]+)/availability/$',
        UpdateAvailabilityRequestCreateView.as_view(),
        name='availability'
    ),
    re_path(
        r'^availability/(?P<request_pk>[0-9]+)/$',
        UpdateAvailabilityRequestDetailsView.as_view(),
        name='availability-details'
    ),
    re_path(
        r'^details/(?P<detail_type>[a-z_]+)/$',
        ProfileDetailsSearchView.as_view(),
        name='details-search'
    ),
    re_path(
        r'^change_password/$',
        ProfileChangePasswordView.as_view(),
        name='change-password'
    ),
    re_path(
        r'^(?P<pk>[0-9]+)/new_view/$',
        FreelancerProfileViewedView.as_view(),
        name='new_view'
    ),
    re_path(
        r'^(?P<pk>[0-9]+)/rate/$',
        RateProfileView.as_view(),
        name='rate'
    ),
    re_path(
        r'^(?P<pk>[0-9]+)/rating_contest/$',
        RatingContestView.as_view(),
        name='rating-contest'
    ),
    re_path(
        r'^(?P<pk>[0-9]+)/referral_code/$',
        ReferralCodeViewSet.as_view({'get': 'list', 'post': 'create'}),
        name='referral-code'
    ),
    re_path(
        r'^confirm_availability/$',
        ConfirmAvailabilityView.as_view(),
        name='confirm-availability'
    ),

    # to analyze
    re_path(
        r'^options/$',
        ProfileOptionsView.as_view(),
        name="options"
    ),
    re_path(
        r'^me/unlocks/$',
        MeUnlockedProfilesView.as_view(),
        name="me-unlocks"
    ),
    re_path(
        r'^me/requests/$',
        MeRequestProfilesView.as_view(),
        name="me-requests"
    ),
    re_path(
        r'^me/counts/$',
        MeCountsProfilesView.as_view(),
        name="me-counts"
    ),
    re_path(
        r'^unlocks/$',
        UnlockedProfilesView.as_view(),
        name="unlocks"
    ),
    re_path(
        r'^favourites/$',
        FavouriteProfilesView.as_view(),
        name="favourites"
    ),
    re_path(
        r'^requests/$',
        RequestProfilesView.as_view(),
        name="requests"
    ),
    re_path(
        r'^(?P<pk>[0-9]+)/$',
        ProfileDetailsView.as_view(),
        name="profile"
    ),
    re_path(
        r'^(?P<pk>[0-9]+)/unlock/$',
        UnlockProfileView.as_view(),
        name="unlock"
    ),
    re_path(
        r'^(?P<pk>[0-9]+)/favourite/$',
        FavouriteProfileView.as_view(),
        name="favourite"
    ),
    re_path(
        r'^umbrella_options/$',
        UmbrellaCompanyOptionsListView.as_view(),
        name="umbrella_options"
    ),
]