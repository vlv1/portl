
# django
from django.contrib.gis.db import models
from django.conf import settings

# portl
from django.contrib.postgres.fields import ArrayField

from projectme.models import TimestampedModel
from projectme.profile.notifications import welcome_client, \
    welcome_freelancer, unlocked_profile, availability_request, \
    availability_updated
from projectme.profile.search import FreelancerProfileIndex
from projectme.profile.constants import RateType, CashFlowType

# third party
from model_utils.managers import InheritanceManager


class BioApproval(TimestampedModel):
    bio = models.TextField()
    profile = models.ForeignKey(
        'FreelancerProfile',
        related_name="bio_approvals",
        on_delete=models.CASCADE
    )

    REJECTED = -2
    CANCELLED = -1
    PENDING = 0
    ACCEPTED = 1

    STATUSES = (
        (REJECTED, 'Rejected'),
        (CANCELLED, 'Cancelled'),
        (PENDING, 'Pending'),
        (ACCEPTED, 'Accepted')
    )

    status = models.IntegerField(choices=STATUSES, default=PENDING)


class Profile(TimestampedModel):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        related_name="profile",
        on_delete=models.CASCADE
    )
    company = models.OneToOneField(
        'Company',
        blank=True, null=True,
        on_delete=models.CASCADE
    )
    job_title = models.ForeignKey(
        'JobTitle',
        related_name="profiles",
        blank=True, null=True,
        on_delete=models.CASCADE
    )
    rating = models.FloatField(
        verbose_name='rating',
        default=0
    )

    objects = InheritanceManager()

    def __str__(self):
        return "{}".format(self.user)

    def update_rating(self):
        ratings_received = self.ratings_received.all()
        if not ratings_received.exists():
            return 0

        total_score = sum([rate.avg_score for rate in ratings_received])
        avs_score = total_score / ratings_received.count()
        self.rating = avs_score
        self.save()

    def add_credits(self, credits_amount, description=''):
        raise NotImplementedError('Profile has no credits')


class ClientProfile(Profile):
    credits = models.IntegerField(default=0)
    vetted_filter = models.BooleanField(
        verbose_name='vetted filter enabled',
        default=False
    )

    def save(self, *args, **kwargs):
        if self.pk is None:
            welcome_client(self.user)
        super(ClientProfile, self).save(*args, **kwargs)

    def add_credits(self, credits_amount, description=''):
        self.credits += credits_amount
        self.save()

        CashFlow.objects.create(
            profile=self,
            flow_type=CashFlowType.INCREASE,
            description=description,
            amount=credits_amount
        )

    def take_credits(self, credits_amount, description=''):
        self.credits -= credits_amount
        self.save()

        CashFlow.objects.create(
            profile=self,
            flow_type=CashFlowType.DECREASE,
            description=description,
            amount=credits_amount
        )


class FreelancerProfile(Profile):
    postcode = models.CharField(
        verbose_name='postcode',
        max_length=10,
        blank=True
    )
    location = models.ForeignKey(
        'Location',
        related_name='profiles',
        on_delete=models.CASCADE,
        blank=True, null=True
    )
    key_skills = models.ManyToManyField(
        'KeySkill',
        related_name="profiles",
        blank=True
    )
    sectors = models.ManyToManyField(
        'Sector',
        related_name="profiles",
        blank=True
    )
    qualifications = models.ManyToManyField(
        'Qualification',
        related_name="profiles",
        blank=True
    )
    languages = models.ManyToManyField(
        'Language',
        related_name="profiles",
        blank=True
    )

    rate_minimum = models.PositiveIntegerField(
        verbose_name='minimum rate',
        blank=True, null=True
    )
    rate_maximum = models.PositiveIntegerField(
        verbose_name='maximum rate',
        blank=True, null=True
    )
    rate_type = models.IntegerField(
        choices=RateType.CHOICES, default=RateType.HOURLY
    )

    unavailable_weekends = models.BooleanField(default=True)
    unavailable_dates = ArrayField(
        models.DateField(),
        verbose_name='unavailable dates',
        default=list
    )

    always_available = models.BooleanField(default=False)
    bio = models.TextField(blank=True, null=True)
    on_hold = models.BooleanField(default=False)

    vetted = models.BooleanField(default=False)
    vetting_in_progress = models.BooleanField(default=False)

    def index_remove(self):
        fpi = FreelancerProfileIndex.get(id=self.id)
        fpi.delete()

    def indexing(self):
        FreelancerProfileIndex.init()

        job_title = self.job_title.title if self.job_title else ''
        location = self.location.area_name if self.location else ''
        fpi = FreelancerProfileIndex(
            meta={'id': self.id},
            job_title=job_title,
            location=location,
            email=self.user.email
        )

        # add key skills
        for ks in self.key_skills.only('title'):
            fpi.add_key_skill(ks.title)

        # add qualifications
        for q in self.qualifications.only('title'):
            fpi.add_qualification(q.title)

        # add sectors
        for s in self.sectors.only('title'):
            fpi.add_sector(s.title)

        # add languages
        for l in self.languages.only('title'):
            fpi.add_language(l.title)

        fpi.save()
        return fpi.to_dict(include_meta=True)

    @property
    def rate_type_verbose(self):
        return dict(RateType.CHOICES).get(self.rate_type)

    def unlocked(self, user):
        if user.is_anonymous or Unlock.objects.filter(
                user=user,
                profile=self
        ).count() == 0:
            return False

        return True

    def favourite(self, user):
        if user.is_anonymous or Favourite.objects.filter(
                user=user,
                profile=self
        ).count() == 0:
            return False
        return True

    def is_unlocked_by(self, user):
        return Unlock.objects.filter(
            user=user,
            profile=self
        ).exists()

    def is_rated_by(self, client_profile):
        return ProfileRating.objects.filter(
            profile=client_profile,
            rated_profile=self
        ).exists()

    def unlocked_at(self, user):
        if user.is_anonymous:
            return None

        unlocks = Unlock.objects.filter(
            user=user,
            profile=self
        ).order_by('-created_at')
        if unlocks.count() == 0:
            return None

        return unlocks[0].created_at

    def save(self, *args, **kwargs):
        if self.pk is None:
            welcome_freelancer(self.user)
        super(FreelancerProfile, self).save(*args, **kwargs)

        if self.bio is not None and \
                self.bio != '' and \
                BioApproval.objects.filter(
                    bio=self.bio,
                    profile=self,
                    status__gte=BioApproval.PENDING
                ).count() == 0:

            BioApproval.objects.filter(
                profile=self,
                status__gte=BioApproval.PENDING
            ).update(status=BioApproval.CANCELLED)
            BioApproval.objects.create(bio=self.bio, profile=self)


class VettingRequestStatus:
    PENDING = 'pending'
    ACCEPTED = 'accepted'
    REJECTED = 'rejected'

    CHOICES = (
        (PENDING, 'Pending'),
        (ACCEPTED, 'Accepted'),
        (REJECTED, 'Rejected')
    )


class VettingRequest(TimestampedModel):
    profile = models.ForeignKey(
        'FreelancerProfile',
        related_name='vetting_requests',
        on_delete=models.CASCADE
    )
    status = models.CharField(
        verbose_name='status',
        max_length=20,
        choices=VettingRequestStatus.CHOICES,
        default=VettingRequestStatus.PENDING
    )

    def save(self, *args, **kwargs):
        if self.status == VettingRequestStatus.ACCEPTED:
            self.profile.vetted = True
            self.profile.vetting_in_progress = False

        super(VettingRequest, self).save(*args, **kwargs)

    @property
    def status_verbose(self):
        return dict(VettingRequestStatus.CHOICES).get(self.status)


class ProfileDetail(TimestampedModel):
    title = models.CharField(max_length=255)
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True, null=True,
        on_delete=models.CASCADE
    )
    accepted = models.BooleanField(default=True)
    priority = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['title', ]


class JobTitle(ProfileDetail):
    pass


class KeySkill(ProfileDetail):
    def in_search(self, search):
        return self in search.key_skills.all()


class Sector(ProfileDetail):
    def in_search(self, search):
        return self in search.sectors.all()


class Qualification(ProfileDetail):
    def in_search(self, search):
        return self in search.qualifications.all()


class Language(ProfileDetail):
    def in_search(self, search):
        return self in search.languages.all()


class Company(TimestampedModel):
    company_name = models.CharField(
        verbose_name='company name',
        max_length=255
    )

    objects = InheritanceManager()

    def __str__(self):
        return self.company_name

    class Meta:
        verbose_name_plural = "companies"


class LimitedCompany(Company):
    registration_number = models.CharField(max_length=20)
    postcode = models.CharField(max_length=8)

    class Meta:
        verbose_name_plural = "limited companies"


class UmbrellaCompany(Company):
    class Meta:
        verbose_name_plural = "umbrella companies"


class SoleTraderCompany(Company):
    class Meta:
        verbose_name_plural = "sole traders"


class Experience(TimestampedModel):
    years = models.IntegerField(default=0)
    detail = models.ForeignKey(
        'ProfileDetail',
        related_name="experiences",
        on_delete=models.CASCADE
    )
    profile = models.ForeignKey(
        'FreelancerProfile',
        related_name="experiences",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return "{} - {}".format(self.detail, self.years)


class Availability(TimestampedModel):
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return "{} - {}".format(self.start_date, self.end_date)

    class Meta:
        verbose_name_plural = "availabilities"


class Location(models.Model):
    area_code = models.CharField(max_length=255)
    area_name = models.CharField(max_length=255)

    def __str__(self):
        return '{} ({})'.format(self.area_name, self.area_code)

    class Meta:
        ordering = ['area_name']
        verbose_name_plural = 'locations'


class Favourite(TimestampedModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="favourited_profiles",
        on_delete=models.CASCADE
    )
    profile = models.ForeignKey(
        'FreelancerProfile',
        related_name="favourites",
        on_delete=models.CASCADE
    )


class Unlock(TimestampedModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="unlocked_profiles",
        on_delete=models.CASCADE
    )
    profile = models.ForeignKey(
        'FreelancerProfile',
        related_name="unlocks",
        on_delete=models.CASCADE
    )
    seen = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.pk is None:
            unlocked_profile(self)
        super(Unlock, self).save(*args, **kwargs)


class ProfileView(TimestampedModel):

    profile = models.ForeignKey(
        'FreelancerProfile',
        related_name='views',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return '{} view'.format(self.profile)


class ProfileSearchMatch(TimestampedModel):

    profile = models.ForeignKey(
        'FreelancerProfile',
        related_name='search_matches',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return '{} found'.format(self.profile)


class UpdateAvailabilityRequest(TimestampedModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='requests',
        on_delete=models.CASCADE
    )
    profile = models.ForeignKey(
        'FreelancerProfile',
        related_name='requests',
        on_delete=models.CASCADE
    )
    updated = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.pk is None:
            availability_request(self)

        if self.updated is True:
            availability_updated(self)
        super(UpdateAvailabilityRequest, self).save(*args, **kwargs)


class UmbrellaCompanyOption(TimestampedModel):
    company_name = models.CharField(max_length=255)

    class Meta:
        ordering = ['company_name']


class ProfileRating(TimestampedModel):
    """
    Stores profile rating (client or freelancer)
    """
    rated_profile = models.ForeignKey(
        Profile,
        verbose_name='rated profile',
        related_name='ratings_received',
        on_delete=models.CASCADE
    )
    profile = models.ForeignKey(
        Profile,
        verbose_name='profile',
        related_name='ratings_given',
        on_delete=models.CASCADE
    )
    work_score = models.PositiveIntegerField(
        verbose_name='work score'
    )

    class Meta:
        unique_together = ('rated_profile', 'profile')

    def __str__(self):
        return 'Rating from {} to {} at {}'.format(
            self.profile, self.rated_profile, self.created_at
        )

    @property
    def avg_score(self):
        return self.work_score


class RatingContest(TimestampedModel):

    freelancer_profile = models.ForeignKey(
        FreelancerProfile,
        verbose_name='freelancer profile',
        related_name='contest_ratings',
        on_delete=models.CASCADE
    )
    client_profile = models.ForeignKey(
        ClientProfile,
        verbose_name='client profile',
        related_name='contest_ratings',
        on_delete=models.CASCADE
    )
    comment = models.TextField(verbose_name='comment', blank=True)
    processed = models.BooleanField(verbose_name='processed', default=False)

    class Meta:
        ordering = ('processed', )

    def __str__(self):
        return 'Rating contest by {} to {}'.format(
            self.freelancer_profile, self.client_profile
        )


class CashFlow(TimestampedModel):
    """
    Stores clients cash flow data
    """
    profile = models.ForeignKey(
        ClientProfile,
        verbose_name='profile',
        related_name='cash_flows',
        on_delete=models.CASCADE
    )
    flow_type = models.CharField(
        verbose_name='cash flow type',
        max_length=1,
        choices=CashFlowType.CHOICES
    )
    amount = models.PositiveIntegerField(verbose_name='amount')
    description = models.TextField(
        verbose_name='description',
        blank=True
    )

    def __str__(self):
        return '{} balance {}. Reason: {}'.format(
            self.profile, self.flow_type_verbose, self.description
        )

    @property
    def flow_type_verbose(self):
        return dict(CashFlowType.CHOICES).get(self.flow_type)
