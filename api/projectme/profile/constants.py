

class RateType:
    HOURLY = 0
    DAILY = 1

    CHOICES = (
        (HOURLY, 'Hourly Rate'),
        (DAILY, 'Daily Rate')
    )


class CashFlowType:
    INCREASE = 'I'
    DECREASE = 'D'

    CHOICES = (
        (INCREASE, 'Increase'),
        (DECREASE, 'Decrease')
    )
