from dateutil.relativedelta import relativedelta

from django.utils import timezone
from django.contrib import admin

from projectme.onboarding.models import User, ReferralCode
from projectme.profile.models import JobTitle, KeySkill, Sector, \
    Qualification, Language, Experience, ClientProfile, FreelancerProfile, \
    Company, LimitedCompany, UmbrellaCompany, \
    SoleTraderCompany, UpdateAvailabilityRequest, Unlock, BioApproval, \
    Availability, UmbrellaCompanyOption, Location, VettingRequest, \
    ProfileRating, RatingContest, ProfileView, ProfileSearchMatch


class ProfileDetailAdmin(admin.ModelAdmin):
    list_display = ['title', 'creator', 'type', 'priority', 'accepted']
    list_filter = ['creator__user_type', ]
    search_fields = ['title', ]
    ordering = ['title', ]

    def type(self, obj):
        if obj.creator is not None:
            if obj.creator.user_type == User.FREELANCER:
                return 'Freelancer'
            elif obj.creator.user_type == User.CLIENT:
                return 'Client'
        return '-'


class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'created_at']


class FreelancerProfileAdmin(ProfileAdmin):
    filter_horizontal = (
        'key_skills', 'sectors', 'qualifications', 'languages'
    )
    list_display = [
        'user', 'created_at', 'rate_minimum', 'rate_maximum', 'rate_type',
        'location'
    ]
    exclude = ('unavailable_dates', )

    def rate_type(self, obj):
        return obj.rate_type_verbose


class CompanyAdmin(admin.ModelAdmin):
    list_display = ['company_name', 'profile']
    search_fields = ['company_name']


class UpdateAvailabilityRequestAdmin(admin.ModelAdmin):
    list_display = ['profile', 'user', 'updated', 'created_at']


class BioApprovalAdmin(admin.ModelAdmin):
    list_display = ['profile', 'bio', 'status']
    list_filter = ['status', ]
    search_fields = ['profile__user__first_name', 'profile__user__last_name']


class ExperienceAdmin(admin.ModelAdmin):
    list_display = [
        'detail', 'years', 'profile', 'updated_at', 'requires_update'
    ]

    def requires_update(self, obj):
        year_ago = timezone.now() - relativedelta(years=1)
        return obj.updated_at <= year_ago


class UmbrellaOptionsAdmin(admin.ModelAdmin):
    list_display = ['company_name', 'created_at']
    search_fields = ['company_name']


class LocationAdmin(admin.ModelAdmin):
    list_display = ['id', 'area_code', 'area_name']


class UnlockAdmin(admin.ModelAdmin):
    list_display = ['user', 'profile', 'created_at']
    search_fields = [
        'user__email', 'user__first_name', 'user__last_name', 'created_at'
    ]


class VettingRequestAdmin(admin.ModelAdmin):
    list_display = ['profile', 'status', 'created_at', 'updated_at']
    list_filter = ['status', ]


class ProfileRatingAdmin(admin.ModelAdmin):
    list_display = [
        'rated_profile', 'profile',
        'work_score',
        'created_at', 'updated_at'
    ]


class RatingContestAdmin(admin.ModelAdmin):
    list_display = [
        'freelancer_profile', 'client_profile', 'processed'
    ]


class ProfileViewAdmin(admin.ModelAdmin):
    list_display = ['profile', 'created_at']


class ProfileSearchMatchAdmin(admin.ModelAdmin):
    list_display = ['profile', 'created_at']


class ReferralCodeAdmin(admin.ModelAdmin):
    list_display = ['code', 'referrer', 'referred', 'expired_at', 'url']


admin.site.register(ProfileView, ProfileViewAdmin)
admin.site.register(ProfileSearchMatch, ProfileSearchMatchAdmin)
admin.site.register(JobTitle, ProfileDetailAdmin)
admin.site.register(KeySkill, ProfileDetailAdmin)
admin.site.register(Sector, ProfileDetailAdmin)
admin.site.register(Qualification, ProfileDetailAdmin)
admin.site.register(Language, ProfileDetailAdmin)
admin.site.register(Experience, ExperienceAdmin)
admin.site.register(ClientProfile, ProfileAdmin)
admin.site.register(FreelancerProfile, FreelancerProfileAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(LimitedCompany, CompanyAdmin)
admin.site.register(UmbrellaCompany, CompanyAdmin)
admin.site.register(SoleTraderCompany, CompanyAdmin)
admin.site.register(UpdateAvailabilityRequest, UpdateAvailabilityRequestAdmin)
admin.site.register(Unlock, UnlockAdmin)
admin.site.register(BioApproval, BioApprovalAdmin)
admin.site.register(Availability)
admin.site.register(Location, LocationAdmin)
admin.site.register(UmbrellaCompanyOption, UmbrellaOptionsAdmin)
admin.site.register(VettingRequest, VettingRequestAdmin)
admin.site.register(ProfileRating, ProfileRatingAdmin)
admin.site.register(RatingContest, RatingContestAdmin)
admin.site.register(ReferralCode, ReferralCodeAdmin)
