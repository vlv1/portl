

def freelancer_index(sender, instance, **kwargs):
    instance.indexing()


def freelancer_index_remove(sender, instance, **kwargs):
    instance.index_remove()


def recalculate_rate(sender, instance, **kwargs):
    rated_profile = instance.rated_profile
    rated_profile.update_rating()
