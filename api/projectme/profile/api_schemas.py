# third party
from drf_yasg import openapi


job_title_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(
            type=openapi.TYPE_INTEGER,
        ),
        'title': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        )
    }
)

key_skills_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(
            type=openapi.TYPE_INTEGER,
        ),
        'title': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        ),
        'experience': openapi.Schema(
            type=openapi.TYPE_INTEGER,
            description='Key skill experience (in years)'
        ),
        'accepted': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
        'match': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
    }
)


sectors_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(
            type=openapi.TYPE_INTEGER,
        ),
        'title': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        ),
        'experience': openapi.Schema(
            type=openapi.TYPE_INTEGER,
            description='Sector experience (in years)'
        ),
        'accepted': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
        'match': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
    }
)


qualifications_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(
            type=openapi.TYPE_INTEGER,
        ),
        'title': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        ),
        'experience': openapi.Schema(
            type=openapi.TYPE_INTEGER,
            description='Qualification experience (in years)'
        ),
        'accepted': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
        'match': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
    }
)

languages_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(
            type=openapi.TYPE_INTEGER,
        ),
        'title': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        ),
        'experience': openapi.Schema(
            type=openapi.TYPE_INTEGER,
            description='Language experience (in years)'
        ),
        'accepted': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
        'match': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
    }
)


profile_detail_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(
            type=openapi.TYPE_INTEGER,
        ),
        'title': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        ),
        'experience': openapi.Schema(
            type=openapi.TYPE_INTEGER,
            description='Experience (in years)'
        ),
        'accepted': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
        'match': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
    }
)


location_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(
            type=openapi.TYPE_INTEGER,
        ),
        'area_code': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        ),
        'area_name': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        ),
    }
)


response_user_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(
            type=openapi.TYPE_INTEGER,
        ),
        'initials': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=2, maxLength=2
        ),
        'email': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=254,
            description='available only for unlocked users'
        ),
        'first_name': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255,
            description='available only for unlocked users'
        ),
        'last_name': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255,
            description='available only for unlocked users'
        ),
        'avatar': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='User\'s avatar url (available only for unlocked users)'
        ),
        'is_active': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
            description='Designates whether '
                        'this user should be treated '
                        'as active. Unselect this '
                        'instead of deleting accounts'
                        ' (available only for current user)'
        ),
        'verified_mobile': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
            description='available only for unlocked users'
        ),
        'verified_email': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
            description='available only for unlocked users'
        ),
        'mobile': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=128,
            description='available only for unlocked users'
        ),
        'user_type': openapi.Schema(
            type=openapi.TYPE_INTEGER,
            enum=[-1, 0, 1],
            description="""
                Possible values:
                -1 - N/A
                 0 - Client
                 1 - Freelancer
                 
                (available only for current user)
                """
        ),
    }
)

request_user_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'initials': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=2, maxLength=2
        ),
        'email': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=254,
            description='available only for unlocked users'
        ),
        'first_name': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255,
            description='available only for unlocked users'
        ),
        'last_name': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255,
            description='available only for unlocked users'
        ),
        'avatar': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='User\'s avatar url (available only for unlocked users)'
        ),
        'is_active': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
            description='Designates whether '
                        'this user should be treated '
                        'as active. Unselect this '
                        'instead of deleting accounts'
                        ' (available only for current user)'
        ),
        'verified_mobile': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
            description='available only for unlocked users'
        ),
        'verified_email': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
            description='available only for unlocked users'
        ),
        'mobile': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=128,
            description='available only for unlocked users'
        ),
        'current_password': openapi.Schema(
            type=openapi.TYPE_STRING,
            maxLength=128,
            description='available only for current user (for password change)'
        ),
        'password': openapi.Schema(
            type=openapi.TYPE_STRING,
            maxLength=128,
            description='available only for current user'
        ),
        'user_type': openapi.Schema(
            type=openapi.TYPE_INTEGER,
            enum=[-1, 0, 1],
            description="""
                Possible values:
                -1 - N/A
                 0 - Client
                 1 - Freelancer
                 
                (available only for current user)
                """
        ),
    }
)


client_company_request_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    required=['company_name'],
    properties={
        'company_name': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        )
    },
)


freelancer_company_request_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    required=['company_name', 'company_type'],
    properties={
        'company_name': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        ),
        'company_type': openapi.Schema(
            type=openapi.TYPE_STRING,
            description="""
                Possible values:
                     - limited_company
                     - umbrella_company
                     - sole_trader
                """,
        ),
        'postcode': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=8,
            description='REQUIRED only if company_type == Limited Company'
        ),
        'registration_number': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=20,
            description='REQUIRED only if company_type == Limited Company'
        )
    },
)


client_company_response_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(
            type=openapi.TYPE_INTEGER,
        ),
        'company_name': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255,
        ),
    },
)

freelancer_company_response_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(
            type=openapi.TYPE_INTEGER,
        ),
        'company_name': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255,
        ),
        'company_type': openapi.Schema(
            type=openapi.TYPE_STRING,
            description="""
                Possible values:
                     - Limited Company
                     - Umbrella Company
                     - Sole Trader
                """,
        ),
        'postcode': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=8,
            description='Available only if company_type == Limited Company',
        ),
        'registration_number': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=20,
            description='Available only if company_type == Limited Company',
        ),
        'created_at': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='Example: 2018-10-31T08:57:41.734756Z',
        ),
        'updated_at': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='Example: 2018-10-31T08:57:41.734756Z',
        )
    },
)


freelancer_profile_request_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'job_title': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        ),
        'company': freelancer_company_request_schema,
        'user': request_user_schema,
        'key_skills': key_skills_schema,
        'sectors': sectors_schema,
        'qualifications': qualifications_schema,
        'languages': languages_schema,
        'location': location_schema,
        'rate_minimum': openapi.Schema(
            type=openapi.TYPE_INTEGER,
        ),
        'rate_maximum': openapi.Schema(
            type=openapi.TYPE_INTEGER
        ),
        'rate_type': openapi.Schema(
            type=openapi.TYPE_INTEGER,
            description="""
            Possible values:
                 0 - Hourly
                 1 - Daily
            """
        ),
        'unavailable_dates': openapi.Schema(
            type=openapi.TYPE_ARRAY,
            items=openapi.Schema(
                type=openapi.TYPE_STRING,
                format=openapi.FORMAT_DATE,
                description='Date example: 2018-11-08'
            ),
            description='Array of dates'
        ),
        'unlocked': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
            description='Freelancer is unlocked by current user'
        ),
        'favourite': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
            description='Freelancer is favourited by current user'
        ),
        'unlocked_at': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='Example: 2018-10-31T08:57:41.734756Z'
        ),
        'availability_updated': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
            description='If last availability request from current user '
                        'to the freelancer profile is accepted'
        ),
        'availability_requested_at': openapi.Schema(
            type=openapi.TYPE_STRING,
            description="""
                Last availability request created at
                Example: 2018-10-31T08:57:41.734756Z
            """
        ),
        'availability_updated_at': openapi.Schema(
            type=openapi.TYPE_STRING,
            description="""
                Last availability request updated at
                Example: 2018-10-31T08:57:41.734756Z
            """
        ),
        'can_favourite': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
        ),
        'bio': openapi.Schema(
            type=openapi.TYPE_STRING,
        ),
        'last_login': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='Example: 2018-10-31T08:57:41.734756Z'
        ),
        'postcode': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=10
        ),
        'unavailable_weekends': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
        'always_available': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
        'on_hold': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
        'vetted': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
        'vetting_in_progress': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
    }
)


freelancer_profile_response_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(type=openapi.TYPE_INTEGER),
        'company': freelancer_company_response_schema,
        'job_title': job_title_schema,
        'user': response_user_schema,
        'key_skills': key_skills_schema,
        'sectors': sectors_schema,
        'qualifications': qualifications_schema,
        'languages': languages_schema,
        'location': location_schema,
        'rate_minimum': openapi.Schema(
            type=openapi.TYPE_INTEGER,
        ),
        'rate_maximum': openapi.Schema(
            type=openapi.TYPE_INTEGER
        ),
        'rate_type': openapi.Schema(
            type=openapi.TYPE_INTEGER,
            description="""
            Possible values:
                 0 - Hourly
                 1 - Daily
            """
        ),
        'unavailable_dates': openapi.Schema(
            type=openapi.TYPE_ARRAY,
            items=openapi.Schema(
                type=openapi.TYPE_STRING,
                format=openapi.FORMAT_DATE,
                description='Date example: 2018-11-08'
            ),
            description='Array of dates'
        ),
        'unlocked': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
            description='Freelancer is unlocked by current user'
        ),
        'favourite': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
            description='Freelancer is favourited by current user'
        ),
        'unlocked_at': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='Example: 2018-10-31T08:57:41.734756Z'
        ),
        'availability_updated': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
            description='If last availability request from current user '
                        'to the freelancer profile is accepted'
        ),
        'availability_requested_at': openapi.Schema(
            type=openapi.TYPE_STRING,
            description="""
                Last availability request created at
                Example: 2018-10-31T08:57:41.734756Z
            """
        ),
        'availability_updated_at': openapi.Schema(
            type=openapi.TYPE_STRING,
            description="""
                Last availability request updated at
                Example: 2018-10-31T08:57:41.734756Z
            """
        ),
        'can_favourite': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
        ),
        'rating': openapi.Schema(
            type=openapi.TYPE_NUMBER,
        ),
        'bio': openapi.Schema(
            type=openapi.TYPE_STRING,
        ),
        'last_login': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='Example: 2018-10-31T08:57:41.734756Z'
        ),
        'created_at': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='Example: 2018-10-31T08:57:41.734756Z'
        ),
        'updated_at': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='Example: 2018-10-31T08:57:41.734756Z'
        ),
        'postcode': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=10
        ),
        'unavailable_weekends': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
        'always_available': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
        'on_hold': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
        'vetted': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
        'vetting_in_progress': openapi.Schema(
            type=openapi.TYPE_BOOLEAN
        ),
    }
)


client_profile_request_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(type=openapi.TYPE_INTEGER),
        'company': client_company_request_schema,
        'credits': openapi.Schema(type=openapi.TYPE_INTEGER),
        'vetted_filter': openapi.Schema(type=openapi.TYPE_BOOLEAN),
        'user': request_user_schema,
        'job_title': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        )
    }
)


client_profile_response_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'id': openapi.Schema(type=openapi.TYPE_INTEGER),
        'company': client_company_response_schema,
        'unlocked_profiles': openapi.Schema(type=openapi.TYPE_INTEGER),
        'user': response_user_schema,
        'credits': openapi.Schema(type=openapi.TYPE_INTEGER),
        'vetted_filter': openapi.Schema(
            type=openapi.TYPE_BOOLEAN,
            description='Vetted filter is enabled'
        ),
        'job_title': job_title_schema,
        'rating': openapi.Schema(
            type=openapi.TYPE_NUMBER,
        )
    }
)


referral_code_response_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'code': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=40
        ),
        'url': openapi.Schema(
            type=openapi.TYPE_STRING
        ),
        'referred': response_user_schema,
        'created_at': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='Example: 2018-10-31T08:57:41.734756Z'
        ),
        'updated_at': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='Example: 2018-10-31T08:57:41.734756Z'
        ),
        'expired_at': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='Example: 2018-10-31T08:57:41.734756Z'
        )
    }
)
