# django
from django.conf import settings

# third party
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, InnerDoc, Text, Nested


connections.create_connection()


class QualificationIndex(InnerDoc):
    title = Text()


class SectorIndex(InnerDoc):
    title = Text()


class KeySkillIndex(InnerDoc):
    title = Text()


class LanguageIndex(InnerDoc):
    title = Text()


class FreelancerProfileIndex(DocType):
    email = Text()
    job_title = Text()
    key_skills = Nested(KeySkillIndex)
    qualifications = Nested(QualificationIndex)
    sectors = Nested(SectorIndex)
    languages = Nested(LanguageIndex)
    location = Text()

    class Meta:
        index = settings.FREELANCER_PROFILE_INDEX

    def add_key_skill(self, title):
        self.key_skills.append(KeySkillIndex(title=title))

    def add_qualification(self, title):
        self.qualifications.append(QualificationIndex(title=title))

    def add_sector(self, title):
        self.sectors.append(SectorIndex(title=title))

    def add_language(self, title):
        self.languages.append(LanguageIndex(title=title))
