from projectme.notifications.models import ProfileNotification, \
    NotificationConfiguration, OnboardingNotification


def welcome_client(user):
    OnboardingNotification.objects.create(
        user=user,
        notification_type=NotificationConfiguration.WELCOME_CLIENT
    )


def welcome_freelancer(user):
    OnboardingNotification.objects.create(
        user=user,
        notification_type=NotificationConfiguration.WELCOME_FREELANCER
    )


def unlocked_profile(unlock):
    ProfileNotification.objects.create(
        user=unlock.profile.user,
        related_user=unlock.user,
        profile=unlock.profile,
        attributes={},
        notification_type=NotificationConfiguration.UNLOCK_PROFILE
    )


def availability_request(request):
    ProfileNotification.objects.create(
        user=request.profile.user,
        related_user=request.user,
        profile=request.profile,
        attributes={},
        notification_type=NotificationConfiguration.AVAILABILITY_REQUEST
    )


def availability_updated(request):
    ProfileNotification.objects.create(
        user=request.user,
        related_user=request.profile.user,
        profile=request.profile,
        attributes={},
        notification_type=NotificationConfiguration.AVAILABILITY_UPDATED
    )
