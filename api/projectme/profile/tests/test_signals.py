from django.test import TestCase

from projectme.onboarding.models import User
from projectme.profile.models import ClientProfile, FreelancerProfile, \
    ProfileRating


class ProfileRatingSignalsTestCase(TestCase):

    def setUp(self):
        self.freelancer = User.objects.create_user(
            email='freelancer1@example.com',
            first_name='Freelancer 1',
            last_name='user'
        )
        self.freelancer_profile = FreelancerProfile.objects.create(
            user=self.freelancer,
            postcode='postcode',
        )

        self.client = User.objects.create_user(
            email='client1@example.com',
            first_name='Client 1',
            last_name='user'
        )
        self.client_profile = ClientProfile.objects.create(user=self.client)

    def test_signal_called(self):
        """
        Test if rating recalculated by signal on ProfileRating save
        """
        ProfileRating.objects.create(
            rated_profile=self.freelancer_profile,
            profile=self.client_profile,
            work_score=4
        )
        # 4
        self.assertEqual(self.freelancer_profile.rating, 4)

        ProfileRating.objects.create(
            rated_profile=self.client_profile,
            profile=self.freelancer_profile,
            work_score=6
        )

        # 6
        self.assertEqual(self.client_profile.rating, 6)


