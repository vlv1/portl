# system
import datetime
from unittest.mock import patch

# django
from django.urls import reverse
from django.conf import settings
from django.utils import timezone

# third party
from rest_framework import status
from rest_framework.test import APITestCase

# portl
from projectme.core.models import Action
from projectme.onboarding.models import MobileVerification, Verification, User
from projectme.profile.constants import RateType, CashFlowType
from projectme.profile.models import ClientProfile, FreelancerProfile, \
    UpdateAvailabilityRequest, KeySkill, VettingRequest, Unlock, CashFlow, \
    Company, LimitedCompany


class PortlAPITestCase(APITestCase):

    def get_url(self, name, args={}, kwargs={}):
        return reverse(
            'v1:profile:{}'.format(name),
            args=args,
            kwargs=kwargs
        )


class ProfileTests(PortlAPITestCase):

    @patch('projectme.onboarding.serializers.add_feed_event')
    def setUp(self, add_feed_event_mock):
        MobileVerification.objects.create(
            mobile="+447710998870",
            status=Verification.CONFIRMED
        )

        url = reverse('v1:onboarding:client')
        data = {
            "first_name": "Johnny",
            "last_name": "Lee",
            "email": "johnny.lee@hedgehoglab.com",
            "mobile": "+447710998870",
            "password": "password123",
            "profile": {
                "company": "hedgehog lab",
                "job_title": "Client Services Director"
            }
        }

        self.client.post(url, data)

        self.user_client = User.objects.get(email="johnny.lee@hedgehoglab.com")
        self.client_profile = ClientProfile.objects.get(user=self.user_client)
        self.client_profile.credits = 50
        self.client_profile.save()

        MobileVerification.objects.create(
            mobile="+447710998370",
            status=Verification.CONFIRMED
        )

        url = reverse('v1:onboarding:freelancer')
        data = {
            "first_name": "Michael",
            "last_name": "Hutchinson",
            "email": "michael.hutchinson@hedgehoglab.com",
            "mobile": "+447710998370",
            "password": "password123",
            "profile": {}
        }
        self.client.post(url, data)

        self.user_freelancer = User.objects.get(
            email="michael.hutchinson@hedgehoglab.com"
        )
        self.freelancer_profile = FreelancerProfile.objects.get(
            user=self.user_freelancer
        )

    def test_profile_details(self):
        """
        Test ProfileDetailsView do not requires authentication
        and returns correct data
        """
        url = self.get_url(
            'profile', kwargs={"pk": self.freelancer_profile.pk}
        )

        # we do not authenticate before request because endpoint is public
        response = self.client.get(url)
        response_json = response.json()

        self.assertEqual(
            response_json['last_login'],
            self.freelancer_profile.user.last_login.strftime(
                '%Y-%m-%dT%H:%M:%S.%fZ'
            )
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_me_profile_details(self):
        """
        Test GET request to /api/v1/profile/me/
        """
        url = self.get_url('me')

        # try to send request without authentication
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)

        # try to send request after authentication as client
        self.client.force_authenticate(user=self.user_client)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # try to send request after authentication as freelancer
        self.client.force_authenticate(user=self.user_freelancer)
        response = self.client.get(url)
        self.assertEqual(
            response.json()['last_login'],
            self.freelancer_profile.user.last_login.strftime(
                '%Y-%m-%dT%H:%M:%S.%fZ'
            )
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_me_profile_update(self):
        """
        Test PATCH request to /api/v1/profile/me/
        """
        Action.objects.all().delete()

        url = self.get_url('me')

        # try to send request without authentication
        response = self.client.patch(url)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(self.client_profile.credits, 50)

        # try to send request after authentication as client
        self.client.force_authenticate(user=self.user_client)

        self.assertEqual(Action.objects.count(), 0)
        response = self.client.patch(
            url,
            {'credits': 7, 'company': {'company_name': 'New company'}}
        )
        self.client_profile.refresh_from_db()
        self.assertEqual(self.client_profile.credits, 7)
        self.assertEqual(
            self.client_profile.company.company_name,
            'New company'
        )
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

        # action is not generated on client profile edit
        self.assertEqual(Action.objects.count(), 0)

        # try to send request after authentication as freelancer
        self.assertFalse(self.freelancer_profile.always_available)

        self.client.logout()
        self.client.force_authenticate(user=self.user_freelancer)

        response = self.client.patch(
            url,
            {
                'always_available': True,
                'rate_minimum': 3,
                'rate_maximum': 5,
                'rate_type': RateType.HOURLY,
                'company': {
                    'company_type': 'limited_company',
                    'company_name': 'Freelancer new company',
                    'registration_number': 'ALJSDHJS78HJKSD',
                    'postcode': '3223423'
                }
            }
        )

        self.assertEqual(
            response.json()['last_login'],
            self.freelancer_profile.user.last_login.strftime(
                '%Y-%m-%dT%H:%M:%S.%fZ'
            )
        )

        self.freelancer_profile.refresh_from_db()
        self.assertTrue(self.freelancer_profile.always_available)
        self.assertEqual(self.freelancer_profile.rate_minimum, 3)
        self.assertEqual(self.freelancer_profile.rate_maximum, 5)
        self.assertEqual(self.freelancer_profile.rate_type, RateType.HOURLY)

        f_company = Company.objects.get_subclass(
            id=self.freelancer_profile.company.id
        )
        self.assertEqual(f_company.company_name, 'Freelancer new company')
        self.assertEqual(f_company.registration_number, 'ALJSDHJS78HJKSD')
        self.assertEqual(f_company.postcode, '3223423')
        self.assertTrue(isinstance(f_company, LimitedCompany))

        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

        self.assertEqual(Action.objects.count(), 1)
        action = Action.objects.last()
        self.assertEqual(action.user, self.user_freelancer)
        self.assertEqual(action.verb, 'edited profile')

    def test_me_profile_delete(self):
        """
        Test DELETE request to /api/v1/profile/me/
        """
        url = self.get_url('me')

        # try to send request without authentication
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 401)

        # authenticate
        self.client.force_authenticate(user=self.user_client)
        response = self.client.delete(url)

        # user became inactive
        self.user_client.refresh_from_db()
        self.assertFalse(self.user_client.is_active)

        # project has not been deleted
        self.assertTrue(
            ClientProfile.objects.filter(pk=self.client_profile.pk).exists()
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_profile_details_search(self):
        """
        Test GET request to /api/v1/profile/details/<detail_type>/
        """
        ks_1 = KeySkill.objects.create(title='Lean Office')
        ks_2 = KeySkill.objects.create(title='Process Design')
        ks_3 = KeySkill.objects.create(title='Business Case Development')

        url = self.get_url(
            'details-search',
            kwargs={'detail_type': 'key_skill'}
        )

        # try to send request without authentication
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)

        # authenticate
        self.client.force_authenticate(user=self.user_client)

        # if query is empty - all key skills are returned
        response = self.client.get(url)
        self.assertEqual(
            sorted([ks['id'] for ks in response.json()]),
            sorted([ks_1.id, ks_2.id, ks_3.id])
        )
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

        # query is 'lean'
        url = '{}?query={}'.format(url, 'lean')
        response = self.client.get(url)
        self.assertEqual(
            sorted([ks['id'] for ks in response.json()]),
            sorted([ks_1.id])
        )
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

    def test_profile_change_password(self):
        """
        Test GET request to /api/v1/profile/change_password/
        """
        url = self.get_url('change-password')

        # try to send request without authentication
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)

        # authenticate
        self.client.force_authenticate(user=self.user_client)

        # test if current password is wrong
        'password123'
        patch_data = {
            'current_password': 'wrong_password',
            'new_password': 'password12345',
            'confirm_password': 'password12345'
        }
        response = self.client.patch(url, patch_data)
        self.assertEqual(
            response.json(),
            {'current_password': ['Current password is not correct']}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # test if current passwords do not match
        patch_data = {
            'current_password': 'password123',
            'new_password': 'password12345',
            'confirm_password': 'password54321'
        }
        response = self.client.patch(url, patch_data)
        self.assertEqual(
            response.json(),
            {'non_field_errors': ['Passwords do not match']}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # test if everything is ok
        patch_data = {
            'current_password': 'password123',
            'new_password': 'password12345',
            'confirm_password': 'password12345'
        }
        response = self.client.patch(url, patch_data)
        self.assertEqual(response.json(), {})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('projectme.profile.api.add_feed_event')
    def test_unlock_profile(self, add_feed_event_mock):
        Action.objects.all().delete()

        url = self.get_url('unlock', kwargs={"pk": self.freelancer_profile.pk})
        data = {}

        self.client.force_authenticate(user=self.user_client)

        self.assertEqual(Action.objects.count(), 0)
        self.assertEqual(self.client_profile.cash_flows.count(), 0)

        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertEqual(Action.objects.count(), 1)
        action = Action.objects.first()
        self.assertEqual(action.user, self.user_client)
        self.assertEqual(action.verb, 'unlocked profile')
        self.assertEqual(action.target, self.freelancer_profile)

        self.client_profile.refresh_from_db()

        self.assertEqual(self.client_profile.cash_flows.count(), 1)
        client_cash_flow = self.client_profile.cash_flows.first()
        self.assertEqual(client_cash_flow.profile, self.client_profile)
        self.assertEqual(client_cash_flow.amount, settings.UNLOCK_PRICE)
        self.assertEqual(client_cash_flow.flow_type, CashFlowType.DECREASE)
        self.assertEqual(client_cash_flow.description, 'for profile unlock')

    def test_favourite_profile(self):
        url = self.get_url(
            'favourite',
            kwargs={"pk": self.freelancer_profile.pk}
        )
        data = {}

        self.client.force_authenticate(user=self.user_client)
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_unfavourite_profile(self):
        self.test_favourite_profile()

        url = self.get_url(
            'favourite',
            kwargs={"pk": self.freelancer_profile.pk}
        )
        data = {}

        self.client.force_authenticate(user=self.user_client)
        response = self.client.delete(url, data)

        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

    def test_list_unlocked_profiles(self):
        self.test_unlock_profile()

        url = self.get_url('unlocks')
        data = {}

        self.client.force_authenticate(user=self.user_client)
        response = self.client.get(url, data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_list_favourited_profiles(self):
        self.test_favourite_profile()

        url = self.get_url('favourites')
        data = {}

        self.client.force_authenticate(user=self.user_client)
        response = self.client.get(url, data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    @patch('projectme.profile.api.add_feed_event')
    def test_list_request_availability_updates_profiles(self,
                                                        add_feed_event_mock):
        # TODO: just create new request instead of running separate test again
        self.test_request_availability_create()

        url = self.get_url('requests')

        self.client.force_authenticate(user=self.user_client)
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    @patch('projectme.profile.api.add_feed_event')
    def test_request_availability_create(self, add_feed_event_mock):
        url = self.get_url(
            'availability',
            kwargs={'pk': self.freelancer_profile.pk}
        )
        data = {}

        self.assertEqual(
            UpdateAvailabilityRequest.objects.filter(
                user=self.user_client
            ).count(), 0
        )

        self.client.force_authenticate(user=self.user_client)

        # create first request
        response = self.client.post(url, data)

        self.assertEqual(
            UpdateAvailabilityRequest.objects.filter(
                user=self.user_client
            ).count(), 1
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(add_feed_event_mock.call_count, 1)

        # second request should not be created because
        # settings.CLIENT_REQUEST_DELAY not has not passed
        response = self.client.post(url, data)

        self.assertEqual(
            UpdateAvailabilityRequest.objects.filter(
                user=self.user_client
            ).count(), 1
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(add_feed_event_mock.call_count, 1)

        # set availability request created more than
        # settings.CLIENT_REQUEST_DELAY seconds ago
        av_request = UpdateAvailabilityRequest.objects.filter(
            user=self.user_client
        ).first()
        av_request.created_at = timezone.now()-datetime.timedelta(
            seconds=settings.CLIENT_REQUEST_DELAY + 5
        )
        av_request.save()

        response = self.client.post(url, data)

        self.assertEqual(
            UpdateAvailabilityRequest.objects.filter(
                user=self.user_client
            ).count(), 2
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(add_feed_event_mock.call_count, 2)

    def test_request_availability_update_details(self):
        """
        Test GET request to /api/v1/profile/<id>/availability/<id>/
        """
        av_req = UpdateAvailabilityRequest.objects.create(
            profile=self.freelancer_profile,
            user=self.user_client
        )
        url = self.get_url(
            'availability-details',
            kwargs={'request_pk': av_req.pk}
        )

        # try to send request as a client, not freelancer
        self.client.force_authenticate(user=self.user_client)
        response = self.client.get(url)
        self.assertEqual(
            response.json(),
            {'status': 'error', 'errors': 'Only available for freelancer'}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # send request as a freelancer
        self.client.force_authenticate(user=self.user_freelancer)
        response = self.client.get(url)
        self.assertEqual(
            response.json(),
            {
                'client': self.user_client.profile.company.company_name,
                'updated': av_req.updated
            }
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('projectme.profile.serializers.set_read_availability_events')
    def test_request_availability_update_edit(
            self, set_read_availability_events_mock):
        """
        Test PATCH request to /api/v1/profile/<id>/availability/<id>/
        """
        Action.objects.all().delete()

        av_req_1 = UpdateAvailabilityRequest.objects.create(
            profile=self.freelancer_profile,
            user=self.user_client
        )
        av_req_2 = UpdateAvailabilityRequest.objects.create(
            profile=self.freelancer_profile,
            user=self.user_client
        )
        url = self.get_url(
            'availability-details',
            kwargs={'request_pk': av_req_1.pk}
        )
        data = {'updated': True}

        # try to send request as a client, not freelancer
        self.client.force_authenticate(user=self.user_client)
        response = self.client.patch(url, data)
        self.assertEqual(
            response.json(),
            {'status': 'error', 'errors': 'Only available for freelancer'}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertFalse(set_read_availability_events_mock.called)
        self.assertFalse(Action.objects.exists())

        # send request as a freelancer
        self.client.logout()
        self.client.force_authenticate(user=self.user_freelancer)
        response = self.client.patch(url, data)

        av_req_1.refresh_from_db()
        av_req_2.refresh_from_db()
        self.assertTrue(av_req_1.updated)
        self.assertTrue(av_req_2.updated)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # check if new event was created
        set_read_availability_events_mock.assert_called_once_with(
            self.user_freelancer.id
        )

        # check in action was tracked
        self.assertEqual(Action.objects.count(), 1)
        action = Action.objects.first()
        self.assertEqual(action.user, self.user_freelancer)
        self.assertEqual(
            action.verb,
            'updated availability (responding to availability request)'
        )

    @patch('projectme.profile.api.add_feed_event')
    def test_list_availability_requests(self, add_feed_event_mock):
        # TODO: just create new request instead of running separate test again
        self.test_request_availability_create()

        url = self.get_url('me-requests')
        data = {}

        self.client.force_authenticate(user=self.user_freelancer)
        response = self.client.get(url, data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_list_unlocked_by_clients(self):
        self.test_unlock_profile()

        url = self.get_url('me-unlocks')
        data = {}

        self.client.force_authenticate(user=self.user_freelancer)
        response = self.client.get(url, data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_vetting_request_create(self):
        """
        Test POST request to /api/v1/vetting/
        """
        url = self.get_url('vetting')

        # try to send request without authentication
        response = self.client.post(url)
        self.assertEqual(response.status_code, 401)
        self.assertFalse(VettingRequest.objects.exists())

        # try to send request as client (not allowed)
        self.client.force_authenticate(user=self.user_client)
        response = self.client.post(url, {})

        self.assertFalse(VettingRequest.objects.exists())
        self.assertEqual(
            response.json(),
            {"status": "error", "errors": "Only available for freelancer"}
        )
        self.assertEqual(response.status_code, 400)

        # try to send request after authentication as freelancer
        self.client.logout()
        self.client.force_authenticate(user=self.user_freelancer)
        response = self.client.post(url, {})
        self.assertTrue(VettingRequest.objects.count(), 1)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class RateProfileViewTestCase(PortlAPITestCase):
    """
    Test /api/profile/<pk>/rate/ api endpoint
    """
    def setUp(self):
        self.freelancer_1 = User.objects.create_user(
            email='freelancer1@example.com',
            first_name='Freelancer 1',
            last_name='user'
        )
        self.freelancer_2 = User.objects.create_user(
            email='freelancer2@example.com',
            first_name='Freelancer 2',
            last_name='user'
        )
        self.freelancer_profile_1 = FreelancerProfile.objects.create(
            user=self.freelancer_1,
            postcode='postcode',
            company=Company.objects.create(company_name='Test company')
        )
        self.freelancer_profile_2 = FreelancerProfile.objects.create(
            user=self.freelancer_2,
            postcode='postcode',
            company=Company.objects.create(company_name='Test company')
        )

        self.client_1 = User.objects.create_user(
            email='client1@example.com',
            first_name='Client 1',
            last_name='user'
        )
        self.client_2 = User.objects.create_user(
            email='client2@example.com',
            first_name='Client 2',
            last_name='user'
        )
        self.client_profile_1 = ClientProfile.objects.create(
            user=self.client_1,
            company=Company.objects.create(company_name='Test company')
        )
        self.client_profile_2 = ClientProfile.objects.create(
            user=self.client_2,
            company=Company.objects.create(company_name='Test company')
        )

    def test_rate_unauthenticated(self):
        """
        Test rate fails when user is not authenticated
        """
        api_url = self.get_url(
            'rate',
            kwargs={'pk': 999999}
        )

        response = self.client.post(api_url, {})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.content,
            b'{"detail":"Authentication credentials were not provided."}'
        )

    def test_rate_unknown_profile(self):
        """
        Test rate fails with 404 while trying to rate unknown user
        """
        api_url = self.get_url(
            'rate',
            kwargs={'pk': 999999}
        )

        self.client.force_authenticate(user=self.freelancer_1)
        response = self.client.post(api_url, {})
        self.assertEqual(response.status_code, 404)

    def test_rate_same_profile_types(self):
        """
        Test rate fails with 400 while trying
        to rate freelancer as freelancer or client as client
        """
        # try to rate freelancer as freelancer
        api_url = self.get_url(
            'rate',
            kwargs={'pk': self.freelancer_profile_2.pk}
        )

        self.client.force_authenticate(user=self.freelancer_1)

        post_data = {'work_score': 1}
        response = self.client.post(api_url, post_data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json(),
            {"status": "error", "errors": "Profiles should differ by user type"}
        )

        # try to rate client as client
        api_url = self.get_url(
            'rate',
            kwargs={'pk': self.client_profile_2.pk}
        )

        self.client.force_authenticate(user=self.client_1)

        post_data = {'work_score': 1}
        response = self.client.post(api_url, post_data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json(),
            {"status": "error", "errors": "Profiles should differ by user type"}
        )

    def test_client_rate_not_unlocked_profile(self):
        """
        Test client cannot rate not unlocked freelancer
        """
        api_url = self.get_url(
            'rate',
            kwargs={'pk': self.freelancer_profile_1.pk}
        )

        self.client.force_authenticate(user=self.client_1)

        post_data = {'work_score': 1}
        response = self.client.post(api_url, post_data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json(),
            {"status": "error", "errors": "Freelancer must be unlocked first"}
        )

    def test_freelancer_rate_client_not_rated_by(self):
        """
        Test freelancer cannot rate client he wasn't rated by
        """
        api_url = self.get_url(
            'rate',
            kwargs={'pk': self.client_profile_1.pk}
        )

        self.client.force_authenticate(user=self.freelancer_1)

        post_data = {'work_score': 1}
        response = self.client.post(api_url, post_data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json(),
            {
                'status': 'error',
                'errors': 'You should be rated by this client first'
            }
        )

    def test_rate_freelancer_by_client(self):
        """
        Test client rate freelancer
        """
        api_url = self.get_url(
            'rate',
            kwargs={'pk': self.freelancer_profile_1.pk}
        )

        self.client.force_authenticate(user=self.client_1)

        # unlock freelancer by client first
        Unlock.objects.create(
            user=self.client_1,
            profile=self.freelancer_profile_1
        )

        post_data = {'work_score': 4}
        response = self.client.post(api_url, post_data)
        self.assertEqual(response.status_code, 202)

        self.freelancer_profile_1.refresh_from_db()
        self.assertEqual(self.freelancer_profile_1.rating, 4)


class RatingContestViewTestCase(PortlAPITestCase):
    """
    Test /api/profile/<pk>/rating_contest/ api endpoint
    """
    def setUp(self):
        self.freelancer_1 = User.objects.create_user(
            email='freelancer1@example.com',
            first_name='Freelancer 1',
            last_name='user'
        )
        self.freelancer_profile_1 = FreelancerProfile.objects.create(
            user=self.freelancer_1,
            postcode='postcode'
        )

        self.client_1 = User.objects.create_user(
            email='client1@example.com',
            first_name='Client 1',
            last_name='user'
        )
        self.client_profile_1 = ClientProfile.objects.create(user=self.client_1)

    def test_rate_unauthenticated(self):
        """
        Test rate fails when user is not authenticated
        """
        api_url = self.get_url(
            'rating-contest',
            kwargs={'pk': 999999}
        )

        response = self.client.post(api_url, {})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.content,
            b'{"detail":"Authentication credentials were not provided."}'
        )

    def test_rate_unknown_profile(self):
        """
        Test rate fails with 404 while trying to rate unknown user
        """
        api_url = self.get_url(
            'rating-contest',
            kwargs={'pk': 999999}
        )

        self.client.force_authenticate(user=self.freelancer_1)
        response = self.client.post(api_url, {})
        self.assertEqual(response.status_code, 404)

    def test_rating_contest_from_client(self):
        """
        Test rating contest is not allowed from client
        """
        api_url = self.get_url(
            'rating-contest',
            kwargs={'pk': self.freelancer_profile_1.pk}
        )

        self.client.force_authenticate(user=self.client_1)

        response = self.client.post(api_url, {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json(),
            {'status': 'error', 'errors': 'Only freelancer can create contest'}
        )

    def test_rating_contest(self):
        """
        Test rating contest
        """
        # try to rate freelancer as freelancer
        api_url = self.get_url(
            'rating-contest',
            kwargs={'pk': self.client_profile_1.pk}
        )

        self.client.force_authenticate(user=self.freelancer_1)

        post_data = {'comment': 'Some comment'}
        response = self.client.post(api_url, post_data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.json(),
            {'comment': 'Some comment'}
        )


class ConfirmAvailabilityViewTestCase(PortlAPITestCase):
    """
    Test /api/profile/confirm_availability/ api endpoint
    """
    def setUp(self):
        Action.objects.all().delete()

        self.freelancer_1 = User.objects.create_user(
            email='freelancer1@example.com',
            first_name='Freelancer 1',
            last_name='user'
        )
        self.freelancer_profile_1 = FreelancerProfile.objects.create(
            user=self.freelancer_1,
            postcode='postcode'
        )

        self.client_1 = User.objects.create_user(
            email='client1@example.com',
            first_name='Client 1',
            last_name='user'
        )
        self.client_profile_1 = ClientProfile.objects.create(user=self.client_1)

    def test_confirm_unauthenticated(self):
        """
        Test confirm fails when user is not authenticated
        """
        api_url = self.get_url('confirm-availability')

        response = self.client.post(api_url, {})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.content,
            b'{"detail":"Authentication credentials were not provided."}'
        )
        self.assertFalse(Action.objects.exists())

    def test_confirm_from_client(self):
        """
        Test rating contest is not allowed from client
        """
        api_url = self.get_url('confirm-availability')

        self.client.force_authenticate(user=self.client_1)

        response = self.client.post(api_url, {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json(),
            {
                'status': 'error',
                'errors': 'Only freelancer can confirm availability'
            }
        )
        self.assertFalse(Action.objects.exists())

    def test_post(self):
        """
        Test confirm availability
        """
        av_req_1 = UpdateAvailabilityRequest.objects.create(
            profile=self.freelancer_profile_1,
            user=self.client_1
        )
        av_req_2 = UpdateAvailabilityRequest.objects.create(
            profile=self.freelancer_profile_1,
            user=self.client_1
        )
        self.assertFalse(av_req_1.updated)
        self.assertFalse(av_req_2.updated)

        self.assertFalse(Action.objects.exists())

        # send request
        api_url = self.get_url('confirm-availability')

        self.client.force_authenticate(user=self.freelancer_1)

        response = self.client.post(api_url, {})
        self.assertEqual(response.status_code, 202)
        self.assertEqual(response.json(), {})

        av_req_1.refresh_from_db()
        av_req_2.refresh_from_db()
        self.assertTrue(av_req_1.updated)
        self.assertTrue(av_req_2.updated)

        self.assertEqual(Action.objects.count(), 1)
        action = Action.objects.first()
        self.assertEqual(action.user, self.freelancer_1)
        self.assertEqual(
            action.verb,
            'updated availability (without availability request)'
        )


class ReferralCodeViewTestCase(PortlAPITestCase):
    """
    Test /api/profile/<pk>/referral_code/ api endpoint
    """
    def setUp(self):
        self.freelancer_1 = User.objects.create_user(
            email='freelancer1@example.com',
            first_name='Freelancer 1',
            last_name='user'
        )
        self.freelancer_profile_1 = FreelancerProfile.objects.create(
            user=self.freelancer_1,
            postcode='postcode'
        )

        self.client_1 = User.objects.create_user(
            email='client1@example.com',
            first_name='Client 1',
            last_name='user'
        )
        self.client_profile_1 = ClientProfile.objects.create(user=self.client_1)

    def test_unauthenticated(self):
        """
        Test request fails when user is not authenticated
        """
        api_url = self.get_url(
            'referral-code',
            kwargs={'pk': self.client_profile_1.pk}
        )

        response = self.client.get(api_url)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.content,
            b'{"detail":"Authentication credentials were not provided."}'
        )

    def test_get_referrals_from_freelancer(self):
        """
        Test referrals api is not allowed from freelancer
        """
        api_url = self.get_url(
            'referral-code',
            kwargs={'pk': self.client_profile_1.pk}
        )

        self.client.force_authenticate(user=self.freelancer_1)

        response = self.client.get(api_url)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json(),
            {'errors': 'Can be used only by clients', 'status': 'error'}
        )

    def test_get(self):
        """
        Test get referral codes
        """
        api_url = self.get_url(
            'referral-code',
            kwargs={'pk': self.client_profile_1.pk}
        )

        self.client.force_authenticate(user=self.client_1)

        response = self.client.get(api_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

    def test_post(self):
        """
        Test create referral codes
        """
        api_url = self.get_url(
            'referral-code',
            kwargs={'pk': self.client_profile_1.pk}
        )

        self.client.force_authenticate(user=self.freelancer_1)

        response = self.client.post(api_url, {})
        self.assertEqual(response.status_code, 201)
