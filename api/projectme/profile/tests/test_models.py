from django.test import TestCase

from projectme.onboarding.models import User
from projectme.profile.constants import CashFlowType
from projectme.profile.models import ClientProfile, FreelancerProfile, \
    ProfileRating, CashFlow


class ProfileTestCase(TestCase):

    def setUp(self):
        self.freelancer_1 = User.objects.create_user(
            email='freelancer1@example.com',
            first_name='Freelancer 1',
            last_name='user'
        )
        self.freelancer_2 = User.objects.create_user(
            email='freelancer2@example.com',
            first_name='Freelancer 2',
            last_name='user'
        )
        self.freelancer_profile_1 = FreelancerProfile.objects.create(
            user=self.freelancer_1,
            postcode='postcode',
        )
        self.freelancer_profile_2 = FreelancerProfile.objects.create(
            user=self.freelancer_2,
            postcode='postcode',
        )

        self.client_1 = User.objects.create_user(
            email='client1@example.com',
            first_name='Client 1',
            last_name='user'
        )
        self.client_2 = User.objects.create_user(
            email='client2@example.com',
            first_name='Client 2',
            last_name='user'
        )
        self.client_profile_1 = ClientProfile.objects.create(
            user=self.client_1
        )
        self.client_profile_2 = ClientProfile.objects.create(
            user=self.client_2
        )

    def test_update_rating(self):

        # test method without any ratings
        self.freelancer_profile_1.update_rating()
        self.assertEqual(self.freelancer_profile_1.rating, 0)

        self.client_profile_1.update_rating()
        self.assertEqual(self.client_profile_1.rating, 0)

        # create some ratings
        ProfileRating.objects.create(
            rated_profile=self.freelancer_profile_1,
            profile=self.client_profile_1,
            work_score=2
        )
        ProfileRating.objects.create(
            rated_profile=self.freelancer_profile_1,
            profile=self.client_profile_2,
            work_score=1
        )

        self.freelancer_profile_1.update_rating()

        # (2 + 1) / 2 = 4
        self.assertEqual(self.freelancer_profile_1.rating, 1.5)

        ProfileRating.objects.create(
            rated_profile=self.client_profile_2,
            profile=self.freelancer_profile_1,
            work_score=3
        )
        ProfileRating.objects.create(
            rated_profile=self.client_profile_2,
            profile=self.freelancer_profile_2,
            work_score=5
        )
        self.client_profile_2.update_rating()

        # (3 + 5) / 2 = 4
        self.assertEqual(self.client_profile_2.rating, 4)

    def test_add_credits(self):
        """
        Test ClientProfile.add_credits method
        """
        self.assertEqual(self.client_profile_1.credits, 0)
        self.assertFalse(
            CashFlow.objects.filter(profile=self.client_profile_1).exists()
        )

        self.client_profile_1.add_credits(7, description='Credits bought')

        self.assertEqual(self.client_profile_1.credits, 7)
        client_cash_flows = CashFlow.objects.filter(
            profile=self.client_profile_1
        )
        client_cash_flow = client_cash_flows.first()
        self.assertEqual(client_cash_flows.count(), 1)
        self.assertEqual(client_cash_flow.profile, self.client_profile_1)
        self.assertEqual(client_cash_flow.flow_type, CashFlowType.INCREASE)
        self.assertEqual(client_cash_flow.description, 'Credits bought')