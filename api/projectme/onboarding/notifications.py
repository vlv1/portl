from projectme.notifications.models import NotificationConfiguration, \
    OnboardingNotification


def mobile_verification(verification):
    attributes = {
        "CODE": verification.verification_code,
        "MOBILE": verification.mobile.as_e164 if verification.mobile else ''
    }

    if verification.user is None:
        if verification.first_name is not None:
            attributes["USER_FIRSTNAME"] = "{}".format(verification.first_name)

            if verification.last_name is not None:
                attributes["USER"] = "{} {}".format(
                    verification.first_name,
                    verification.last_name
                )
            else:
                attributes["USER"] = "{}".format(verification.first_name)

    OnboardingNotification.objects.create(
        user=verification.user,
        attributes=attributes,
        notification_type=NotificationConfiguration.MOBILE_VERIFICATION_CODE
    )


def verify_email(verification):
    url = 'api/v1/onboard/verify-email/{}/'.format(
        verification.verification_code
    )

    OnboardingNotification.objects.create(
        notification_type=NotificationConfiguration.VERIFY_EMAIL,
        url=url,
        user=verification.user
    )


def reset_password(reset):
    OnboardingNotification.objects.create(
        user=reset.user,
        url="change-password/{}/".format(reset.hash),
        attributes={"HASH": reset.hash},
        notification_type=NotificationConfiguration.RESET_PASSWORD
    )
