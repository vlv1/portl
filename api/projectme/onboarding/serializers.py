# django
from django.shortcuts import get_object_or_404

# third party
from rest_framework import serializers
from rest_framework import validators

# portl
from projectme.feed.utils import add_feed_event
from projectme.feed.models import FeedEvent

from projectme.onboarding.models import User, MobileVerification, \
    Verification, ReferralCode
from projectme.profile.models import ClientProfile, FreelancerProfile, \
    Company, JobTitle


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)

    def validate(self, data):
        email = data['email']
        password = data['password']

        try:
            user = User.objects.get_active().get(email=email)
        except User.DoesNotExist:
            raise serializers.ValidationError(
                "The email you have entered does not exist, please try again."
            )

        if user.check_password(password) is False:
            raise serializers.ValidationError(
                "The password you have entered is incorrect, please try again."
            )

        if not user.is_active:
            raise serializers.ValidationError("User isn't active")

        self.instance = user
        return data


class LoginClientSerializer(LoginSerializer):

    def validate(self, data):
        data = super().validate(data)

        try:
            ClientProfile.objects.get(user=self.instance)
        except ClientProfile.DoesNotExist:
            raise serializers.ValidationError(
                'Only clients are allowed to login'
            )

        if not self.instance.verified_email:
            raise serializers.ValidationError(
                'Please confirm your email to login into Portl'
            )

        return data


class LoginFreelancerSerializer(LoginSerializer):

    def validate(self, data):
        data = super().validate(data)
        try:
            FreelancerProfile.objects.get(user=self.instance)
        except FreelancerProfile.DoesNotExist:
            raise serializers.ValidationError(
                'Only freelancers are allowed to login'
            )

        return data


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id', 'first_name', 'last_name', 'avatar', 'initials', 'email',
            'mobile', 'verified_mobile', 'verified_email'
        )


class CreateClientProfileSerializer(serializers.Serializer):
    user = serializers.IntegerField(required=False)
    company = serializers.CharField(required=False)
    job_title = serializers.CharField(required=False)

    def create(self, validated_data):
        company = validated_data.pop('company', None)
        if company:
            validated_data['company'] = Company.objects.create(
                company_name=company
            )

        job_title = validated_data.pop('job_title', None)
        if job_title:
            job_titles = JobTitle.objects.filter(
                title=job_title
            ).order_by('created_at')

            if job_titles.count() > 0:
                job_title = job_titles[0]
            else:
                job_title = JobTitle.objects.create(
                    title=job_title,
                    accepted=False,
                    creator=validated_data['user']
                )

            validated_data['job_title'] = job_title

        return ClientProfile.objects.create(**validated_data)

    def validate_user(self, value):
        return get_object_or_404(User, pk=value)


class CreateFreelancerProfileSerializer(serializers.Serializer):

    def create(self, validated_data):
        user = self.context['user']
        profile = FreelancerProfile.objects.create(
            user=user, **validated_data
        )
        return profile


class RegisterClientSerializer(serializers.ModelSerializer):
    profile = CreateClientProfileSerializer(required=False)
    referral_code = serializers.CharField(required=False)
    avatar = serializers.ImageField(
        required=False,
        help_text='base64 encoded image (as string)'
    )

    def __init__(self, *args, **kwargs):
        super(RegisterClientSerializer, self).__init__(*args, **kwargs)
        # Find UniqueValidator and set custom message
        for validator in self.fields['email'].validators:
            if isinstance(validator, validators.UniqueValidator):
                validator.message = "A user with this email already exists"

    def create(self, validated_data):
        password = validated_data.pop('password')
        profile = validated_data.pop('profile', {})

        # mark referral code as processed if specified
        ref_code = validated_data.pop('referral_code', None)

        validated_data['user_type'] = User.CLIENT
        user = User(**validated_data)
        user.set_password(password)
        user.is_active = True
        user.verified_mobile = True
        user.save()

        profile['user'] = user.pk

        serializer = CreateClientProfileSerializer(
            data=profile,
            context=self.context
        )

        if not serializer.is_valid():
            raise serializers.ValidationError("Error creating client profile")

        serializer.save()

        if ref_code:
            referral_code = ReferralCode.objects.get(code=ref_code)
            referral_code.process(referred_user=user)

        verifications = MobileVerification.objects.filter(
            user=None,
            mobile=user.mobile,
            status=Verification.CONFIRMED
        )
        verifications.update(user=user)
        return user

    def validate(self, data):
        code = data.get('referral_code')
        if code:
            referral_code = ReferralCode.objects.filter(
                code=code,
                expired_at__isnull=True
            )
            if not referral_code.exists():
                data.pop('referral_code')

        return data

    def validate_email(self, value):
        try:
            User.objects.get(email=value)
            raise serializers.ValidationError(
                "A user with this email already exists"
            )
        except User.DoesNotExist:
            pass

        return value

    class Meta:
        model = User
        fields = (
            'email', 'password', 'first_name', 'last_name', 'mobile',
            'avatar', 'profile', 'referral_code'
        )
        extra_kwargs = {'password': {'write_only': True}}


class RegisterFreelancerSerializer(serializers.ModelSerializer):

    profile = CreateFreelancerProfileSerializer(required=False)

    def __init__(self, *args, **kwargs):
        super(RegisterFreelancerSerializer, self).__init__(*args, **kwargs)
        # Find UniqueValidator and set custom message
        for validator in self.fields['email'].validators:
            if isinstance(validator, validators.UniqueValidator):
                validator.message = "A user with this email already exists"

    def create(self, validated_data):
        password = validated_data.pop('password')
        profile = validated_data.pop('profile', {})

        validated_data['user_type'] = User.FREELANCER
        user = User(**validated_data)
        user.set_password(password)
        user.is_active = True
        user.verified_mobile = True
        user.save()

        profile['user'] = user.pk

        context = self.context
        context['user'] = user
        serializer = CreateFreelancerProfileSerializer(
            data=profile,
            context=context
        )

        if not serializer.is_valid():
            raise serializers.ValidationError(
                'Error creating freelancer profile'
            )

        serializer.save()

        verifications = MobileVerification.objects.filter(
            user=None,
            mobile=user.mobile,
            status=Verification.CONFIRMED
        )
        verifications.update(user=user)

        add_feed_event(user, type=FeedEvent.EVENT_TYPE_COMPLETE_PROFILE)
        add_feed_event(user, type=FeedEvent.EVENT_TYPE_CONFIRM_EMAIL)

        return user

    def validate_email(self, value):
        try:
            User.objects.get(email=value)
            raise serializers.ValidationError(
                "A user with this email already exists"
            )
        except User.DoesNotExist:
            pass

        return value

    def validate_mobile(self, value):
        if not value:
            raise serializers.ValidationError('Mobile number is required')

        try:
            User.objects.get(mobile=value)
            raise serializers.ValidationError(
                "A user with this mobile already exists"
            )
        except User.DoesNotExist:
            pass

        return value

    class Meta:
        model = User
        fields = (
            'email', 'password', 'first_name', 'last_name', 'mobile',
            'avatar', 'profile'
        )
        extra_kwargs = {
            'password': {'write_only': True},
            'mobile': {'required': True}
        }


class LockedUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'initials')


class MyUserSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(MyUserSerializer, self).__init__(*args, **kwargs)
        # Find UniqueValidator and set custom message
        for validator in self.fields['email'].validators:
            if isinstance(validator, validators.UniqueValidator):
                validator.message = "A user with this email already exists"

    def validate_email(self, value):
        try:
            User.objects.get(email=value)
            raise serializers.ValidationError(
                "A user with this email already exists"
            )
        except User.DoesNotExist:
            pass
        return value

    def validate_mobile(self, value):
        # verifications = MobileVerification.objects.filter(
        #     user=self.instance,
        #     mobile=value,
        #     status=Verification.CONFIRMED
        # )
        #
        # if verifications.count() == 0:
        #     raise serializers.ValidationError("Mobile isn't verified")
        return value

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.mobile = validated_data.get('mobile', instance.mobile)
        instance.email = validated_data.get('email', instance.email)
        instance.avatar = validated_data.get('avatar', instance.avatar)

        if validated_data.get('password', None) is not None:
            instance.set_password(validated_data['password'])

        instance.save()
        return instance

    class Meta:
        model = User
        fields = (
            'id', 'first_name', 'last_name', 'avatar', 'is_active',
            'verified_mobile', 'verified_email', 'password', 'mobile',
            'email', 'user_type', 'initials'
        )
        extra_kwargs = {'password': {'write_only': True}}


class LoginUserSerializer(serializers.Serializer):

    token = serializers.CharField(required=False)
    user = MyUserSerializer()

    def to_representation(self, obj):
        response_dict = {'user': MyUserSerializer(obj['user']).data}

        if obj.get('token'):
            response_dict['token'] = obj['token']

        return response_dict


class MobileVerificationSerializer(serializers.ModelSerializer):

    def validate_status(self, value):
        if value == Verification.CONFIRMED and self.instance.status != value:
            return value
        raise serializers.ValidationError("Unable to change status")

    def update(self, instance, validated_data):
        instance.status = validated_data.get('status', instance.status)
        instance.save()
        return instance

    class Meta:
        model = MobileVerification
        fields = '__all__'
