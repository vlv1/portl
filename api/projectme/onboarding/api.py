import base64
import datetime

from django.conf import settings
from django.contrib.auth.forms import SetPasswordForm
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.core.files.base import ContentFile
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny

from projectme.core.models import Action
from projectme.notifications.utils import unregister_device
from projectme.onboarding.api_schemas import register_client_request_schema, \
    register_freelancer_request_schema, register_client_response_schema
from projectme.onboarding.models import MobileVerification, Verification, \
    User, PasswordResetRequest, EmailVerification
from projectme.onboarding.serializers import RegisterClientSerializer, \
    LoginUserSerializer, RegisterFreelancerSerializer, LoginSerializer, \
    MobileVerificationSerializer, LoginClientSerializer, \
    LoginFreelancerSerializer
from projectme.onboarding.token import get_token, delete_token
from projectme.utils import ErrorResponse
from projectme.feed.models import FeedEvent
from projectme.feed.utils import add_feed_event, set_read_confirm_email_event


class RegisterClientView(APIView):
    permission_classes = (AllowAny,)

    @swagger_auto_schema(
        request_body=register_client_request_schema,
        responses={
            status.HTTP_200_OK: register_client_response_schema
        }
    )
    def post(self, request):
        data = request.data

        avatar = data.pop('avatar', None)

        if avatar is not None:
            format, imgstr = avatar.split(';base64,')
            ext = format.split('/')[-1]

            data['avatar'] = ContentFile(
                base64.b64decode(imgstr),
                name='temp.' + ext
            )

        serializer = RegisterClientSerializer(
            data=data,
            context={'request': request}
        )

        if not serializer.is_valid():
            return ErrorResponse.build_serializer_error(
                serializer,
                status.HTTP_400_BAD_REQUEST
            )

        user = serializer.save()

        # track event
        Action.track(user=user, verb='registered')

        # generate response
        data = {'user': user}

        if user.verified_email:
            token = get_token(user)
            data['token'] = token.key

        serializer = LoginUserSerializer(data)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class RegisterFreelancerView(APIView):
    permission_classes = (AllowAny, )

    @swagger_auto_schema(
        request_body=register_freelancer_request_schema,
        responses={
            status.HTTP_200_OK: LoginUserSerializer
        }
    )
    def post(self, request):
        data = request.data

        serializer = RegisterFreelancerSerializer(
            data=data,
            context={'request': request}
        )

        if not serializer.is_valid():
            return ErrorResponse.build_serializer_error(
                serializer,
                status.HTTP_400_BAD_REQUEST
            )

        user = serializer.save()

        # track event
        Action.track(user=user, verb='registered')

        # create new mobile verification
        MobileVerification.objects.create(
            user=user,
            mobile=user.mobile,
            first_name=user.first_name,
            last_name=user.last_name
        )

        #   event is saved into feed, but the push should not be sent
        add_feed_event(
            user,
            type=FeedEvent.EVENT_TYPE_COMPLETE_PROFILE,
            send_push=False
        )
        add_feed_event(
            user,
            type=FeedEvent.EVENT_TYPE_CONFIRM_EMAIL,
            send_push=False
        )

        # generate response
        data = {'user': user}
        serializer = LoginUserSerializer(data)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class LoginView(APIView):
    serializer_class = LoginSerializer
    permission_classes = (AllowAny,)

    @swagger_auto_schema(
        request_body=LoginSerializer,
        responses={
            status.HTTP_200_OK: LoginUserSerializer
        }
    )
    def post(self, request):
        data = request.data

        serializer = self.serializer_class(
            data=data,
            context={'request': request}
        )

        if not serializer.is_valid():
            return ErrorResponse.build_serializer_error(
                serializer,
                status.HTTP_400_BAD_REQUEST
            )

        user = serializer.instance
        user.save()

        token = get_token(user)

        # Return login user
        data = {"token": token.key, "user": user}

        serializer = LoginUserSerializer(data)
        return Response(serializer.data)


class LoginClientView(LoginView):
    serializer_class = LoginClientSerializer
    permission_classes = (AllowAny, )


class LoginFreelancerView(LoginView):
    serializer_class = LoginFreelancerSerializer
    permission_classes = (AllowAny, )


class LogoutView(APIView):

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='token',
                in_=openapi.IN_FORM,
                type=openapi.TYPE_STRING,
                description="Device token",
                required=True
            ),
        ],
        responses={
            status.HTTP_200_OK: openapi.Response(
                description="no data"
            )
        }
    )
    def post(self, request):
        user = request.user

        # delete api token & unregister device
        delete_token(user)
        unregister_device(request.user, request.data.get('token', None))
        return Response({}, status=status.HTTP_200_OK)


class MobileVerifyView(APIView):
    permission_classes = (AllowAny, )

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            required=['mobile', 'verification_code'],
            properties={
                'mobile': openapi.Schema(
                    type=openapi.TYPE_STRING,
                ),
                'verification_code': openapi.Schema(
                    type=openapi.TYPE_STRING,
                    minLength=6, maxLength=6,
                )
            }
        ),
        responses={
            status.HTTP_202_ACCEPTED: openapi.Response(
                description='',
                schema=openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties={}
                ),
            )
        }
    )
    def post(self, request):
        data = request.data

        verification_code = data.get('verification_code', None)
        mobile = data.get('mobile', None)

        if verification_code is None:
            return ErrorResponse.build_text_error(
                "Verification code not specified",
                status.HTTP_400_BAD_REQUEST
            )

        if mobile is None:
            return ErrorResponse.build_text_error(
                "Mobile number not specified",
                status.HTTP_400_BAD_REQUEST
            )

        if settings.IMITATE_MOBILE_VERIFICATION:
            verification = get_object_or_404(
                MobileVerification,
                mobile=mobile,
                status=Verification.PENDING,
                created_at__gte=timezone.now() - datetime.timedelta(
                    minutes=settings.SMS_VERIFICATION_CODE_TTL
                )
            )
        else:
            verification = get_object_or_404(
                MobileVerification,
                mobile=mobile,
                verification_code=verification_code,
                status=Verification.PENDING,
                created_at__gte=timezone.now() - datetime.timedelta(
                    minutes=settings.SMS_VERIFICATION_CODE_TTL
                )
            )

        data = {'status': Verification.CONFIRMED}
        serializer = MobileVerificationSerializer(
            verification,
            data=data,
            partial=True,
            context={"request": request}
        )
        if not serializer.is_valid():
            return ErrorResponse.build_serializer_error(
                serializer, status.HTTP_400_BAD_REQUEST
            )

        serializer.save()

        # generate response
        user = get_object_or_404(
            User,
            mobile=mobile,
        )
        token = get_token(user)
        data = {'token': token.key, 'user': user}
        serializer = LoginUserSerializer(data)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            required=['mobile'],
            properties={
                'mobile': openapi.Schema(
                    type=openapi.TYPE_STRING,
                    description='Mobile number',
                )
            }
        ),
        responses={
            status.HTTP_201_CREATED: openapi.Response(
                description='',
                schema=openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties={
                        'token': openapi.Schema(
                            type=openapi.TYPE_STRING,
                            minLength=40, maxLength=40
                        ),
                        # TODO: Fill it!
                        'user': openapi.Schema(
                            type=openapi.TYPE_OBJECT,
                            properties={},
                        ),
                    }
                ),
            )
        }
    )
    def patch(self, request):
        data = request.data
        mobile = data.get('mobile', None)

        if not mobile:
            return ErrorResponse.build_text_error(
                "Mobile number not specified",
                status.HTTP_400_BAD_REQUEST
            )

        try:
            user = User.objects.get(mobile=mobile)
            data['user'] = user
        except User.DoesNotExist:
            return ErrorResponse.build_text_error(
                'User with specified mobile not found',
                status.HTTP_400_BAD_REQUEST
            )

        MobileVerification.objects.create(**data)

        return Response({}, status=status.HTTP_201_CREATED)


class ResetPasswordView(APIView):
    permission_classes = (AllowAny,)

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='email',
                in_=openapi.IN_FORM,
                type=openapi.TYPE_STRING,
                description="Email address",
                required=True
            ),
        ],
        responses={
            status.HTTP_201_CREATED: 'None',
            status.HTTP_400_BAD_REQUEST: 'Email not supplied'
        }
    )
    def post(self, request):
        data = request.data

        email = data.get('email', None)

        if email is not None:
            user = get_object_or_404(User, email=email)
            requests = PasswordResetRequest.objects.filter(
                user=user,
                status__lt=PasswordResetRequest.RESET
            )
            requests.update(status=PasswordResetRequest.CANCELLED)
            PasswordResetRequest.objects.create(
                user=user,
                expires=timezone.now() + datetime.timedelta(days=1)
            )
            return Response(status=status.HTTP_201_CREATED)

        return ErrorResponse.build_text_error(
            "Email not supplied",
            status.HTTP_400_BAD_REQUEST
        )


def ChangePasswordView(request, token):
    # If we have arrived from password submission
    if request.method == 'POST':
        data = request.POST
        forgotten_password = get_object_or_404(
            PasswordResetRequest,
            hash=token,
            status=PasswordResetRequest.PENDING
        )
        form = SetPasswordForm(data=data, user=forgotten_password.user)
        if form.is_valid():
            forgotten_password.status = PasswordResetRequest.RESET
            forgotten_password.save()
            form.save()
            return render(
                request,
                'change_password.html',
                {'form': form, "token": token, "success": True}
            )
        else:
            return render(
                request,
                'change_password.html',
                {'form': form, "token": token}
            )
    else:
        # If arrived via GET then make sure token is valid
        # before displaying form
        try:
            forgotten_password = PasswordResetRequest.objects.get(
                hash=token,
                status=PasswordResetRequest.PENDING,
                expires__gt=timezone.now()
            )
        except PasswordResetRequest.DoesNotExist:
            return render(request, 'expired.html')

        form = SetPasswordForm(user=forgotten_password.user)
        return render(
            request,
            'change_password.html',
            {'form': form, "token": token}
        )


def EmailVerifyView(request, verification_code):
    # Gets email verification or returns error
    try:
        email_verification = EmailVerification.objects.get(
            verification_code=verification_code,
            status__gte=Verification.PENDING
        )
    except EmailVerification.DoesNotExist:
        return render(request, 'expired_verification.html')

    if email_verification.status == Verification.PENDING:
        # Updates the status
        email_verification.status = Verification.CONFIRMED
        email_verification.save()
        set_read_confirm_email_event(request.user)

    # Return the html file
    return render(request, 'verify_email.html')


class ResendVerificationEmailView(APIView):

    @swagger_auto_schema(
        responses={
            status.HTTP_202_ACCEPTED:
                'Success, now check your email to verify account',
        }
    )
    def patch(self, request):
        email_verification = EmailVerification.objects.create(
            user=request.user,
            email=request.user.email
        )
        email_verification.send_code()
        return Response(
            {"message": "Success, now check your email to verify account"},
            status=status.HTTP_202_ACCEPTED
        )
