# third party
from drf_yasg import openapi


register_freelancer_request_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    required=['email', 'password', 'first_name', 'last_name', 'mobile'],
    properties={
        'email': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=254,
            description='Must be unique'
        ),
        'first_name': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        ),
        'last_name': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        ),
        'password': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=128
        ),
        'mobile': openapi.Schema(
            type=openapi.TYPE_STRING,
        )
    }
)


register_client_request_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    required=['email', 'password', 'first_name', 'last_name'],
    properties={
        'referral_code': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=40,
        ),
        'email': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=254
        ),
        'first_name': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        ),
        'last_name': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=255
        ),
        'password': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=128
        ),
        'mobile': openapi.Schema(
            type=openapi.TYPE_STRING,
            minLength=1, maxLength=128
        ),
        'avatar': openapi.Schema(
            type=openapi.TYPE_STRING,
            description='Base64 encoded image. Format: '
                        'data:image/jpeg;base64,/<image base64>'
        ),
        'profile': openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'company': openapi.Schema(
                    type=openapi.TYPE_STRING,
                    minLength=1, maxLength=255
                ),
                'job_title': openapi.Schema(
                    type=openapi.TYPE_STRING,
                    minLength=1, maxLength=255
                )
            }

        ),
    }
)


register_client_response_schema = openapi.Response(
    schema=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'token': openapi.Schema(
                type=openapi.TYPE_STRING,
                minLength=1, maxLength=254
            ),
            'user': openapi.Schema(
                type=openapi.TYPE_OBJECT,
                properties={
                    'id': openapi.Schema(
                        type=openapi.TYPE_INTEGER,
                    ),
                    'email': openapi.Schema(
                        type=openapi.TYPE_STRING,
                        minLength=1, maxLength=254
                    ),
                    'first_name': openapi.Schema(
                        type=openapi.TYPE_STRING,
                        minLength=1, maxLength=255
                    ),
                    'last_name': openapi.Schema(
                        type=openapi.TYPE_STRING,
                        minLength=1, maxLength=255
                    ),
                    'avatar': openapi.Schema(
                        type=openapi.TYPE_STRING,
                        description='User\'s avatar url'
                    ),
                    'is_active': openapi.Schema(
                        type=openapi.TYPE_BOOLEAN,
                        description='Designates whether '
                                    'this user should be treated '
                                    'as active. Unselect this '
                                    'instead of deleting accounts'
                    ),
                    'verified_mobile': openapi.Schema(
                        type=openapi.TYPE_BOOLEAN
                    ),
                    'verified_email': openapi.Schema(
                        type=openapi.TYPE_BOOLEAN
                    ),
                    'mobile': openapi.Schema(
                        type=openapi.TYPE_STRING,
                        minLength=1, maxLength=128
                    ),
                    'user_type': openapi.Schema(
                        type=openapi.TYPE_INTEGER,
                        enum=[-1, 0, 1],
                        description="""
                                    Possible values:
                                    -1 - N/A
                                     0 - Client
                                     1 - Freelancer
                                    """
                    ),
                    'initials': openapi.Schema(
                        type=openapi.TYPE_STRING,
                        minLength=2, maxLength=2
                    ),
                }
            ),
        }
    ),
    description=''
)