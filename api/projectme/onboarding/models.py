import os
import random
import binascii

from django.conf import settings
from django.contrib.gis.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.sites.models import Site
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from imagekit.models import ProcessedImageField

from phonenumber_field.modelfields import PhoneNumberField
from pilkit.processors import Adjust
from scarface.models import Device

from projectme.models import TimestampedModel
from projectme.onboarding.managers import UserManager
from projectme.onboarding.notifications import mobile_verification, \
    verify_email, reset_password
from projectme.onboarding.token import delete_token, get_token
from projectme.onboarding.utils import generate_referral_code
from projectme.profile.models import ClientProfile
from projectme.utils import random_file_name


class User(TimestampedModel, AbstractBaseUser, PermissionsMixin):

    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField(unique=True)
    mobile = PhoneNumberField(blank=True)

    NOT_APPLICABLE = -1
    CLIENT = 0
    FREELANCER = 1

    USER_TYPES = (
        (NOT_APPLICABLE, 'N/A'),
        (CLIENT, 'Client'),
        (FREELANCER, 'Freelancer')
    )

    user_type = models.IntegerField(choices=USER_TYPES, default=NOT_APPLICABLE)

    avatar = ProcessedImageField(
        blank=True, null=True,
        upload_to=random_file_name,
        processors=[Adjust(color=0)]
    )
    verified_mobile = models.BooleanField(default=False)
    verified_email = models.BooleanField(default=False)

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.')
    )
    is_active = models.BooleanField(
        _('active'),
        default=False,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.')
    )

    devices = models.ManyToManyField(Device, related_name="users", blank=True)
    show_while_away = models.BooleanField(default=False)
    previous_last_login = models.DateTimeField(blank=True, null=True)

    objects = UserManager()

    def get_full_name(self):
        return "{} {}".format(self.first_name, self.last_name)

    def get_short_name(self):
        return '{}'.format(self.first_name)

    def username(self):
        return self.email

    def initials(self):
        return "{}{}".format(self.first_name[:1], self.last_name[:1]).upper()

    def __str__(self):
        return "{} - {}".format(self.get_full_name(), self.email)

    def save(self, *args, **kwargs):
        email_verified = EmailVerification.objects.filter(
            user=self,
            email=self.email,
            status__gte=Verification.CONFIRMED
        ).exists()

        if not email_verified:
            self.verified_email = False

        if self.pk is None:
            self.last_login = timezone.now()
            self.previous_last_login = timezone.now()

        super(User, self).save(*args, **kwargs)

        if not email_verified:
            email_verification = EmailVerification.objects.create(
                user=self,
                email=self.email
            )
            email_verification.send_code()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', ]


def random_code():
    return random.randint(100000, 999999)


class Verification(TimestampedModel):
    user = models.ForeignKey(
        'User',
        blank=True, null=True,
        on_delete=models.CASCADE
    )

    CANCELLED = -1
    PENDING = 0
    CONFIRMED = 1

    STATUSES = (
        (CANCELLED, 'Cancelled'),
        (PENDING, 'Pending'),
        (CONFIRMED, 'Confirmed')
    )

    status = models.IntegerField(choices=STATUSES, default=PENDING)


class MobileVerification(Verification):
    mobile = PhoneNumberField()
    first_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255, blank=True)
    verification_code = models.CharField(
        max_length=6,
        default=random_code,
        unique=True
    )

    def save(self, *args, **kwargs):
        if self.pk is None:
            # Update existing verifications to be cancelled
            # as being replaced with a new one
            verifications = MobileVerification.objects.filter(
                user=self.user,
                mobile=self.mobile,
                status__lt=Verification.CONFIRMED
            )
            verifications.update(status=Verification.CANCELLED)

            mobile_verification(self)

            if self.user:
                # Resets the verified check on a user if the numbers match
                if self.status == Verification.PENDING \
                        and self.user.mobile == self.mobile:
                    self.user.verified_mobile = False
                    self.user.save()

        if self.user:
            # Updates the verified check if status is confirmed
            # and the numbers match
            if self.status == Verification.CONFIRMED and \
                    self.user.mobile == self.mobile:
                self.user.verified_mobile = True
                self.user.save()

        super(MobileVerification, self).save(*args, **kwargs)


def createHash():
    return binascii.hexlify(os.urandom(30))


class EmailVerification(Verification):
    verification_code = models.CharField(
        default=createHash,
        max_length=90,
        unique=True
    )
    email = models.EmailField()

    def __str__(self):
        return "{}".format(self.user)

    def save(self, *args, **kwargs):
        if self.pk is None:
            # Update existing verifications to be cancelled
            # as being replaced with a new one
            verifications = EmailVerification.objects.filter(
                user=self.user,
                status__lt=Verification.CONFIRMED
            )
            verifications.update(status=Verification.CANCELLED)

        super(EmailVerification, self).save(*args, **kwargs)

        if self.status == Verification.PENDING:
            verify_email(self)

        if self.user:
            # Updates the verified check if status is confirmed
            # and the numbers match
            if self.user.verified_email is False \
                    and self.status == Verification.CONFIRMED \
                    and self.user.email == self.email:
                self.user.verified_email = True
                self.user.save()

    def send_code(self):
        pass


class PasswordResetRequest(TimestampedModel):
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    hash = models.CharField(max_length=90, default=createHash, unique=True)

    CANCELLED = -1
    PENDING = 0
    RESET = 1

    STATUSES = (
        (CANCELLED, 'Cancelled'),
        (PENDING, 'Pending'),
        (RESET, 'Reset')
    )

    status = models.IntegerField(choices=STATUSES, default=PENDING)

    expires = models.DateTimeField()

    def save(self, *args, **kwargs):
        if self.pk is None:
            reset_password(self)

            # delete token
            delete_token(self.user)

        if self.status == PasswordResetRequest.RESET:
            # If the token has been removed as above, it'll create
            get_token(self.user)

        super(PasswordResetRequest, self).save(*args, **kwargs)


class ReferralCode(TimestampedModel):
    code = models.CharField(verbose_name='code', max_length=40, unique=True)
    referrer = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='referral_codes',
    )
    referred = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name='referral_code',
        blank=True, null=True
    )
    expired_at = models.DateTimeField(
        verbose_name='expired at',
        null=True, blank=True
    )

    def __str__(self):
        return 'Referrer: {}. Referral code: {}'.format(
            self.referrer, self.code
        )

    def save(self, *args, **kwargs):
        if not self.code:
            self.code = generate_referral_code()
        return super().save(*args, **kwargs)

    @property
    def url(self):
        path = reverse('index')
        domain = Site.objects.get_current().domain
        protocol = 'https' if settings.REFERRALS_SECURE_URLS else 'http'
        return '{}://{}{}registration/{}/'.format(
            protocol, domain, path, self.code
        )

    def process(self, referred_user):
        """
        :param referred_user: User registered using self.code
        :return: mark code as expired and add credits to the referrer user
        """
        self.expired_at = timezone.now()
        self.referred = referred_user
        self.save()

        referrer_client_profile = ClientProfile.objects.get(user=self.referrer)
        referrer_client_profile.add_credits(settings.REFERRER_CREDITS_BONUS)

        referred_client_profile = ClientProfile.objects.get(user=self.referred)
        referred_client_profile.add_credits(settings.REFERRED_CREDITS_BONUS)
