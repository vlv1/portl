from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _

from projectme.onboarding.models import User, MobileVerification, \
    EmailVerification, PasswordResetRequest


class VerificationAdmin(admin.ModelAdmin):
    list_display = ['user','verification_code','status']


class PasswordResetRequestAdmin(admin.ModelAdmin):
    list_display = ['user','hash','status']


class UserAdmin(BaseUserAdmin):
    search_fields = ('email', 'first_name', 'last_name')
    list_display = (
        'email', 'first_name', 'last_name', 'user_type', 'is_staff',
        'is_superuser'
    )
    list_filter = ('user_type', )
    ordering = ('id', )

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'avatar', 'devices', 'mobile')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'previous_last_login', 'show_while_away')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide', 'mobile'),
            'fields': ('email', 'password1', 'password2'),
        }),
    )


admin.site.register(User, UserAdmin)
admin.site.register(MobileVerification, VerificationAdmin)
admin.site.register(EmailVerification, VerificationAdmin)
admin.site.register(PasswordResetRequest, PasswordResetRequestAdmin)
