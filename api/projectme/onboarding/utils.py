# system
import random


def generate_referral_code():
    """
    Generate unique code for the referral link
    """
    from projectme.onboarding.models import ReferralCode

    def _generate_code():
        t = 'abcdefghijkmnopqrstuvwwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ1234567890'
        return "".join([random.choice(t) for i in range(40)])

    code = _generate_code()

    while ReferralCode.objects.filter(code=code).exists():
        code = _generate_code()

    return code
