from django.urls import re_path

from projectme.onboarding.api import RegisterClientView, \
    RegisterFreelancerView, LoginView, MobileVerifyView, \
    ResendVerificationEmailView, ResetPasswordView, EmailVerifyView, LogoutView, \
    LoginClientView, LoginFreelancerView

urlpatterns = [
    re_path(r'^client/$', RegisterClientView.as_view(), name='client'),
    re_path(
        r'^freelancer/$',
        RegisterFreelancerView.as_view(),
        name='freelancer'
    ),

    re_path(
        r'^login/$',
        LoginView.as_view(),
        name='login'
    ),
    re_path(
        r'^login/client/$',
        LoginClientView.as_view(),
        name='login-client'
    ),
    re_path(
        r'^login/freelancer/$',
        LoginFreelancerView.as_view(),
        name='login-freelancer'
    ),

    re_path(r'^logout/$', LogoutView.as_view(), name='logout'),

    # mobile verification
    re_path(r'^verify/$', MobileVerifyView.as_view(), name='verify'),

    # email verification
    re_path(
        r'^verify-email/$',
        ResendVerificationEmailView.as_view(),
        name='resend-verification'
    ),
    re_path(
        r'^verify-email/(?P<verification_code>[A-Za-z0-9]+)/$',
        EmailVerifyView,
        name='verify-email'
    ),
    re_path(
        r'^reset-password/$',
        ResetPasswordView.as_view(),
        name='reset-password'
    ),
]
