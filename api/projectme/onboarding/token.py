from django.utils import timezone

from rest_framework.authtoken.models import Token


def delete_token(user):
    try:
        Token.objects.get(user=user).delete()
    except Token.DoesNotExist:
        pass


def regenerate_token(user):
    delete_token(user)
    token, created = Token.objects.get_or_create(user=user)
    return token


def get_token(user):
    token, created = Token.objects.get_or_create(user=user)

    user.previous_last_login = user.last_login
    user.last_login = timezone.now()
    user.show_while_away = user.last_login > user.previous_last_login
    user.save()
    return token
