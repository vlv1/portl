# django
from django.utils import timezone
from django.contrib.auth.models import UserManager as UM
from django.utils.translation import ugettext_lazy as _


class UserManager(UM):
    def _create_user(self, first_name, last_name, email, password,
                     is_staff, is_superuser, **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError(_('The given email must be set'))

        email = self.normalize_email(email)
        user = self.model(
            first_name=first_name, last_name=last_name, email=email,
            is_staff=is_staff, is_superuser=is_superuser, is_active=True,
            last_login=now, **extra_fields
        )
        if password is not None:
            user.set_password(password)

        user.save(using=self._db)
        return user

    def create_user(self, first_name, last_name, email=None, password=None,
                    **extra_fields):
        return self._create_user(
            first_name, last_name, email, password, False, False,
            **extra_fields
        )

    def create_superuser(self, first_name, last_name, email, password,
                         **extra_fields):
        user = self._create_user(
            first_name, last_name, email, password, True, True,
            **extra_fields
        )
        user.is_active = True
        user.save(using=self._db)
        return user

    def get_active(self):
        return self.get_queryset().filter(
            profile__isnull=False,
            is_active=True
        )
