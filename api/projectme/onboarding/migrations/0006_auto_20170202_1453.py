# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-02-02 14:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('onboarding', '0005_auto_20161222_2157'),
    ]

    operations = [
        migrations.AddField(
            model_name='verification',
            name='first_name',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='verification',
            name='last_name',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
