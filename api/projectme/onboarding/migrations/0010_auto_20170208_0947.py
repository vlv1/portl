# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-02-08 09:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('onboarding', '0009_auto_20170208_0945'),
    ]

    operations = [
        migrations.AlterField(
            model_name='passwordresetrequest',
            name='expires',
            field=models.DateTimeField(),
        ),
    ]
