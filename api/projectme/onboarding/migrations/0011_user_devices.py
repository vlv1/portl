# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-02-27 18:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scarface', '0004_auto_20151217_1131'),
        ('onboarding', '0010_auto_20170208_0947'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='devices',
            field=models.ManyToManyField(blank=True, to='scarface.Device'),
        ),
    ]
