# system
import datetime
from unittest import skip
from unittest.mock import patch

# django
from django.utils import timezone
from django.test import TestCase
from django.urls import reverse
from django.conf import settings

# third party
from rest_framework import status
from rest_framework.test import APITestCase

# portl
from projectme.core.models import Action
from projectme.onboarding.models import MobileVerification, Verification, \
    User, ReferralCode, EmailVerification
from projectme.profile.models import ClientProfile, Company, JobTitle, \
    FreelancerProfile


class OnboardingTests(APITestCase):

    def get_url(self, name, args={}, kwargs={}):
        return reverse(
            'v1:onboarding:{}'.format(name),
            args=args,
            kwargs=kwargs
        )

    def setUp(self):
        pass

    def _create_client(self):
        client_user = User.objects.create_user(
            first_name='Johnny',
            last_name='Lee',
            email='johnny.lee@hedgehoglab.com',
            password='password123'
        )
        client_profile = ClientProfile.objects.create(
            user=client_user,
            company=Company.objects.create(company_name='hedgehog lab'),
            job_title=JobTitle.objects.create(title='Client Services Director')
        )
        return client_user, client_profile

    def _create_freelancer(self):
        freelancer_user = User.objects.create_user(
            first_name='Michael',
            last_name='Hutchinson',
            email='michael.hutchinson@hedgehoglab.com',
            mobile='+447710998870',
            password='password123'
        )
        freelancer_profile = FreelancerProfile.objects.create(user=freelancer_user)
        return freelancer_user, freelancer_profile

    def test_create_client(self):
        Action.objects.all().delete()

        MobileVerification.objects.create(
            mobile="+447710998870",
            status=Verification.CONFIRMED
        )
        url = self.get_url('client')
        data = {
            "first_name": "Johnny",
            "last_name": "Lee",
            "email": "johnny.lee@hedgehoglab.com",
            "mobile": "+447710998870",
            "password": "password123",
            "profile": {
                "company": "hedgehog lab",
                "job_title": "Client Services Director"
            }
        }
        self.assertEqual(Action.objects.count(), 0)
        response = self.client.post(url, data)

        # user is not logged in because email is not verified
        self.assertFalse('token' in response.json())
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertEqual(Action.objects.count(), 1)
        action = Action.objects.first()

        user = User.objects.get(email='johnny.lee@hedgehoglab.com')
        self.assertEqual(user.user_type, User.CLIENT)

        self.assertEqual(action.user, user)
        self.assertEqual(action.verb, 'registered')

        # create second client
        data = {
            "first_name": "Jane",
            "last_name": "Austen",
            "email": "jane.austen@example.com",
            "password": "password123"
        }
        self.assertEqual(Action.objects.count(), 1)
        response = self.client.post(url, data)

        # user is not logged in because email is not verified
        self.assertFalse('token' in response.json())
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertEqual(Action.objects.count(), 2)
        action = Action.objects.first()

        user = User.objects.get(email='jane.austen@example.com')
        self.assertEqual(user.user_type, User.CLIENT)

        self.assertEqual(action.user, user)
        self.assertEqual(action.verb, 'registered')

    def test_create_client_with_referrer_code(self):

        self.assertEqual(User.objects.count(), 0)
        MobileVerification.objects.create(
            mobile="+447710998870",
            status=Verification.CONFIRMED
        )
        url = self.get_url('client')
        data = {
            "first_name": "Johnny",
            "last_name": "Lee",
            "email": "johnny.lee@hedgehoglab.com",
            "mobile": "+447710998870",
            "password": "password123",
            "profile": {
                "company": "hedgehog lab",
                "job_title": "Client Services Director"
            }
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)

        client_user = User.objects.first()
        client_user_profile = ClientProfile.objects.get(user=client_user)
        self.assertEqual(client_user_profile.credits, 0)

        # create another client using referral code
        referral_code = ReferralCode.objects.create(referrer=client_user)
        data = {
            "first_name": "John",
            "last_name": "Doe",
            "email": "john.doe@example.com",
            "mobile": "+447710998870",
            "password": "password456",
            "profile": {
                "company": "company",
                "job_title": "Client Services Director"
            },
            "referral_code": referral_code.code

        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 2)

        new_client_user = User.objects.last()

        referral_code.refresh_from_db()
        client_user_profile.refresh_from_db()

        self.assertEqual(referral_code.referrer, client_user)
        self.assertEqual(referral_code.referred, new_client_user)
        self.assertIsNotNone(referral_code.expired_at)
        self.assertEqual(
            client_user_profile.credits,
            settings.REFERRER_CREDITS_BONUS
        )

        new_client_user_profile = ClientProfile.objects.get(
            user=new_client_user
        )
        self.assertEqual(
            new_client_user_profile.credits,
            settings.REFERRED_CREDITS_BONUS
        )

    @patch(
        'projectme.notifications.models.EmailNotificationConfiguration.send'
    )
    @patch('projectme.onboarding.serializers.add_feed_event')
    def test_create_freelancer(self, add_feed_event_mock, send_email_mock):
        Action.objects.all().delete()
        User.objects.all().delete()

        verification = MobileVerification.objects.create(
            mobile='+447710998870',
            status=Verification.CONFIRMED
        )

        url = self.get_url('freelancer')
        data = {
            "first_name": "Michael",
            "last_name": "Hutchinson",
            "email": "michael.hutchinson@hedgehoglab.com",
            "password": "password123",
            "mobile": "+447710998870"
        }
        self.assertEqual(Action.objects.count(), 0)
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # old verification has not been cancelled as it is confirmed
        verification.refresh_from_db()
        self.assertEqual(verification.status, Verification.CONFIRMED)

        self.assertEqual(Action.objects.count(), 1)
        action = Action.objects.first()
        user = User.objects.get(email='michael.hutchinson@hedgehoglab.com')
        self.assertEqual(user.user_type, User.FREELANCER)

        self.assertEqual(action.user, user)
        self.assertEqual(action.verb, 'registered')

        # create second freelancer
        data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'john.doe@example.com',
            'mobile': '+447965433770',
            'password': 'password12345'
        }
        self.assertEqual(Action.objects.count(), 1)
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertEqual(Action.objects.count(), 2)
        action = Action.objects.first()

        user = User.objects.get(email='john.doe@example.com')
        self.assertEqual(user.user_type, User.FREELANCER)

        self.assertEqual(user.mobile.as_e164, '+447965433770')

        self.assertEqual(action.user, user)
        self.assertEqual(action.verb, 'registered')

        # try to create freelancer without mobile or with duplicate mobile
        data = {
            "first_name": "Michael",
            "last_name": "Hutchinson",
            "email": "michael.hutchinson1@hedgehoglab.com",
            "password": "password123"
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                'status': 'error',
                'errors': {'mobile': ['This field is required.']}
            }
        )

        data = {
            "first_name": "Michael",
            "last_name": "Hutchinson",
            "email": "michael.hutchinson2@hedgehoglab.com",
            "password": "password123",
            "mobile": "+447710998870"
        }
        response = self.client.post(url, data)
        self.assertEqual(
            response.json(),
            {
                'status': 'error',
                'errors': {'mobile': ['A user with this mobile already exists']}
            }
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @patch('projectme.onboarding.serializers.add_feed_event')
    def test_login_client(self, add_feed_event_mock):
        client_user, client_profile = self._create_client()
        freelancer_user, freelancer_profile = self._create_freelancer()

        url = self.get_url('login-client')

        # try to login as client (with unverified email)
        data = {
            "email": "johnny.lee@hedgehoglab.com",
            "password": "password123"
        }

        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                'status': 'error',
                'errors': {
                    'non_field_errors': [
                        'Please confirm your email to login into Portl'
                    ]
                }
            }
        )

        # try to login as client (with verified email)
        user = User.objects.get(email='johnny.lee@hedgehoglab.com')
        EmailVerification.objects.create(
            user=user,
            verification_code='code',
            email='johnny.lee@hedgehoglab.com',
            status=Verification.CONFIRMED
        )
        user.verified_email = True
        user.save()

        data = {
            "email": "johnny.lee@hedgehoglab.com",
            "password": "password123"
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # try to login as freelancer (now allowed)
        data = {
            "email": "michael.hutchinson@hedgehoglab.com",
            "password": "password123"
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                'status': 'error',
                'errors': {
                    'non_field_errors': ['Only clients are allowed to login']
                }
            }
        )

    @patch('projectme.onboarding.serializers.add_feed_event')
    def test_login_freelancer(self, add_feed_event_mock):
        client_user, client_profile = self._create_client()
        freelancer_user, freelancer_profile = self._create_freelancer()

        url = self.get_url('login-freelancer')

        # try to login as freelancer
        data = {
            "email": "michael.hutchinson@hedgehoglab.com",
            "password": "password123"
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # try to login as client (not allowed)
        url = self.get_url('login-freelancer')
        data = {
            "email": "johnny.lee@hedgehoglab.com",
            "password": "password123"
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                'status': 'error',
                'errors': {
                    'non_field_errors': [
                        'Only freelancers are allowed to login'
                    ]
                }
            }
        )

    def test_verify_mobile(self):
        """
        Test POST request to /api/profile/verify/
        """
        freelancer_user, freelancer_profile = self._create_freelancer()

        url = self.get_url('verify')

        # verification is not expired
        verification = MobileVerification.objects.create(
            mobile='+447710998870'
        )
        data = {
            'mobile': '+447710998870',
            'verification_code': verification.verification_code
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

        # make verification - expired
        verification.created_at = timezone.now() - datetime.timedelta(
            minutes=settings.SMS_VERIFICATION_CODE_TTL + 10
        )
        verification.save()
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @skip
    @patch(
        'projectme.notifications.models.EmailNotificationConfiguration.send'
    )
    @patch('projectme.onboarding.serializers.add_feed_event')
    def test_resend_mobile_verification(self, add_feed_event_mock,
                                        send_email_mock):
        client_user, client_profile = self._create_client()
        freelancer_user, freelancer_profile = self._create_freelancer()
        verification = MobileVerification.objects.create(
            mobile='+447710998870'
        )

        url = self.get_url('verify')
        data = {'mobile': verification.mobile.as_e164}

        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        verification.refresh_from_db()
        self.assertEqual(verification.status, Verification.CANCELLED)

        new_verification = MobileVerification.objects.filter(
            mobile='+447710998870'
        ).last()
        self.assertEqual(new_verification.status, Verification.PENDING)

    def test_send_password_request(self):
        client_user, client_profile = self._create_client()

        url = self.get_url('reset-password')
        data = {"email": "johnny.lee@hedgehoglab.com"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class UserManagerTestCase(TestCase):

    def setUp(self):
        self.test_user = User.objects.create_user(
            email='test_user@example.com',
            first_name='test',
            last_name='user'
        )
        self.test_user_profile = ClientProfile.objects.create(
            user=self.test_user
        )

    def test_get_active(self):
        # test if user is active and has profile
        self.assertEqual(User.objects.get_active().count(), 1)
        self.assertEqual(User.objects.get_active().first(), self.test_user)

        # test if user is inactive and has profile
        self.test_user.is_active = False
        self.test_user.save()

        self.assertEqual(User.objects.get_active().count(), 0)

        # test if user is active and has no profile
        self.test_user.is_active = False
        self.test_user.save()

        self.test_user_profile.delete()

        self.assertEqual(User.objects.get_active().count(), 0)
