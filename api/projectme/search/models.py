from django.contrib.gis.db import models
from django.conf import settings

from projectme.models import TimestampedModel


class Search(TimestampedModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="searches",
        blank=True, null=True,
        on_delete=models.CASCADE
    )
    job_title = models.ForeignKey(
        'profile.JobTitle',
        related_name="searches",
        blank=True, null=True,
        on_delete=models.CASCADE
    )
    location = models.ForeignKey(
        'profile.Location',
        related_name="searches",
        blank=True, null=True,
        on_delete=models.CASCADE
    )
    distance = models.IntegerField(default=0)
    key_skills = models.ManyToManyField(
        'profile.KeySkill',
        related_name="searches",
        blank=True
    )
    sectors = models.ManyToManyField(
        'profile.Sector',
        related_name="searches",
        blank=True
    )
    qualifications = models.ManyToManyField(
        'profile.Qualification',
        related_name="searches",
        blank=True
    )
    languages = models.ManyToManyField(
        'profile.Language',
        related_name="searches",
        blank=True
    )
    availability = models.ForeignKey(
        'profile.Availability',
        related_name="searches",
        blank=True, null=True,
        on_delete=models.CASCADE
    )
    available_weekends = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)

    def __str__(self):
        return "{} - {}".format(self.pk, self.user)

    class Meta:
        verbose_name_plural = "searches"


class SearchMatch(TimestampedModel):
    search = models.ForeignKey(
        'Search',
        related_name="matches",
        on_delete=models.CASCADE
    )
    profile = models.ForeignKey(
        'profile.FreelancerProfile',
        related_name="matches",
        on_delete=models.CASCADE
    )
    percentage = models.FloatField(default=0)
    exact_percentage = models.FloatField(default=0)
    max_percentage = models.FloatField(default=0)
    job_title = models.FloatField(default=0)
    location = models.FloatField(default=0)
    key_skills = models.FloatField(default=0)
    sectors = models.FloatField(default=0)
    qualifications = models.FloatField(default=0)
    languages = models.FloatField(default=0)
    rate = models.FloatField(default=0)
    availability = models.FloatField(default=0)

    def __str__(self):
        return "{}".format(self.profile)

    class Meta:
        verbose_name_plural = "search matches"


class SearchPercentageSetting(TimestampedModel):
    JOB_TITLE = 0
    LOCATION = 1
    KEY_SKILLS = 2
    SECTORS = 3
    QUALIFICATIONS = 4
    LANGUAGES = 5
    RATE = 6
    AVAILABILITY = 7

    SETTINGS = (
        (JOB_TITLE, 'Job Title'),
        (LOCATION, 'Location'),
        (KEY_SKILLS, 'Key Skills'),
        (SECTORS, 'Sectors'),
        (QUALIFICATIONS, 'Qualifions'),
        (LANGUAGES, 'Languages'),
        (RATE, 'Rate'),
        (AVAILABILITY, 'Availability')
    )

    setting = models.IntegerField(choices=SETTINGS)
    percentage = models.FloatField()

    def __str__(self):
        return "{}".format(
            [s[1] for s in self.SETTINGS if s[0] == self.setting][0]
        )
