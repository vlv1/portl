# django
from django.urls import re_path

# portl
from projectme.search.api import SearchView, LocationsAutocompleteView


urlpatterns = [
    re_path(r'^$', SearchView.as_view(), name="start-search"),
    re_path(
        r'^autocomplete/location/$',
        LocationsAutocompleteView.as_view(),
        name="location-autocomplete"
    ),
]
