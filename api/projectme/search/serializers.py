# third party
from rest_framework import serializers


class SearchSerializer(serializers.Serializer):
    query = serializers.CharField(allow_blank=True)
    vetted = serializers.BooleanField(required=False)
    page = serializers.CharField(required=False)
    locations = serializers.CharField(required=False)
    key_skills = serializers.CharField(required=False)
    sectors = serializers.CharField(required=False)
    qualifications = serializers.CharField(required=False)
    daily_rate_from = serializers.DecimalField(
        max_digits=10, decimal_places=2,
        required=False
    )
    daily_rate_to = serializers.DecimalField(
        max_digits=10, decimal_places=2,
        required=False
    )
    available_from = serializers.DateField(required=False)
    available_to = serializers.DateField(required=False)

    class Meta:
        fields = (
            'query', 'locations', 'key_skills', 'sectors', 'qualifications',
            'daily_rate_from', 'daily_rate_to',
            'available_from', 'available_to'
        )
