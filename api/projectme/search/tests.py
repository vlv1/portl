# system
import unittest
import datetime

# django
from django.conf import settings
from django.test import override_settings
from django.urls import reverse
from django.utils import timezone

# third party
from rest_framework import status
from rest_framework.test import APITestCase

# portl
from projectme.onboarding.models import User
from projectme.profile.constants import RateType
from projectme.profile.models import FreelancerProfile, JobTitle, KeySkill, \
    Language, Qualification, Sector


class SearchTests(APITestCase):

    def setUp(self):
        self.today = timezone.now().date()
        self.two_days_before = self.today - datetime.timedelta(days=2)
        self.two_days_after = self.today + datetime.timedelta(days=2)
        self.four_days_after = self.today + datetime.timedelta(days=4)

        self.user_1 = User.objects.create_user(
            email='api_test_user_1@test.com',
            first_name='API user 1',
            last_name='test'
        )
        self.user_2 = User.objects.create_user(
            email='api_test_user_2@test.com',
            first_name='API user 2',
            last_name='test'
        )
        self.user_3 = User.objects.create_user(
            email='api_test_user_3@test.com',
            first_name='API user 3',
            last_name='test'
        )
        self.profile_1 = FreelancerProfile.objects.create(user=self.user_1)
        self.profile_2 = FreelancerProfile.objects.create(user=self.user_2)
        self.profile_3 = FreelancerProfile.objects.create(user=self.user_3)

        # profile 3 is unavailable from two days before till two days after
        self.profile_3.unavailable_dates = [
            str(self.two_days_before),
            str(self.two_days_before + datetime.timedelta(days=1)),
            str(self.today),  # today
            str(self.today + datetime.timedelta(days=1)),
            str(self.two_days_after),
        ]
        self.profile_3.save()

        # create job titles
        self.jt_1 = JobTitle.objects.create(title='Lean Champion')
        self.jt_2 = JobTitle.objects.create(title='Business Consultant')
        self.jt_3 = JobTitle.objects.create(title='Process Architect')

        # create key skills
        self.ks_1 = KeySkill.objects.create(title='Lean Office')
        self.ks_2 = KeySkill.objects.create(title='Process Design')
        self.ks_3 = KeySkill.objects.create(title='Business Case Development')

        # Languages
        self.l_1 = Language.objects.create(title='Chechen')
        self.l_2 = Language.objects.create(title='English')
        self.l_3 = Language.objects.create(title='Danish')

        # Qualifications
        self.q_1 = Qualification.objects.create(
            title='Lean Six Sigma Black Belt Certified'
        )

        # Sectors
        self.s_1 = Sector.objects.create(title='Accounting')

        # profile 1
        # Job title: Lean Champion
        # Key skills: Process Design
        # Languages: English, Danish
        # Qualifications: Lean Six Sigma Black Belt Certified
        # Sectors: Accounting
        self.profile_1.job_title = self.jt_1
        self.profile_1.key_skills.add(self.ks_2)
        self.profile_1.languages.add(self.l_2, self.l_3)
        self.profile_1.qualifications.add(self.q_1)
        self.profile_1.sectors.add(self.s_1)
        self.profile_1.save()
        self.profile_1.indexing()

        # profile 2
        # Job title: Process Architect
        # Key skills: Business Case Development
        # Languages: Chechen, Danish
        # Qualifications: None
        # Sectors: Accounting
        self.profile_2.job_title = self.jt_2
        self.profile_2.key_skills.add(self.ks_3)
        self.profile_2.languages.add(self.l_2, self.l_3)
        self.profile_2.sectors.add(self.s_1)
        self.profile_2.save()
        self.profile_2.indexing()

        # profile 3
        # Job title: Business Consultant
        # Key skills: Lean Office
        # Languages: Danish
        # Qualifications: Lean Six Sigma Black Belt Certified
        # Sectors: None
        self.profile_3.job_title = self.jt_3
        self.profile_3.key_skills.add(self.ks_1)
        self.profile_3.languages.add(self.l_3)
        self.profile_3.qualifications.add(self.q_1)
        self.profile_3.save()
        self.profile_3.indexing()

    def get_url(self, name, args={}, kwargs={}):
        return reverse('v1:search:{}'.format(name), args=args, kwargs=kwargs)

    @unittest.skip
    def test_search(self):
        url = self.get_url('start-search')
        data = {"query": "lean"}

        response = self.client.get(url, data)

        # only first and third freelancers match search criteria
        freelancer_ids = [f['id'] for f in response.json()['freelancers']]
        self.assertListEqual(
            sorted(freelancer_ids),
            sorted([self.profile_1.id, self.profile_3.id])
        )
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

    @unittest.skip
    @override_settings(SEARCH_PAGE_SIZE=2)
    def test_search_pagination(self):
        url = self.get_url('start-search')
        data = {'query': ''}

        response = self.client.get(url, data)

        # only first and third freelancers match search criteria
        freelancer_ids = [f['id'] for f in response.json()['freelancers']]
        self.assertListEqual(
            sorted(freelancer_ids),
            sorted([self.profile_1.id, self.profile_2.id, self.profile_3.id])
        )
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

    @unittest.skip
    def test_search_filter_rate(self):
        url = self.get_url('start-search')

        # set rates
        self.profile_1.rate_minimum = 96   # 12 * 8 = 96
        self.profile_1.rate_maximum = 128  # 16 * 8 = 128
        self.profile_1.rate_type = RateType.DAILY
        self.profile_1.save()
        self.profile_1.indexing()

        self.profile_2.rate_minimum = 10  # 10 * 8 = 80
        self.profile_2.rate_maximum = 11  # 11 * 8 = 88
        self.profile_2.rate_type = RateType.HOURLY
        self.profile_2.save()
        self.profile_2.indexing()

        self.profile_3.rate_minimum = 5  # 5 * 8 = 40
        self.profile_3.rate_maximum = 8  # 8 * 8 = 64
        self.profile_3.rate_type = RateType.DAILY
        self.profile_3.save()
        self.profile_3.indexing()

        data = {
            'query': '',
            'daily_rate_from': 50
        }
        response = self.client.get(url, data)

        # 1st and 2nd freelancers match search criteria
        freelancer_ids = [f['id'] for f in response.json()['freelancers']]
        self.assertListEqual(
            sorted(freelancer_ids),
            sorted([self.profile_1.id, self.profile_2.id])
        )
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

    @unittest.skip
    def test_search_filter_availability(self):
        url = self.get_url('start-search')

        # add availability date range to the filters
        available_from = self.today.strftime('%Y-%m-%d')
        available_to = self.four_days_after.strftime('%Y-%m-%d')

        data = {
            'query': 'lean',
            'available_from': available_from,
            'available_to': available_to
        }

        response = self.client.get(url, data)

        # only first freelancer match search criteria
        # (third one is unavailable)
        freelancer_ids = [f['id'] for f in response.json()['freelancers']]
        self.assertListEqual(
            sorted(freelancer_ids),
            sorted([self.profile_1.id])
        )
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

    @unittest.skip
    def test_search_filter_key_skills(self):
        url = self.get_url('start-search')

        # add key skills to the filters (Lean Office, Business Case Development)
        data = {
            'query': '',
            'key_skills': ','.join([self.ks_1.title, self.ks_3.title])
        }

        response = self.client.get(url, data)

        # only third freelancer match search criteria
        freelancer_ids = [f['id'] for f in response.json()['freelancers']]
        self.assertListEqual(
            sorted(freelancer_ids),
            sorted([self.profile_3.id])
        )
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)
