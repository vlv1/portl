# system
import datetime

# django
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.conf import settings
from django.core.paginator import Paginator

# third party
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import APIException
from rest_framework.generics import ListAPIView

# portl
from projectme.core.models import Action
from projectme.profile.models import FreelancerProfile, Location, KeySkill, \
    Sector, Qualification, Language, ProfileSearchMatch, Profile
from projectme.profile.serializers import FreelancerProfileSerializer, \
    LocationSerializer, KeySkillSerializer, SectorSerializer, \
    QualificationSerializer, LanguageSerializer
from projectme.profile.utils import start_search
from projectme.profile.constants import RateType
from projectme.search.models import Search, SearchMatch
from projectme.search.serializers import SearchSerializer
from projectme.utils import ErrorResponse


class SearchView(APIView):
    permission_classes = (AllowAny, )

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='query',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description='Query',
                required=False
            ),
            openapi.Parameter(
                name='locations',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description='Locations (comma-separates area names)',
                required=False
            ),
            openapi.Parameter(
                name='key_skills',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description='Key skills (comma-separated titles)',
                required=False
            ),
            openapi.Parameter(
                name='languages',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description='Languages (comma-separated titles)',
                required=False
            ),
            openapi.Parameter(
                name='sectors',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description='Sectors (comma-separated titles)',
                required=False
            ),
            openapi.Parameter(
                name='qualifications',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description='Qualifications (comma-separated titles)',
                required=False
            ),
            openapi.Parameter(
                name='daily_rate_from',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_INTEGER,
                description='Daily rate from (decimal)',
                required=False
            ),
            openapi.Parameter(
                name='daily_rate_to',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_INTEGER,
                description='Daily rate to (decimal)',
                required=False
            ),
            openapi.Parameter(
                name='available_from',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description='Availability range start (YYYY-MM-DD)',
                required=False
            ),
            openapi.Parameter(
                name='available_to',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description='Availability range end (YYYY-MM-DD)',
                required=False
            ),
            openapi.Parameter(
                name='vetted',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_BOOLEAN,
                description='Filter freelancers by vetted field.'
                            'Filter is paid. '
                            'See `vetted_filter` in the client profile data.',
                required=False
            ),
            openapi.Parameter(
                name='page',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_INTEGER,
                description='Page number',
                required=False
            )
        ],
        responses={
            status.HTTP_200_OK: FreelancerProfileSerializer(many=True)
        }
    )
    def get(self, request):
        serializer = SearchSerializer(
            data=request.GET,
            context={"request": request}
        )

        if not serializer.is_valid():
            return ErrorResponse.build_serializer_error(
                serializer,
                status.HTTP_400_BAD_REQUEST
            )

        vd = serializer.validated_data

        query = vd.get('query')

        # register search action
        Action.track(
            user=self.request.user, verb='searched for "{}"'.format(query)
        )

        # query_wildcard = '.*{}.*'.format(query)
        freelancer_profiles_ids_scores = start_search(query)
        freelancer_profiles_ids = [
            fp[0] for fp in freelancer_profiles_ids_scores
        ]
        fps = FreelancerProfile.objects.filter(id__in=freelancer_profiles_ids)

        # apply filters
        locations = vd.get('locations')
        if locations:
            locations = locations.split(',')
            locations = Location.objects.filter(area_name__in=locations)
            fps = fps.filter(location__in=locations)

        key_skills = vd.get('key_skills')
        if key_skills:
            key_skills_titles = key_skills.split(',')
            key_skills = KeySkill.objects.filter(title__in=key_skills_titles)
            fps = fps.filter(key_skills__in=key_skills)

        sectors = vd.get('sectors')
        if sectors:
            sectors_titles = sectors.split(',')
            sectors = Sector.objects.filter(title__in=sectors_titles)
            fps = fps.filter(sectors__in=sectors)

        qualifications = vd.get('qualifications')
        if qualifications:
            qualifications_titles = qualifications.split(',')
            qualifications = Qualification.objects.filter(
                title__in=qualifications_titles
            )
            fps = fps.filter(qualifications__in=qualifications)

        languages = vd.get('languages')
        if languages:
            languages_titles = languages.split(',')
            languages = Language.objects.filter(title__in=languages_titles)
            fps = fps.filter(languages__in=languages)

        daily_rate_from = vd.get('daily_rate_from')
        if daily_rate_from:
            hourly_rate_from = daily_rate_from / settings.HOURS_IN_A_DAY
            fps = fps.filter(
                Q(
                    rate_type=RateType.DAILY,
                    rate_minimum__gte=daily_rate_from
                ) |
                Q(
                    rate_type=RateType.HOURLY,
                    rate_minimum__gte=hourly_rate_from
                )

            )

        daily_rate_to = vd.get('daily_rate_to')
        if daily_rate_to:
            hourly_rate_to = daily_rate_to / settings.HOURS_IN_A_DAY
            fps = fps.filter(
                Q(
                    rate_type=RateType.DAILY,
                    rate_maximum__lte=daily_rate_to
                ) |
                Q(
                    rate_type=RateType.HOURLY,
                    rate_maximum__lte=hourly_rate_to
                )

            )

        available_from = vd.get('available_from')
        available_to = vd.get('available_to')
        if available_from and available_to:
            # get unavailable periods in the specified range
            num_days = (available_to - available_from).days
            availability_range = [
                available_from + datetime.timedelta(days=x)
                for x in range(0, num_days + 1)
            ]
            fps = fps.exclude(
                unavailable_dates__contained_by=availability_range
            )

        # apply vetted filter and check if it is enabled
        if request.user.is_authenticated:
            profile = Profile.objects.get_subclass(user=request.user)
            if profile.vetted_filter:
                vetted = vd.get('vetted')
                if vetted is not None:
                    fps = fps.filter(vetted=vetted)

        profiles_ids = []
        profiles_data = []
        search_results_data = {}

        freelancer_profiles_ids_scores = list(filter(
            lambda x: int(x[0]) in list(fps.values_list('id', flat=True)),
            freelancer_profiles_ids_scores
        ))
        for fp_id_score in freelancer_profiles_ids_scores:
            fp_id, fp_score = fp_id_score
            fp = FreelancerProfile.objects.get(id=fp_id)
            ProfileSearchMatch.objects.create(profile_id=fp_id)
            fp_data = FreelancerProfileSerializer(
                fp,
                context={'request': request}
            ).data
            fp_data['score'] = fp_score
            profiles_ids.append(fp_id)
            profiles_data.append(fp_data)

        profiles = FreelancerProfile.objects.filter(id__in=profiles_ids)

        # data for filters
        languages = Language.objects.filter(profiles__in=profiles)
        key_skills = KeySkill.objects.filter(profiles__in=profiles)
        sectors = Sector.objects.filter(profiles__in=profiles)
        qualifications = Qualification.objects.filter(profiles__in=profiles)

        # get min and max rates
        daily_rates = profiles.filter(
            rate_type=RateType.DAILY,
            rate_minimum__isnull=False, rate_maximum__isnull=False
        ).values(
            'rate_minimum', 'rate_maximum'
        )
        daily_rates = [
            {'minimum': r['rate_minimum'], 'maximum': r['rate_maximum']}
            for r in daily_rates
        ]
        hourly_rates = profiles.filter(
            rate_type=RateType.HOURLY,
            rate_minimum__isnull=False, rate_maximum__isnull=False
        ).values(
            'rate_minimum', 'rate_maximum'
        )
        daily_rates_from_hourly = [
            {
                'minimum': r['rate_minimum'] * settings.HOURS_IN_A_DAY,
                'maximum': r['rate_maximum'] * settings.HOURS_IN_A_DAY
            }
            for r in hourly_rates
        ]
        daily_rates.extend(daily_rates_from_hourly)
        min_daily_rates = [r['minimum'] for r in daily_rates]
        max_daily_rates = [r['maximum'] for r in daily_rates]

        # paginate profiles list
        page = vd.get('page', 1)
        paginator = Paginator(profiles_data, settings.SEARCH_PAGE_SIZE)
        profiles_page = paginator.get_page(page)
        profiles_data = profiles_page.object_list

        search_results_data['freelancers'] = profiles_data
        search_results_data['filters'] = {
            'languages': LanguageSerializer(languages, many=True).data,
            'key_skills': KeySkillSerializer(key_skills, many=True).data,
            'sectors': SectorSerializer(sectors, many=True).data,
            'qualifications': QualificationSerializer(qualifications, many=True).data,
            'daily_rates': {
                'minimum': min(min_daily_rates) if min_daily_rates else 0,
                'maximum': max(max_daily_rates) if max_daily_rates else 0
            }
        }
        return Response(search_results_data, status=status.HTTP_202_ACCEPTED)


class SearchNotFinished(APIException):
    status_code = 204
    default_detail = 'Search hasn\'t completed, try again later.'
    default_code = 'search_notfinished'
    permission_classes = (AllowAny,)
    serializer_class = FreelancerProfileSerializer

    def get_serializer_context(self):
        return {
            "request": self.request,
            "search": Search.objects.get(id=self.kwargs['pk'])
        }

    # Get Search results
    def get_queryset(self):
        search = get_object_or_404(Search, id=self.kwargs['pk'])

        if search.user is None and self.request.user.is_anonymous is False:
            search.user = self.request.user
            search.save()

        if search.finished is False:
            raise SearchNotFinished

        ordered_profiles = [
            m.profile for m in SearchMatch.objects.select_related(
                'profile'
            ).filter(search=search).order_by('-percentage')
        ]
        return ordered_profiles


@method_decorator(name='list', decorator=swagger_auto_schema())
class LocationsAutocompleteView(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = LocationSerializer
    queryset = Location.objects.all()

    def get_queryset(self):
        query = self.request.GET.get('query')
        if query:
            return Location.objects.filter(area_name__startswith=query)
        return Location.objects.all()

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='query',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description="Location query",
                required=False
            ),
        ],
        responses={
            status.HTTP_200_OK: LocationSerializer(many=True)
        }
    )
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)
