from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from projectme.onboarding.models import MobileVerification, Verification, User


class FeedbackTests(APITestCase):

    def get_url(self, name, args={}, kwargs={}):
        return reverse(
            'v1:feedback:{}'.format(name),
            args=args, kwargs=kwargs
        )

    def setUp(self):
        MobileVerification.objects.create(
            mobile="+447710998870",
            status=Verification.CONFIRMED
        )
        url = reverse('v1:onboarding:client')
        data = {
            "first_name": "Johnny",
            "last_name": "Lee",
            "email": "johnny.lee@hedgehoglab.com",
            "mobile": "+447710998870",
            "password": "password123",
            "profile": {
                "company": "hedgehog lab",
                "job_title": "Client Services Director"
            }
        }
        self.client.post(url, data)
        user = User.objects.get(email="johnny.lee@hedgehoglab.com")
        self.client.force_authenticate(user=user)

    def test_report_issue(self):
        url = self.get_url('report-issue')
        data = {"description": "XYZ is broken"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_suggest_improvement(self):
        url = self.get_url('suggest-improvement')
        data = {
            "description": "X is good, but it could be great with Y and Z"
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
