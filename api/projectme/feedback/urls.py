from django.urls import re_path

from projectme.feedback.api import ReportIssueView, SuggestImprovementView


urlpatterns = [
    re_path(
        r'^report/$',
        ReportIssueView.as_view(),
        name="report-issue"
    ),
    re_path(
        r'^improvement/$',
        SuggestImprovementView.as_view(),
        name="suggest-improvement"
    ),
]
