from projectme.models import TimestampedModel
from django.contrib.gis.db import models
from django.conf import settings


class Feedback(TimestampedModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    description = models.TextField()

    def __str__(self):
        return "{}".format(self.user)


class ReportedIssue(Feedback):
    pass


class SuggestedImprovement(Feedback):
    pass
