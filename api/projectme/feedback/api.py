from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from projectme.feedback.serializers import ReportedIssueSerializer, \
    SuggestedImprovementSerializer
from projectme.utils import ErrorResponse


class ReportIssueView(APIView):

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='description',
                in_=openapi.IN_FORM,
                type=openapi.TYPE_STRING,
                description='Issue description',
                required=True
            ),
        ],
        responses={
            status.HTTP_201_CREATED: openapi.Response(description='no data')
        }
    )
    def post(self, request):
        data = request.data
        data['user'] = request.user.pk

        serializer = ReportedIssueSerializer(
            data=data,
            context={'request': request}
        )
        if not serializer.is_valid():
            return ErrorResponse.build_serializer_error(
                serializer,
                status.HTTP_400_BAD_REQUEST
            )
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)


class SuggestImprovementView(APIView):

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                name='description',
                in_=openapi.IN_FORM,
                type=openapi.TYPE_STRING,
                description='Improvement description',
                required=True
            ),
        ],
        responses={
            status.HTTP_201_CREATED: openapi.Response(description='no data')
        }
    )
    def post(self, request):
        data = request.data
        data['user'] = request.user.pk

        serializer = SuggestedImprovementSerializer(
            data=data,
            context={'request': request}
        )

        if not serializer.is_valid():
            return ErrorResponse.build_serializer_error(
                serializer,
                status.HTTP_400_BAD_REQUEST
            )

        serializer.save()
        return Response(status=status.HTTP_201_CREATED)
