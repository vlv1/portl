from django.contrib import admin

from projectme.feedback.models import ReportedIssue, SuggestedImprovement


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ['user', 'description', 'created_at']


admin.site.register(ReportedIssue, FeedbackAdmin)
admin.site.register(SuggestedImprovement, FeedbackAdmin)
