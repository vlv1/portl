from rest_framework import serializers

from projectme.feedback.models import ReportedIssue, SuggestedImprovement


class ReportedIssueSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportedIssue
        fields = ('user', 'description')


class SuggestedImprovementSerializer(serializers.ModelSerializer):
    class Meta:
        model = SuggestedImprovement
        fields = ('user', 'description')
