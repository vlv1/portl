import os
import uuid

from rest_framework.response import Response


class ErrorResponse:

    @staticmethod
    def build_serializer_error(serializer, status):
        return Response(
            {"status": "error", "errors" : serializer.errors},
            status=status
        )

    @staticmethod
    def build_text_error(text, status):
        return Response({"status": "error", "errors" : text}, status=status)


def random_file_name(instance, old_filename):
    extension = os.path.splitext(old_filename)[1]
    filename = str(uuid.uuid4()) + extension
    return filename
