"""
Django settings for api project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '30gcyd!)=j5^8(_k7-1hpuvy1ndpvd@zf-+zn9ffm@85ewyv9@'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*', ]


# Application definition

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django.contrib.sites',
    'django_extensions',
    'reversion',
    'djrill',
    'django_nose',
    'markdownx',
    'projectme',
    'projectme.onboarding',

    'projectme.profile',

    'projectme.payments',
    'projectme.search',
    'projectme.transactions',
    'projectme.feedback',
    'projectme.workers',
    'projectme.notifications',
    'projectme.feed',
    'projectme.core',

    # 'cachalot',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_framework_gis',
    'django_filters',
    'scarface',
    'bootstrap3',
    'storages',
    'django_celery_results',
    'drf_yasg',
    'imagekit'
)

MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'qinspect.middleware.QueryInspectMiddleware',
    'projectme.core.middleware.RequestMiddleware'
)


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            '/var/sites/projectme/releases/current/projectme/templates',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
            ],
            'debug': True
        },
    },
]

ROOT_URLCONF = 'api.urls'

WSGI_APPLICATION = 'api.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'projectme',
        'USER': 'projectme',
        'PASSWORD': '!UAA~sk59-X{-<_3',
        'HOST': '127.0.0.1',
        'TEST': {
            'NAME': 'portl_test'
        }
    }
}

ES_CONNECTIONS = {
    'default': {'hosts': 'localhost'}
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        }
    },
}

QUERY_INSPECT_ENABLED = False

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/London'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'assets'),
]

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder'
)

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = '/var/sites/projectme/shared/static/'

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = '/var/sites/projectme/shared/uploads/'


DATA_UPLOAD_MAX_MEMORY_SIZE = 10485760


# REST Framework
# http://www.django-rest-framework.org/#installation
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DATETIME_FORMAT': "%Y-%m-%dT%H:%M:%SZ",
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
        'rest_framework_xml.parsers.XMLParser',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework_xml.renderers.XMLRenderer',
    ),
    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.NamespaceVersioning',
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
    'TEST_REQUEST_RENDERER_CLASSES': (
        'rest_framework.renderers.MultiPartRenderer',
        'rest_framework.renderers.JSONRenderer',
    )
}


LOGIN_URL = "/admin/login/"

BASE_URL = 'http://192.168.56.111/'

AUTH_USER_MODEL = "onboarding.User"

TWILIO_ACCOUNT_SID = 'ACd9a8ce33e56a1f138eedf56082cad515'
TWILIO_AUTH_TOKEN = '9f1dd20c873d45dae6b17e3eb591cdb3'
TWILIO_DEFAULT_CALLERID = 'Portl'

MANDRILL_API_KEY = "42YEbtDxxLN7BK24x5SvGw"
EMAIL_BACKEND = "djrill.mail.backends.djrill.DjrillBackend"
DEFAULT_FROM_EMAIL = "Portl <noreply@portl-app.com>"

SCARFACE_REGION_NAME = 'us-east-1'
SCARFACE_APNS_MODE = "APNS_SANDBOX"
SCARFACE_APNS_PLATFORM = SCARFACE_APNS_MODE
SCARFACE_APNS_ARN = "arn:aws:sns:us-east-1:074483771027:app/APNS_SANDBOX/portl_dev_apns"
SCARFACE_GCM_PLATFORM = 'GCM'
SCARFACE_GCM_ARN = "arn:aws:sns:us-east-1:074483771027:app/GCM/portl_dev_gcm"
SCARFACE_LOGGING_ENABLED = True
SCARFACE_SNS_APP_PREFIX = "PORTL"
SCARFACE_APNS_APPLICATION_NAME = SCARFACE_SNS_APP_PREFIX + "_" + SCARFACE_APNS_MODE
SCARFACE_GCM_APPLICATION_NAME = SCARFACE_SNS_APP_PREFIX + "_" + SCARFACE_GCM_PLATFORM
SCARFACE_MESSAGE_TRIM_LENGTH = 400
AWS_STORAGE_BUCKET_NAME = 'project-me-dev'
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
AWS_ACCESS_KEY = 'AKIAJ4FIHMOAWSEYI64A'
AWS_ACCESS_KEY_ID = AWS_ACCESS_KEY
AWS_SECRET_ACCESS_KEY = '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'
AWS_DEFAULT_ACL = 'public-read'
AWS_HEADERS = {
    'Cache-Control': 'max-age=86400',
}
AWS_QUERYSTRING_AUTH = False
AWS_DYNAMODB_REGION_NAME = 'eu-west-2'
AWS_DYNAMODB_NAME = 'feed_dev'

AWS_SQS_REGION_NAME = 'eu-west-2'
SQS_QUEUE_NAME = 'portl_dev'

SAVE_TO_DYNAMO = True

# send events synchronously, or use SNS-SQS-Celery-SNS chain
USE_ASYNC_FEED_NOTIFICATION = False



CELERY_RESULT_BACKEND = 'django-db'
CELERY_TASK_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_RESULT_SERIALIZER = 'json'

CELERY_ENABLE_UTC = True
CELERY_DISABLE_RATE_LIMITS = True



SITE_ID = 1

DEFAULT_FILE_STORAGE = 'api.projectmestorages.DefaultStorage'
AVATAR_UPLOADS = 'avatars'

HOURS_IN_A_DAY = 8

# number of seconds after previous availability request within which a new request will not be registered
CLIENT_REQUEST_DELAY = 300

# PAYPAL SETTINGS
PAYPAL_SANDBOX_CLIENT_ID = 'AezQPbobJ0xMilm9xavDJ2QxSZ3HNAH6QbOyMAKREag_dZzYSohRE4DaPvbGOQhw_JMT4fbzgFzAKYsX'
PAYPAL_PROD_CLIENT_ID = ''

STRIPE_PUBLISHABLE_KEY = 'pk_test_9mWlaC3VgN0Iyxb7Ts8SyXNg'
STRIPE_SECRET_KEY = 'sk_test_LGotaQrCX2PkVvnjFs1divSe'

# REFERRALS
REFERRALS_SECURE_URLS = False
REFERRER_CREDITS_BONUS = 2
REFERRED_CREDITS_BONUS = 1

# PRICES
UNLOCK_PRICE = 1  # credits

# VERIFICATION
SMS_VERIFICATION_CODE_TTL = 10080  # 1 week in minutes

# =========================================
# SEARCH
# =========================================
COMMON_PAGE_SIZE = 10
SEARCH_PAGE_SIZE = 10

FREELANCER_PROFILE_INDEX = 'freelancer-profile'
if 'test' in sys.argv:
    FREELANCER_PROFILE_INDEX = 'freelancer-profile-test'

# =========================================
# REQUESTS LOGGING
# =========================================
REQUEST_IGNORE_PATHS = ('admin/', 'media/')

# =========================================
# DEBUG SETTINGS
# =========================================
IMITATE_MOBILE_VERIFICATION = True


try:
    from .settings_local import *
except ImportError:
    pass

BROKER_URL = 'sqs://%s:%s@' % (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
BROKER_TRANSPORT_OPTIONS = {
    'region': AWS_SQS_REGION_NAME,
}
CELERY_DEFAULT_QUEUE = SQS_QUEUE_NAME