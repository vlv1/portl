# system
import json

# django
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, DetailView

# third party
# ..

# portl
from projectme.core.models import Action
from projectme.onboarding.token import get_token
from projectme.payments.models import CreditPackage, Payment, PaymentStatus
from projectme.profile.models import Profile


class IndexView(TemplateView):
    template_name = 'index.html'


class CreditPackagesView(LoginRequiredMixin, TemplateView):
    template_name = 'credit_packages.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['packages'] = CreditPackage.objects.all()

        token = get_token(self.request.user)
        ctx['token'] = token.key
        return ctx


class PaymentTypesView(LoginRequiredMixin, DetailView):
    template_name = 'payment_types.html'
    model = CreditPackage
    pk_url_kwarg = 'package_id'
    context_object_name = 'package'


class StripeView(LoginRequiredMixin, DetailView):
    template_name = 'stripe.html'
    model = CreditPackage
    pk_url_kwarg = 'package_id'
    context_object_name = 'package'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['publishable_key'] = settings.STRIPE_PUBLISHABLE_KEY

        token = get_token(self.request.user)
        ctx['token'] = token.key
        return ctx


class PayPalView(LoginRequiredMixin, DetailView):
    template_name = 'paypal.html'
    model = CreditPackage
    pk_url_kwarg = 'package_id'
    context_object_name = 'package'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['client_id'] = settings.PAYPAL_SANDBOX_CLIENT_ID

        token = get_token(self.request.user)
        ctx['token'] = token.key
        return ctx


def paypal_webhook(request):
    print(request.body)
    return HttpResponse()


@csrf_exempt
def stripe_webhook(request):
    request_json = json.loads(request.body)
    obj, action, *misc = request_json['type'].split('.')

    if obj == 'charge':
        charge_object = request_json['data']['object']
        payment_id = charge_object['metadata'].get('payment_id')

        if not payment_id:
            return HttpResponse()

        try:
            payment = Payment.objects.get(id=payment_id)
        except Payment.DoesNotExist:
            print('Payment does not exist')
            return HttpResponse()

        payment.webhook_data = request.body
        if action == 'succeeded':
            payment.status = PaymentStatus.SUCCESS
            profile = Profile.objects.get_subclass(user=payment.user)
            profile.add_credits(payment.credits, description='credits bought')

            # track action
            Action.track(
                user=payment.user,
                verb='bought {} credits'.format(payment.credits)
            )
        elif action == 'failed':
            payment.status = PaymentStatus.FAILED

        payment.save()

    return HttpResponse()
