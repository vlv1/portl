from django.conf import settings

from storages.backends.s3boto import S3BotoStorage


class DefaultStorage(S3BotoStorage):
    location = settings.AVATAR_UPLOADS
    default_acl = 'public-read'
