# django
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.urls import re_path, include
from django.views.static import serve

# third party
from rest_framework import permissions
from rest_framework.urlpatterns import format_suffix_patterns
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

# portl
from api.views import StripeView, CreditPackagesView, \
    PaymentTypesView, IndexView, PayPalView, paypal_webhook, stripe_webhook
from projectme.onboarding.api import ChangePasswordView


schema_view = get_schema_view(
    openapi.Info(
        title="Portl API",
        default_version='v1',
        description="",
    ),
    validators=['flex', 'ssv'],
    public=True,
    permission_classes=(permissions.AllowAny, ),
)


urlpatterns = [
    # re_path(
    #     r'^swagger(?P<format>\.json|\.yaml)$',
    #     schema_view.without_ui(cache_timeout=0),
    #     name='schema-json'
    # ),
    re_path(
        r'^api/doc/$',
        schema_view.with_ui('swagger', cache_timeout=0),
        name='schema-swagger-ui'
    ),
    re_path(
        r'^api/redoc/$',
        schema_view.with_ui('redoc', cache_timeout=0),
        name='schema-redoc'
    ),

    re_path(
        r'^api/v1/',
        include(([
            re_path(
                r'^feedback/',
                include(
                    ('projectme.feedback.urls', 'feedback'),
                    namespace='feedback'
                )
            ),
            re_path(
                r'^notifications/',
                include(
                    ('projectme.notifications.urls', 'notifications'),
                    namespace='notifications'
                )
            ),
            re_path(
                r'^onboard/',
                include(
                    ('projectme.onboarding.urls', 'onboarding'),
                    namespace='onboarding'
                )
            ),
            re_path(
                r'^profile/',
                include(
                    ('projectme.profile.urls', 'profile'),
                    namespace='profile'
                )
            ),
            re_path(
                r'^search/',
                include(
                    ('projectme.search.urls', 'search'),
                    namespace='search'
                )
            ),
            re_path(
                r'^transactions/',
                include(
                    ('projectme.transactions.urls', 'transactions'),
                    namespace='transactions'
                )
            ),
            re_path(
                r'^payments/',
                include(
                    ('projectme.payments.urls', 'payments'),
                    namespace='payments'
                )
            ),
             re_path(
                 r'^feed/',
                 include(
                     ('projectme.feed.urls', 'feed'),
                     namespace='feed'
                 )
             ),
        ], 'api'), namespace='v1'),
    ),
]

urlpatterns = format_suffix_patterns(urlpatterns)

urlpatterns += [
    re_path(r'^$', IndexView.as_view(), name='index'),

    # payments
    re_path(
        r'^packages/$',
        CreditPackagesView.as_view(),
        name='credit-packages'
    ),
    re_path(
        r'^packages/(?P<package_id>\d+)/payment_types/$',
        PaymentTypesView.as_view(),
        name='payment-types'
    ),
    re_path(
        r'^packages/(?P<package_id>\d+)/stripe/$',
        StripeView.as_view(),
        name='stripe'
    ),
    re_path(
        r'^packages/(?P<package_id>\d+)/paypal/$',
        PayPalView.as_view(),
        name='paypal'
    ),
    re_path(
        r'^webhook/paypal/$',
        paypal_webhook,
        name='paypal-webhook'
    ),
    re_path(
        r'^webhook/stripe/$',
        stripe_webhook,
        name='stripe-webhook'
    ),

    re_path(r'^admin/', admin.site.urls),
    re_path(
        r'^uploads/(.*)$',
        serve,
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}
    ),
    re_path(
        r'^change-password/(?P<token>[A-Za-z0-9]+)/$',
        ChangePasswordView,
        name="change-password"
    ),
    re_path(r'^markdownx/', include('markdownx.urls')),
    re_path(r'^.*', IndexView.as_view(), name='index'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
