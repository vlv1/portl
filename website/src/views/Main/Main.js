import React from 'react';

import Search from 'components/Search';
import Landing from 'components/Landing';

export default class Main extends React.Component {
  render() {
    return (
      <Landing>
        <Search />
      </Landing>
    );
  }
}
