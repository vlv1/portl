import { connect } from 'react-redux';
import ForgotPassword from './ResetPassword';

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = () => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgotPassword);
