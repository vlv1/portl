import TermsOfServiceContainer from 'components/TermsOfService';
import React from "react";

export default class TermsOfService extends React.Component {
  render() {
    return <TermsOfServiceContainer />;
  }
}
