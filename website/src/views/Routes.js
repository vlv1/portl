import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Header from 'components/Header';
import Main from './Main';
import Search from './Search';
import Cabinet from './Cabinet';
import SearchResult from './SearchResult';
import Login from './Login';
import {SectionWithFooter} from '../components/Section/Section';
import {FooterBlock} from '../components/Landing';
import Helmet from 'react-helmet';
import Registration from './Registration';
import ResetPassword from './ResetPassword';
import Modal from 'components/Modal';
import connect from "react-redux/es/connect/connect";
import {init} from "../store/init";
import {bindActionCreators} from "redux";
import {applyFilter, clearRangeFilter, setRangeFilterValue} from "../store/Search/actions";
import withRouter from "react-router/es/withRouter";
import TermsOfService from 'components/TermsOfService';

const DefaultLayout = ({component: Component, disableFooter = false, ...rest}) => {
    return (
        <Route
            {...rest}
            render={matchProps => (
                <SectionWithFooter>
                    <div>
                        <Header/>
                        <Component {...matchProps} />
                    </div>
                    {!disableFooter ? <FooterBlock/> : null}
                </SectionWithFooter>
            )}
        />
    );
};

class Routes extends React.Component {
    componentDidMount() {
        this.props.actions.init()
    }
    render() {
        return (
            <div>
                <Helmet>
                    <meta charSet="utf-8"/>
                    <title>Portl</title>
                </Helmet>
                <Modal/>
                <Switch>
                    <DefaultLayout exact path="/" component={Main}/>
                    <DefaultLayout path="/freelancer/:id" component={SearchResult}/>
                    <DefaultLayout path="/cabinet" component={Cabinet}/>
                    <DefaultLayout disableFooter path="/search" component={Search}/>
                    <DefaultLayout disableFooter path="/login" component={() => <Login/>}/>
                    <DefaultLayout disableFooter path="/registration" component={() => <Registration/>}/>
                    <DefaultLayout disableFooter path="/reset-password" component={() => <ResetPassword/>}/>
                    <DefaultLayout path="/terms-of-service" component={() => <TermsOfService />} />
                    <Route path="" component={() => <div>404</div>}/>
                </Switch>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({ init }, dispatch)
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Routes));
