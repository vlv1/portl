import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { searchRequest, parseFieldsFromQueryString } from 'store/Search/actions';
import { bindActionCreators } from 'redux';
import { getResultsData } from 'store/Search/selectors';
import Search from './Search';

const mapStateToProps = state => {
  return {
    data: getResultsData(state)
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators({ searchRequest, parseFieldsFromQueryString }, dispatch) };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Search)
);
