import React from 'react';

import Search from 'components/Search';
import Filters from 'components/Filters';
import Result from 'components/Result';
import queryString from 'query-string';

export default class SearchView extends React.Component {
  componentDidMount() {
    const {
      actions: { searchRequest, parseFieldsFromQueryString },
      data,
      location: { search }
    } = this.props;

    const searchParams = queryString.parse(search);
    if (searchParams.from_landing === '1') {
      searchRequest();
    }

    if (searchParams.query && data === undefined) {
      parseFieldsFromQueryString(searchParams);
    }

    if (data === undefined) {
      searchRequest();
    }
  }

  render() {
    return (
      <div>
        <Search />
        <Filters />
        <Result />
      </div>
    );
  }
}
