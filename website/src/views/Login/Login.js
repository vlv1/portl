import React from 'react';
import LoginContainer from 'components/Login';
import Cookies from 'js-cookie';

export default class Login extends React.Component {
  componentDidMount() {
    Cookies.remove('Token');
  }
  render() {
    return <LoginContainer />;
  }
}
