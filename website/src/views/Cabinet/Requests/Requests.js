import React from 'react';
import { Route } from 'react-router-dom';
import ControlPanel from 'components/Cabinet/ControlPanel';
import Result from '../../../components/Result/Result';


class Requests extends React.Component {
  componentDidMount() {
    this.props.actions.fetchRequests();
  }

  render() {
    const additionalMenu = [
      {
        title: 'Confirmed',
        id: 'confirmed'
      },
      {
        title: 'Pending',
        id: 'pending'
      }
    ];
      const { favouriteUser, requestAvailability, unfavouriteUser, unlockFreelancer, modalShow } = this.props.actions;

      return (
      <div>
        <ControlPanel page={`requests`} additionalMenu={additionalMenu} />
        <Result
          type={'request'}
          data={this.props.requests}
          actions={{ favouriteUser, requestAvailability, unfavouriteUser, unlockFreelancer, modalShow }}
        />
      </div>
    );
  }
}
export default Requests;
