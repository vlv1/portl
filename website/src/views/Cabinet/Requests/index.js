import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Requests from './Requests';
import { fetchRequests } from '../../../store/Requests/actions';
import { withRouter } from 'react-router';
import { getRequestsByPathName } from '../../../store/Requests/selectors';
import { requestAvailability, unlockFreelancer} from "../../../store/Search/actions";
import {
    unfavouriteUser, favouriteUser
} from '../../../store/Requests/actions';
import { modalShow } from 'store/Modal/actions';

const mapStateToProps = (state, props) => {
  return {requests: getRequestsByPathName(state, props.location) };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators({ fetchRequests, favouriteUser, requestAvailability, unlockFreelancer, unfavouriteUser, modalShow }, dispatch) };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Requests)
);
