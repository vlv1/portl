import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Profile from './Profile';
import { saveClientChanges } from '../../../store/User/actions';
import {getAuthentication, getCabinetData} from '../../../store/User/selectors';
import { modalShow } from '../../../store/Modal/actions';

const mapStateToProps = state => {
  return { userData: getCabinetData(state), userAuthData: getAuthentication(state) };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators({ saveClientChanges, modalShow }, dispatch) };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
