import React from 'react';
import { Route } from 'react-router-dom';
import ControlPanel from 'components/Cabinet/ControlPanel';
import EditProfile from '../../../components/EditProfile/EditProfile';

const Profile = (props) => {
  const AdditionalMenu = [
    {
      title: 'Profile',
      id: 'profile'
    },
    {
      title: 'Change Password',
      id: 'change password'
    },
    {
      title: 'Purchase History',
      id: 'purchase history'
    }
  ];

  return (
    <div>
      <ControlPanel />
      {props.userData ? <EditProfile {...props} /> : null}
    </div>
  );
};

export default Profile;
