export default {
  isFetching: false,
  data: [
    {
      id: 977,
      company: null,
      job_title: {
        id: 15,
        title: 'Lean Champion'
      },
      user: {
        id: 978,
        first_name: 'sonya',
        last_name: 'fortunato',
        avatar: null,
        initials: 'SF',
        email: 'sonya.fortunato@example.com',
        mobile: '',
        verified_mobile: false,
        verified_email: false
      },
      key_skills: [
        {
          id: 60,
          title: 'Business Case Development',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 64,
          title: 'Commercial Excellence',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 88,
          title: 'Process Excellence',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 93,
          title: 'Root Cause Analysis',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      sectors: [
        {
          id: 168,
          title: 'Computer Games',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 181,
          title: 'Education Management',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 287,
          title: 'Utilities',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      qualifications: [
        {
          id: 111,
          title: 'CAPM Certification (Certified Associate of Project Management)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 144,
          title: 'PgMP (Programme Management Professional)',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      languages: [
        {
          id: 53,
          title: 'French',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      location: null,
      rate: {
        minimum: 7,
        maximum: 15,
        type: 0
      },
      unavailable_periods: [],
      unlocked: true,
      favourite: true,
      unlocked_at: '2018-10-28T18:30:44.807316Z',
      availability_updated: false,
      availability_requested_at: '2018-10-31T12:25:31.781873Z',
      availability_updated_at: '2018-10-30T11:48:12.372365Z',
      can_favourite: true,
      search_percentage: null,
      rating: 0,
      bio: null,
      last_login: '2018-10-09T19:55:07.564867Z',
      created_at: '2018-10-09T20:55:07Z',
      updated_at: '2018-10-30T11:48:12Z',
      postcode: 'WC2N 5DU',
      unavailable_weekends: true,
      always_available: false,
      on_hold: false,
      vetted: false,
      vetting_in_progress: false,
      score: 1
    },
    {
      id: 980,
      company: null,
      job_title: {
        id: 28,
        title: 'OpEx Consultant'
      },
      user: {
        id: 981,
        first_name: 'robert',
        last_name: 'peterson',
        avatar: null,
        initials: 'RP',
        email: 'robert.peterson@example.com',
        mobile: '',
        verified_mobile: false,
        verified_email: false
      },
      key_skills: [
        {
          id: 55,
          title: '5S',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 62,
          title: 'Change Management',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 100,
          title: 'Target Operating Model (TOM)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 104,
          title: 'Value Stream Mapping (VSM)',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      sectors: [
        {
          id: 152,
          title: 'Animation',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 186,
          title: 'Events Services',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 197,
          title: 'Gambling & Casinos',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 216,
          title: 'Investment Banking',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 234,
          title: 'Medical Devices',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 241,
          title: 'Music',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      qualifications: [
        {
          id: 127,
          title: 'Lean Six Sigma Black Belt Certified',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 139,
          title: 'MSP (Managing Successful Programmes) Advanced',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      languages: [
        {
          id: 52,
          title: 'English',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 53,
          title: 'French',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 54,
          title: 'Russian',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      location: null,
      rate: {
        minimum: 7,
        maximum: 15,
        type: 0
      },
      unavailable_periods: [],
      unlocked: true,
      favourite: true,
      unlocked_at: '2018-10-29T09:11:35.354420Z',
      availability_updated: false,
      availability_requested_at: null,
      availability_updated_at: '2018-10-30T11:48:12.384926Z',
      can_favourite: true,
      search_percentage: null,
      rating: 0,
      bio: null,
      last_login: '2018-10-09T19:55:07.866935Z',
      created_at: '2018-10-09T20:55:07Z',
      updated_at: '2018-10-30T11:48:12Z',
      postcode: 'WC2N 5DU',
      unavailable_weekends: true,
      always_available: false,
      on_hold: false,
      vetted: false,
      vetting_in_progress: false,
      score: 1
    },
    {
      id: 982,
      company: null,
      job_title: {
        id: 38,
        title: 'Six Sigma Green Belt'
      },
      user: {
        id: 983,
        first_name: 'john',
        last_name: 'lopez',
        avatar: null,
        initials: 'JL',
        email: 'john.lopez@example.com',
        mobile: '',
        verified_mobile: false,
        verified_email: false
      },
      key_skills: [
        {
          id: 74,
          title: 'Kaizen',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 77,
          title: 'Lean',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 83,
          title: 'Overall Equipment Effectiveness (OEE)',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      sectors: [
        {
          id: 185,
          title: 'Environmental Services',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 221,
          title: 'Legal Services',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      qualifications: [
        {
          id: 115,
          title: 'Chartered Accountant (CA)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 120,
          title: 'LCS Level 1a',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 131,
          title: 'Lean Six Sigma Master Black Belt Certified',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 133,
          title: 'Lean Six Sigma Yellow Belt Certified',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 50,
          title: 'Packaging',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 147,
          title: 'Prince2 Practitioner',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      languages: [
        {
          id: 52,
          title: 'English',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 54,
          title: 'Russian',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      location: null,
      rate: {
        minimum: 6,
        maximum: 13,
        type: 0
      },
      unavailable_periods: [],
      unlocked: true,
      favourite: true,
      unlocked_at: '2018-10-31T12:36:41.038105Z',
      availability_updated: false,
      availability_requested_at: '2018-10-31T12:36:51.865004Z',
      availability_updated_at: '2018-10-30T11:48:12.391000Z',
      can_favourite: true,
      search_percentage: null,
      rating: 0,
      bio: null,
      last_login: '2018-10-09T19:55:08.064402Z',
      created_at: '2018-10-09T20:55:08Z',
      updated_at: '2018-10-30T11:48:12Z',
      postcode: 'WC2N 5DU',
      unavailable_weekends: true,
      always_available: false,
      on_hold: false,
      vetted: false,
      vetting_in_progress: false,
      score: 1
    },
    {
      id: 984,
      company: null,
      job_title: {
        id: 33,
        title: 'Programme Manager'
      },
      user: {
        id: 985,
        first_name: 'armando',
        last_name: 'denton',
        avatar: null,
        initials: 'AD',
        email: 'armando.denton@example.com',
        mobile: '',
        verified_mobile: false,
        verified_email: false
      },
      key_skills: [
        {
          id: 79,
          title: 'Lean Office',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      sectors: [
        {
          id: 174,
          title: 'Consumer Electronics',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      qualifications: [
        {
          id: 112,
          title: 'Certified Management Consultant (CMC)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 115,
          title: 'Chartered Accountant (CA)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 127,
          title: 'Lean Six Sigma Black Belt Certified',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 142,
          title: 'National Vocational Qualification (NVQ)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 146,
          title: 'Prince2 Foundation',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      languages: [
        {
          id: 52,
          title: 'English',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 53,
          title: 'French',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 54,
          title: 'Russian',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      location: null,
      rate: {
        minimum: 4,
        maximum: 9,
        type: 0
      },
      unavailable_periods: [],
      unlocked: true,
      favourite: false,
      unlocked_at: '2018-10-29T15:43:14.420724Z',
      availability_updated: false,
      availability_requested_at: null,
      availability_updated_at: '2018-10-30T11:48:12.397273Z',
      can_favourite: true,
      search_percentage: null,
      rating: 0,
      bio: null,
      last_login: '2018-10-09T19:55:08.300428Z',
      created_at: '2018-10-09T20:55:08Z',
      updated_at: '2018-10-30T11:48:12Z',
      postcode: 'WC2N 5DU',
      unavailable_weekends: true,
      always_available: false,
      on_hold: false,
      vetted: false,
      vetting_in_progress: false,
      score: 1
    },
    {
      id: 985,
      company: null,
      job_title: {
        id: 25,
        title: 'Lean Sigma Trainer'
      },
      user: {
        id: 986,
        first_name: 'mildred',
        last_name: 'gill',
        avatar: null,
        initials: 'MG',
        email: 'mildred.gill@example.com',
        mobile: '',
        verified_mobile: false,
        verified_email: false
      },
      key_skills: [
        {
          id: 96,
          title: 'Standard Work',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 101,
          title: 'Total Productive Maintenance (TPM)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 103,
          title: 'Training',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      sectors: [
        {
          id: 159,
          title: 'Biotechnology',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 176,
          title: 'Consumer Services',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 189,
          title: 'Farming',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 234,
          title: 'Medical Devices',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 288,
          title: 'Venture Capital & Private Equity',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 290,
          title: 'Warehousing',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      qualifications: [
        {
          id: 110,
          title: "Bachelor's degree",
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 129,
          title: 'Lean Six Sigma Green Belt Certified',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      languages: [
        {
          id: 52,
          title: 'English',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 53,
          title: 'French',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 54,
          title: 'Russian',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      location: null,
      rate: {
        minimum: 3,
        maximum: 13,
        type: 0
      },
      unavailable_periods: [],
      unlocked: true,
      favourite: true,
      unlocked_at: '2018-11-02T11:06:43.381729Z',
      availability_updated: false,
      availability_requested_at: null,
      availability_updated_at: '2018-10-30T11:48:12.400349Z',
      can_favourite: true,
      search_percentage: null,
      rating: 0,
      bio: null,
      last_login: '2018-10-09T19:55:08.396528Z',
      created_at: '2018-10-09T20:55:08Z',
      updated_at: '2018-10-30T11:48:12Z',
      postcode: 'WC2N 5DU',
      unavailable_weekends: true,
      always_available: false,
      on_hold: false,
      vetted: false,
      vetting_in_progress: false,
      score: 1
    },
    {
      id: 994,
      company: null,
      job_title: {
        id: 17,
        title: 'Lean Consultant'
      },
      user: {
        id: 995,
        first_name: 'erik',
        last_name: 'rowley',
        avatar: null,
        initials: 'ER',
        email: 'erik.rowley@example.com',
        mobile: '',
        verified_mobile: false,
        verified_email: false
      },
      key_skills: [
        {
          id: 62,
          title: 'Change Management',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 90,
          title: 'Process Management',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 98,
          title: 'Supply Chain Management (SCM)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 104,
          title: 'Value Stream Mapping (VSM)',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      sectors: [
        {
          id: 148,
          title: 'Accounting',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 185,
          title: 'Environmental Services',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 223,
          title: 'Leisure, Travel & Tourism',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 233,
          title: 'Media Production',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 276,
          title: 'Shipbuilding',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      qualifications: [
        {
          id: 112,
          title: 'Certified Management Consultant (CMC)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 115,
          title: 'Chartered Accountant (CA)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 118,
          title: 'Chartered Public Finance Accountant (CPFA)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 119,
          title: 'Doctorate',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 129,
          title: 'Lean Six Sigma Green Belt Certified',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 145,
          title: 'PMP (Project Management Professional)',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      languages: [
        {
          id: 54,
          title: 'Russian',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      location: null,
      rate: {
        minimum: 4,
        maximum: 11,
        type: 0
      },
      unavailable_periods: [],
      unlocked: true,
      favourite: false,
      unlocked_at: '2018-11-02T12:44:50.523840Z',
      availability_updated: false,
      availability_requested_at: null,
      availability_updated_at: '2018-10-30T11:48:12.427953Z',
      can_favourite: true,
      search_percentage: null,
      rating: 0,
      bio: null,
      last_login: '2018-10-09T19:55:09.310206Z',
      created_at: '2018-10-09T20:55:09Z',
      updated_at: '2018-10-30T11:48:12Z',
      postcode: 'WC2N 5DU',
      unavailable_weekends: true,
      always_available: false,
      on_hold: false,
      vetted: false,
      vetting_in_progress: false,
      score: 1
    },
    {
      id: 995,
      company: null,
      job_title: {
        id: 31,
        title: 'Process Consultant'
      },
      user: {
        id: 996,
        initials: 'GT'
      },
      key_skills: [
        {
          id: 56,
          title: 'A3',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 64,
          title: 'Commercial Excellence',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 80,
          title: 'Lean Sigma',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 97,
          title: 'Supplier Development',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      sectors: [
        {
          id: 155,
          title: 'Arts and Crafts',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 221,
          title: 'Legal Services',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      qualifications: [
        {
          id: 108,
          title: 'APMP',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 112,
          title: 'Certified Management Consultant (CMC)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 136,
          title: 'MBA',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 140,
          title: 'MSP (Managing Successful Programmes) Foundation',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 146,
          title: 'Prince2 Foundation',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      languages: [
        {
          id: 52,
          title: 'English',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 54,
          title: 'Russian',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      location: null,
      rate: {
        minimum: 6,
        maximum: 13,
        type: 0
      },
      unavailable_periods: [],
      unlocked: false,
      favourite: false,
      unlocked_at: null,
      availability_updated: false,
      availability_requested_at: null,
      availability_updated_at: '2018-10-30T11:48:12.430981Z',
      can_favourite: false,
      search_percentage: null,
      rating: 0,
      bio: null,
      last_login: '2018-10-09T19:55:09.405985Z',
      created_at: '2018-10-09T20:55:09Z',
      updated_at: '2018-10-30T11:48:12Z',
      postcode: 'WC2N 5DU',
      unavailable_weekends: true,
      always_available: false,
      on_hold: false,
      vetted: false,
      vetting_in_progress: false,
      score: 1
    },
    {
      id: 1001,
      company: null,
      job_title: {
        id: 15,
        title: 'Lean Champion'
      },
      user: {
        id: 1002,
        initials: 'SR'
      },
      key_skills: [
        {
          id: 81,
          title: 'Operational Excellence',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 101,
          title: 'Total Productive Maintenance (TPM)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 102,
          title: 'Toyota Production System (TPS)',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      sectors: [
        {
          id: 209,
          title: 'Industrial Automation',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      qualifications: [
        {
          id: 108,
          title: 'APMP',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 122,
          title: 'LCS Level 1c',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 124,
          title: 'LCS Level 2b',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 128,
          title: 'Lean Six Sigma Black Belt Trained',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 130,
          title: 'Lean Six Sigma Green Belt Trained',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 143,
          title: 'Operations Excellence MSc (Cranfield University)',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      languages: [
        {
          id: 52,
          title: 'English',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 53,
          title: 'French',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 54,
          title: 'Russian',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      location: null,
      rate: {
        minimum: 5,
        maximum: 14,
        type: 0
      },
      unavailable_periods: [],
      unlocked: false,
      favourite: false,
      unlocked_at: null,
      availability_updated: false,
      availability_requested_at: null,
      availability_updated_at: '2018-10-30T11:48:12.449610Z',
      can_favourite: false,
      search_percentage: null,
      rating: 0,
      bio: null,
      last_login: '2018-10-09T19:55:10.029182Z',
      created_at: '2018-10-09T20:55:10Z',
      updated_at: '2018-10-30T11:48:12Z',
      postcode: 'WC2N 5DU',
      unavailable_weekends: true,
      always_available: false,
      on_hold: false,
      vetted: false,
      vetting_in_progress: false,
      score: 1
    },
    {
      id: 1004,
      company: null,
      job_title: {
        id: 20,
        title: 'Lean Master'
      },
      user: {
        id: 1005,
        initials: 'KP'
      },
      key_skills: [
        {
          id: 64,
          title: 'Commercial Excellence',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 75,
          title: 'Kanban',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 98,
          title: 'Supply Chain Management (SCM)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 103,
          title: 'Training',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      sectors: [
        {
          id: 155,
          title: 'Arts and Crafts',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 197,
          title: 'Gambling & Casinos',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 200,
          title: 'Government Relations',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 201,
          title: 'Graphic Design',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 234,
          title: 'Medical Devices',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 235,
          title: 'Medical Practice',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      qualifications: [
        {
          id: 108,
          title: 'APMP',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 115,
          title: 'Chartered Accountant (CA)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 130,
          title: 'Lean Six Sigma Green Belt Trained',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 143,
          title: 'Operations Excellence MSc (Cranfield University)',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 146,
          title: 'Prince2 Foundation',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      languages: [
        {
          id: 52,
          title: 'English',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 53,
          title: 'French',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 54,
          title: 'Russian',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      location: null,
      rate: {
        minimum: 7,
        maximum: 12,
        type: 0
      },
      unavailable_periods: [],
      unlocked: false,
      favourite: false,
      unlocked_at: null,
      availability_updated: false,
      availability_requested_at: null,
      availability_updated_at: '2018-10-30T11:48:12.458999Z',
      can_favourite: false,
      search_percentage: null,
      rating: 0,
      bio: null,
      last_login: '2018-10-09T19:55:10.377096Z',
      created_at: '2018-10-09T20:55:10Z',
      updated_at: '2018-10-30T11:48:12Z',
      postcode: 'WC2N 5DU',
      unavailable_weekends: true,
      always_available: false,
      on_hold: false,
      vetted: false,
      vetting_in_progress: false,
      score: 1
    },
    {
      id: 1006,
      company: null,
      job_title: {
        id: 1,
        title: 'BPR Consultant'
      },
      user: {
        id: 1007,
        first_name: 'minnie',
        last_name: 'vrabel',
        avatar: null,
        initials: 'MV',
        email: 'minnie.vrabel@example.com',
        mobile: '',
        verified_mobile: false,
        verified_email: false
      },
      key_skills: [
        {
          id: 63,
          title: 'Coaching',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 78,
          title: 'Lean Manufacturing',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 96,
          title: 'Standard Work',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      sectors: [
        {
          id: 168,
          title: 'Computer Games',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 261,
          title: 'Public Policy',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      qualifications: [
        {
          id: 120,
          title: 'LCS Level 1a',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      languages: [
        {
          id: 52,
          title: 'English',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 53,
          title: 'French',
          experience: 0,
          accepted: true,
          match: false
        },
        {
          id: 54,
          title: 'Russian',
          experience: 0,
          accepted: true,
          match: false
        }
      ],
      location: null,
      rate: {
        minimum: 4,
        maximum: 15,
        type: 0
      },
      unavailable_periods: [],
      unlocked: true,
      favourite: false,
      unlocked_at: '2018-10-31T12:55:50.606948Z',
      availability_updated: false,
      availability_requested_at: null,
      availability_updated_at: '2018-10-30T11:48:12.465287Z',
      can_favourite: true,
      search_percentage: null,
      rating: 0,
      bio: null,
      last_login: '2018-10-09T19:55:10.582741Z',
      created_at: '2018-10-09T20:55:10Z',
      updated_at: '2018-10-30T11:48:12Z',
      postcode: 'WC2N 5DU',
      unavailable_weekends: true,
      always_available: false,
      on_hold: false,
      vetted: false,
      vetting_in_progress: false,
      score: 1
    }
  ]
};
