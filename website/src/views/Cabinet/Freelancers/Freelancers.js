import React from 'react';
import { Route } from 'react-router-dom';
import ControlPanel from 'components/Cabinet/ControlPanel';
import Result from 'components/Result';

class Freelancers extends React.Component {
  componentDidMount() {
    this.props.actions.fetchFavourites();
    this.props.actions.fetchUnlocks();
  }
  render() {
    const additionalMenu = [
      {
        title: 'Unlocked',
        id: 'unlocked'
      },
      {
        title: 'Saved',
        id: 'saved'
      }
    ];

    const { favouriteUser, requestAvailability, unfavouriteUser, unlockFreelancer } = this.props.actions;
    return (
      <div>
        <ControlPanel page={`freelancers`} additionalMenu={additionalMenu} />
        <Result
          data={this.props.freelancers}
          actions={{ favouriteUser, requestAvailability, unfavouriteUser, unlockFreelancer }}
        />
      </div>
    );
  }
}

export default Freelancers;
