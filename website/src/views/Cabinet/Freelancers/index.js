import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Freelancers from './Freelancers';
import { fetchFavourites, fetchUnlocks } from '../../../store/Freelancers/actions';
import { withRouter } from 'react-router';
import { getFreelancersByPathName } from '../../../store/Freelancers/selectors';
import { requestAvailability, unlockFreelancer} from "../../../store/Search/actions";
import {
    unfavouriteUser, favouriteUser
} from '../../../store/Freelancers/actions';
const mapStateToProps = (state, props) => {
  return {
    freelancers: getFreelancersByPathName(state, props.location)
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators({ fetchFavourites, fetchUnlocks,   favouriteUser,
          requestAvailability,
          unfavouriteUser,
          unlockFreelancer }, dispatch) };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Freelancers)
);
