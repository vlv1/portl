import React from 'react';
import { Route } from 'react-router-dom';
import Freelancers from './Freelancers';
import Settings from './Settings';
import Requests from './Requests';
import Profile from './Profile';

export default class Cabinet extends React.Component {
  componentDidMount() {
    this.props.getClientData();
  }
  render() {
    const { match } = this.props;
    return (
      <div>
        <Route path={`${match.url}/freelancers/`} render={() => <Freelancers />} />
        <Route path={`${match.url}/requests/`} render={() => <Requests />} />
        <Route path={`${match.url}/profile/`} render={() => <Profile />} />
        <Route path={`${match.url}/settings/`} render={() => <Settings />} />
      </div>
    );
  }
}
