import React from 'react';
import { Route } from 'react-router-dom';
import ControlPanel from 'components/Cabinet/ControlPanel';
import ChangePassword from '../../../components/ChangePassword/ChangePassword';

const Settings = ({ match }) => {
  return (
    <div>
      <ControlPanel />
      <ChangePassword />
    </div>
  );
};

export default Settings;
