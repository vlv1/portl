import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Cabinet from './Cabinet';
import { getClientData } from '../../store/User/actions';
import { getCabinetData } from '../../store/User/selectors';

const mapStateToProps = state => {
  return { cabinet: getCabinetData(state) };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ getClientData }, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cabinet);
