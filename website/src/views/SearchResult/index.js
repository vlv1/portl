import { connect } from 'react-redux';
import SearchResult from './SearchResult';

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = () => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchResult);
