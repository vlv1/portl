import React from 'react';
import { BackButton } from 'components/Buttons';
import FreelancerPage from 'components/FreelancerPage';

export default class SearchResult extends React.Component {
  render() {
    return (
      <div>
        <BackButton />
        <FreelancerPage />
      </div>
    );
  }
}
