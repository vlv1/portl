import React from 'react';
export const getLocationsOutput = (locations, short) => {
  if (!locations) return '';
  if (short) return locations[0].name;
  return locations.map(l => l.name).join(', ');
};

export const getRateOutput = rate => {
  const rate_types = {
    '0': 'hour',
    '1': 'day'
  };
  return `£${rate.minimum}-${rate.maximum} / ${rate_types[rate.type]}`;
};

export const getDatasetOutput = (_data, shortView = false) => {
  if (!shortView) {
    return `${_data.map(e => e.title).join(', ')}`;
  }
  const data = _data.slice();
  const displayedSkills = data.splice(0, 3);
  const delimiter = data.length === 0 ? '.' : <span>... +{data.length}</span>;

  return (
    <>
      {displayedSkills.map(e => e.title).join(', ')}{delimiter}
    </>
  );
};
