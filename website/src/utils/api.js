import request from './request';

const api = {
  search: async query =>
    request(`search/`, {
      method: 'GET',
      query
    }),
    locations: async query =>
        request(`search/autocomplete/location/`, {
            method: 'GET'
        }),
    payments: {
      packages: {
          get: async () =>
              request(`payments/credit_packages/`, {
                  method: 'GET'
              }),
      },
        payments: {
          get: async () =>
              request(`payments/payment/`, {
                    method: 'GET'
              }),
            post: async (data) =>
                request(`payments/payment/`, {
                    method: 'POST',
                    body: data
                }),
        },
        stripe_pay: async (token) =>
            request(`payments/stripe_pay/`, {
                method: 'POST',
                body: token
            })
    },
  authentication: {
    login: async (email, password) =>
      request(`onboard/login/`, {
        method: 'POST',
        body: { email, password }
      }),
    registration: async fields =>
      request(`onboard/client/`, {
        method: 'POST',
        body: fields
      }),
    resetPassword: async email =>
      request('onboard/reset-password/', {
        method: 'POST',
        body: { email }
      })
  },
  profile: {
    me: {
      get: async () =>
        request('profile/me/', {
          method: 'get'
        }),
      patch: async data =>
        request('profile/me/', {
          method: 'PATCH',
          body: data
        })
    },
    get: async id =>
      request(`profile/${id}/`, {
        method: 'get'
      }),
    favourites: {
      get: async (page) =>
        request(`profile/favourites/?page=${page}`, {
          method: 'GET'
        })
    },
    unlocks: {
      get: async (page) =>
        request(`profile/unlocks/?page=${page}`, {
          method: 'get'
        })
    },
    favourite: {
      get: async id =>
        request(`profile/${id}/favourite/`, {
          method: 'get'
        }),
      set: async id =>
        request(`profile/${id}/favourite/`, {
          method: 'post'
        }),
      delete: async id =>
        request(`profile/${id}/favourite/`, {
          method: 'delete'
        })
    },
    unlock: async id =>
      request(`profile/${id}/unlock/`, {
        method: 'post'
      }),
    availability: async id =>
      request(`profile/${id}/availability/`, {
        method: 'post'
      }),
    changePassword: async data =>
      request('profile/change_password/', {
        method: 'put',
        body: data
      }),
    requests: async () =>
      request('profile/requests/', {
        method: 'get'
      }),
    purchaseHistory: async () =>
      request('profile/statistics/', {
        method: 'get'
      }),
    rate: async (id, data) =>
      request(`profile/${id}/rate/`, {
        method: 'post',
        body: data
      }),
    new_view: async (id, data) =>
      request(`profile/${id}/new_view/`, {
        method: 'post',
        body: data
      })
  }
};

export default api;
