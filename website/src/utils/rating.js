export const roundRating = (rating) => Number(Math.round(rating+'e1')+'e-1');
export const getImageContainerWidth = (roundedRating) => ((roundedRating / 5) * 80).toFixed(2) + 'px';