function encodeQueryData(data) {
  return Object.keys(data).reduce((acc, key) => {
    return data[key]
      ? acc.concat(`${encodeURIComponent(key)}=${encodeURIComponent(data[key])}&`)
      : acc;
  }, '?');
}

export { encodeQueryData };
