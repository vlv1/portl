import url from 'url';
import fp from 'lodash/fp';
import { encodeQueryData } from './query';
import Cookies from 'js-cookie';

const API_ROOT = '/api/v1/';

function parseJSON(response) {
  if (response.status === 204 || response.status === 205) {
    return null;
  }
  return response.json();
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.status);
  error.response = response;
  throw error;
}

const request = async (path, customOptions) => {
  const TOKEN = Cookies.get('Token');
  const Authorization = TOKEN ? { Authorization: `Token ${TOKEN}` } : {};
  const defaultOptions = {
    headers: {
      'Access-Control-Allow-Origin': '*',
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...Authorization
    },
    credentials: 'include'
  };
  const options = fp.merge(defaultOptions, customOptions);
  let query = '';
  if (
    options.method.toUpperCase() === 'GET' &&
    options.query &&
    encodeQueryData(options.query) !== '?'
  ) {
    query = encodeQueryData(options.query);
  }
  const apiUrl = url.resolve(API_ROOT, path + query);

  if (options.method.toUpperCase() === 'POST' && options.body) {
    options.body = JSON.stringify(options.body);
  }

  return fetch(apiUrl, options)
    .then(checkStatus)
    .then(parseJSON);
};

export default request;
