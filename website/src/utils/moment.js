import moment from 'moment';
import 'moment/locale/en-gb';

moment.locale('en-gb', {});
moment.updateLocale('en-gb', {
    weekdaysMin: 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_')
});

export default moment