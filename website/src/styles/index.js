import { initGlobalStyles } from './global';
import theme from './theme';
import { gradient, borders, shadows } from './mixins';

export { initGlobalStyles, theme, gradient, borders, shadows };
