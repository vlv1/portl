const colors = {
  white: '#ffffff',
  black5: 'rgba(0, 0, 0, 0.05)',
  black8: 'rgba(0, 0, 0, 0.08)',
  black16: 'rgba(0, 0, 0, 0.16)',
  black48: 'rgba(0, 0, 0, 0.48)',
  black56: 'rgba(0, 0, 0, 0.56)',
  black88: 'rgba(0, 0, 0, 0.88)',
  grey: '#f2f2f3',
  gradient: {
    start: '#F97722',
    center: '#F81A38',
    end: '#F928C3'
  }
};

export default colors;
