import { borders, shadows } from './mixins';
import colors from './colors';

const theme = {
  colors,
  borders,
  shadows
};

export default theme;
