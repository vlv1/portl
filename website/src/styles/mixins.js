import { css } from 'styled-components';
import colors from './colors';

const gradient = css`
  background: linear-gradient(
    45deg,
    ${colors.gradient.start} 0%,
    ${colors.gradient.center} 50%,
    ${colors.gradient.end} 100%
  );
`;

const borders = {
  input8: css`
    border: 1px solid ${colors.black8};
  `,
  input16: css`
    border: 1px solid ${colors.black16};
  `,
  gradientColoredBorder: css`
    position: relative;
    border: 1px solid transparent;
    background: white;
    background-clip: padding-box;

    &:after {
      position: absolute;
      top: -1px;
      bottom: -1px;
      z-index: -1;
      background: linear-gradient(
        45deg,
        ${colors.gradient.start},
        ${colors.gradient.center} 50%,
        ${colors.gradient.end} 100%
      );
      content: '';
      border-radius: 4px;
      left: -1px;
      right: -1px;
    }
  `,
  gradientBorder: css`
    border-width: 1px;
    border-style: solid;
    border-image: linear-gradient(to bottom, ${colors.black8}, ${colors.black16}) 100%;
  `,
  whiteShadow: css`
    background: linear-gradient(180deg, #ffffff 81.77%, rgba(255, 255, 255, 0) 0%);
  `
};

const shadows = {
  searchInput: css`
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  `,
  flatButton: css`
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.15);
  `,
  box: css`
    box-shadow: 0px 24px 40px rgba(0, 0, 0, 0.14);
  `,
  absoluteBox: css`
    box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.16);
  `
};

export { gradient, borders, shadows };
