import styled from 'styled-components';

const PaymentMethodWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 38px 48px;
  width: 504px;
  .title {
    color: rgba(0, 0, 0, 0.88);
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 25px;
  }
`;

const ButtonsPanel = styled.div`
  display: flex;
  justify-content: flex-end;
  text-align: center;
    > div:first-child {
      margin-right: 16px;
    }
`;

export { PaymentMethodWrapper, ButtonsPanel };