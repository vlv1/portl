import React from 'react';
import { PaymentMethodWrapper, ButtonsPanel } from './styles';
import { GradientButton, WhiteButton } from 'components/Buttons';

const PaymentMethod = ({ actions }) => (
  <PaymentMethodWrapper>
    <p className={'title'}>Choose a payment method</p>
    <p>Stripe</p>
    <p>PayPal</p>
    <ButtonsPanel>
      <WhiteButton onClick={() => actions.modalShow('CREDITS')} small>Back</WhiteButton>
      <GradientButton small onClick={() => actions.modalShow('PAYMENT_SUCCESS')}>
        Continue
      </GradientButton>
    </ButtonsPanel>
  </PaymentMethodWrapper>
);

export default PaymentMethod;
