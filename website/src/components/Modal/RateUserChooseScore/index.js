import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { modalShow, modalClose } from 'store/Modal/actions';
import {rateFreelancer} from "../../../store/Profiles/actions";
import RateUserChooseScore from "./RateUserChooseScore";

const mapStateToProps = state => {
  const user  = state.modal.data;
  return { user }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ modalShow, modalClose, rateFreelancer }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RateUserChooseScore)