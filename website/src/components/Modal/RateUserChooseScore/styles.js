import styled from "styled-components";
  const RateUserChooseScoreWrapper = styled.div`
    display: flex;
    flex-direction: column;
    padding: 38px 48px;
    width: 504px;
    .title {
      color: rgba(0, 0, 0, 0.88);
      font-size: 18px;
      font-weight: bold;
      margin-bottom: 27px;
    }
    .message {
      margin-bottom: 8px;
    }
    span {
      font-weight: 600;
    }
`;

const ButtonsPanel = styled.div`
  display: flex;
  justify-content: flex-end;
  text-align: center;
  margin-bottom: 6px;
  > div:first-child {
      margin-right: 16px;
  }
`;

export { RateUserChooseScoreWrapper, ButtonsPanel }