import React, { useState } from 'react';
import { RateUserChooseScoreWrapper, ButtonsPanel } from './styles';
import { GradientButton, WhiteButton } from 'components/Buttons';
import HollowStar from 'assets/SIngl Star Default 68x68.svg'
import Stars from 'components/Stars';
import api from "../../../utils/api";

const RateUserChooseScore = ({ actions, user }) => {
  const [score, setScore] = useState(undefined);
  const {username} = user;
  const handleClick = () => {
      if(score) {
          actions.rateFreelancer(user.id, score)
      }
      actions.modalClose()
  }
  return(
    <RateUserChooseScoreWrapper>
      <p className={'title'}>Rate the freelancer</p>
      <p className={'message'}>Please rate your experience working with <span>{user.username}</span></p>
      <Stars score={score} setScore={setScore} />
      <ButtonsPanel>
        <WhiteButton small onClick={() => actions.modalShow('RATE_USER_PROJECT_DATE', {...user})}>Back</WhiteButton>
        <GradientButton small onClick={handleClick}>
          Finish
        </GradientButton>
      </ButtonsPanel>
    </RateUserChooseScoreWrapper>
  )
};

export default RateUserChooseScore;
