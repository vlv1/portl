import React from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import {CreditsPurchaseWrapper, ButtonsPanel, Option, Select, Line} from './styles';
import { GradientButton, WhiteButton } from 'components/Buttons';
import api from "../../../utils/api";
import StripeForm from "../../Payment/StripeForm";
import PaypalExpressBtn from "../../Payment/PayPalCheckout";
import StripeCheckout from 'react-stripe-checkout';
import PayPalLogo from 'assets/PayPal.svg'
import CardsLogo from 'assets/cards.svg'

class CreditsPurchase extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            packages: [],
            selectedPackage: 0,
            step: 0,
            selectedPayment: 0
        };
        this.stripe = React.createRef();
        this.paypal = React.createRef();
    }
    async componentDidMount() {
        const packages = await api.payments.packages.get();
        this.setState({packages})
    }

    render() {
        const {actions} = this.props;
        const onToken = async (token) => {
            const selectedPackage = this.state.packages[this.state.selectedPackage];
            try {
                const payment = await api.payments.stripe_pay({token: token.id, amount: selectedPackage.amount, currency: selectedPackage.currency})
                const status = payment.status === "succeeded" ? "success" : "new";
                const resPayment = await api.payments.payments.post({type: 'stripe', amount:  selectedPackage.amount, currency: selectedPackage.currency, status, credits:  selectedPackage.amount});
                actions.modalShow('PAYMENT_SUCCESS');
                console.log('data', payment);
            } catch (e) {
                console.log(e)
            }

        };

        const onSuccess = async (payment) => {
            const selectedPackage = this.state.packages[this.state.selectedPackage];
            try {
                const resPayment = await api.payments.payments.post({type: 'stripe', amount:  selectedPackage.amount, currency: selectedPackage.currency, status: "success", credits:  selectedPackage.amount});
                actions.modalShow('PAYMENT_SUCCESS');
                console.log('data', resPayment);
            } catch (e) {
                console.log(e)
            }

        }


        const showModal = () => console.log(document.querySelector('.paypal-button-number-0'));

        const {step} = this.state;
        const activePackage = (index) => index === this.state.selectedPackage;
        const activePayment = (index) => index === this.state.selectedPayment;
        const selectedPackage = this.state.packages[this.state.selectedPackage];
        console.log(selectedPackage);
        return <CreditsPurchaseWrapper>
            {
                step === 0 ? <p className={'title'}>Select a credit package</p> : null
            }
            {
                step === 1 ? <p className={'title'}>Choose a payment method</p> : null
            }

            {
                step === 0
                    ? <div>
                        {this.state.packages.map((e, index) =>
                            <Option active={activePackage(index)} onClick={() => this.setState({selectedPackage: index})}>
                                <Select active={activePackage(index)}/>
                                <Line active={activePackage(index)}>
                                    <div className="amount">{e.amount} {e.amount === 1 ? 'credit' : 'credits'}</div>
                                    <div className="credit">£{e.price / e.amount} per credit</div>
                                    <div className="price">£{e.price}</div>
                                </Line>
                            </Option>
                        )}
                    </div>
                    : null
            }
            {
                step === 1
                    ? <div>
                        <Option active={activePayment(0)} onClick={() => this.setState({selectedPayment: 0})}>
                            <Line active={activePayment(0)}>
                                <PaypalExpressBtn client={{
                                    sandbox:    'AezQPbobJ0xMilm9xavDJ2QxSZ3HNAH6QbOyMAKREag_dZzYSohRE4DaPvbGOQhw_JMT4fbzgFzAKYsX',
                                    production: 'Your-Production-Client-ID',
                                }} env={'sandbox'} currency={selectedPackage.currency} onSuccess={onSuccess} total={100*selectedPackage.price} />

                            </Line>
                        </Option>
                        <Option active={activePayment(1)} onClick={() => this.setState({selectedPayment: 1})}>
                            <Line active={activePayment(1)}>
                                <StripeCheckout
                                    token={onToken}
                                    amount={100*selectedPackage.price}
                                    currency={selectedPackage.currency}
                                    stripeKey="pk_test_9mWlaC3VgN0Iyxb7Ts8SyXNg"
                                    ComponentClass={'stripe-button'}
                                ><img src={CardsLogo} alt=""/></StripeCheckout>


                            </Line>
                        </Option>
                 </div>

                : null
            }

            <ButtonsPanel>
                <WhiteButton small onClick={() => actions.modalClose()}>Cancel</WhiteButton>
                {
                    step !== 1 ? <GradientButton small onClick={() => this.setState(state => ({step: state.step + 1}))}>
                        Continue
                    </GradientButton> : null
                }
            </ButtonsPanel>

        </CreditsPurchaseWrapper>
    }
}

export default CreditsPurchase;
