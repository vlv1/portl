import styled from 'styled-components';

const CreditsPurchaseWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 38px 48px;
  width: 504px;
  .title {
    color: rgba(0, 0, 0, 0.88);
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 27px;
  }
`;

const ButtonsPanel = styled.div`
  display: flex;
  justify-content: flex-end;
  text-align: center;
  margin-top: 34px;
    > div:first-child {
      margin-right: 16px;
    }
`;

const Option = styled.div`
    font-weight: ${props => props.active ? '600': '300' };
    color: ${props => props.active ? 'rgba(0, 0, 0, 0.88)' : 'rgba(0, 0, 0, 0.48)'};
    font-size: 16px;
    cursor: pointer;
     margin-bottom: 24px;
    display: flex;
    align-items: center;
`;

const Select = styled.div`
    display: inline-block;
    position: relative;
    width: 22px;
    height: 22px;
    border: solid 1px rgba(0, 0, 0, 0.16);
    border-radius: 50%;
    margin-right: 12px;

    &:after {
        opacity: ${props => props.active ? '1': '0' }
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        width: 10px;
        height: 10px;
        background-color: rgba(0, 0, 0, 0.88);
        border-radius: 50%;
        content: '';
    }
`;

const Line = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
    .amount {
        width: 82px;
    }
    .credit {
        width: 136px;
    }
    .price {
        width: 82px;
    }
`;

export { CreditsPurchaseWrapper, ButtonsPanel, Option, Select, Line };
