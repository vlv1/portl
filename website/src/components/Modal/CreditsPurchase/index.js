import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { modalClose, modalShow } from 'store/Modal/actions';
import { modalTypes } from '../../../store/Modal/constants';
import CreditsPurchase from './CreditsPurchase';

const mapStateToProps = state => {
  const { open, modalType } = state.modal;

  return {
    open,
    modalType
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ modalClose, modalShow }, dispatch)
});

export default
connect(
  mapStateToProps,
  mapDispatchToProps
)(CreditsPurchase);
