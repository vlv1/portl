import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { modalShow, modalClose } from 'store/Modal/actions';
import RateUserProjectDate from './RateUserProjectDate';

const mapStateToProps = state => {
  const user = state.modal.data;
  return { user }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ modalShow, modalClose }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RateUserProjectDate)