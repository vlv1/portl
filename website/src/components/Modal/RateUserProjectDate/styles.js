import styled from "styled-components";

const RateUserProjectDateWrapper = styled.div`
    display: flex;
      flex-direction: column;
      padding: 38px 48px;
      width: 504px;
      .title {
        color: rgba(0, 0, 0, 0.88);
        font-size: 18px;
        font-weight: bold;
        margin-bottom: 27px;
    }
      .message {
        margin-bottom: 32px;
      }
`;

const ButtonsPanel = styled.div`
  display: flex;
  justify-content: flex-end;
  text-align: center;
  margin-bottom: 6px;
    > div:first-child {
      margin-right: 16px;
    }
`;

const Input = styled.input`
  width: 100%;
  height: 48px;
  border: 1px solid rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  padding-left: 12px;
  outline: none;
  font-family: 'Muli';
  font-weight: bold;
  font-size: 16px;
  color: ${props => props.theme.colors.black56};
  margin-bottom: 72px;
`;

export { RateUserProjectDateWrapper, ButtonsPanel, Input }