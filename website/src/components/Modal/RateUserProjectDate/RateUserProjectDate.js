import React, { useState } from 'react';
import { RateUserProjectDateWrapper, ButtonsPanel, Input } from './styles';
import { GradientButton, WhiteButton } from 'components/Buttons';

const RateUserProjectDate = ({ actions, user }) => {
    const [dateInput, setDateInput] = useState('');
    return (
        <RateUserProjectDateWrapper>
        <p className={'title'}>Rate the freelancer</p>
        <p className={'message'}>When did the project finish?</p>
        <Input type={'text'}
           placeholder={'Date, e.g 24/06/2018'}
           value={dateInput}
           onChange={(e) => setDateInput(e.target.value)}
        />
        <ButtonsPanel>
            <WhiteButton small onClick={() => actions.modalShow('RATE_USER', { user })}>Back</WhiteButton>
            <GradientButton small onClick={() => actions.modalShow('RATE_USER_CHOOSE_SCORE', { ...user })}>
                Continue
            </GradientButton>
        </ButtonsPanel>
    </RateUserProjectDateWrapper>
    )
};

export default RateUserProjectDate;
