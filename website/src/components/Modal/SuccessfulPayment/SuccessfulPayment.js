import React from 'react';
import { SuccessWrapper, ButtonsPanel, SuccessIcon } from './styles';
import { GradientButton } from 'components/Buttons';
import icon from 'assets/Success Chack Icon 112x112.png';

const SuccessfulPayment = ({ actions }) => (
  <SuccessWrapper>
    <p className={'title'}>Successful payment!</p>
    <SuccessIcon src={icon} alt="" />
    <p className={'message'}>
      Thank you for choosing us. Your credits were accrued. Good luck with headhunting!
    </p>
    <ButtonsPanel>
      <GradientButton small onClick={() => actions.modalClose()}>
        OK
      </GradientButton>
    </ButtonsPanel>
  </SuccessWrapper>
);

export default SuccessfulPayment;
