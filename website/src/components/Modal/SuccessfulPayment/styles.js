import styled from 'styled-components';

const SuccessWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 38px 48px;
  width: 504px;
  .title {
    color: rgba(0, 0, 0, 0.88);
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 35px;
  }
  .message {
    font-size: 16px;
    text-align: center;
    font-weight: 300;
    color: rgba(0, 0, 0, 0.88);
    line-height: 1.5;
    margin-top: 32px
    margin-bottom: 64px;
  }
`;

const ButtonsPanel = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;
`;

const SuccessIcon = styled.img`
  width: 112px;
  height: 112px;
`;

export { SuccessWrapper, ButtonsPanel, SuccessIcon };
