import React from 'react';
import {SignInWrapper} from './styles';
import Login from "components/Login";

export default  ({ actions }) => (
    <SignInWrapper>
        <Login modal={true}></Login>
    </SignInWrapper>
);;
