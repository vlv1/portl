import styled from 'styled-components';

const ModalOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10;
  background-color: rgba(255, 255, 255, 0.8);
`;

const ModalContainer = styled.div`
  position: absolute;
  top: 100px;
  left: 50%;
  transform: translate(-50%, 0);
  z-index: 2;
  box-shadow: 0 24px 40px 0 rgba(0, 0, 0, 0.14);
  background-color: #ffffff;
  border-style: solid;
  border-width: 1px;
  border-image-source: linear-gradient(to bottom, rgba(0, 0, 0, 0.08), rgba(0, 0, 0, 0.16));
  border-image-slice: 1;
`;

const CloseButton = styled.img`
  position: absolute;
  top: 34px;
  right: 38px;
  cursor: pointer;
`;

export { ModalOverlay, ModalContainer, CloseButton };
