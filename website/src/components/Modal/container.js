import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { modalClose, modalShow } from 'store/Modal/actions';
import { modalTypes } from '../../store/Modal/constants';

const mapStateToProps = state => {
  const { open, modalType } = state.modal;

  return {
    open,
    modalType
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ modalClose, modalShow }, dispatch)
});

export default component =>
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(component);
