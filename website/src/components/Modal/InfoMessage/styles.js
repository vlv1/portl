import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 38px 48px;
  width: 504px;
  .status {
    color: rgba(0, 0, 0, 0.88);
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 19px;
  }
  .message {
    font-size: 16px;
    font-weight: 300;
    color: rgba(0, 0, 0, 0.88);
    line-height: 1.5;
    margin-bottom: 43px;
  }
`;

const ButtonsPanel = styled.div`
  display: flex;
  flex-direction: row-reverse;
`;

export { Wrapper, ButtonsPanel };
