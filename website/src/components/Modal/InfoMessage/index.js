import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { modalClose } from 'store/Modal/actions';
import InfoMessage from './InfoMessage';

const mapStateToProps = state => {
  const { status, message } = state.modal.data;
  return {
    status, message
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ modalClose }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(InfoMessage);
