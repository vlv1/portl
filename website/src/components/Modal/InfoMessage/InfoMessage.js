import React from 'react';
import { Wrapper, ButtonsPanel } from './styles';
import { GradientButton } from 'components/Buttons'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { modalClose, modalShow } from 'store/Modal/actions';

const mapStateToProps = state => {
    const { open, modalType, data } = state.modal;

    return {
        open,
        modalType,
        data
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({ modalClose, modalShow }, dispatch)
});



const InfoMessage = ({ data: {status, message}, actions }) => (
  <Wrapper>
    <p className={'status'}>{status}</p>
    <p className={'message'}>{message}</p>
    <ButtonsPanel>
      <GradientButton small onClick={actions.modalClose}>
        OK
      </GradientButton>
    </ButtonsPanel>
  </Wrapper>
);
export default
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(InfoMessage);