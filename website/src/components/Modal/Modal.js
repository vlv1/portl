import React from 'react';
import container from './container';
import { ModalContainer, ModalOverlay, CloseButton } from './styles';
import close from 'assets/modalClose.svg';
import types from 'utils/modalTypes';
import Error from 'components/Error';
import SuccessfulPayment from './SuccessfulPayment';
import CropImage from '../EditProfile/CropImage';
import CreditsPurchase from './CreditsPurchase';
import PaymentMethod from './PaymentMethod';
import InfoMessage from './InfoMessage';
import RateUserHire from './RateUserHire';
import RateUserProjectDate from './RateUserProjectDate';
import RateUserChooseScore from './RateUserChooseScore';
import Login from "./Login";
import SignUp from "./SignUp";

const ModalContent = ({ modalType, actions }) => {
  switch (modalType) {
    case types.ERROR:
      return <Error />;
    case types.CREDITS:
      return <CreditsPurchase />;
    case types.PAYMENT_METHOD:
      return <PaymentMethod />;
    case types.INFO_MESSAGE:
      return <InfoMessage />;
    case types.RATE_USER_HIRE:
      return <RateUserHire />;
    case types.RATE_USER_PROJECT_DATE:
      return <RateUserProjectDate />;
    case types.RATE_USER_CHOOSE_SCORE:
      return <RateUserChooseScore />;
    case types.PAYMENT_SUCCESS:
      return <SuccessfulPayment />;
    case types.PHOTO_EDIT:
      return <CropImage />;
    case types.SIGN_IN:
      return <Login/>;
    case types.SIGN_UP:
      return <SignUp/>;
  }
};

const Close = ({ modalType, ...props }) => {
  const withIcon = [
    types.PHOTO_EDIT,
    types.CREDITS,
    types.PAYMENT_METHOD,
    types.SIGN_UP,
    types.SIGN_IN,
    types.RATE_USER_HIRE,
    types.RATE_USER_PROJECT_DATE,
    types.RATE_USER_CHOOSE_SCORE
  ];
  return withIcon.includes(modalType) ? <CloseButton {...props} /> : null;
};

const Modal = ({ open, modalType, actions: { modalClose, modalShow } }) =>
  open ? (
    <ModalOverlay onClick={modalClose}>
      <ModalContainer onClick={e => e.stopPropagation()}>
        <Close src={close} modalType={modalType} onClick={modalClose} />
        <ModalContent modalType={modalType} actions={{modalClose}}/>
      </ModalContainer>
    </ModalOverlay>
  ) : null;

export default container(Modal);
