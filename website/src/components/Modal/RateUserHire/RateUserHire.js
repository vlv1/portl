import React from 'react';
import { RateUserHireWrapper, ButtonsPanel } from './styles';
import { GradientButton, WhiteButton } from 'components/Buttons';

const RateUserHire = ({ actions, user}) => (
    <RateUserHireWrapper>
        <p className={'title'}>Rate the freelancer</p>
        <p className={'message'}>Did you hire <span>{user.username}</span>?</p>
        <ButtonsPanel>
            <WhiteButton small onClick={() => actions.modalClose()}>No</WhiteButton>
            <GradientButton small onClick={() => actions.modalShow('RATE_USER_PROJECT_DATE', { ...user })}>
                Yes
            </GradientButton>
        </ButtonsPanel>
    </RateUserHireWrapper>
);

export default RateUserHire;
