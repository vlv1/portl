import React from 'react';
import {SignInWrapper} from './styles';
import Registration from "components/Registration";

export default  ({ actions }) => (
    <SignInWrapper>
        <Registration modal={true}></Registration>
    </SignInWrapper>
);;
