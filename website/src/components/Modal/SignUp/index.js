import SignUp from './SignUp';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { modalClose, modalShow } from 'store/Modal/actions';

const mapStateToProps = state => {
    const { } = state.modal;

    return {};
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({ modalClose, modalShow }, dispatch)
});

export default connect(
        mapStateToProps,
        mapDispatchToProps
    )(SignUp);
