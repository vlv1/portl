import React from 'react';
import { TermsOfServiceWrapper } from "./styles";

const TermsOfService = () => (
  <TermsOfServiceWrapper>
    <p className={'title'}>Terms of Service</p>
    <p className={'misc'}>Last modified: 17 October 2018</p>
    <div className={'rule'}>
      <h2>Statement of Rights and Responsibilities</h2>
      <p>This Statement of Rights and Responsibilities derives from the Portl Team Principles, and is our terms of use that governs our relationship with users and others persons who interact with <span>“Portl App”</span>.</p>
    </div>
    <div className={'rule'}>
      <h2>1. Privacy</h2>
      <p>Your privacy is very important to us. We designed our <a href={'#'}>Privacy Policy</a> to make important disclosures about how you can use your content and informations.</p>
    </div>
    <div className={'rule'}>
      <h2>2. Safety</h2>
      <p>We do our best to keep Portl App safe, but we cannot guarantee it. We need your help to keep Portl App safe, which includes the following commitments by you:</p>
      <div>
        <p>1. You will not solicit login information or access an account belonging to someone else.</p>
        <p>2. You will not use Portl App to do anything unlawful, misleading, malicious, or discriminatory.</p>
        <p>3. And so on...</p>
      </div>
    </div>
  </TermsOfServiceWrapper>
);

export default TermsOfService