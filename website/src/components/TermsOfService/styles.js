import styled from 'styled-components';

const TermsOfServiceWrapper = styled.div`
  width: 840px;
  color: rgba(0, 0, 0, 0.88);
  font-size: 18px;
  .title {
    font-size: 40px;
    font-weight: bold;
    margin-bottom: 22px;
  }
  .misc {
    color: rgba(0, 0, 0, 0.48);
    font-weight: 300;
    margin-top: 22px;
  }
  .rule {
    h2 {
      font-size: 24px;
      font-weight: bold;
      margin: 26px 0 7px 0;
    }
    > p {
      line-height: 1.33;
    }
    a {
      color: #f81a38;
    }
    span {
      font-weight: 600;
    }
    div {
      margin-top: 32px;
      padding-left: 48px;
    }
    div > p {
      margin-bottom: 38px;
    }
  }
`

export { TermsOfServiceWrapper }