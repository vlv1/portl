import React from 'react';
import styled from 'styled-components';
import ResultItem from './ResultItem';
import InfiniteScroll from 'react-infinite-scroller';

const ResultWrapper = styled.div`
  position: relative;
`;

const FilterOverlay = styled.div`
  position: absolute;
  top: -10px;
  bottom: 0;
  right: 0;
  left: 0;
  background-color: rgba(255, 255, 255, 0.8);
  z-index: 1;
`;

const NoFound = styled.p`
  font-weight: 300;
`;

class ResultsView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasMore: props.data.length === 10
        }
    }
    componentDidUpdate(prevProps) {
        if(prevProps.data.length <  this.props.data.length) this.setState({hasMore: true})
     }

 render() {
     const {data, actions} = this.props;
     return <InfiniteScroll pageStart={0} loadMore={actions.loadMore} hasMore={this.state.hasMore}>
         {data.map(user => <ResultItem actions={actions} {...this.props} key={user.id} user={user} />)}
     </InfiniteScroll>
 }
}

const Result = ({ data = [], isFetching, isFilterActive = false, actions, ...props }) => {
    if(isFetching) return <p>Loading...</p>;
    if(data.length === 0 ) return <NoFound>No freelancers were found</NoFound>
    return <ResultsView data={data} actions={actions} {...props}/>
};

export default (props) => <ResultWrapper>
    { props.isFilterActive ? <FilterOverlay /> : null}
    <Result {...props}/>
</ResultWrapper>


