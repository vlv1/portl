import React from 'react';
import styled from 'styled-components';
import HollowRatingIcon from 'assets/Rating Icons Default  80x24.svg';
import FilledRatingIcon from 'assets/RatingIcons.svg';
import verify from 'assets/verify.svg';
import locked from 'assets/locked.svg';
import avatarEmpty from 'assets/avatar-empty-small.svg';
import availability from 'assets/availability.svg';
import save from 'assets/save.svg';
import saved from 'assets/saved.svg';
import { roundRating, getImageContainerWidth } from "utils/rating";
import { Link } from 'react-router-dom';
import { getDatasetOutput, getLocationsOutput, getRateOutput } from 'utils/dataFormating';
import GradientButton from '../Buttons/GradientButton';
import { FunctionalButton, FunctionalButtonWrapper } from 'components/Buttons';
import DividerDot from '../Buttons/DividerDot';
import { CalendarPopup } from './styles';
import Calendar from '../Calendar/AvailabilityCalendar';
import {modalShow} from "../../store/Modal/actions";
import Rating from '../Rating';
const enhanceWithClickOutside = require('react-click-outside');

const ResultLink = styled(Link)`
  text-decoration: none;
`;

const Score = styled.div`
  display: flex;
  align-items: center;
  font-weight: bold;
  color: ${props => props.theme.colors.black88};
  line-height: 24px;
  font-size: 18px;
  margin-left: 8px;
  position: relative;
    > div {
      margin-right: 80px;
    }
    div.hollowRating {
      height: 24px;
    }
    div.hollowRating, div.filledRating {
      top: 0;
      position: absolute;
    }
    div.filledRating img {
      transform: scaleX(0.96);
    }
    div.filledRating {
      top: -0.5px;
      left: 1.5px;
      width: ${props => props.imageContainerWidth};
      overflow: hidden;
      height: 24px;
    }
`;

const AvatarColumn = styled.div`
  transform: translateY(-10px);
  box-sizing: border-box;
  position: relative;
  display: flex;
  align-items: center;
  width: 46px;
  font-weight: bold;
  color: ${props => props.theme.colors.black88};
  line-height: 23px;
  font-size: 18px;
  ${props =>
    !props.unlocked
      ? `  &:after {
    position: absolute;
    background-image: url("${locked}");
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 14px;
    height: 17px;
    background-size: cover;
    content: '';
  }`
      : ''} img {
      object-fit: cover;
       width: 42px;
       height: 42px;
    border-radius: 50%;
    ${props => props.theme.borders.input16};
  }
`;

const ResultColumn = styled.div`
  margin-left: 12px;
  width: 100%;
`;

const ResultItem = styled.div`
  width: 840px;
  position: relative;
  display: flex;
  align-items: start;
  margin-bottom: 50px;
  cursor: pointer;

  &:hover ${FunctionalButton} {
    opacity: 1;
  }
`;

const ResultHeading = styled.div`
  display: flex;
  align-items: center;
  line-height: 24px;
  font-size: 18px;
  font-weight: 300;
  margin-bottom: 11px;
  color: ${props => props.theme.colors.black48};
  .group {
    display: flex;
    align-items: center;
  }
  .locations {
    line-height: 24px;
  }
`;
const ResultName = styled.span`
  font-weight: bold;
  font-size: 18px;
  color: ${props => props.theme.colors.black88};
`;

const ResultInfo = styled.div`
  display: flex;
  align-items: center;
  font-size: 16px;
  line-height: normal;
  font-weight: bold;
  color: ${props => props.theme.colors.black88};
  margin-bottom: 3px;
`;

const ResultInfoGroup = styled.div`
  margin-bottom: 13px;
`;

const ResultDescription = styled.div`
  position: relative;
  display: flex;
  margin-bottom: 3px;
  align-items: baseline;
  font-weight: 300;
  line-height: normal;
  font-size: 16px;
  color: ${props => props.theme.colors.black88};
  span {
    font-weight: bold;
  }
  .date {
    font-family: Muli;
    font-size: 16px;
    font-weight: 600;
    color: rgba(0, 0, 0, 0.88);
  }
  .status {
    font-size: 16px;
    font-weight: 600;
    color: #139419;
  }
`;
const ResultDescriptionName = styled.p`
  display: inline-block;
  font-size: 14px;
  font-weight: 600;
  color: ${props => props.theme.colors.black48};
  margin-right: 10px;
`;

const HeadingWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: start;
`;

const Unlock = styled(GradientButton)`
  position: absolute;
  right: 0;
  top: 50%;
  transform: translateY(-50%);
  opacity: 0;
  ${ResultItem}:hover & {
    opacity: 1;
  }
`;
const Rate = styled(Unlock)`
  ${ResultItem}:hover & {
    opacity: 1;
  }
`

class ResultItemClass extends React.Component {
  state = {
    showCalendar: false
  };

  handleClickOutside() {
    this.setState(prevState => ({ showCalendar: false }));
  }

  render() {
    const {
      user: {
        id,
        rating,
        locations,
        job_title,
        user,
        key_skills,
        sectors,
        unlocked,
        favourite,
        unavailable_periods,
        unavailable_weekends,
        always_available,
        availability_updated_at,
        availability_updated,
          user: {
              avatar = avatarEmpty,
          },
          rate_minimum,
          rate_maximum,
          rate_type,

      },
      actions: { favouriteUser, unfavouriteUser, unlockFreelancer, requestAvailability, modalShow },
      type = ''
    } = this.props;
const  rate = {
          minimum: rate_minimum,
          maximum: rate_maximum,
          type: rate_type
      }
    return (
      <ResultItem>
        <AvatarColumn unlocked={unlocked}>
          <img src={avatar || avatarEmpty} alt="avatar" />
        </AvatarColumn>
        <ResultColumn>
          <HeadingWrapper>
            <ResultLink to={`/freelancer/${id}`}>
              <ResultHeading>
                <div className={'group'}>
                  <ResultName>
                    {unlocked ? `${user.first_name} ${user.last_name} ` : user.initials}{' '}
                  </ResultName>{' '}
                  <img src={verify} alt="verify icon" />
                </div>
                {locations ? <DividerDot light /> : null}
                <div className={'locations'}>{getLocationsOutput(locations, true)}</div>
              </ResultHeading>
            </ResultLink>
            <FunctionalButtonWrapper hidden={!this.state.showCalendar} saved={favourite}>
              <FunctionalButton
                onClick={() =>
                  this.setState(prevState => ({ showCalendar: !prevState.showCalendar }))
                }>
                <img src={availability} alt="" />
                See availability
                {this.state.showCalendar ? (
                  <Calendar
                    popup
                    {...{
                      unavailable_periods,
                      unavailable_weekends,
                      always_available,
                      availability_updated_at,
                      requestAvailability,
                      id
                    }}
                  />
                ) : null}
              </FunctionalButton>
              {!favourite ? (
                <FunctionalButton onClick={() => favouriteUser(id)}>
                  <img src={save} alt="" />
                  Save to favorites
                </FunctionalButton>
              ) : (
                <FunctionalButton id={'saved'} onClick={() => unfavouriteUser(id)}>
                  <img src={saved} alt="" />
                  Favourites
                </FunctionalButton>
              )}
            </FunctionalButtonWrapper>

          </HeadingWrapper>

          <ResultInfoGroup>
            <ResultInfo>
                {job_title.title}
                <Score imageContainerWidth={getImageContainerWidth(roundRating(rating))}>
                    <div>
                      <div className={'hollowRating'}>
                      <img src={HollowRatingIcon} alt="rating icon" />
                      </div>
                    <div className={'filledRating'}>
                      <img src={FilledRatingIcon} alt="rating icon"/>
                    </div>
                    </div>
                    <Rating rating={rating} />
                </Score>
            </ResultInfo>
            <ResultInfo>{getRateOutput(rate)}</ResultInfo>
          </ResultInfoGroup>
          {availability_updated && type === 'request' ? (
            <React.Fragment>
              <ResultDescription>
                <ResultDescriptionName>REQUESTED: </ResultDescriptionName>
                <span className="date">Today</span>
                <Rate small onClick={() => modalShow('RATE_USER_HIRE', { username: `${user.first_name} ${user.last_name}` })}>Rate Me</Rate>
              </ResultDescription>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <ResultDescription>
                <ResultDescriptionName>SKILLS: </ResultDescriptionName>
                {getDatasetOutput(key_skills, true)}
                {!unlocked ? (
                  <Unlock small onClick={() => unlockFreelancer(id)}>
                    Unlock
                  </Unlock>
                ) : <Rate small onClick={() => modalShow('RATE_USER_HIRE', { username: `${user.first_name} ${user.last_name}`, ...user, id: id })}>Rate Me</Rate>}
              </ResultDescription>
              <ResultDescription>
                <ResultDescriptionName>SECTORS: </ResultDescriptionName>
                {getDatasetOutput(sectors, true)}
              </ResultDescription>
            </React.Fragment>
          )}
        </ResultColumn>
      </ResultItem>
    );
  }
}
export default enhanceWithClickOutside(ResultItemClass);
