import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getResults, hasActiveFilter } from 'store/Search/selectors';
import { modalShow } from 'store/Modal/actions';
import Result from './Result';
import {
  favouriteUser,
  requestAvailability,
  unfavouriteUser,
  unlockFreelancer,
  searchRequest
} from '../../store/Search/actions';
import {getFreelancersFromResult} from "../../store/Search/selectors";

const mapStateToProps = state => ({
  data: getFreelancersFromResult(state),
  isFetching:   getResults(state).isFetching,
  hasMorePages:   getResults(state).hasMorePages,
  isFilterActive: hasActiveFilter(state)
});

const mapDispatchToProps =(dispatch, ownProps) => ({
  actions: {...ownProps.actions, ...bindActionCreators({
      loadMore: searchRequest,
      favouriteUser,
      unfavouriteUser,
      unlockFreelancer,
      requestAvailability,
      modalShow,
    }, dispatch)}
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Result);