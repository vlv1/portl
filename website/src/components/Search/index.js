import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCityInput, getSearchInput } from 'store/Search/selectors';
import Search from './Search';
import { handleFieldInput, cleanField, searchRequest } from 'store/Search/actions';
import { withRouter } from 'react-router';

const mapStateToProps = state => ({
  search: getSearchInput(state),
  city: getCityInput(state)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ handleFieldInput, cleanField, searchRequest }, dispatch)
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Search)
);