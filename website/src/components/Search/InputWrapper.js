import React from 'react';
import styled from 'styled-components';
import clear from 'assets/clear.svg';
import locationIcon from 'assets/location.svg';
import searchIcon from 'assets/search.svg';

const getFieldInfo = name => {
  if (name === 'city') {
    return {
      width: '198px',
      icon: locationIcon
    };
  }

  return {
    width: '680px',
    icon: searchIcon
  };
};

const InputWrapper = styled.div`
  display: flex;
  width: ${props => props.width};
`;

const InputIcon = styled.img`
  margin: 0 19px;
`;

const InputClear = styled.img`
  margin: 0 17px 0 5px;
  cursor: pointer;
`;

const Input = styled.input`
  width: 100%;
  height: 56px;
  border: none;
  outline: none;
  font-family: 'Muli';
  font-weight: bold;
  font-size: 18px;
  color: ${props => props.theme.colors.black88};
`;

export default ({
  field: { name, placeholder, value },
  actions: { handleFieldInput, cleanField },
  onKeyPress
}) => {
  const { width, icon } = getFieldInfo(name);
  return (
    <InputWrapper width={width}>
      <InputIcon src={icon} />
      <Input
        type="text"
        name={name}
        value={value}
        placeholder={placeholder}
        onKeyPress={onKeyPress}
        onChange={handleFieldInput}
      />
      {value && value.length && <InputClear src={clear} onClick={() => cleanField(name)} />}
    </InputWrapper>
  );
};
