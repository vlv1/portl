import styled from 'styled-components';

const Divider = styled.div`
  width: 1px;
  height: 40px;
  display: block;
  border: 1px ${props => props.theme.colors.black8} solid;
  margin-top: 8px;
`;

export default Divider;
