import React from 'react';
import styled from 'styled-components';
import { shadows, borders } from 'styles';
import { GradientButton } from 'components/Buttons';
import InputWrapper from 'components/Search/InputWrapper';
import Divider from './Divider';

const SearchContainer = styled.div`
  display: flex;
  margin-bottom: 32px;
`;

const Search = styled.div`
  margin-right: 16px;
  display: flex;
  ${shadows.searchInput} ${borders.input8};
  border-radius: 4px;
`;
const handleKeyPress = callback => e => {
  if (e.key === 'Enter') {
    callback();
  }
};

export default ({ search, city, actions }) => (
  <SearchContainer>
    <Search>
      <InputWrapper
        onKeyPress={handleKeyPress(actions.searchRequest)}
        field={search}
        actions={actions}
      />
      {/*<Divider />*/}
      {/*<InputWrapper width="198px" field={city} actions={actions} />*/}
    </Search>
    <GradientButton onClick={actions.searchRequest}>Search</GradientButton>
  </SearchContainer>
);
