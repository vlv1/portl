import React, { useState } from 'react';
import HollowStar from 'assets/SIngl Star Default 68x68.svg';
import FilledStar from 'assets/SIngl Star Achived 68x68.svg';
import { StarsWrapper, Star } from './styled';

export default ( {score, setScore} ) => {
  const [hoveredId, setHoveredId] = useState(undefined);
  return(
    <StarsWrapper>
      {
        Array(5).fill('', 0, 5).map((image, i) => {
          return (
            <Star src={i <= hoveredId || i < score ? FilledStar : HollowStar}
              alt=""
              key={i}
              onClick={() => setScore(i + 1)}
              onMouseEnter={() => setHoveredId(i)}
              onMouseLeave={() => setHoveredId(undefined)}
              className={'star'}
            />
          )
        })}
    </StarsWrapper>
  )
}