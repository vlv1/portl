import styled from 'styled-components';

const StarsWrapper = styled.div`
  text-align: center;
  margin-bottom: 71.7px;
`

const Star = styled.img`
  cursor: pointer;
`

export { StarsWrapper, Star }