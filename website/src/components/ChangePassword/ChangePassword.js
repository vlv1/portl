import React from 'react';
import styled from 'styled-components';
import Input from 'components/Input';
import { GradientButton } from 'components/Buttons';
import Error from '../Login/Error';
import api from 'utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { modalClose, modalShow } from 'store/Modal/actions';

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({ modalShow }, dispatch)
});


const RegistrationBox = styled.div`
  width: 408px;
  .title {
    font-size: 18px;
    font-weight: bold;
    color: rgba(0, 0, 0, 0.88);
    margin-bottom: 35px;
  }
  > div.fields {
    text-align: left;
    margin-bottom: 10px;
  }
  .fields {
    display: inline-block;
    width: 100%;
  }
  input {
    border: 1px solid rgba(0, 0, 0, 0.48);
  }
  p {
    cursor: pointer;
  }
  .terms {
    width: 100%;
    color: rgba(0, 0, 0, 0.88);
    font-size: 13px;
    font-weight: 300;
    margin-bottom: 29px;
    a {
      text-decoration: none;
      color: #f81a38;
    }
  }
  .error {
    margin-top: 20px;
    color: #f81a38;
  }
  .divider {
    height: 1px;
    background-color: rgba(0, 0, 0, 0.08);
    margin-top: 43px;
    margin-bottom: 32px;
  }

  .switch-page {
    font-size: 16px;
    font-weight: 600;
    text-align: center;
    a {
      text-decoration: none;
      color: #f81a38;
    }
  }
  .form-button {
    margin-bottom: 30px;
  }
`;

class ChangePassword extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      current_password: '',
      new_password: '',
      confirm_password: '',
      errorsState: []
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState(prevState => ({
      ...prevState,
      [name]: value
    }));
  }

  async handleSubmit(e) {
    e.preventDefault();
    try {
      const answer = await api.profile.changePassword(JSON.stringify({ ...this.state }));
        this.props.actions.modalShow('INFO_MESSAGE', {status: 'Info', message: 'Password successfully updated'})
      console.log(answer);
    } catch (e) {
      const errors = await e.response.json();
      this.setState({ errorsState: errors });
    }
  }

  render() {
    const { errors } = this.props;
    const { errorsState } = this.state;
    const { current_password, new_password, confirm_password } = this.state;
    return (
      <RegistrationBox>
        <p className={'title'}>Change password</p>
        {Object.keys(errorsState).length > 0 && errorsState.non_field_errors ? <Error text={errorsState.non_field_errors} /> : null}

        <div className={'fields'}>
          <Input
            name="current_password"
            type={'password'}
            error={errorsState.current_password}
            placeholder={'Current password'}
            value={current_password}
            onChange={this.handleChange}
          />
          <Input
            name="new_password"
            type={'password'}
            error={errorsState.new_password}

            placeholder={'New password'}
            value={new_password}
            onChange={this.handleChange}
          />
          <Input
            name="confirm_password"
            type={'password'}
            error={errorsState.confirm_password}

            placeholder={'Confirm password'}
            value={confirm_password}
            onChange={this.handleChange}
          />
        </div>

        <GradientButton className={'form-button'} onClick={this.handleSubmit} small>
          Save
        </GradientButton>

        <p className="terms">
          Forgot your password? <a href="">Reset via email.</a>.
        </p>
      </RegistrationBox>
    );
  }
}

export default connect(()=>({}), mapDispatchToProps)(ChangePassword);
