import styled from 'styled-components';
const ControlPanelWrapper = styled.div`
  margin-top: 50px;
`;
const MainPanel = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 26px;
`;
const Tabs = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
    div[name="more"] {
    position: relative;
    bottom: 25%;
    letter-spacing: 3px;
    }
`;
const Tab = styled.div`
  margin-right: 40px;
  font-size: 18px;
  font-weight: 600;
  color: ${props => (props.active === props.name || props.highlight ? `rgba(0, 0, 0, 0.48)` : `rgba(0, 0, 0, 0.88)`)};
`;

const Balance = styled.div`
  font-size: 18px;
  font-weight: 300;
  color: rgba(0, 0, 0, 0.88);
  cursor: pointer;
  span {
    font-weight: 600;
  }
`;
const Divider = styled.div`
  height: 1px;
  background-color: rgba(0, 0, 0, 0.08);
  margin-bottom: 32px;
`;
const AdditionalPanel = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row-reverse;

  margin-bottom: ${props => (props.additionalMenu ? '28px' : '0px')};
`;
const AdditionalTabs = styled.div`
  display: flex;
  align-items: center;
  ${Tab} {
    font-size: 16px;
    margin-right: 32px;
  }
`;
const CreditsPanel = styled.div`
  display: flex;
  > div {
    margin-left: 32px;
  }
`;
const PurchaseHistoryModal = styled.div`
  position: relative;
  display: flex;
  font-size: 16px;
  font-weight: 600;
  color: rgba(0, 0, 0, 0.88);
  align-items: center;
  cursor: pointer;

  img {
    margin-right: 4px;
  }
`;

export {
  ControlPanelWrapper,
  MainPanel,
  Tabs,
  Tab,
  Balance,
  Divider,
  AdditionalPanel,
  AdditionalTabs,
  CreditsPanel,
  PurchaseHistoryModal
};
