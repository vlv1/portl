import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import HistoryIcon from 'assets/history.svg';
import {
  ControlPanelWrapper,
  MainPanel,
  Tabs,
  Tab,
  Balance,
  Divider,
  AdditionalPanel,
  AdditionalTabs,
  CreditsPanel,
  PurchaseHistoryModal
} from './styles';
import GradientButton from 'components/Buttons/GradientButton';
import PurchaseHistory from 'components/PurchaseHistory';
import MoreOptions from 'components/MoreOptions';
import { withRouter } from 'react-router';
import { matchPath } from 'react-router';
import { modalShow, modalClose } from 'store/Modal/actions';

const ControlPanel = ({ cabinet, additionalMenu, page, history, actions }) => {
  const path = history.location.pathname.split('/');
  const pathArray = path.slice(path.indexOf('cabinet'));
  const tab = pathArray[1];
  const filter = pathArray[2] || 'confirmed';
  const [showHistory, setHistoryShow] = useState(false);
  const [showMoreOptions, setMoreOptionsShow] = useState(false);

  return (
    <ControlPanelWrapper>
      <MainPanel>
        <Tabs>
          <Link to={`/cabinet/freelancers/`}>
            <Tab active={tab} name={'freelancers'}>
              Freelancers
            </Tab>
          </Link>
          <Link to={`/cabinet/requests/`}>
            <Tab active={tab} name={'requests'}>
              Requests
            </Tab>
          </Link>
          <Link to={`/cabinet/profile/`}>
            <Tab active={tab} name={'profile'}>
              Profile
            </Tab>
          </Link>
          <Link to={`/cabinet/settings/`}>
            <Tab active={tab} name={'settings'}>
              Settings
            </Tab>
          </Link>
            <Tab name={'more'} highlight={showMoreOptions} onClick={() => setMoreOptionsShow(!showMoreOptions)}>
              ...
              { showMoreOptions ? <MoreOptions onClickOutside={() => setMoreOptionsShow(false)} /> : null }
            </Tab>
        </Tabs>

      </MainPanel>
      <Divider />
      <AdditionalPanel additionalMenu={additionalMenu}>
        <CreditsPanel>
          <PurchaseHistoryModal onClick={() => setHistoryShow(!showHistory)}>
            <img src={HistoryIcon} alt="" />
            Purchase history
            {
              showHistory ? <PurchaseHistory/> : null
            }
          </PurchaseHistoryModal>
          <GradientButton small onClick={() => actions.modalShow('CREDITS')}>Buy Credits</GradientButton>
        </CreditsPanel>
        {additionalMenu ? (
          <AdditionalTabs>
            {additionalMenu.map(item => (
              <Link to={`/cabinet/${page}/${item.id}`}>
                <Tab active={filter} name={item.id} key={item.id}>
                  {item.title}
                </Tab>
              </Link>
            ))}
          </AdditionalTabs>
        ) : null}
      </AdditionalPanel>
    </ControlPanelWrapper>
  );
};

export default withRouter(ControlPanel);
