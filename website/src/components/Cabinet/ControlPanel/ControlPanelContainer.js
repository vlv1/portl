import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ControlPanel from './ControlPanel';
import { getCabinetData } from '../../../store/User/selectors';
import { modalShow, modalClose } from '../../../store/Modal/actions';

const mapStateToProps = state => {
  return { cabinet: getCabinetData(state) };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({modalShow, modalClose}, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ControlPanel);
