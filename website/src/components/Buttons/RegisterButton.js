import React from 'react';
import styled from 'styled-components';
import { gradient, shadows } from 'styles/index';

const GradientButton = styled.div`
  ${gradient} ${shadows.flatButton}
  cursor: pointer;
  display: inline-block;
  border-radius: 4px;
  width: 100%;
  padding: 0 16px;

  div {
    display: flex;
    justify-content: center;
    align-items: center;
    height: ${props => (props.small ? '40px' : '56px')};
  }

  p {
    display: inline-block;
    font-size: ${props => (props.small ? '16px' : '18px')};
    line-height: 19px;
    color: #ffffff;
    font-weight: ${props => (props.small ? '600' : 'bold')};
  }
`;

export default ({ children, onClick, small, ...props }) => (
  <GradientButton small={small} onClick={onClick} {...props}>
    <div>
      <p>{children}</p>
    </div>
  </GradientButton>
);
