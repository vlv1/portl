import React from 'react';
import styled from 'styled-components';

const GradientEmptyButton = styled.div`
  ${props => props.theme.borders.gradientColoredBorder};
  cursor: pointer;
  display: inline-block;
  border-radius: 4px;
  padding: 0 16px;
  min-width: 120px;

  div {
    display: flex;
    align-items: center;
    justify-content: center;
    height: ${props => (props.small ? '40px' : '56px')};
  }

  p {
    display: inline-block;
    font-size: ${props => (props.small ? '16px' : '18px')};
    line-height: 19px;
    color: #f81a38;
    font-weight: ${props => (props.small ? '600' : 'bold')};
  }
`;

export default ({ children, onClick, small }) => (
  <GradientEmptyButton small={small} onClick={onClick}>
    <div>
      <p>{children}</p>
    </div>
  </GradientEmptyButton>
);
