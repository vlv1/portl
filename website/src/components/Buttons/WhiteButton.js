import React from 'react';
import styled from 'styled-components';

const WhiteButton = styled.div`
  cursor: pointer;
  display: inline-block;
  border-radius: 4px;
  min-width: ${props => props.width || (props.small ? '120px' : '144px')};
  padding: 0 16px;
  border: solid 1px rgba(0, 0, 0, 0.08);

  div {
    display: flex;
    align-items: center;
    height: ${props => (props.small ? '40px' : '56px')};
  }

  p {
    width: 100%;
    display: inline-block;
    font-size: ${props => (props.small ? '16px' : '18px')};
    line-height: 19px;
    color: rgba(0, 0, 0, 0.56);
    font-weight: ${props => (props.small ? '600' : 'bold')};
  }
`;

export default ({ children, small, ...props }) => (
  <WhiteButton small={small} {...props}>
    <div>
      <p>{children}</p>
    </div>
  </WhiteButton>
);
