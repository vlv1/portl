import React from 'react';
import styled from 'styled-components';

const SignInButton = styled.div`
  cursor: pointer;
  display: inline-block;

  div {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 40px;
  }

  p {
    display: inline-block;
    font-size: 16px;
    line-height: 19px;
    font-weight: 600;
    color: ${props => props.theme.colors.black88};
  }
`;

export default ({ children, onClick, small }) => (
  <SignInButton small={small} onClick={onClick}>
    <div>
      <p>{children}</p>
    </div>
  </SignInButton>
);
