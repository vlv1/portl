import styled from 'styled-components';

export const FunctionalButton = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  font-size: 16px;
  font-weight: 600;
  line-height: 1.5;
  color: ${props => props.theme.colors.black88};
  margin-left: 28px;
  
  &#saved {
    opacity: 1;
  }
`;


export const FunctionalButtonWrapper = styled.div`
  display: flex;
  ${FunctionalButton} {
    opacity: ${props => props.hidden ? '0' :  '1'};
    };
  }
`;
