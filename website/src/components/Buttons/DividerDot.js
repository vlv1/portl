import styled from 'styled-components';

const DividerDot = styled.div`
  display: inline-block;
  width: 3px;
  height: 3px;
  margin: 0 8px;
  background-color: ${props => (props.light ? 'rgba(0, 0, 0, 0.48)' : 'rgba(0, 0, 0, 0.88)')};
  border-radius: 50%;
`;

export default DividerDot;
