import React from 'react';
import styled from 'styled-components';
import backIcon from 'assets/back.svg';
import { withRouter } from 'react-router-dom';

const BackButton = styled.a`
  cursor: pointer;
  display: inline-block;
  text-decoration: none;
  margin-bottom: 43px;

  img {
    margin-right: 10px;
  }

  p {
    display: inline-block;
    font-size: 16px;
    font-weight: 600;
    color: ${props => props.theme.colors.black88};
  }
`;

export default withRouter(({ history }) => (
  <BackButton onClick={history.goBack}>
    <img src={backIcon} alt="" />
    <p>Back to search results</p>
  </BackButton>
));
