import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { modalClose, modalShow } from 'store/Modal/actions';
import Error from './Error';

const mapStateToProps = state => {
  const { error, data } = state.modal;

  return {
    error,
    data
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ modalClose, modalShow }, dispatch)
});

export default
connect(
  mapStateToProps,
  mapDispatchToProps
)(Error);
