import React from 'react';
import { ErrorWrapper, ButtonsPanel } from './styles';
import { GradientButton } from 'components/Buttons';

const Error = ({ data, error, actions }) => (
  <ErrorWrapper>
    <p className={'title'}>Error</p>
    <p className={'message'}>Text</p>
    <ButtonsPanel>
      <GradientButton small onClick={actions.modalClose}>
        OK
      </GradientButton>
    </ButtonsPanel>
  </ErrorWrapper>
);

export default Error;
