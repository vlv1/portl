import React from 'react';
import styled from 'styled-components';
import Input from 'components/Input';
import Error from './Error';
import RegisterButton from '../Buttons/RegisterButton';

const LoginBox = styled.div`
  ${props => !props.modal ? `
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  `: ``}

  width: 408px;

  .title {
    font-size: 18px;
    font-weight: bold;
    color: rgba(0, 0, 0, 0.88);
    margin-bottom: 35px;
  }
  input {
    border: 1px solid rgba(0, 0, 0, 0.48);
  }
  p {
    cursor: pointer;
  }
  .error {
    margin-top: 20px;
    color: #f81a38;
  }
  .divider {
    height: 1px;
    background-color: rgba(0, 0, 0, 0.08);
    margin-top: 43px;
    margin-bottom: 32px;
  }

  .switch-page {
    font-size: 16px;
    font-weight: 600;
    text-align: center;
    a {
      text-decoration: none;
      color: #f81a38;
    }
  }
`;
class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const { email, password } = this.state;
    const { actions } = this.props;
    if (email && password) {
      actions.login(email, password);
    }
  }

  render() {
    const { errors, modal = false } = this.props;
    const { email, password } = this.state;
    return (
      <LoginBox modal={modal}>
        <p className={'title'}>Sign in to Portl</p>
        {errors &&  errors.non_field_errors && errors.non_field_errors.length > 0 ? (
          <Error text={errors.non_field_errors.map(e => `${e}`)} />
        ) : null}
        <div className="fields">
          <Input
            name="email"
            type={'text'}
            error={errors.email ? errors.email[0] : false}
            placeholder={'Email'}
            value={email}
            onChange={this.handleChange}
          />
          <Input
            name="password"
            type={'password'}
            error={errors.password ? errors.password[0] : false}

            placeholder={'Password'}
            value={password}
            onChange={this.handleChange}
          />
        </div>

        <RegisterButton onClick={this.handleSubmit}>Sign In</RegisterButton>
        <div className="divider" />
        <p className="switch-page">
          Don’t have an account? <a href="/registration/">Register</a>
        </p>
      </LoginBox>
    );
  }
}

export default Login;
