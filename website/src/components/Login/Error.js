import React, { useState } from 'react';
import styled from 'styled-components';
import warning from 'assets/warningMessage.svg';
const Error = styled.div`
  background-color: rgba(248, 26, 56, 0.05);
  display: flex;
  padding: 18px 50px;
  margin-bottom: 40px;
  .icon {
    margin-right: 15px;
  }
  .message {
    color: #f81a38;
    font-weight: 600;
  }
`;

export default function({ text }) {
  return (
    <Error>
      <img className={'icon'} src={warning} />
      <p className="message">{text}</p>
    </Error>
  );
}
