import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Login from './Login';
import { login } from 'store/User/actions';
import { getErrors } from 'store/User/selectors';

const mapStateToProps = state => ({
  errors: getErrors(state)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ login }, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);