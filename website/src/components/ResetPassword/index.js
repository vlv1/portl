import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { resetPassword } from 'store/User/actions';
import { getErrors } from 'store/User/selectors';
import ResetPassword from './ResetPassword';

const mapStateToProps = state => ({
  errors: getErrors(state)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ resetPassword }, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResetPassword);
