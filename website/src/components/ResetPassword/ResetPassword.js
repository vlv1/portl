import React, { Component } from 'react';
import Input from 'components/Input';
import { RegisterButton } from 'components/Buttons/';
import styled from 'styled-components';

const ResetPasswordBox = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 408px;
  .title {
    font-size: 18px;
    font-weight: bold;
    color: rgba(0, 0, 0, 0.88);
    margin-bottom: 19px;
  }
  .description {
    font-size: 16px;
    font-weight: 300;
    margin-bottom: 38px;
  }
  div {
    text-align: left;
  }
  .fields {
    display: inline-block;
    width: 100%;
    margin-bottom: 4px;
  }
  input {
    border: 1px solid rgba(0, 0, 0, 0.48);
  }
  p {
    cursor: pointer;
  }
  .error {
    margin-top: 20px;
    color: #f81a38;
  }
  .divider {
    height: 1px;
    background-color: rgba(0, 0, 0, 0.08);
    margin-top: 43px;
    margin-bottom: 32px;
  }
  .contact {
    text-align: center;
    font-weight: 600;
    a {
        text-decoration: none;
        color: #f81a38;
    }
  }
`;

class ResetPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({
      email: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const email = this.state.email;
    const { actions } = this.props;
    actions.resetPassword(email);
  }

  render() {
    const email = this.state.email;
    return (
      <ResetPasswordBox>
        <p className={'title'}>Forgot your password?</p>

        <p className={'description'}>Enter the email address linked to your account.</p>

        <div className={'fields'}>
          <Input
            name="email"
            type={'text'}
            placeholder={'Email'}
            value={email}
            onChange={this.handleChange}
          />
        </div>


        <RegisterButton onClick={this.handleSubmit}>Reset Password</RegisterButton>
        <div className="divider" />
        <p className="contact">
          If you need more help, just <a href="">contact us</a>
        </p>
      </ResetPasswordBox>
    );
  }
}

export default ResetPassword;
