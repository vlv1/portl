import React, {useState} from 'react';
import { DropDown, DropDownItem } from './styles';
import {compose, lifecycle} from "recompose";
import enhanceWithClickOutside from "react-click-outside";

const withOuterClick = compose(
  enhanceWithClickOutside,
  lifecycle({
    handleClickOutside() {
      console.log('here')
      this.props.onClickOutside();
    }
  })
);

const MoreOptions = ({onClickOutside}) => {
  return (
    <DropDown onClick={onClickOutside}>
      <div>
        <DropDownItem>Invite Friends</DropDownItem>
        <DropDownItem>Leave Feedback</DropDownItem>
        <DropDownItem>Partner Products</DropDownItem>
      </div>
    </DropDown>
  )
}

export default withOuterClick(MoreOptions);