import styled from 'styled-components';

const DropDown = styled.div`
  position: absolute;
  width: 232px;
  top: calc(100% + 21px);
  left: -26px;
  z-index: 2;
  background-color: #ffffff;
  border: double 1px transparent;
  background-image: linear-gradient(white, white),
    radial-gradient(circle at top, rgba(0, 0, 0, 0.08), rgba(0, 0, 0, 0.16));
  background-origin: border-box;
  background-clip: content-box, border-box;
  border-radius: 4px;
  box-shadow: 0 24px 40px 0 rgba(0, 0, 0, 0.14);
  letter-spacing: initial;
  > div {
    padding: 12px 24px 18px 24px;
    border-radius: 4px;
    background: #ffffff;
  }
  a {
    text-decoration: none;
  }
`;
const DropDownItem = styled.div`
  color: rgba(0, 0, 0, 0.88);
  text-decoration: none !important;
  font-size: 16px;
  font-weight: 600;
  line-height: 48px;
  cursor: pointer;
  &:hover {
    color: rgba(0, 0, 0, 0.48);
  }
`

export { DropDown, DropDownItem }