import React from 'react';
import { DropDown, DropDownItem, Divider } from '../styles';
import enhanceWithClickOutside from 'react-click-outside';
import { lifecycle, compose } from 'recompose';
import container from './DropDownСontainer';
import { userService } from '../../../services';
import { Link } from 'react-router-dom';

const withOuterClick = compose(
  enhanceWithClickOutside,
  lifecycle({
    handleClickOutside() {
      console.log('here')
      this.props.onClickOutside();
    }
  })
);

const logout = () => {
  userService.logout();
  window.location.reload();
};

const DropDownBlock = ({onClickOutside}) => (
  <DropDown onClick={onClickOutside}>
    <div>
      <Link to={`/cabinet/freelancers/`}>
        <DropDownItem>Freelancers</DropDownItem>
      </Link>
      <Link to={`/cabinet/requests/`}>
        <DropDownItem>Requests</DropDownItem>
      </Link>
      <Link to={`/cabinet/profile/`}>
        <DropDownItem>Profile</DropDownItem>
      </Link>
      <Link to={`/cabinet/settings/`}>
        <DropDownItem>Settings</DropDownItem>
      </Link>
      <Divider />
      <DropDownItem onClick={logout}>Sign out</DropDownItem>
    </div>
  </DropDown>
);

export default withOuterClick(DropDownBlock);
