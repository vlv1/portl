import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { login } from 'store/User/actions';
import { getCurrentUser } from 'store/User/selectors';
import { withRouter } from 'react-router';
import DropDown from './DropDown';

const mapStateToProps = state => ({
  user: getCurrentUser(state)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ login }, dispatch)
});

export default
  withRouter(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(DropDown)
  );
