import styled from 'styled-components';
import { borders } from '../../styles';
const Menu = styled.div`
  margin: 0 -18px;
  display: flex;
  align-items: center;
`;

const MenuItem = styled.div`
  position: relative;
  margin: 0 16px;
`;

const UserName = styled.p`
  color: ${props => props.theme.colors.black88};
  font-size: 16px;
  font-weight: 600;
  margin-left: 8px;
`;

const UserAvatar = styled.img`
  border-radius: 50%;
  ${borders.input16};
  width: 28px;
  height: 28px;
`;

const UserLabel = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
`;

const SavedText = styled.p`
  color: ${props => props.theme.colors.black88};
  font-size: 16px;
  font-weight: 600;
  margin-left: 4px;
  line-height: 24px;
`;

const SavedIcon = styled.img``;

const Saved = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
`;

const DropDown = styled.div`
  position: absolute;
  width: 232px;
  top: calc(100% + 28px);
  right: 0;
  z-index: 4;
  background-color: #ffffff;
  border: double 1px transparent;
  background-image: linear-gradient(white, white),
    radial-gradient(circle at top, rgba(0, 0, 0, 0.08), rgba(0, 0, 0, 0.16));
  background-origin: border-box;
  background-clip: content-box, border-box;
  border-radius: 4px;
  box-shadow: 0 24px 40px 0 rgba(0, 0, 0, 0.14);
  > div {
    padding: 12px 24px 18px 24px;
    border-radius: 4px;
    background: #ffffff;
  }
  a {
    text-decoration: none;
  }
`;
const DropDownItem = styled.div`
  color: rgba(0, 0, 0, 0.88);
  text-decoration: none !important;
  font-size: 16px;
  font-weight: 600;
  line-height: 48px;
  cursor: pointer;
  &:hover {
    color: rgba(0, 0, 0, 0.48);
  }
`;
const Divider = styled.div`
  width: 100%;
  height: 1px;
  display: block;
  border: 1px ${props => props.theme.colors.black8} solid;
`;
const Balance = styled.div`
  font-size: 16px;
  font-weight: 300;
  color: rgba(0, 0, 0, 0.88);
  cursor: pointer;
  span {
  margin-left: 5px;
    &.red {
      color: #f81a38;
    }
    font-weight: 600;
    text-decoration: underline;
  }
`;

export {
  UserName,
  UserAvatar,
  UserLabel,
  MenuItem,
  Menu,
  SavedText,
  SavedIcon,
  Saved,
  DropDown,
  DropDownItem,
  Divider,
    Balance
};
