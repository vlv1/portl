import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import UserLabel from './UserLabel';
import { login } from 'store/User/actions';
import { getCurrentUser } from 'store/User/selectors';
import { withRouter } from 'react-router';

const mapStateToProps = state => ({
  user: getCurrentUser(state)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ login }, dispatch)
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(UserLabel)
);
