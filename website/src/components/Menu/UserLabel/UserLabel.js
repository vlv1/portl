import React from 'react';
import defaultAvatar from 'assets/avatar-empty-small.svg';
import { UserName, UserAvatar, UserLabel } from '../styles';
import { userService } from '../../../services/index';

export default ({ user: { first_name, last_name, avatar }, ...props }) => (
  <UserLabel {...props}>
    {' '}
    <UserAvatar src={avatar || defaultAvatar} alt={''} />
    <UserName>{`${first_name} ${last_name}`}</UserName>
  </UserLabel>
);
