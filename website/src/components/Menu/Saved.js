import React from 'react';
import saved from 'assets/saved.svg';
import { Saved, SavedIcon, SavedText } from './styles';
import { Link } from 'react-router-dom';

export default () => (
  <Link to={'/cabinet/freelancers/saved'}>
    <Saved>
      <SavedIcon src={saved} />
      <SavedText>Favourites</SavedText>
    </Saved>
  </Link>
);
