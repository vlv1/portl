import React, { useState } from 'react';
import UserLabel from './UserLabel';
import { GradientEmptyButton, SignInButton } from 'components/Buttons';
import { Link } from 'react-router-dom';
import { MenuItem, Menu } from './styles';
import Saved from './Saved';
import DropDown from './DropDown';
import {Balance} from "./styles";

export default ({ isLoggedIn, cabinet }) => {
  const [showDropDown, setValue] = useState(false);
  const handleDropdownState = (value = false) => setValue(value);
  return (
    <Menu>
      {isLoggedIn ? (
        <>
            <MenuItem>
                { cabinet ? <Balance>
                    Balance:
                    {
                        cabinet.credits > 0
                          ? <span>{cabinet.credits} credits</span>
                          : <span className={cabinet.credits ? 'red' : ''}>0 credits</span>
                    }
                </Balance> : null }
            </MenuItem>
          <MenuItem>
            <Saved />
          </MenuItem>{' '}
          <MenuItem>
            <UserLabel onClick={() => handleDropdownState(true)} />
            {showDropDown ? <DropDown onClickOutside={() => handleDropdownState(false)} /> : null}
          </MenuItem>
        </>
      ) : null}
      {!isLoggedIn ? (
        <>
          <MenuItem>
            <Link to={'/login'}>
              <SignInButton small>Log In</SignInButton>
            </Link>
          </MenuItem>
          <MenuItem>
            <Link to={'/registration'}>
              <GradientEmptyButton small>Register</GradientEmptyButton>
            </Link>
          </MenuItem>
        </>
      ) : null}
    </Menu>
  );
};
