import React from 'react';
import Landing, {
  Title,
  SubTitle,
  Text,
  SocialLinksWrapper,
  SocialLink,
  ServiceLink,
  ServiceLinksWrapper,
  PrivasyPolicy,
  Footer
} from './Landing';
import { Link } from 'react-router-dom';
export const FooterBlock = () => (
  <Footer>
    <PrivasyPolicy>© 2018 Portl. All rights reserved.</PrivasyPolicy>
    <ServiceLinksWrapper>
      <ServiceLink>About</ServiceLink>
      <Link to={'/terms-of-service'} className={'serviceLink'}>
        Terms of Service
      </Link>
      <ServiceLink>Privacy Policy</ServiceLink>
    </ServiceLinksWrapper>
    <SocialLinksWrapper>
      <SocialLink>Twitter</SocialLink>
      <SocialLink>Instagram</SocialLink>
      <SocialLink>Facebook</SocialLink>
      <SocialLink>Linkedin</SocialLink>
    </SocialLinksWrapper>
  </Footer>
);

export default ({ children }) => (
  <Landing>
    <div>
      <Title>Find Freelancers</Title>
      <SubTitle>The fastest place to source quality freelancers</SubTitle>
      {children}
      <Text>
        Enter key words to begin your search. You'll be able to filter the results to refine your search.
      </Text>
    </div>
  </Landing>
);
