import styled from 'styled-components';

const Landing = styled.div`
  margin-top: 64px;
  display: flex;
`;

const Title = styled.h1`
  color: ${props => props.theme.colors.black88};
  font-weight: bold;
  line-height: 1.2;
  font-size: 40px;
`;

const SubTitle = styled.h2`
  color: ${props => props.theme.colors.black88};
  font-weight: 300;
  line-height: 1.2;
  font-size: 40px;
  margin-bottom: 80px;
`;

const Text = styled.p`
  color: ${props => props.theme.colors.black48};
  font-weight: 300;
  line-height: 1.5;
  font-size: 16px;
  margin-top: 72px;

  .italic {
    font-style: italic;
  }
`;

const ServiceLinksWrapper = styled.div`
  width: 296px;
  display: flex;
  justify-content: space-between;
`;

const ServiceLink = styled.a`
  cursor: pointer;
  font-weight: 300;
  line-height: 24px;
  font-size: 14px;
  color: ${props => props.theme.colors.black88};
`;
const SocialLinksWrapper = styled.div`
  width: 329px;
  display: flex;
  justify-content: space-between;
  .serviceLink {
    cursor: pointer;
    font-weight: 300;
    line-height: 24px;
    font-size: 14px;
    color: ${props => props.theme.colors.black88}
  }
`;

const SocialLink = styled.a`
  cursor: pointer;
  font-weight: 600;
  line-height: 24px;
  font-size: 14px;
  color: ${props => props.theme.colors.black88};
`;

const PrivasyPolicy = styled.p`
  color: ${props => props.theme.colors.black48};
  font-weight: 300;
  line-height: 24px;
  font-size: 14px;
`;

const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  vertical-align: center;
  margin-bottom: 62px;
`;
