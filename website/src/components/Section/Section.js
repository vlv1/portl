import styled from 'styled-components';

const Section = styled.div`
  width: calc(100% - 216px);
  margin: 0 auto;
`;

const SectionWithFooter = styled(Section)`
  display: flex;
  height: 100vh;
  justify-content: space-between;
  flex-direction: column;
`;

export { SectionWithFooter };
export default Section;
