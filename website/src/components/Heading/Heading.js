import styled from 'styled-components';

const Heading = styled.h1`
  font-size: 18px;
  font-weight: bold;
`;

export default Heading;
