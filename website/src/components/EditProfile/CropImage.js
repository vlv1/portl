import React from 'react';
import { Avatar, EditAvatarBlock, UploadButton, CropImageWrapper, ButtonWrapper } from './styles';
import AvatarEditor from 'react-avatar-editor';
import { GradientButton, WhiteButton } from 'components/Buttons';
import api from '../../utils/api';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { modalClose } from '../../store/Modal/actions';
import { saveClientChanges } from '../../store/User/actions';

const cropImg = preview => console.log(preview);

class CropImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { dataImage: localStorage.getItem('imgData') };

    this.editorRef = React.createRef();
  }

  saveAvatar = async () => {
    const canvas = this.editorRef.current.getImageScaledToCanvas().toDataURL();
    this.props.actions.saveClientChanges(JSON.stringify({ user: { avatar: canvas } }));
    this.props.actions.modalClose();
  };

  cancelEdit = () => {
    this.props.actions.modalClose();
  };

  render() {
    return (
      <CropImageWrapper>
        <p className={'title'}>Profile photo</p>
        <AvatarEditor
          ref={this.editorRef}
          border={80}
          width={250}
          height={250}
          borderRadius={125}
          className={'editorpic'}
          image={this.state.dataImage}
          alt=""
        />
        <ButtonWrapper>
          <GradientButton small onClick={() => this.saveAvatar()}>
            Apply
          </GradientButton>
          <WhiteButton small onClick={() => this.cancelEdit()}>
            Cancel
          </WhiteButton>
        </ButtonWrapper>
      </CropImageWrapper>
    );
  }
}

const mapStateToProps = state => {
  return { cabinet: {} };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators({ modalClose, saveClientChanges }, dispatch) };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CropImage);
