import React from 'react';
import styled from 'styled-components';
import Input from 'components/Input';
import { GradientButton } from 'components/Buttons';
import Error from '../Login/Error';
import EditAvatar from './EditAvatar';
import { modalShow } from '../../store/Modal/actions';
import api from 'utils/api';
import {CLIENT_DATA_UPDATE} from "../../store/User/constants";

const RegistrationBox = styled.div`
  width: 408px;
  .title {
    font-size: 18px;
    font-weight: bold;
    color: rgba(0, 0, 0, 0.88);
    margin-bottom: 35px;
  }
  > div {
    text-align: left;
    margin-bottom: 10px;
  }
  .fields {
    display: inline-block;
    width: 100%;
  }
  input {
    border: 1px solid rgba(0, 0, 0, 0.48);
  }
  p {
    cursor: pointer;
  }
  .terms {
    width: 100%;
    color: rgba(0, 0, 0, 0.88);
    font-size: 13px;
    font-weight: 300;
    margin-bottom: 29px;
    a {
      text-decoration: none;
      color: #f81a38;
    }
  }
  .error {
    margin-top: 20px;
    color: #f81a38;
  }
  .divider {
    height: 1px;
    background-color: rgba(0, 0, 0, 0.08);
    margin-top: 43px;
    margin-bottom: 32px;
  }

  .switch-page {
    font-size: 16px;
    font-weight: 600;
    text-align: center;
    a {
      text-decoration: none;
      color: #f81a38;
    }
  }
  .form-button {
    margin-bottom: 30px;
  }
`;

class EditProfile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        id: '',
        first_name: '',
        company_name: '',
        position: '',
        last_name: '',
        email: '',
        mobile: ''
      },
        errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const {
      id,
      company: { company_name },
      job_title: { title }
    } = this.props.userData;
    const {user} = this.props.userAuthData;
    this.setState({
      id,
      user,
      company_name,
      job_title: title
    });
  }

  handleChange(e) {
    const { name, value } = e.target;
    if (['company_name', 'job_title'].includes(name)) {
      this.setState(prevState => ({
        ...prevState,
        [name]: value
      }));
    }
    this.setState(prevState => ({
      ...prevState,
      user: {
        ...prevState.user,
        [name]: value
      }
    }));
  }

  async handleSubmit(e) {
    e.preventDefault();
    const {
      id,
      user: { first_name, last_name, email, mobile },
      company_name,
      job_title
    } = this.state;

    const updateData = {
      id,
      user: { first_name, last_name },
      job_title,
      company_name
    };
      try {
          this.props.actions.saveClientChanges(JSON.stringify(updateData));
          this.props.actions.modalShow('INFO_MESSAGE', {status: 'Info', message: 'User data successfully updated'})

      } catch (error) {
          const e = await error.response.json();
          this.setState({errors: e.errors})

      }
  }

  render() {
    const {

      actions: { modalShow }
    } = this.props;
    const {
      id,
      user: { first_name, last_name, email, mobile, id: userId },
      company_name,
      job_title,
        errors
    } = this.state;
      const {user: {avatar}} = this.props.userAuthData;
    return (
      <RegistrationBox>
        <EditAvatar img={avatar} modalShow={modalShow} />
        <div className={'fields'}>
            <form>
            <Input
            id="first_name"
            name="first_name"
            type={'text'}
            placeholder={'First name'}
            error={errors.first_name}
            value={first_name}
            onChange={this.handleChange}
          />
          <Input
              id="last_name"
            name="last_name"
              error={errors.last_name}

              type={'text'}
            placeholder={'Surname'}
            value={last_name}
            onChange={this.handleChange}
          />
          <Input
              id="company_name"
            name="company_name"
              error={errors.company_name}
            type={'text'}
            placeholder={'Company name'}
            value={company_name}
            onChange={this.handleChange}
          />
          <Input
              id="job_title"
            name="job_title"
              error={errors.job_title}
            type={'text'}
            placeholder={'Position'}
            value={job_title}
            onChange={this.handleChange}
          />
          <Input
              id="email"
            name="email"
              error={errors.email}
            type={'text'}
            placeholder={'Email'}
            value={email}
            onChange={this.handleChange}
          />
          <Input
              id="mobile"
            name="mobile"
              error={errors.mobile}
            type={'text'}
            placeholder={'Mobile'}
            value={mobile}
            onChange={this.handleChange}
          />
            </form>
        </div>

        <GradientButton className={'form-button'} onClick={this.handleSubmit} small>
          Save
        </GradientButton>
        <p className="terms">
          Forgot your password? <a href="">Reset via email.</a>.
        </p>
      </RegistrationBox>
    );
  }
}

export default EditProfile;
