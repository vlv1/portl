import styled from 'styled-components';

const EditAvatarBlock = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 44px !important;
  .editorpic {
    width: 408px;
    height: 408px;
  }
`;

const UploadButton = styled.div`
  width: 154px;
  position: relative;
  input {
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    opacity: 0;
    z-index: 2;
    cursor: pointer;
  }
  .hint {
    position: absolute;
    top: calc(100% + 11px);
    font-size: 13px;
    font-weight: 300;
    color: rgba(0, 0, 0, 0.56);
    white-space: nowrap;
  }
`;

const Avatar = styled.div`
  width: 120px;
  height: 120px;
  border: solid 1px rgba(0, 0, 0, 0.16);
  border-radius: 50%;
  position: relative;
  background-color: #f2f2f3;
  margin-right: 32px;

  canvas {
    position: absolute;
    top: 100%;
    left: 100%;
    opacity: 0;
  }

  .editorpic {
    position: absolute;
    z-index: 6;
    top: 100%;
    left: 100%;
  }

  img {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border-radius: 50%;
    width: 100%;
    height: 100%;
    &.empty {
      width: 80px;
      height: 80px;
    }
  }
`;

const CropImageWrapper = styled.div`
  padding: 38px 48px;
  width: 504px;
  .title {
    color: rgba(0, 0, 0, 0.88);
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 35px;
  }
`;

const ButtonWrapper = styled.div`
  margin-top: 44px;
  display: flex;
  flex-direction: row-reverse;
  > div {
    margin-left: 16px !important;
    text-align: center;
  }
`;

export { EditAvatarBlock, Avatar, UploadButton, CropImageWrapper, ButtonWrapper };
