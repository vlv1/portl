import React from 'react';
import { Avatar, EditAvatarBlock, UploadButton } from './styles';
import PersonIcon from 'assets/person.svg';
import PersonPhoto from 'assets/avatarExample.png';
import { WhiteButton } from 'components/Buttons';
import AvatarEditor from 'react-avatar-editor';
import { modalShow } from '../../store/Modal/actions';

class EditAvatar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imagePreview: false
    };
    this.canvasRef = React.createRef();
  }

  grayscaleImageData = imageData => {
    var data = imageData.data;
    for (var i = 0; i < data.length; i += 4) {
      var brightness = 0.34 * data[i] + 0.5 * data[i + 1] + 0.16 * data[i + 2];
      data[i] = brightness;
      data[i + 1] = brightness;
      data[i + 2] = brightness;
    }
    return imageData;
  };

  handleChange = e => {
    e.preventDefault();
    const file = e.target.files[0];
    const imageURL = URL.createObjectURL(file);
    let image = new Image();
    image.src = imageURL;
    const canvas = this.canvasRef.current;

    const ctx = canvas.getContext('2d');

    image.onload = () => {
      canvas.width = image.width;
      canvas.height = image.height;
      ctx.drawImage(image, 0, 0, image.width, image.height);
      const imageData = ctx.getImageData(0, 0, image.width, image.height);
      const grayscaleImage = this.grayscaleImageData(imageData);
      ctx.putImageData(grayscaleImage, 0, 0);
      this.setState({ imagePreview: canvas.toDataURL('image/jpeg') });
      localStorage.setItem('imgData', canvas.toDataURL('image/jpeg'));
      this.props.modalShow('PHOTO_EDIT');
    };
  };
  render() {
    return (
      <EditAvatarBlock>
        <Avatar>
          <canvas ref={this.canvasRef} />

          {this.props.img ? <img src={this.props.img} alt="" /> : null}
          {this.props.img ? (
            <img src={this.props.img} alt="" />
          ) : (
            <img className={'empty'} src={PersonIcon} alt="" />
          )}
        </Avatar>
        <UploadButton>
          <input id={'finput'} type="file" accept="image/*" onChange={e => this.handleChange(e)} />
          <WhiteButton small width={'154px'}>
            Upload a Photo
          </WhiteButton>
          <p className={'hint'}>Maximum upload size: 5 MB</p>
        </UploadButton>
      </EditAvatarBlock>
    );
  }
}

export default EditAvatar;
