import styled from 'styled-components';
import leftArrow from 'assets/arrow-left.svg';
import rightArrow from 'assets/arrow-active-right.svg';

const CustomCalendar = styled.div`
  .rc-calendar {
    outline: none;

    &-panel {
    }
    &-date-panel {
    }
    &-input {
      display: none;
    }
    &-header {
    }
    &-my-select {
      display: none;
      font-size: 18px;
      font-weight: bold;
      color: rgba(0, 0, 0, 0.88);
    }
    &-date-panel {
      display: flex;
      justify-content: space-between;
    }

    &-range-part {
      width: 308px;
    }
    &-body {
      margin-left: -12px;
    }
    &-cell {
      width: 44px;
      height: 44px;
      text-align: center;
      ${props =>
        !props.readonly
          ? `
      &:hover {
        background-color: rgba(0, 0, 0, 0.12);
      }`
          : ``};
    }
    &-next-month-btn-day {
      display: none;
      cursor: default;
    }
    &-last-month-cell {
      color: white !important;
      background: white !important;
      cursor: default;
      .crossout {
        display: none;
      }
    }

    &-column-header {
      font-weight: 300;
    }

    &-selected-day {
      ${props => (!props.readonly ? `background-color: rgba(0, 0, 0, 0.12)` : '')};
      border-radius: 4px;
    }

    &-disabled-cell {
      color: rgba(0, 0, 0, 0.32);
    }
    &-in-range-cell {
      ${props => (!props.readonly ? `background-color: rgba(0, 0, 0, 0.04)` : '')};
    }
    &-date {
      height: 44px;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    &-inner {
      width: 24px;
      height: 24px;
    }

    &-date-not-available {
      position: relative;
      &:before {
        content: '';
        border-bottom: 1px solid #1f1f1f;
        width: 100%;
        height: 2px;
        background: #ffffff;
        position: absolute;
        right: 0;
        top: 50%;
        transform: skewY(45deg) translate(-1px, -1px);
      }
    }

    &-range-middle {
      display: none;
    }

    &-prev-month-btn {
      position: absolute;
      left: 600px;
      top: -62px;
      width: 8.5px;
      height: 16px;
      background: url(${rightArrow});
      transform: rotate(180deg);
      color: black;
      font-size: 24px;
      &:after {
        content: '';
      }
    }
    &-next-month-btn {
      position: absolute;
      right: 0;
      top: -62px;
      width: 8.5px;
      height: 16px;
      color: black;
      font-size: 24px;
      background: url(${rightArrow});

      &:after {
        content: '';
      }
    }
  }
`;

const Header = styled.div`
  font-size: 18px;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.88);
  margin-bottom: 42px;
  display: flex;
  justify-content: space-between;
  p {
    width: 308px;
  }
`;

const CalendarPopup = styled.div`
  width: ${props => (props.popup ? `763px` : `660px`)};
  position: relative;

  z-index: 2;
  background-color: #ffffff;

  ${props => (!props.popup ? `` : ``)} ${props =>
    props.popup
      ? `
    position: absolute;
  top: -40px;
  left: calc(100% + 16px);
  border-style: solid;
  border-width: 1px;
  border-image-source: linear-gradient(to bottom, rgba(0, 0, 0, 0.08), rgba(0, 0, 0, 0.16));
  border-image-slice: 1;
    box-shadow: 0 24px 40px 0 rgba(0, 0, 0, 0.14);
  padding: 38px 60px;

  `
      : ``}
  &:after, &:before {
    right: 100%;
    top: 50px;
    border: solid transparent;
    content: ' ';
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
  }

  &:after {
    border-color: rgba(255, 255, 255, 0);
    border-right-color: #ffffff;
    border-width: 8px;
    margin-top: -8px;
  }
  &:before {
    border-color: rgba(0, 0, 0, 0);
    border-right-color: rgba(0, 0, 0, 0.08);
    border-width: 9px;
    margin-top: -9px;
  }
`;

const CalendarControls = styled.div`
  display: flex;
  flex-direction: row-reverse;
  justify-content: space-between;
  margin-top: 40px;
  align-items: center;
`;

const InfoBlocksWrapper = styled.div``;

const ControlsWrapper = styled.div`
  display: flex;
  align-items: center;
  > div {
    margin-left: 24px;
  }
`;

const LastUpdate = styled.div`
  font-size: 16px;
  font-weight: 300;
  color: rgba(0, 0, 0, 0.48);
`;

const InfoBlock = styled.div`
  display: flex;
  width: 200px;
  font-size: 16px;
  font-weight: 300;
  color: rgba(0, 0, 0, 0.88);
  img {
    margin-right: 8px;
  }
  p {
    white-space: nowrap;
  }
`;

export {
  Header,
  CalendarPopup,
  CalendarControls,
  InfoBlock,
  ControlsWrapper,
  InfoBlocksWrapper,
  LastUpdate
};
export default CustomCalendar;
