import React from 'react';
import CustomCalendar, { Header } from './styles';
import CalendarComponent from 'rc-calendar/lib/RangeCalendar';
import moment from 'utils/moment';

const defaultCalendarProps = {
  minDetail: 'month',
  locale: 'en-EN',
  selectRange: true,
  showNeighboringMonth: false
};

const formatDate = date => {
  return new Intl.DateTimeFormat('default', {
    month: 'long',
    year: 'numeric'
  }).format(date);
};

function disabledDate(current) {
  const date = moment();
  date.hour(0);
  date.minute(0);
  date.second(0);
  return current.isBefore(date); // can not select days before today
}

class Calendar extends React.Component {
  constructor(props) {
    super(props);
    const currentTime = moment();
    this.state = {
      startValue: currentTime,
      endValue: currentTime.clone().add(1, 'month'),
      range: []
    };
  }

  onChange = date => {
    if (date[0] && date[1]) {
      if (date[0].isBefore(this.state.startValue)) {
        this.setState(prevState => ({
          startValue: date[0] || prevState.startValue,
          endValue: date[0].clone().add(1, 'month') || prevState.endValue
        }));
      } else if (date[1].isAfter(this.state.endValue)) {
        this.setState(prevState => ({
          startValue: date[1].clone().subtract(1, 'month') || prevState.startValue,
          endValue: date[1] || prevState.endValue
        }));
      }
    }
  };

  onSelect = date => {
    this.setState({ range: date });
    this.props.onSelect(date);
  };

  render() {
    return (
      <CustomCalendar>
        <div>
          <Header>
            <p>{formatDate(this.state.startValue)}</p>
            <p>{formatDate(this.state.endValue)}</p>
          </Header>
        </div>
        <CalendarComponent
          disabledDate={disabledDate}
          onChange={this.onChange}
          onSelect={this.onSelect}
          defaultValue={moment()}
          format={'MMM'}
          showToday={false}
          value={[this.state.startValue, this.state.endValue]}
          onValueChange={this.onChange}
        />
      </CustomCalendar>
    );
  }
}
export default Calendar;
