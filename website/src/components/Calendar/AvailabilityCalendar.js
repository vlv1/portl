import React from 'react';
import CustomCalendar, {
  LastUpdate,
  CalendarControls,
  CalendarPopup,
  Header,
  InfoBlock,
  ControlsWrapper,
  InfoBlocksWrapper
} from './styles';
import CalendarComponent from 'rc-calendar/lib/RangeCalendar';
import moment from 'utils/moment';
import alert from 'assets/alert.svg';
import GradientButton from '../Buttons/GradientButton';

const defaultCalendarProps = {
  minDetail: 'month',
  locale: 'en-EN',
  selectRange: true,
  showNeighboringMonth: false
};

const formatDate = date => {
  return new Intl.DateTimeFormat('default', {
    month: 'long',
    year: 'numeric'
  }).format(date);
};

function disabledDate(current) {
  const date = moment();
  date.hour(0);
  date.minute(0);
  date.second(0);
  return current.isBefore(date); // can not select days before today
}

class Calendar extends React.Component {
  constructor(props) {
    super(props);
    const currentTime = moment();
    this.state = {
      startValue: currentTime,
      endValue: currentTime.clone().add(1, 'month'),
      range: []
    };
  }

  onChange = date => {
    if (date[0] && date[1]) {
      if (date[0].isBefore(this.state.startValue)) {
        this.setState(prevState => ({
          startValue: date[0] || prevState.startValue,
          endValue: date[0].clone().add(1, 'month') || prevState.endValue
        }));
      } else if (date[1].isAfter(this.state.endValue)) {
        this.setState(prevState => ({
          startValue: date[1].clone().subtract(1, 'month') || prevState.startValue,
          endValue: date[1] || prevState.endValue
        }));
      }
    }
  };

  onSelect = date => {
    this.setState({ range: date });
  };

  dateRender = (data, current, unavailable_periods = []) => {
    const notAvailable = unavailable_periods.some(e =>
      moment(data).isBetween(e.start_date, e.end_date, null, '[]')
    );
    return (
      <div className="rc-calendar-date" aria-selected="false" aria-disabled="false">
        <div
          className={`rc-calendar-inner crossout ${
            notAvailable ? 'rc-calendar-date-not-available' : ''
          }`}>
          {data.format('D')}
        </div>
      </div>
    );
  };

  disabledDate = date => {
    const current = moment();
    current.hour(0);
    current.minute(0);
    current.second(0);
    const day = date.format('d');
    const weekend = (this.props.unavailable_weekends && day === '0') || day === '6';
    return weekend || date.isBefore(current);
  };

  render() {
    const {
      unavailable_periods,
      popup,
      unavailable_weekends,
      always_available,
      availability_updated_at,
      requestAvailability,
      id
    } = this.props;
    return (
      <CalendarPopup popup={popup} onClick={e => e.stopPropagation()}>
        <CustomCalendar readonly={true}>
          <div>
            <Header>
              <p>{formatDate(this.state.startValue)}</p>
              <p>{formatDate(this.state.endValue)}</p>
            </Header>
          </div>
          <CalendarComponent
            dateRender={(current, data) => this.dateRender(current, data, unavailable_periods)}
            disabledDate={this.disabledDate}
            onChange={this.onChange}
            onSelect={this.onSelect}
            defaultValue={moment()}
            format={'MMM'}
            showToday={false}
            value={[this.state.startValue, this.state.endValue]}
            onValueChange={this.onChange}
          />
          <CalendarControls>
            <ControlsWrapper>
              <LastUpdate>
                {moment(availability_updated_at).format('MMMM, DD')} – Last update
              </LastUpdate>
              <GradientButton small onClick={() => requestAvailability(id)}>Request Update</GradientButton>
            </ControlsWrapper>
            <InfoBlocksWrapper>
              {unavailable_weekends ? (
                <InfoBlock>
                  <img src={alert} alt="" />
                  <p>Doesn't work on weekends.</p>
                </InfoBlock>
              ) : null}
            </InfoBlocksWrapper>
          </CalendarControls>
        </CustomCalendar>
      </CalendarPopup>
    );
  }
}
export default Calendar;
