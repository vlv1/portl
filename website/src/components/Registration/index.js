import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Registration from './Registration';
import { registration } from 'store/User/actions';
import { getErrors } from 'store/User/selectors';

const mapStateToProps = state => ({
  errors: getErrors(state)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ registration }, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Registration);