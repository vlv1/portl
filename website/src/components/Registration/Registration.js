import React from 'react';
import styled from 'styled-components';
import Input from 'components/Input';
import { RegisterButton } from 'components/Buttons';
import Error from '../Login/Error';
const RegistrationBox = styled.div`
  ${props => !props.modal ? `
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
 `: ''}

  width: 408px;
  .title {
    font-size: 18px;
    font-weight: bold;
    color: rgba(0, 0, 0, 0.88);
    margin-bottom: 35px;
  }
  div {
    text-align: left;
    
  }
  .fields {
    display: inline-block;
    width: 100%;
  }
  input {
    border: 1px solid rgba(0, 0, 0, 0.48);
  }
  p {
    cursor: pointer;
  }
  .terms {
    width: 100%;
    text-align: center;
    color: rgba(0, 0, 0, 0.88);
    font-size: 13px;
    font-weight: 300;
    margin-bottom: 29px;
    a {
      text-decoration: none;
      color: #f81a38;
    }
  }
  .error {
    margin-top: 20px;
    color: #f81a38;
  }
  .divider {
    height: 1px;
    background-color: rgba(0, 0, 0, 0.08);
    margin-top: 43px;
    margin-bottom: 32px;
  }

  .switch-page {
    font-size: 16px;
    font-weight: 600;
    text-align: center;
    a {
      text-decoration: none;
      color: #f81a38;
    }
  }
`;
const getErrorText = errors => {
  const profile = errors.profile
    ? Object.values(errors.profile).map(e => (typeof e[0] === 'string' ? `${e[0]}` : ''))
    : '';
  return Object.values(errors)
    .map(e => (typeof e[0] === 'string' ? `${e[0]}` : ''))
    .concat(' ')
    .concat(profile);
};

class Registration extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fields: {
        email: '',
        password: '',
        first_name: '',
        last_name: ''
      },
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState(prevState => ({
      fields: {
        ...prevState.fields,
        [name]: value
      }
    }));
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const {
      email,
      password,
      first_name,
      last_name
    } = this.state.fields;
    const registrationFields = {
      email,
      password,
      first_name,
      last_name,
        profile: 0
    };
    const { actions } = this.props;
    actions.registration(registrationFields);
  }

  render() {
    const { errors, modal = false } = this.props;
    const {
      email,
      password,
      first_name,
      last_name
    } = this.state.fields;
    return (
      <RegistrationBox modal={modal}>
        <p className={'title'}>Join Portl</p>
          {errors &&  errors.non_field_errors && errors.non_field_errors.length > 0 ? (
              <Error text={errors.non_field_errors.map(e => `${e}`)} />
          ) : null}

        <div className={'fields'}>
          <Input
            name="email"
            type={'text'}
            error={errors.email}
            placeholder={'Email'}
            value={email}
            onChange={this.handleChange}
          />
          <Input
            name="first_name"
            type={'text'}
            error={errors.first_name}

            placeholder={'First name'}
            value={first_name}
            onChange={this.handleChange}
          />
          <Input
            name="last_name"
            error={errors.last_name}

            type={'text'}
            placeholder={'Surname'}
            value={last_name}
            onChange={this.handleChange}
          />
          <Input
            name="password"
            type={'password'}
            error={errors.password}

            placeholder={'Create a password'}
            value={password}
            onChange={this.handleChange}
          />
        </div>

        <p className="terms">
          By proceeding you agree to our <a href="">Terms</a> &
          <a href="">Conditions</a>.
        </p>
        <RegisterButton onClick={this.handleSubmit}>Register</RegisterButton>
        <div className="divider" />
        <p className="switch-page">
          Already have an account? <a href="/login/">Log in</a>
        </p>
      </RegistrationBox>
    );
  }
}

export default Registration;
