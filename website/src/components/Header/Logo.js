import React from 'react';
import logo from 'assets/logo.png';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const LogoImage = styled.img`
  width: 63px;
  height: 25px;
`;

const Logo = () => (
  <Link to="/search/">
    <LogoImage src={logo} alt="" />
  </Link>
);
export default Logo;
