import React from 'react';
import styled from 'styled-components';
import Logo from './Logo';
import Menu from '../Menu';

const Header = styled.div`
  display: flex;
  align-items: center;
  margin: 25px 0;
  justify-content: space-between;
`;

export default props => (
  <Header>
    <Logo />
    <Menu {...props} />
  </Header>
)
