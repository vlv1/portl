import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Header from './Header';
import {getCabinetData, isLoggedIn} from 'store/User/selectors';

const mapStateToProps = state => ({
  isLoggedIn: isLoggedIn(state),
  cabinet: getCabinetData(state)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({}, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);