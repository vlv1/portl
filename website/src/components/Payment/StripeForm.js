import React, {Component} from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import api from "../../utils/api";
import StripeCheckout from 'react-stripe-checkout';

class StripeForm extends Component {
    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
    }

    async submit(ev) {
        const a = {
            amount: 1,
                currency: "GBP",
        id: 1,
        price: "5.00",
        };


        let {token} = await this.props.stripe.createToken({name: 'Test'});

        let response = await api.payments.stripe_pay({token: token.id, currency:a.currency, amount: a.amount });

        if (response.ok) console.log("Purchase Complete!")
    }

    render() {
        const styles = {
            base: {
                color: '#32325d',
                lineHeight: '18px',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        }
        return (
            <div className="checkout">
                <CardElement />
                <button onClick={this.submit}>Send</button>
            </div>
        );
    }
}

export default injectStripe(StripeForm);