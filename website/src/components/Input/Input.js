import React, { useState } from 'react';
import TextField, { HelperText, Input } from '@material/react-text-field';
import { OutlineField, CustomHelperText } from './styles';
import Visibility from 'assets/Visibility.svg';
import VisibilityOff from 'assets/VisibilityOff.svg';

export default function({ placeholder, type, error, ...props }) {
  const [hidePassword, setValue] = useState(type === 'password' ? true : null);
  const handleClickShowPassword = () => setValue(!hidePassword);
  const getTypeForPasswordField = (type, hidePassword) =>
    hidePassword === null ? type : hidePassword ? 'password' : 'input';
  return (
    <OutlineField error={error}>
      <TextField
        label={placeholder}
        helperText={
          <CustomHelperText error={error}>
            {error}
          </CustomHelperText>
        }
        outlined
        {...props}
        trailingIcon={
          hidePassword !== null ? (
            <img
              onClick={handleClickShowPassword}
              tabIndex="1"
              src={hidePassword ? Visibility : VisibilityOff}
              alt=""
            />
          ) : null
        }
        >
        <Input {...props}
               type={getTypeForPasswordField(type, hidePassword)} />
      </TextField>
    </OutlineField>
  );
}
