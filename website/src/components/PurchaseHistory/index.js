import { connect } from 'react-redux';
import { getPurchaseHistory } from '../../store/User/actions';
import PurchaseHistory from './PurchaseHistory';
import { bindActionCreators } from 'redux';


const mapDispatchToProps = dispatch => {
  return bindActionCreators({ getPurchaseHistory }, dispatch);
};

export default connect(mapDispatchToProps)(PurchaseHistory);
