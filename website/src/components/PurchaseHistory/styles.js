import styled from 'styled-components';

const PurchaseHistoryWrapper = styled.div`
  padding: 38px 48px;
  width: 544px;
`

const History = styled.div`
  display: flex;
  height: 333px;
  p {
    line-height: 2.5
    font-size: 16px;
  }
`;

const Column = styled.div`
  display: flex;
  flex-basis: 33.3%;
  flex-direction: column;
  color: rgba(0, 0, 0, 0.88);
    &.creditsList {
      font-weight: 600;
    }
    &.priceList, &.dateList {
      font-weight: 300;
    }
    &.priceList {
      align-items: center;
    }
    &.dateList {
      align-items: flex-end;
    }
    .quantity {
      font-size: 18px;
      margin-bottom: 5px;
      }
    .label {
      font-size: 13px;
      font-weight: 300;
    }
`;

const Statistics = styled.div`
  display: flex;
  background-color: rgba(0, 0, 0, 0.04);
  text-align: center;
  border-radius: 4px;
`;


export { PurchaseHistoryWrapper, History, Column, Statistics };
