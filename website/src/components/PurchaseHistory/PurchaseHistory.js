import React, { Component } from 'react';
import {
  PurchaseHistoryWrapper,
  History,
  Statistics,
  Column
} from './styles';

class PurchaseHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profiles_unlocked: 0,
      credits_bought: 0,
      credits_spent: 0
    }
  }

  render() {
    return(
        <PurchaseHistoryWrapper>
            <Statistics>
              <Column>
                <p className={'quantity'}>5</p>
                <p className={'label'}>Freelancers unlocked</p>
              </Column>
              <Column>
                <p className={'quantity'}>225</p>
                <p className={'label'}>Credits bought</p>
              </Column>
              <Column>
                <p className={'quantity'}>225</p>
                <p className={'label'}>Credits spent</p>
              </Column>
            </Statistics>
        <History>
          <Column className={'creditsList'}>
            <p>5 credits</p>
            <p>5 credits</p>
          </Column>
          <Column className={'priceList'}>
            <p>$15.99</p>
            <p>$15.99</p>
          </Column>
          <Column className={'dateList'}>
            <p>Today</p>
            <p>Today</p>
          </Column>
        </History>
        <div className={'divider'} />
        </PurchaseHistoryWrapper>
    )
  }
}

export default PurchaseHistory