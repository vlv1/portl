import styled from 'styled-components';

const Rating = styled.span`
      font-weight: bold;
      color: ${props => props.transparent ? `rgba(0, 0, 0, 0.18)` : `rgba(0, 0, 0, 0.88)`};
      font-size: ${props => props.large ? '18px' : '16px'};
`

export { Rating }