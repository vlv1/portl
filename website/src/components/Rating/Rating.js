import React from 'react';
import { Rating } from './styles';
import { roundRating } from 'utils/rating';

export default ({ rating, large = false }) => {
  return(
    <Rating transparent={rating === 0} large={large}>{rating === 0 ? 'N/A' : roundRating(rating)}</Rating>
  )
}