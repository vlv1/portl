import React from 'react';
import styled from 'styled-components';
import FilterChip from './FilterChip/index';

const Filters = styled.div`
  display: flex;
  margin-bottom: 54px;
`;

export default ({ filters, activeFilter, actions }) => (
  <Filters>
    {Object.values(filters).map(filter => (
      <FilterChip
        name={filter.name}
        key={filter.name}
        hideFilter={actions.hideFilter}
        onClick={() => actions.filterClick(filter.name)}
        clearFilter={() => actions.clearFilter(filter.name)}
      />
    ))}
  </Filters>
);
