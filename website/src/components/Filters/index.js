import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getFilters, getActiveFilter } from 'store/Search/selectors';
import Filters from './Filters';
import { filterClick, hideFilter } from 'store/Search/actions';
import { clearFilter } from '../../store/Search/actions';

const mapStateToProps = state => ({
  filters: getFilters(state),
  activeFilter: getActiveFilter(state)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ filterClick, hideFilter, clearFilter }, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filters);
