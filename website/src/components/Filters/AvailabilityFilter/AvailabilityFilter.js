import React from 'react';
import FilterInput from '../FilterInput';
import { FilterButtonsGroup } from '../FilterBody';
import { Checkbox, CheckboxBox } from '../Checkbox';
import Fuse from 'fuse.js';
import { Divider } from '../styles';
import WhiteButton from '../../Buttons/WhiteButton';
import GradientButton from '../../Buttons/GradientButton';
import FilterBody from '../FilterBody';
import Calendar from 'components/Calendar';
import { inputFilter } from '../../../store/Search/actions';

const options = {
  shouldSort: true,
  threshold: 0.3,
  tokenize: true,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: ['title']
};

class AvailabilityFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      range: []
    };
  }

  handleInput(value) {
    console.log(value);
    this.props.actions.inputFilter(this.props.filter.name, value);
  }

  clearField() {
    this.setState({ range: [] });
  }

  handleApply(e) {
    e.stopPropagation();
    this.props.actions.applyFilter(this.props.filter.name);
  }

  render() {
    const { input, searchResult } = this.state;
    const {
      filter,
      unselected,
      selected,
      actions: { clearFilter, inputFilter }
    } = this.props;

    return (
      <FilterBody type={'availability'}>
        <Calendar onSelect={value => this.handleInput(value)} />

        <FilterButtonsGroup>
          <WhiteButton small onClick={() => clearFilter(filter.name)}>
            Clear
          </WhiteButton>
          <GradientButton small onClick={e => this.handleApply(e)}>
            Apply
          </GradientButton>
        </FilterButtonsGroup>
      </FilterBody>
    );
  }
}

export default AvailabilityFilter;
