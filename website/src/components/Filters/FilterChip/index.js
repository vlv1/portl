import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getFilter, isFilterActive } from 'store/Search/selectors';
import FilterChip from './FilterChip';

const mapStateToProps = (state, ownProps) => ({
  filter: getFilter(state, ownProps.name),
  active: isFilterActive(state, ownProps.name)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({}, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilterChip);
