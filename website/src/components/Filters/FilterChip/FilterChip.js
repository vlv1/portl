import React from 'react';
import styled from 'styled-components';
import enhanceWithClickOutside from 'react-click-outside';
import { borders } from 'styles/mixins';
import roundClear from 'assets/round-clear.svg';
import FilterResolver from '../FiltersResolver';
import { DividerDot } from '../../Buttons/index';

const ChipTitle = styled.p`
  padding: 0 16px;
`;

const ChipValue = styled.p`
  padding: 0 8px 0 16px;
`;

const ChipClear = styled.img`
  padding: 0 8px 0 0;
`;

const SplittedChip = styled.div`
  display: flex;
  align-items: center;
`;

const FilterChipWrapper = styled.div`
    position: relative;
    cursor: pointer;
    height: 40px;
    border-radius: 20px;
    margin-right: 8px;
    font-size: 16px;
    font-weight: 600;
    display: flex;    
    justify-content: space-between;
    align-items: center;
    line-height: 24px;
    ${props => props.active && `background-color: rgba(242, 242, 243, 0.56);`}
    color: ${props =>
      props.active || props.value ? props.theme.colors.black88 : props.theme.colors.black56};
    ${borders.input8};
`;

const stopEvent = cb => e => {
  e.stopPropagation();
  cb();
};

const getChipTitle = (name, value, placeholder) => {
  if (name === 'availability') {
    return `${value[0].format('MMM DD')} - ${value[1].format('MMM DD')}`;
  }
  if (name === 'rate') return `£${value[0]} - £${value[1]}`;
  if (name === 'languages') return `More filters`;
  if (name === 'qualifications' || name === 'sectors' || name === 'key_skills' || name === 'locations')
    return (
      <SplittedChip>
        {placeholder} <DividerDot /> {value.length}
      </SplittedChip>
    );
  return value;
};

const ChipWithValue = ({ placeholder, clearFilter }) => (
  <>
    <ChipValue>{placeholder}</ChipValue>
    <ChipClear onClick={stopEvent(clearFilter)} src={roundClear} />
  </>
);

class FilterChip extends React.Component {
  handleClickOutside() {
    const { active, hideFilter } = this.props;
    if (active) {
      hideFilter();
    }
  }

  render() {
    const { active, filter, onClick, clearFilter } = this.props;
    return (
      <FilterChipWrapper onClick={onClick} active={active} name={filter.name}>
        {filter.value ? (
          <ChipWithValue
            clearFilter={clearFilter}
            placeholder={getChipTitle(filter.name, filter.value, filter.placeholder)}
          />
        ) : (
          <ChipTitle>{filter.placeholder}</ChipTitle>
        )}
        {active && <FilterResolver name={filter.name} />}
      </FilterChipWrapper>
    );
  }
}

export default enhanceWithClickOutside(FilterChip);
