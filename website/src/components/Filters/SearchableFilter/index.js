import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  getFilter,
  getOptionsByName,
  getSelectedOptions,
  getUnselectedOptions
} from 'store/Search/selectors';
import SearchableFilter from './SearchableFilter';
import { clickCheckbox, clearFilter } from 'store/Search/actions';
import { applyFilter } from 'store/Search/actions';

const mapStateToProps = (state, ownProps) => ({
  filter: getFilter(state, ownProps.name),
  options: getOptionsByName(state, ownProps.name),
  selected: getSelectedOptions(state, ownProps.name),
  unselected: getUnselectedOptions(state, ownProps.name)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ clickCheckbox, clearFilter, applyFilter }, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchableFilter);
