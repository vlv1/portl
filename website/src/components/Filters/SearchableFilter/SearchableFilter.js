import React from 'react';
import FilterInput from '../FilterInput';
import { FilterButtonsGroup } from '../FilterBody';
import { Checkbox, CheckboxBox, ShadowBox } from '../Checkbox';
import Fuse from 'fuse.js';
import { Divider } from '../styles';
import WhiteButton from '../../Buttons/WhiteButton';
import GradientButton from '../../Buttons/GradientButton';
import FilterBody from '../FilterBody';

const options = {
  shouldSort: true,
  threshold: 0.3,
  tokenize: true,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: ['title']
};

class SearchableFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input: '',
      searchResult: undefined,
      fuse: undefined
    };
  }

  static getDerivedStateFromProps(props, state) {
    return { searchResult: new Fuse(props.unselected, options).search(state.input) };
  }

  handleInput(e) {
    this.setState({
      input: e.target.value,
      searchResult: new Fuse(this.props.unselected, options).search(e.target.value)
    });
  }

  clearField() {
    this.setState({ input: '', searchResult: undefined });
  }

  handleApply(e) {
    e.stopPropagation();
    this.props.actions.applyFilter(this.props.filter.name);
  }

  render() {
    const { input, searchResult } = this.state;
    const {
      filter,
      unselected,
      selected,
      actions: { clickCheckbox, clearFilter }
    } = this.props;
    const options = input.length > 0 && searchResult ? searchResult : unselected;
    return (
      <FilterBody>
        <FilterInput
          name={filter.name}
          value={input}
          placeholder={filter.queryPlaceholder}
          clearField={() => this.clearField()}
          onChange={e => this.handleInput(e)}
        />

        {selected.length ? (
          <>
            {selected.map(option => (
              <Checkbox
                checked={true}
                name={filter.name}
                id={option.id}
                onClick={clickCheckbox.bind(null, true, filter.name, option.id)}
                key={option.id}>
                {option.title}
              </Checkbox>
            ))}
            <Divider />
          </>
        ) : null}
        <ShadowBox>
          <CheckboxBox style={{ height: 250, paddingBottom: '44px' }}>
            {options.map(option => (
              <Checkbox
                checked={false}
                name={filter.name}
                id={option.id}
                onClick={clickCheckbox.bind(null, false, filter.name, option.id)}
                key={option.id}>
                {option.title}
              </Checkbox>
            ))}
          </CheckboxBox>
          <div className="shadow" />
        </ShadowBox>

        <FilterButtonsGroup>
          <WhiteButton small onClick={() => clearFilter(filter.name)}>
            Clear
          </WhiteButton>
          <GradientButton small onClick={e => this.handleApply(e)}>
            Apply
          </GradientButton>
        </FilterButtonsGroup>
      </FilterBody>
    );
  }
}

export default SearchableFilter;
