import styled from 'styled-components';
import React from 'react';
import checkIcon from '../../assets/check.svg';
import { Scrollbars } from 'react-custom-scrollbars';

const CheckboxWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  flex-wrap: wrap;
  margin-bottom: 2px;
`;

const CheckboxLabel = styled.p`
  font-size: 16px;
  font-weight: 300;
  color: ${props => props.theme.colors.black88};
`;

const CheckboxSwitcher = styled.div`
  width: 22px;
  height: 22px;
  background-size: contain;
  background-image: ${props => (props.checked ? `url(${checkIcon})` : 'none')};
  border: 1px solid
    ${props => (props.checked ? props.theme.colors.black16 : props.theme.colors.black8)};
  margin-right: 12px;
`;

const CheckboxItem = styled.div`
  display: flex;
  align-items: center;
  flex-basis: 50%;
  margin-bottom: 18px;

  &:last-child {
    padding-bottom: 44px;
  }
`;

const ShadowBox = styled.div`
  position: relative;
  div.shadow {
    position: absolute;
    bottom: 0;
    height: 44px;
    width: 100%;
    background-image: linear-gradient(to top, #ffffff 18%, rgba(255, 255, 255, 0));
  }
`;

const renderThumb = ({ style, ...props }) => {
  const thumbStyle = {
    backgroundColor: 'rgba(0, 0, 0, 0.08)',
    width: '8px',
    borderRadius: '4px'
  };
  return <div {...props} style={{ ...style, ...thumbStyle }} />;
};

const CheckboxBox = props => (
  <Scrollbars {...props} renderThumbHorizontal={renderThumb}>
    {props.children}
  </Scrollbars>
);

const Checkbox = ({ checked, onClick = () => true, name, id, children }) => (
  <CheckboxItem onClick={() => onClick()}>
    <CheckboxSwitcher checked={checked} />
    <CheckboxLabel>{children}</CheckboxLabel>
  </CheckboxItem>
);

export { Checkbox, CheckboxWrapper, CheckboxBox, ShadowBox };
