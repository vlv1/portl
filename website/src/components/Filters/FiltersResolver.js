import React from 'react';
import SearchableFilter from './SearchableFilter';
import AvailabilityFilter from './AvailabilityFilter';
import AdditionalFilter from './AdditionalFilter';
import RangeFilter from './RangeFilter';

const FilterResolver = ({ name }) => (
  <div onClick={e => e.stopPropagation()}>
    {(name === 'key_skills' || name === 'qualifications' || name === 'sectors' || name === 'locations') && (
      <SearchableFilter name={name} />
    )}

    {name === 'availability' && <AvailabilityFilter name={name} />}
    {name === 'rate' && <RangeFilter name={name} />}
    {name === 'languages' && <AdditionalFilter name={name} />}
  </div>
);

export default FilterResolver;
