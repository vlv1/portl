import React from 'react';
import ReactSlider from 'react-slider';
import styled from 'styled-components';

export const StyledReactSlider = styled(ReactSlider)`
  position: absolute;
  height: 8px;
  background-color: rgba(0, 0, 0, 0.12);
`;

export const RateInputsBox = styled.div`
  width: 304px;
  display: flex;
  justify-content: space-between;
  margin-bottom: 40px;
`;

const RateInputWrapper = styled.div`
  width: 144px;
  height: 48px;
  border-radius: 4px;
  border: solid 1px rgba(0, 0, 0, 0.1);
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 12px;
`;

const RateLabel = styled.p`
  width: 70px;
  color: rgba(0, 0, 0, 0.56);
  font-size: 16px;
`;

const RateValue = styled.input`
  width: 48px;
  display: inline-block;
  font-size: 16px;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.88);
  border: none;
  text-align: right;
  outline: none;
`;

export const RateInput = ({ label, ...props }) => (
  <RateInputWrapper>
    <RateLabel>{label}</RateLabel>
    <RateValue {...props} />
  </RateInputWrapper>
);

export const RangeBar = styled.img`
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  width: 25px;
  height: 32px;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.1);
  border: solid 1px rgba(0, 0, 0, 0.08);
  background-color: #fff;
`;

export const Divider = styled.div`
  width: 100%;
  height: 1px;
  display: block;
  border: 1px ${props => props.theme.colors.black8} solid;
  margin-bottom: 18px;
`;
