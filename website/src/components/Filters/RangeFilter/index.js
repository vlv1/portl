import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getFilter } from 'store/Search/selectors';
import RangeFilter from './RangeFilter';
import { clearRangeFilter, setRangeFilterValue, applyFilter } from 'store/Search/actions';

const mapStateToProps = (state, ownProps) => ({
  filter: getFilter(state, ownProps.name)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ clearRangeFilter, setRangeFilterValue, applyFilter }, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RangeFilter);
