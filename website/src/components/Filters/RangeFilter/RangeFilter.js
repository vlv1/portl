import React from 'react';
import { FilterButtonsGroup, FilterTitle } from '../FilterBody';
import WhiteButton from '../../Buttons/WhiteButton';
import GradientButton from '../../Buttons/GradientButton';
import FilterBody from '../FilterBody';
import { RangeBar, RateInput, RateInputsBox, StyledReactSlider } from '../styles';
import barIcon from 'assets/slidebutton.svg';

class RangeFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentRange: [...(props.filter.value ? props.filter.value : props.filter.range)]
    };
  }

  preventTextInput(e) {
    const isNumber = value => !isNaN(parseInt(value, 10));
    if (!isNumber(e.key)) {
      e.preventDefault();
    }
  }

  handleInput(e, max = false) {
    let value = Number.parseInt(e.target.value);
    if (value < this.props.filter.range[0]) {
      value = this.props.filter.range[0];
    }

    if (value > this.props.filter.range[1]) {
      value = this.props.filter.range[1];
    }

    if (!max && value > this.state.currentRange[1]) {
      value = this.state.currentRange[1];
    }

    if (max && value < this.state.currentRange[0]) {
      value = this.state.currentRange[0];
    }

    if (value) {
      if (!max) {
        this.setState({
          currentRange: [value, this.state.currentRange[1]]
        });
      } else {
        this.setState({
          currentRange: [this.state.currentRange[0], value]
        });
      }
    }
  }

  handleApply(e) {
    e.stopPropagation();
    this.props.actions.setRangeFilterValue(this.state.currentRange);
  }

  onChange(value) {
    this.setState({ currentRange: value });
  }

  render() {
    const {
      filter: { range }
    } = this.props;
    return (
      <FilterBody onClick={this.stopEvent}>
        <FilterTitle>Range for current search results</FilterTitle>
        <RateInputsBox>
          <RateInput
            label={'From, £:'}
            onKeyPress={this.preventTextInput}
            onChange={e => this.handleInput(e)}
            value={this.state.currentRange[0]}
          />
          <RateInput
            label={'To, £:'}
            onKeyPress={this.preventTextInput}
            onChange={e => this.handleInput(e, true)}
            value={this.state.currentRange[1]}
          />
        </RateInputsBox>
        <StyledReactSlider
          onChange={value => this.onChange(value)}
          min={range[0]}
          max={range[1]}
          value={this.state.currentRange}
          withBars
          className={'slider'}>
          <RangeBar src={barIcon} alt={'range bar'} />
          <RangeBar src={barIcon} alt={'range bar'} />
        </StyledReactSlider>
        <FilterButtonsGroup>
          <WhiteButton small onClick={() => this.props.actions.clearRangeFilter()}>
            Clear
          </WhiteButton>
          <GradientButton small onClick={e => this.handleApply(e)}>
            Apply
          </GradientButton>
        </FilterButtonsGroup>
      </FilterBody>
    );
  }
}

export default RangeFilter;
