import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  getFilter,
  getOptionsByName,
  getSelectedOptions,
  getUnselectedOptions
} from 'store/Search/selectors';
import AdditionalFilter from './AdditionalFilter';
import { clickCheckbox, clearFilter, inputFilter } from 'store/Search/actions';
import { applyFilter } from 'store/Search/actions';

const mapStateToProps = (state, ownProps) => ({
  filter: getFilter(state, ownProps.name),
  options: getOptionsByName(state, ownProps.name),
  selected: getSelectedOptions(state, ownProps.name),
  unselected: getUnselectedOptions(state, ownProps.name)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ clickCheckbox, clearFilter, applyFilter, inputFilter }, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdditionalFilter);
