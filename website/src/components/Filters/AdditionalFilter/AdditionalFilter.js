import React from 'react';
import { FilterButtonsGroup, FilterTitle } from '../FilterBody';
import { Checkbox, CheckboxWrapper } from '../Checkbox';
import Divider from '../../Search/Divider';
import WhiteButton from '../../Buttons/WhiteButton';
import GradientButton from '../../Buttons/GradientButton';
import FilterBody from '../FilterBody';
const handleApply = (e, action, name) => {
  e.stopPropagation();
  action(name);
};
const AdditionalFilter = ({ filter, options, selected, unselected, actions }) => (
  <FilterBody>
    {/*<FilterTitle>Work Type</FilterTitle>*/}
    {/*<CheckboxWrapper>*/}
    {/*<Checkbox>On-site</Checkbox>*/}
    {/*<Checkbox>Remote</Checkbox>*/}
    {/*<Checkbox>Flexible</Checkbox>*/}
    {/*</CheckboxWrapper>*/}
    {/*<Divider />*/}
    <FilterTitle>Language</FilterTitle>
    <CheckboxWrapper>
      {options.map(option => {
        const isSelected = selected.some(e => e.id === option.id);
        return (
          <Checkbox
            checked={isSelected}
            name={filter.name}
            id={option.id}
            onClick={actions.clickCheckbox.bind(null, isSelected, filter.name, option.id)}
            key={option.id}>
            {option.title}
          </Checkbox>
        );
      })}
    </CheckboxWrapper>
    {/*<Divider />*/}
    {/*<FilterTitle>Vetting</FilterTitle>*/}
    {/*<CheckboxWrapper>*/}
    {/*<Checkbox>Vetted freelancers only</Checkbox>*/}
    {/*</CheckboxWrapper>*/}
    <FilterButtonsGroup>
      <WhiteButton small onClick={() => actions.clearFilter(filter.name)}>
        Clear
      </WhiteButton>
      <GradientButton small onClick={e => handleApply(e, actions.applyFilter, filter.name)}>
        Apply
      </GradientButton>
    </FilterButtonsGroup>
  </FilterBody>
);

export default AdditionalFilter;
