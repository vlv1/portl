import styled from 'styled-components';

const FilterBody = styled.div`
  width: ${props => (props.type === 'availability' ? '756px' : '520px')};
  position: absolute;
  top: calc(100% + 30px);
  left: 0;
  z-index: 2;
  padding: 40px 48px;
  box-shadow: 0 24px 40px 0 rgba(0, 0, 0, 0.14);
  background-color: #ffffff;
  border-style: solid;
  border-width: 1px;
  border-image-source: linear-gradient(to bottom, rgba(0, 0, 0, 0.08), rgba(0, 0, 0, 0.16));
  border-image-slice: 1;
`;

const FilterButtonsGroup = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 24px;
`;

const FilterTitle = styled.p`
  font-size: 18px;
  font-weight: 600;
  color: ${props => props.theme.colors.black88};
  margin-bottom: 20px;
`;

export { FilterTitle, FilterButtonsGroup };
export default FilterBody;
