import React from 'react';
import styled from 'styled-components';
import clear from 'assets/clear.svg';
import searchIcon from 'assets/search.svg';

const InputWrapper = styled.div`
  display: flex;
  width: ${props => props.width};
`;

const InputIcon = styled.img`
  margin: 0 19px;
`;

const InputClear = styled.img`
  margin: 0 0 0 5px;
  cursor: pointer;
`;

const Input = styled.input`
  width: 100%;
  height: 48px;
  border: none;
  outline: none;
  font-weight: bold;
  font-size: 16px;
  color: ${props => props.theme.colors.black56};
`;
const Border = styled.div`
  border-radius: 4px;
  background-color: #ffffff;
  border: solid 1px rgba(0, 0, 0, 0.1);
  margin-bottom: 24px;
`;
export default props => {
  return (
    <Border>
      <InputWrapper width={'400px'}>
        <InputIcon src={searchIcon} />
        <Input type="text" {...props} />
        {props.value && props.value.length && <InputClear src={clear} onClick={props.clearField} />}
      </InputWrapper>
    </Border>
  );
};
