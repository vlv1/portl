import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FreelancerPage from './FreelancerPage';
import { getFreelancerById } from 'store/Search/selectors';
import withRouter from 'react-router/es/withRouter';
import { modalShow } from 'store/Modal/actions';
import {
  favouriteUser,
  unfavouriteUser,
  unlockFreelancer,
  requestAvailability
} from 'store/Search/actions';
import { fetchUser } from '../../store/Search/actions';

const mapStateToProps = state => ({
  freelancer: getFreelancerById(state)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    { favouriteUser, unfavouriteUser, unlockFreelancer, requestAvailability, fetchUser, modalShow },
    dispatch
  )
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(FreelancerPage))