import React from 'react';
import styled from 'styled-components';
import locked from 'assets/locked.svg';
import AvatarEmpty from 'assets/avatar-empty-normal.svg';
import HollowRatingIcon from 'assets/Rating Icons Default  80x24.svg';
import FilledRatingIcon from 'assets/RatingIcons.svg';
import { roundRating, getImageContainerWidth } from "utils/rating";
import { ResultInfo, ResultInfoGroup, ButtonsPanel } from './styles';
import { GradientButton } from 'components/Buttons';
import Rating from '../Rating';

const ProfileUnlocked = styled.div`
  ${props => props.theme.shadows.absoluteBox};
  position: relative;
  width: 696px;
  display: flex;
  align-items: center;
  margin-top: 24px;
  margin-bottom: 28px;
  border-radius: 8px;
`;

const Score = styled.div`
  display: inline-flex;
  align-items: baseline;
  font-weight: bold;
  color: ${props => props.theme.colors.black88};
  line-height: 24px;
  font-size: 18px;
  position: relative;
    > div {
      margin-right: 80px;
    }
    div.hollowRating {
      height: 24px;
    }
    div.hollowRating, div.filledRating {
      top: 0;
      position: absolute;
    }
    div.filledRating img {
      transform: scaleX(0.96);
    }
    div.filledRating {
      top: -0.5px;
      left: 1.5px;
      width: ${props => props.imageContainerWidth};
      overflow: hidden;
      height: 24px;
    }
`;

const Avatar = styled.div`
  position: relative;
  border-radius: 8px 0 0 8px;
  img {
        object-fit: cover;

    width: 168px;
    height: 226px;
    
    padding: 1px;
  }
  ${props =>
    !props.unlocked
      ? ` &:after {
    position: absolute;
    background-image: url("${locked}");
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 28px;
    height: 34px;
    background-size: cover;
    content: '';
    z-index: 2;
  }`
      : ''};
`;

const InfoBlock = styled.div`
  width: 100%;
  align-self: flex-start;
  padding: 30px 32px 0 32px;
`;

const ContactText = styled.p`
  font-size: 18px;
  font-weight: 300;
  color: ${props => (props.red ? '#f81a38' : 'rgba(0, 0, 0, 0.88)')};
  margin-bottom: 9px;
`;

const ContactRow = styled.div``;

export default ({ id, rate, title, company, user, rating, modalShow }) => {
 const {avatar, phone, email, first_name, last_name} = user;
  return (
     <ProfileUnlocked>
         <Avatar unlocked>
             <img src={avatar || AvatarEmpty} alt="" />
         </Avatar>
         <InfoBlock>
             <ResultInfoGroup>
                 <ResultInfo>
                     <p className="title">{title}</p>
                     <Score imageContainerWidth={getImageContainerWidth(roundRating(rating))}>
                         <div>
                             <div className={'hollowRating'}>
                                 <img src={HollowRatingIcon} alt="rating icon" />
                             </div>
                             <div className={'filledRating'}>
                                 <img src={FilledRatingIcon} alt="rating icon"/>
                             </div>
                         </div>
                       <Rating rating={rating} large />
                     </Score>
                 </ResultInfo>
                 <ResultInfo>{rate}</ResultInfo>
             </ResultInfoGroup>
             <ContactRow>
                 <ContactText>{phone}</ContactText>
                 <ContactText>{email}</ContactText>
             </ContactRow>
             <ButtonsPanel>
                 <GradientButton small onClick={() => modalShow('RATE_USER_HIRE', { ...user, id})}>Rate Me</GradientButton>
             </ButtonsPanel>
         </InfoBlock>
     </ProfileUnlocked>
 )
};
