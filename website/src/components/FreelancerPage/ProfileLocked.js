import React from 'react';
import styled from 'styled-components';
import { GradientButton } from 'components/Buttons';
import coffeIcon from 'assets/coffee.svg';
import AvatarEmpty from 'assets/UserAvatar.png';
import HollowRatingIcon from 'assets/Rating Icons Default  80x24.svg';
import FilledRatingIcon from 'assets/RatingIcons.svg';
import { roundRating, getImageContainerWidth } from "utils/rating";
import locked from 'assets/locked.svg';
import { ResultInfo, ResultInfoGroup } from './styles';
import Rating from '../Rating';

const ProfileLocked = styled.div`
  ${props => props.theme.shadows.absoluteBox};
  position: relative;
  width: 696px;
  display: flex;
  align-items: center;
  margin-top: 24px;
  margin-bottom: 28px;
  border-radius: 8px;
`;

const Avatar = styled.div`
  position: relative;
  background: #c4c4c4;
  border-radius: 8px 0 0 8px;
  img {
    padding: 1px;
  }
  ${props =>
    !props.unlocked
      ? ` &:after {
    position: absolute;
    background-image: url("${locked}");
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 28px;
    height: 34px;
    background-size: cover;
    content: '';
    z-index: 2;
  }`
      : ''};
`;

const Score = styled.div`
  display: inline-flex;
  align-items: baseline;
  font-weight: bold;
  color: ${props => props.theme.colors.black88};
  line-height: 24px;
  font-size: 18px;
  position: relative;
    > div {
      margin-right: 80px;
    }
    div.hollowRating {
      height: 24px;
    }
    div.hollowRating, div.filledRating {
      top: 0;
      position: absolute;
    }
    div.filledRating img {
      transform: scaleX(0.96);
    }
    div.filledRating {
      top: -0.5px;
      left: 1.5px;
      width: ${props => props.imageContainerWidth};
      overflow: hidden;
      height: 24px;
    }
`;

const InfoBlock = styled.div`
  width: 100%;
  padding: 0 40px 0 32px;
`;

const UnlockText = styled.p`
  display: flex;
  align-items: center;
  font-size: 18px;
  font-weight: bold;
  color: ${props => props.theme.colors.black88};
`;

const ContactText = styled.p`
  font-size: 18px;
  font-weight: 300;
  color: #f81a38;
  margin-bottom: 23px;
`;

const UnlockRow = styled.div`
  display: flex;
  justify-content: space-between;
`;

export default ({ rate, title, company, unlockFreelancer, rating }) => (
  <ProfileLocked>
    <Avatar src={AvatarEmpty}>
      <img src={AvatarEmpty} alt="" />
    </Avatar>
    <InfoBlock>
      <ResultInfoGroup>
        <ResultInfo>
          <p className="title">{title}</p>
          <Score imageContainerWidth={getImageContainerWidth(roundRating(rating))}>
          <div>
            <div className={'hollowRating'}>
              <img src={HollowRatingIcon} alt="rating icon" />
            </div>
            <div className={'filledRating'}>
              <img src={FilledRatingIcon} alt="rating icon"/>
            </div>
          </div>
            <Rating rating={rating} large />
        </Score>
        </ResultInfo>
        <ResultInfo>{rate}</ResultInfo>
      </ResultInfoGroup>
      <ContactText>Freelancer’s contacts are hidden.</ContactText>
      <UnlockRow>
        <GradientButton small onClick={unlockFreelancer}>
          Unlock
        </GradientButton>
      </UnlockRow>
    </InfoBlock>
  </ProfileLocked>
);
