import React from 'react';
import ratingIcon from 'assets/rating-big.svg';
import { getDatasetOutput, getLocationsOutput, getRateOutput } from 'utils/dataFormating';
import ProfileLocked from './ProfileLocked';
import {
    HeadingWrapper,
    ScoreColumn,
    ResultColumn,
    ResultItem,
    ResultHeading,
    ResultName,
    ResultDescription,
    ResultDescriptionName,
    ResultDescriptionSkills,
    FreelancerPageWrapper,
    CalendarColumn,
    AvaliabilityTitle, LastLogin
} from './styles';
import { FunctionalButton, FunctionalButtonWrapper } from 'components/Buttons';
import save from 'assets/save.svg';
import dataMock from 'store/Search/dataMock';
import ProfileUnlocked from './ProfileUnlocked';
import AvailabilityCalendar from '../Calendar/AvailabilityCalendar';
import saved from '../../assets/saved.svg';
import moment from 'utils/moment';
import api from 'utils/api';

const getSkillsOutput = skills => {
  return skills.map((skill, i) => (
    <>
      {skill.title} <span>{skill.experience}</span> {skills.length - 1 !== i ? ',' : ''}{' '}
    </>
  ));
};

class FreelancerPage extends React.Component {
  componentDidMount() {
    !this.props.freelancer && this.props.actions.fetchUser(this.props.match.params.id, true);
    api.profile.new_view(this.props.match.params.id, {});
  }
  render() {
    const {
      freelancer,
      match: { params },
      actions: { favouriteUser, unfavouriteUser, unlockFreelancer, requestAvailability, modalShow }
    } = this.props;

    if (!freelancer) {
      return <div />;
    }

    const {
      id,
      company,
        bio,
      rating,
      locations,
      job_title,
      user,
      key_skills,
      sectors,
      qualifications,
      languages,
      unlocked,
      unavailable_periods,
      unavailable_weekends,
      always_available,
      availability_updated_at,
      favourite,
      rate_minimum,
      rate_maximum,
      rate_type,
        last_login
    } = freelancer;
    const  rate = {
        minimum: rate_minimum,
        maximum: rate_maximum,
        type: rate_type
    }
    return freelancer ? (
      <FreelancerPageWrapper>
        <ResultItem>
          <ResultColumn>
            <HeadingWrapper>
              <ResultHeading>
                <ResultName>
                  {unlocked ? `${user.first_name} ${user.last_name} ` : user.initials}{' '}
                </ResultName>{' '}
                · {getLocationsOutput(locations, true)}
              </ResultHeading>
              <FunctionalButtonWrapper>
                {!favourite ? (
                  <FunctionalButton onClick={() => favouriteUser(id)}>
                    <img src={save} alt="" />
                    Save to favorites
                  </FunctionalButton>
                ) : (
                  <FunctionalButton onClick={() => unfavouriteUser(id)}>
                    <img src={saved} alt="" />
                    Saved
                  </FunctionalButton>
                )}
              </FunctionalButtonWrapper>
            </HeadingWrapper>
            {unlocked ? (
              <ProfileUnlocked
                  id={id}
                  user={user}
                rating={rating}
                rate={getRateOutput(rate)}
                title={job_title.title}
                company={company}
                modalShow={modalShow}
              />
            ) : (
              <ProfileLocked
                rate={getRateOutput(rate)}
                rating={rating}
                title={job_title.title}
                company={company}
                unlockFreelancer={() => unlockFreelancer(id)}
              />
            )}
            <ResultDescription>
                <ResultDescriptionName>Skills / <span className={'light'}>Years of Experinece</span>: </ResultDescriptionName>
              <ResultDescriptionSkills>{getSkillsOutput(key_skills)}</ResultDescriptionSkills>
            </ResultDescription>
            <ResultDescription>
              <ResultDescriptionName>SECTORS: </ResultDescriptionName>
              <ResultDescriptionSkills>{getDatasetOutput(sectors)}</ResultDescriptionSkills>
            </ResultDescription>
            <ResultDescription>
              <ResultDescriptionName>QUALIFICATIONS: </ResultDescriptionName>
              <ResultDescriptionSkills>{getDatasetOutput(qualifications)}</ResultDescriptionSkills>
            </ResultDescription>
            <ResultDescription>
              <ResultDescriptionName>LANGUAGES: </ResultDescriptionName>
              <ResultDescriptionSkills>{getDatasetOutput(languages)}</ResultDescriptionSkills>
            </ResultDescription>
              {bio
                  ? <ResultDescription>
                      <ResultDescriptionName>BIO: </ResultDescriptionName>
                      <ResultDescriptionSkills>{bio}</ResultDescriptionSkills>
                  </ResultDescription>
                  : null}
              {company
                  ? <ResultDescription>
                      <ResultDescriptionName>Company type: </ResultDescriptionName>
                      <ResultDescriptionSkills>{company.company_name}</ResultDescriptionSkills>
                  </ResultDescription>
                  : null}
              {last_login ? <LastLogin>{moment(last_login).format('MMMM, DD')} - Last login</LastLogin> : null}
          </ResultColumn>
        </ResultItem>
        <CalendarColumn>
          <AvaliabilityTitle>Availability</AvaliabilityTitle>
          <AvailabilityCalendar
            {...{
              unavailable_periods,
              unavailable_weekends,
              always_available,
              id,
              requestAvailability
            }}
          />
        </CalendarColumn>
      </FreelancerPageWrapper>
    ) : null;
  }
}

export default FreelancerPage;
