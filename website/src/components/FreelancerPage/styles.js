import styled from 'styled-components';

const ScoreColumn = styled.div`
  display: flex;
  align-items: center;
  font-weight: bold;
  color: ${props => props.theme.colors.black88};
  font-size: 24px;
  margin-right: 16px;
`;

const ResultColumn = styled.div`
  width: 696px;
`;

const ResultItem = styled.div`
  display: flex;
  align-items: start;
  margin-bottom: 50px;
  cursor: pointer;
  padding-right: 55px;
  border-right: 1px solid rgba(0, 0, 0, 0.08);
`;

const ResultHeading = styled.div`
  display: flex;
  align-items: center;
  font-size: 24px;
  font-weight: 300;
  color: ${props => props.theme.colors.black48};
`;
const ResultName = styled.span`
  font-weight: bold;
  color: ${props => props.theme.colors.black88};
  margin-right: 8px;
`;

const ResultInfo = styled.div`
  font-size: 18px;
  font-weight: bold;
  color: ${props => props.theme.colors.black88};
  margin-bottom: 9px;
  .title {
      display: inline-block;
      margin-right: 11px;
  }
  > span   {
    font-size: 18px;
    font-weight: 300;
    color: ${props => props.theme.colors.black88};
  }
`;

const ResultInfoGroup = styled.div`
  margin-bottom: 30px;
`;

const ResultDescription = styled.div`
  margin-bottom: 27px;
`;
const ResultDescriptionName = styled.p`
  font-size: 15px;
  font-weight: 600;
  text-transform: uppercase;
  color: ${props => props.theme.colors.black48};
  margin-right: 10px;
  
  .light {
  font-weight: 300;
  }
`;

const ResultDescriptionSkills = styled.p`
  font-weight: 300;
  line-height: 1.78;
  font-size: 18px;
  color: ${props => props.theme.colors.black88};

  span {
    font-weight: 600;
    padding: 0 3px;
    margin: 0 4px;
    border: 1px solid rgba(0, 0, 0, 0.16);
    border-radius: 2px;
  }
`;
const HeadingWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 24px;
`;

const FreelancerPageWrapper = styled.div`
  display: flex;
`;

const CalendarColumn = styled.div`
  width: 660px;
  margin-left: 50px;
`;

const AvaliabilityTitle = styled.div`
  text-transform: uppercase;
  font-weight: 600;
  font-size: 15px;
  color: rgba(0, 0, 0, 0.48);
  margin-bottom: 29px;
`;

const ButtonsPanel = styled.div`
  align-self: flex-end;
  display: flex;
  justify-content: flex-end;
  position: absolute;
  bottom: 32px;
  right: 32px;
`;

const LastLogin = styled.div`
  font-size: 18px;
  font-weight: 300;
  color: rgba(0, 0, 0, 0.48);
`;

export {
  ResultName,
  ScoreColumn,
  ResultColumn,
  ResultItem,
  ResultHeading,
  ResultInfo,
  ResultInfoGroup,
  ResultDescription,
  ResultDescriptionName,
  ResultDescriptionSkills,
  HeadingWrapper,
  FreelancerPageWrapper,
  CalendarColumn,
  AvaliabilityTitle,
  ButtonsPanel,
    LastLogin
};
