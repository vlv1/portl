import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import { ConnectedRouter } from 'connected-react-router';
import { ThemeProvider } from 'styled-components';
import configureStore from './store';
import { initGlobalStyles, theme } from './styles';
import Routes from './views/Routes';

export const history = createBrowserHistory();
const store = configureStore(history);

initGlobalStyles();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <ConnectedRouter history={history}>
            <Routes />
          </ConnectedRouter>
        </ThemeProvider>
      </Provider>
    );
  }
}

export default App;
