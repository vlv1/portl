import api from '../utils/api';
import Cookies from 'js-cookie';
export * from './user';

export const userService = {
  login,
  logout,
  registration
};

async function login(username, password) {
  const response = await api.authentication.login(username, password);

  if (response.token) {
    Cookies.set('Token', response.token);
  }

  return response.user;
}

async function registration(fields) {
  const response = await api.authentication.registration(fields);

  if (response.token) {
    Cookies.set('Token', response.token);
  }

  return response.user;
}

function logout() {
  // remove user from local storage to log user out
  Cookies.remove('Token');
}
