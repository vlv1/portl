import { createSelector } from 'reselect';
import * as fp from 'lodash/array';
import {getProfilesById} from "../Profiles/selectors";
import {getResultsIds} from "../Search/selectors";

//fp.uniqBy(options, 'id')
export const getFreelancers = state => state.freelancers;
export const getSavedFreelancers = (state) => getProfilesById(state, getFreelancers(state).favourites);
export const getUnlockedFreelancers = (state) => getProfilesById(state, getFreelancers(state).unlocks);

export const getAllFreelancers = createSelector(
  getSavedFreelancers,
  getUnlockedFreelancers,
  (saved, unlocked) => fp.uniqBy([...saved, ...unlocked], 'id')
);

export const getFreelancersByPathName = (state, location) => {
  const endOfCurrentPathname = location.pathname.split('/').slice(-1)[0];
  const specified = {
    unlocked: getUnlockedFreelancers(state),
    saved: getSavedFreelancers(state)
  };

  return specified[endOfCurrentPathname] || getAllFreelancers(state);
};
