import api from 'utils/api';
import {
  FETCH_FAVOURITES_START,
  FETCH_FAVOURITES_SUCCESS,
  FETCH_FAVOURITES_FAILURE,
  FETCH_UNLOCKS_START,
  FETCH_UNLOCKS_SUCCESS,
  FETCH_UNLOCKS_FAILURE,
    UPDATE_FREELANCER
} from './constants';
import {addProfiles, updateProfile} from "../Profiles/actions";

export const fetchFavourites = (page = 0) => {
  return async dispatch => {
    try {
      dispatch({ type: FETCH_FAVOURITES_START });
      const data = await api.profile.favourites.get(page);
      dispatch(addProfiles(data));
      dispatch({ type: FETCH_FAVOURITES_SUCCESS, data: data.map(d=>d.id) });
    } catch (e) {
      console.log(e);
    }
  };
};

export const fetchUnlocks = (page = 0) => {
  return async dispatch => {
    try {
      dispatch({ type: FETCH_UNLOCKS_START });
      const data = await api.profile.unlocks.get(page);
        dispatch(addProfiles(data));
      dispatch({ type: FETCH_UNLOCKS_SUCCESS, data: data.map(d=>d.id)  });
    } catch (e) {
      console.log(e);
    }
  };
};

export function favouriteUser(id) {
    return async dispatch => {
        const user = await api.profile.favourite.set(id);
        dispatch(updateProfile(user))
    }
}
export function unfavouriteUser(id) {
    return async dispatch => {
        const user = await api.profile.favourite.delete(id);
        dispatch(updateProfile(user))
    }
}
