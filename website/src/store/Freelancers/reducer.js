import { combineReducers } from 'redux';
import { FETCH_FAVOURITES_SUCCESS, FETCH_UNLOCKS_SUCCESS, UPDATE_FREELANCER } from './constants';

const initialState = {
  favourites: [],
  unlocks: [],
  hasMorePages: false,

};

function freelancerReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_FAVOURITES_SUCCESS:
      return {
        ...state,
        favourites: action.data
      };
      case UPDATE_FREELANCER:
          const index = state.favourites.findIndex(user => user.id === action.id);
          const unlocksIndex = state.unlocks.findIndex(user => user.id === action.id);

          return {
        ...state,
        favourites: [
            ...state.favourites.slice(0,  index),
            {...action.user},
            ...state.favourites.slice(index + 1)
        ],
              unlocks: [
                  ...state.unlocks.slice(0,  unlocksIndex),
                  {...action.user},
                  ...state.unlocks.slice(unlocksIndex + 1)
              ]
      };
    case FETCH_UNLOCKS_SUCCESS:
      return {
        ...state,
        unlocks: action.data
      };
    default:
      return state;
  }
}

export default freelancerReducer;
