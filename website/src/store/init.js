import api from "../utils/api";
import {getClientData} from "./User/actions";

export const init = () => async (dispatch, getState) => {
    dispatch({type: 'APP_INIT'});
    try {
        const locations = await api.locations();
        dispatch({type: 'SET_FILTER_OPTIONS', options: {locations: locations.map(location=>({...location, title: location.area_name}))}})
        dispatch(getClientData())
    } catch (e) {
        console.log(e);
    }
};