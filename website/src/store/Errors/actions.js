import { ADD_ERROR, REMOVE_ERROR } from './constants';

export const removeError = index => ({
  type: REMOVE_ERROR,
  index
});

export const addError = error => ({
  type: ADD_ERROR,
  error
});
