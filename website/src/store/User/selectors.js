import { createSelector } from 'reselect';

export const getAuthentication = state => state.user.authentication;
export const isLoggedIn = createSelector(getAuthentication, auth => auth.loggedIn);
export const getCurrentUser = createSelector(getAuthentication, auth => auth.user);

export const getErrors = state => state.user.errors;

export const getUser = state => state.user;
export const getCabinetData = createSelector(getUser, user => user.cabinet.data);
