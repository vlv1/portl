import { combineReducers } from 'redux';
import {
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGOUT,
  FETCH_CLIENT_DATA_START,
  FETCH_CLIENT_DATA_SUCCESS,
  FETCH_CLIENT_PURCHASE_HISTORY_START,
  FETCH_CLIENT_PURCHASE_HISTORY_SUCCESS,
  CLIENT_DATA_UPDATE,
    UPDATE_ME
} from './constants';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
      case UPDATE_ME:
      return {
        ...state,
        user: action.user
      };
    case LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case LOGIN_FAILURE:
      return {};
    case LOGOUT:
      return {};
    default:
      return state;
  }
}

export function errors(state = {}, action) {
  switch (action.type) {
    case LOGIN_FAILURE:
      return {
        ...action.errors
      };
    default:
      return state;
  }
}
const cabinetInitialState = {
  isFetching: false,
  data: undefined
};

export function clientCabinetReducer(state = cabinetInitialState, action) {
  switch (action.type) {
    case FETCH_CLIENT_DATA_START:
      return {
        isFetching: true
      };
    case FETCH_CLIENT_DATA_SUCCESS:
      return {
        isFetching: false,
        data: action.data
      };
    case CLIENT_DATA_UPDATE:
      return {
        ...state,
        ...action.data
      };
      case UPDATE_ME:
        return {
          ...state,
            data: {...state.data, user: action.user}
        }

    default:
      return state;
  }
}

const clientPurchasesInitialState = {
  isFetching: false,
  data: undefined
}

export function clientPurchaseHistory(state = clientPurchasesInitialState, action) {
  switch (action.type) {
    case FETCH_CLIENT_PURCHASE_HISTORY_START:
      return {
        isFetching: true
      };
  case FETCH_CLIENT_PURCHASE_HISTORY_SUCCESS:
    return {
      isFetching: false,
      data: action.data
    };
  default:
  return state;
  }
}

const userReducer = combineReducers({
  authentication,
  errors,
  cabinet: clientCabinetReducer,
  purchaseHistory: clientPurchaseHistory
});

export default userReducer;
