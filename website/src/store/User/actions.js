import {
    FETCH_CLIENT_DATA_START,
    FETCH_CLIENT_DATA_SUCCESS,
    FETCH_CLIENT_PURCHASE_HISTORY_START,
    FETCH_CLIENT_PURCHASE_HISTORY_SUCCESS,
    FETCH_CLIENT_PURCHASE_HISTORY_FAILURE,
    LOGIN_FAILURE,
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    REGISTRATION_FAILURE,
    REGISTRATION_REQUEST,
    REGISTRATION_SUCCESS,
    CLIENT_DATA_UPDATE,
    CLIENT_DATA_ERROR,
    SET_FREELANCER_RATING_SUCCESS,
    SET_FREELANCER_RATING_ERROR, UPDATE_ME
} from './constants';
import { history } from '../../App';
import { userService } from '../../services';
import api from '../../utils/api';
import { modalShow } from '../Modal/actions';

function login(username, password) {
  return async dispatch => {
    try {
      dispatch({
        type: LOGIN_REQUEST
      });

      const user = await userService.login(username, password);

      dispatch({
        type: LOGIN_SUCCESS,
        user
      });

      history.replace(`/search/`);
    } catch (error) {
      const e = await error.response.json();
      dispatch({
        type: LOGIN_FAILURE,
        errors: e.errors
      });
    }
  };
}

function registration(fields) {
  return async dispatch => {
    try {
      dispatch({
        type: REGISTRATION_REQUEST
      });

      const user = await userService.registration(fields);

      dispatch({
        type: REGISTRATION_SUCCESS,
        user
      });

      history.replace(`/search/`);
    } catch (error) {
      const e = await error.response.json();
      dispatch({
        type: REGISTRATION_FAILURE,
        errors: e.errors
      });
    }
  };
}


function getClientData() {
  return async dispatch => {
    try {
      dispatch({ type: FETCH_CLIENT_DATA_START });
      const data = await api.profile.me.get();
      dispatch({ type: FETCH_CLIENT_DATA_SUCCESS, data });
    } catch (e) {
      console.log(e);
    }
  };
}

function getPurchaseHistory() {
  return async dispatch => {
    try {
      dispatch({ type: FETCH_CLIENT_PURCHASE_HISTORY_START });
      const data = await api.profile.purchaseHistory();
      dispatch({ type: FETCH_CLIENT_PURCHASE_HISTORY_SUCCESS, data });
    } catch (e) {
      console.log(e);
    }
  }
}

function resetPassword() {return ''}

function saveClientChanges(updatedProfile) {
  return async dispatch => {
      const profile = await api.profile.me.patch(updatedProfile);
      dispatch({ type: 'UPDATE_ME', user: profile.user });
  };
}

// function setRating(id, score) {
//   return async dispatch => {
//     try {
//
//     }
//   }
// }

export { login, registration, getClientData, getPurchaseHistory, saveClientChanges, resetPassword };
