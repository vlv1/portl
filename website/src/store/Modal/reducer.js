import { MODAL_SHOW, MODAL_CLOSE } from './constants';

const initialState = {
  open: false,
  modalType: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case MODAL_SHOW:
      return {
        ...state,
        open: true,
        modalType: action.modalType,
        data: action.data
      };
    case MODAL_CLOSE:
      return {
        ...state,
        open: false,
        modalType: ''
      };

    default:
      return state;
  }
};

export default reducer;
