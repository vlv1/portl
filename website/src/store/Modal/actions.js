import { MODAL_SHOW, MODAL_CLOSE } from './constants';

export const modalShow = (modalType, data) => ({
  type: MODAL_SHOW,
  modalType,
  data
});

export const modalClose = () => ({
  type: MODAL_CLOSE,
  error: undefined
});
