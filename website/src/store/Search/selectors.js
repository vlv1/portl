import { createSelector } from 'reselect';
import * as fp from 'lodash/array';
import {getProfileById, getProfilesById} from "../Profiles/selectors";

export const getSearch = state => state.search;
export const getInputs = createSelector(getSearch, search => search.inputs);
export const getSearchInput = createSelector(getInputs, inputs => inputs.search);
export const getCityInput = createSelector(getInputs, inputs => inputs.city);

export const getFilters = createSelector(getSearch, search => search.filters.filters);
export const getActiveFilter = createSelector(getSearch, search => search.filters.active);
export const getFilter = (state, name) => getFilters(state)[name];
export const isFilterActive = (state, name) => getActiveFilter(state) === name;
export const hasActiveFilter = createSelector(
  getActiveFilter,
  activeFilter => activeFilter !== undefined
);

export const getOptions = createSelector(getSearch, search => search.filters.options);
export const getOptionsByName = (state, name) => {
  const options = getOptions(state)[name];
  return fp.uniqBy(options, 'id');
};

export const getSelectedIds = (state, name) => getFilter(state, name).selected;
const resolveOptions = (state, name) => ({
  options: getOptionsByName(state, name),
  selected: getSelectedIds(state, name)
});

export const getCSTitles = (state, name) => {
  const options = getOptionsByName(state, name);
  const selectedIDs = getSelectedIds(state, name);

  return options
    .filter(option => selectedIDs.indexOf(option.id) !== -1)
    .map(e => e.title)
    .join(',');
};

export const getCSIds = (state, name) => getSelectedIds(state, name).join(',');
export const getDailyRate = (state, name) => {
  const value = getFilter(state, name).value;
  return value ? { daily_rate_from: value[0], daily_rate_to: value[1] } : undefined;
};

export const getAvailabilityFilter = state => {
  const value = getFilter(state, 'availability').value;
  return value
    ? { available_from: value[0].format('YYYY-MM-DD'), available_to: value[1].format('YYYY-MM-DD') }
    : {};
};

export const getUnselectedOptions = (state, name) => {
  const { options, selected } = resolveOptions(state, name);
  return fp.pullAllWith(options, selected, (o, s) => o.id === s);
};

export const getSelectedOptions = (state, name) => {
  const { options, selected } = resolveOptions(state, name);
  return fp.intersectionWith(options, selected, (o, s) => o.id === s);
};

export const getResults = createSelector(getSearch, search => search.results);
export const getResultsIds= createSelector(getResults, results => results.ids);
export const getResultsData = createSelector(getResults, results => results.data);

export const getFreelancersFromResult = (state) => getProfilesById(state, getResultsIds(state));

export const getPathname = state => state.router.location.pathname;
export const getSearchQuery = state => state.router.location.search;
export const getFreelancerId = createSelector(getPathname, pathname => Number(pathname.split('/').pop()));
export const getFreelancerById = (state) => {
  console.log(getFreelancerId(state), getProfileById(state, getFreelancerId(state)))
  return getProfileById(state, getFreelancerId(state))
}
