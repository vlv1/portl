import { push } from 'connected-react-router';
import { encodeQueryData } from 'utils/query';
import { history } from 'App';

import api from 'utils/api';
import {
  CLEAN_FIELD,
  FIELD_INPUT,
  FILTER_INPUT,
  FETCH_SEARCH_RESULT_START,
  FETCH_SEARCH_RESULT_SUCCESS,
  CLICK_FILTER,
  OUTER_CLICK_FILTER,
  SET_FILTER_OPTIONS,
  SELECT_CHECKBOX,
  UNSELECT_CHECKBOX,
  CLEAR_FILTER,
  APPLY_FILTER,
  CLEAR_RANGE_FILTER,
  SET_RANGE_FILTER,
  CLEAR_FILTER_VALUE,
  CLOSE_FILTER,
  SET_RANGE_OPTIONS,
  UPDATE_USER,
  ADD_USER
} from './constants';
import {
  getSearchInput,
  getCityInput,
  getPathname,
  getCSIds,
  getCSTitles,
  getDailyRate,
  getFilter,
  getActiveFilter,
  getAvailabilityFilter
} from './selectors';
import { modalShow } from '../Modal/actions';
import {addProfiles, updateProfile} from '../Profiles/actions';

export const searchFreelancers = (query, withoutLoader = false) => async dispatch => {
  try {
    !withoutLoader && dispatch({type: FETCH_SEARCH_RESULT_START});
    const queryToApi = {
      ...query,
      query: query.query === '' ? ' ' : query.query
    };
    const result = await api.search(queryToApi);

    dispatch({
      type: FETCH_SEARCH_RESULT_SUCCESS,
      data: result.freelancers
    });

      dispatch(addProfiles(result.freelancers));

      dispatch({
        type: SET_FILTER_OPTIONS,
        options: result.filters
      });

    const { maximum, minimum } = result.filters.daily_rates;
    dispatch({
      type: SET_RANGE_OPTIONS,
      rate: [minimum, maximum]
    });

    if(query.page !== 0) return 0;
    history.replace(`/search/${encodeQueryData({...query, page: undefined})}`);
  } catch (e) {
    const err = await e.response.json();
    console.log(err);
  }
};

export const parseFieldsFromQueryString = searchParams => async dispatch => {
  const { daily_rate_from, daily_rate_to } = searchParams;
  searchParams.query &&
    dispatch({
      type: FIELD_INPUT,
      name: 'search',
      value: searchParams.query
    });

  searchParams.city &&
    dispatch({
      type: FIELD_INPUT,
      name: 'city',
      value: searchParams.city
    });
  searchParams.available_from &&
    searchParams.available_to &&
    dispatch({
      type: FILTER_INPUT,
      name: 'availability',
      value: [searchParams.available_from, searchParams.available_to]
    });

  const queryData = {
    query: searchParams.query,
    city: searchParams.city
  };

  dispatch(searchFreelancers(queryData));
};

export const searchRequest = (page = 0) => async (dispatch, getState) => {
  const pathname = getPathname(getState());
  if (pathname === '/') {
    dispatch(push('/search?from_landing=1'));
  } else {
    const queryData = {
      query: getSearchInput(getState()).value,
      city: getCityInput(getState()).value,
      key_skills: getCSIds(getState(), 'key_skills'),
      sectors: getCSIds(getState(), 'sectors'),
      qualifications: getCSIds(getState(), 'qualifications'),
      languages: getCSTitles(getState(), 'languages'),
        locations: getCSTitles(getState(), 'locations'),
        page,
      ...getAvailabilityFilter(getState()),
      ...getDailyRate(getState(), 'rate')
    };

    dispatch(searchFreelancers(queryData, page !== 0));
  }
};

export const clickCheckbox = (value, name, id) => ({
  type: value ? UNSELECT_CHECKBOX : SELECT_CHECKBOX,
  name,
  id
});

export const inputFilter = (name, value) => {
  return {
    type: FILTER_INPUT,
    name,
    value
  };
};

export const clearFilter = name => (dispatch, getState) => {
  dispatch({ type: CLEAR_FILTER, name });

  const filter = getFilter(getState(), name);
  if (filter.value) {
    dispatch(searchRequest());
  }
};
export const clearRangeFilter = () => ({ type: CLEAR_RANGE_FILTER });
export const setRangeFilterValue = value => (dispatch, getState) => {
  dispatch({
    type: SET_RANGE_FILTER,
    value
  });

  dispatch({
    type: CLOSE_FILTER
  });
  dispatch(searchRequest());
};

export const filterClick = name => async (dispatch, getState) => {
  const active = getActiveFilter(getState());
  name !== active
    ? dispatch({
        type: CLICK_FILTER,
        name
      })
    : dispatch({
        type: CLOSE_FILTER
      });
};

export const handleFieldInput = event => {
  return {
    type: FIELD_INPUT,
    name: event.target.name,
    value: event.target.value
  };
};

export const cleanField = name => {
  return {
    type: CLEAN_FIELD,
    value: '',
    name
  };
};

export const hideFilter = () => {
  return {
    type: OUTER_CLICK_FILTER
  };
};

export const clearFilterValue = name => {
  return {
    type: CLEAR_FILTER_VALUE,
    name
  };
};

export const applyFilter = name => dispatch => {
  dispatch({
    type: APPLY_FILTER,
    name
  });
  dispatch({
    type: CLOSE_FILTER
  });
  dispatch(searchRequest());
};

export const favouriteUser = id => async dispatch => {
  try {
    const user = await api.profile.favourite.set(id);
    dispatch(updateProfile(user));
  } catch (e) {
      const err = await e.response.json();
      console.log(err);
  }
};
export const unfavouriteUser = id => async dispatch => {
  const user = await api.profile.favourite.delete(id);
  dispatch(updateProfile(user));
};

export const updateUser = (id, data) => dispatch => {
    dispatch(updateProfile(data));
  dispatch({
    type: UPDATE_USER,
    id,
    data
  });
};

export const addUser = (id, data) => dispatch => {
  dispatch({
    type: ADD_USER,
    id,
    data
  });
};

export const unlockFreelancer = id => async dispatch => {
  try {
    const unlocked = await api.profile.unlock(id);
    dispatch(updateProfile(unlocked));
  } catch (e) {
    const err = await e.response.json();
    console.log(err);
    if (err.detail) {
      history.replace(`/registration/`);
    }

    if (err.status === 'error') {
      dispatch(modalShow('ERROR', err.errors));
    }
  }
};

export const requestAvailability = id => async dispatch => {
  try {
    const user = await api.profile.availability(id);
    dispatch(modalShow('INFO_MESSAGE', {status: 'Info', message: 'Request has been sent'}))
  } catch (e) {
      const err = await e.response.json();
      console.log(err);
  }
};

export const fetchUser = (id, addNew = false) => async dispatch => {
  const user = await api.profile.get(id);
  dispatch(updateProfile(user));
    !addNew ? dispatch(updateUser(id, user)) : dispatch(addUser(id, user));
};
