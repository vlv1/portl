import { combineReducers } from 'redux';
import {
  FETCH_SEARCH_RESULT_START,
  FETCH_SEARCH_RESULT_SUCCESS,
  FIELD_INPUT,
  CLEAN_FIELD,
  CLICK_FILTER,
  OUTER_CLICK_FILTER,
  SET_FILTER_OPTIONS,
  SELECT_CHECKBOX,
  UNSELECT_CHECKBOX,
  CLEAR_FILTER,
  APPLY_FILTER,
  CLEAR_RANGE_FILTER,
  SET_RANGE_FILTER,
  CLEAR_FILTER_VALUE,
  FILTER_INPUT,
  CLOSE_FILTER,
  SET_RANGE_OPTIONS,
  UPDATE_USER,
  ADD_USER
} from './constants';

const initialInputsState = {
  search: { name: 'search', value: '', placeholder: 'Try “Project manager automotive with MBA”' },
  city: { name: 'city', value: '', placeholder: 'All cities' }
};

function inputReducer(state = initialInputsState, action) {
  switch (action.type) {
    case FIELD_INPUT:
    case CLEAN_FIELD:
      return {
        ...state,
        [action.name]: {
          ...state[action.name],
          value: action.value
        }
      };
    default:
      return state;
  }
}

const initialFiltersState = {
  active: undefined,
  options: {},
  filters: {
    availability: {
      name: 'availability',
      placeholder: 'Availability',
      value: undefined
    },
    rate: {
      name: 'rate',
      placeholder: 'Daily rate',
      value: undefined,
      range: [1, 10]
    },
    key_skills: {
      name: 'key_skills',
      placeholder: 'Key skills',
      queryPlaceholder: 'Find skill, e.g. “Management”',
      selected: [],
      value: undefined
    },
    sectors: {
      name: 'sectors',
      queryPlaceholder: 'Find sector, e.g. “Packaging”',
      placeholder: 'Sectors',
      selected: [],
      value: undefined
    },
    qualifications: {
      name: 'qualifications',
      selected: [],
      queryPlaceholder: 'Find qualification, e.g. “CPM”',
      placeholder: 'Qualifications',
      value: undefined
    },

      locations: {
      name: 'locations',
      placeholder: 'Locations',
      selected: [],
      value: undefined
    },
      languages: {
          name: 'languages',
          placeholder: 'More filters',
          selected: [],
          value: undefined
      },
  }
};

function filtersReducer(state = initialFiltersState, action) {
  switch (action.type) {
    case FILTER_INPUT:
      return {
        ...state,
        filters: {
          ...state.filters,
          [action.name]: {
            ...state.filters[action.name],
            value: action.value
          }
        }
      };
    case CLICK_FILTER:
      return {
        ...state,
        active: action.name
      };
    case OUTER_CLICK_FILTER:
    case CLOSE_FILTER:
      return {
        ...state,
        active: undefined
      };
      case SET_FILTER_OPTIONS:
      return {
        ...state,
        options: { ...state.options, ...action.options }
      };
    case SET_RANGE_OPTIONS:
      return {
        ...state,
        filters: {
          ...state.filters,
          rate: {
            ...state.filters.rate,
            range: action.rate
          }
        }
      };
    case SELECT_CHECKBOX:
      return {
        ...state,
        filters: {
          ...state.filters,
          [action.name]: {
            ...state.filters[action.name],
            selected: [...state.filters[action.name].selected, action.id],
            value: [...state.filters[action.name].selected, action.id]
          }
        }
      };
    case UNSELECT_CHECKBOX:
      return {
        ...state,
        filters: {
          ...state.filters,
          [action.name]: {
            ...state.filters[action.name],
            selected: state.filters[action.name].selected.filter(e => e !== action.id),
            value: state.filters[action.name].selected.filter(e => e !== action.id)
          }
        }
      };
    case CLEAR_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          [action.name]: {
            ...state.filters[action.name],
            selected: [],
            value: undefined
          }
        },
        active: undefined
      };
    case APPLY_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          [action.name]: {
            ...state.filters[action.name]
          }
        }
      };
    case CLEAR_RANGE_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          rate: {
            ...state.filters.rate,
            value: undefined,
            range: []
          }
        }
      };
    case SET_RANGE_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          rate: {
            ...state.filters.rate,
            value: action.value
          }
        },
        active: undefined
      };
    case CLEAR_FILTER_VALUE:
      return {
        ...state,
        filters: {
          ...state.filters,
          [action.name]: {
            ...state.filters[action.name],
            value: undefined
          }
        }
      };
    default:
      return state;
  }
}

const initialResultState = {
  isFetching: false,
  hasMorePages: false,
  data: undefined,
  ids: []
};

function resultsReducer(state = initialResultState, action) {
  switch (action.type) {
    case FETCH_SEARCH_RESULT_START:
      return {
        ...state,
        isFetching: true
      };
      case FETCH_SEARCH_RESULT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        hasMorePages: action.data.length === 10,
        ids: action.data.map((e)=>e.id)
      };
    case UPDATE_USER:
      const index = state.data.findIndex(user => user.id === action.id);
      return {
        ...state,
        data: [
          ...state.data.slice(0, index),
          {
            ...action.data
          },
          ...state.data.slice(index + 1)
        ]
      };
    case ADD_USER:
      return {
        ...state,
        data: [{ ...action.data }]
      };
    default:
      return state;
  }
}

const searchReducer = combineReducers({
  inputs: inputReducer,
  filters: filtersReducer,
  results: resultsReducer
});

export default searchReducer;
