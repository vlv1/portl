import { combineReducers } from 'redux';
import { errorsReducer } from 'store/Errors';
import { searchReducer } from 'store/Search';
import { userReducer } from 'store/User';
import { modalReducer } from 'store/Modal';
import { freelancersReducer } from 'store/Freelancers';
import { requestsReducer } from 'store/Requests';
import { profilesReducer } from 'store/Profiles';

export default () => {
  return combineReducers({
    errors: errorsReducer,
    search: searchReducer,
    user: userReducer,
    modal: modalReducer,
    freelancers: freelancersReducer,
    requests: requestsReducer,
    profiles: profilesReducer
  });
};
