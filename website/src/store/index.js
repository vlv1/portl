import { createStore, applyMiddleware } from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import combineReducers from './combineReducers';

export default function configureStore(history) {
  const middlewares = [thunk, routerMiddleware(history)];
  const enhancers = composeWithDevTools(applyMiddleware(...middlewares));
  return createStore(connectRouter(history)(combineReducers()), enhancers);
}
