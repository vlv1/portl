import { FETCH_REQUESTS_SUCCESS, UPDATE_REQUSETS_USER } from './constants';

const initialState = [];

function requestsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_REQUESTS_SUCCESS:
      return [...action.data];
    case UPDATE_REQUSETS_USER:
        const index = state.findIndex(user => user.id === action.id);
        return [
            ...state.slice(0, index),
            {
                ...action.user
            },
            ...state.slice(index + 1)
        ]
    default:
      return state;
  }
}

export default requestsReducer;
