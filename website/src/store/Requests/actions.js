import api from 'utils/api';
import { FETCH_REQUESTS_FAILURE, FETCH_REQUESTS_START, FETCH_REQUESTS_SUCCESS, UPDATE_REQUSETS_USER } from './constants';
import {addProfiles, updateProfile} from "../Profiles/actions";

export const fetchRequests = () => {
  return async dispatch => {
    try {
      dispatch({ type: FETCH_REQUESTS_START });
      const data = await api.profile.requests();
        dispatch(addProfiles(data))
        dispatch({ type: FETCH_REQUESTS_SUCCESS, data: data.map(e=>e.id) });
    } catch (e) {
      console.log(e);
    }
  };
};
export function unfavouriteUser(id) {
    return async dipatch => {
        const user = await api.profile.favourite.delete(id);
        dipatch(updateProfile(user))
    }
}
export function favouriteUser(id) {
    return async dipatch => {
        const user = await api.profile.favourite.set(id);
        dipatch(updateProfile(user))
    }
}
