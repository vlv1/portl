import { createSelector } from 'reselect';
import * as fp from 'lodash/array';
import moment from 'moment';
import {getProfilesById} from "../Profiles/selectors";
import {getFreelancers} from "../Freelancers/selectors";

//fp.uniqBy(options, 'id')
export const getRequests = state => getProfilesById(state, state.requests);

export const getConfirmedRequests = createSelector(getRequests, requests =>
  Object.values(requests).filter(e => e.availability_updated)
);
export const getRequestsInPending = createSelector(getRequests, requests =>
  Object.values(requests).filter(e => !e.availability_updated)
);

export const getRequestsByPathName = (state, location) => {
  const endOfCurrentPathname = location.pathname.split('/').slice(-1)[0];
  const specified = {
    confirmed: getConfirmedRequests(state),
    pending: getRequestsInPending(state)
  };

  return specified[endOfCurrentPathname] || getRequests(state);
};
