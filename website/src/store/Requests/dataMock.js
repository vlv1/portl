import UserAvatar from 'assets/UserAvatarUnlocked.png';

export default [
  {
    id: 1,
    company: null,
    job_title: {
      id: 32,
      title: 'Process Improvement Consultant'
    },
    user: {
      id: 931,
      initials: 'SW'
    },
    key_skills: [
      {
        id: 63,
        title: 'Coaching',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 77,
        title: 'Lean',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 86,
        title: 'Poke-Yoke',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 87,
        title: 'Process Design',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 100,
        title: 'Target Operating Model (TOM)',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 103,
        title: 'Training',
        experience: 0,
        accepted: true,
        match: false
      }
    ],
    sectors: [
      {
        id: 230,
        title: 'Marketing and Advertising',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 239,
        title: 'Motion Pictures and Film',
        experience: 0,
        accepted: true,
        match: false
      }
    ],
    qualifications: [
      {
        id: 116,
        title: 'Chartered Certified Accountant (ACCA)',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 145,
        title: 'PMP (Project Management Professional)',
        experience: 0,
        accepted: true,
        match: false
      }
    ],
    languages: [
      {
        id: 53,
        title: 'French',
        experience: 0,
        accepted: true,
        match: false
      }
    ],
    locations: [
      {
        id: 5,
        name: 'Beijing'
      },
      {
        id: 2,
        name: 'Moscow'
      },
      {
        id: 1,
        name: 'New York'
      },
      {
        id: 4,
        name: 'Paris'
      }
    ],
    hour_rate: {
      minimum: 6,
      maximum: 12,
      type: 0
    },
    day_rate: {
      minimum: 30,
      maximum: 112,
      type: 1
    },
    unavailable_periods: [],
    unlocked: false,
    favourite: false,
    unlocked_at: null,
    availability_updated: false,
    availability_requested_at: null,
    availability_updated_at: '2018-10-09T19:55:02.753366Z',
    can_favourite: false,
    search_percentage: null,
    rating: 3.24,
    bio: null,
    created_at: '2018-10-09T20:55:02Z',
    updated_at: '2018-10-09T20:55:02Z',
    unavailable_weekends: true,
    always_available: false,
    on_hold: false,
    score: 1
  },
  {
    id: 2,
    company: 'Sole Trader',
    job_title: {
      id: 32,
      title: 'Process Improvement Consultant'
    },
    user: {
      id: 931,
      initials: 'SW',
      first_name: 'Matthew',
      last_name: 'Williams',
      avatar: UserAvatar,
      is_active: true,
      verified_email: true,
      verified_mobile: true,
      email: 'matthew.williams@gmail.com',
      phone: '+44 7911 125548'
    },
    key_skills: [
      {
        id: 63,
        title: 'Coaching',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 77,
        title: 'Lean',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 86,
        title: 'Poke-Yoke',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 87,
        title: 'Process Design',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 100,
        title: 'Target Operating Model (TOM)',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 103,
        title: 'Training',
        experience: 0,
        accepted: true,
        match: false
      }
    ],
    sectors: [
      {
        id: 230,
        title: 'Marketing and Advertising',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 239,
        title: 'Motion Pictures and Film',
        experience: 0,
        accepted: true,
        match: false
      }
    ],
    qualifications: [
      {
        id: 116,
        title: 'Chartered Certified Accountant (ACCA)',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 145,
        title: 'PMP (Project Management Professional)',
        experience: 0,
        accepted: true,
        match: false
      }
    ],
    languages: [
      {
        id: 53,
        title: 'French',
        experience: 0,
        accepted: true,
        match: false
      }
    ],
    locations: [
      {
        id: 5,
        name: 'Beijing'
      },
      {
        id: 2,
        name: 'Moscow'
      },
      {
        id: 1,
        name: 'New York'
      },
      {
        id: 4,
        name: 'Paris'
      }
    ],
    hour_rate: {
      minimum: 6,
      maximum: 12,
      type: 0
    },
    day_rate: {
      minimum: 30,
      maximum: 112,
      type: 1
    },
    unavailable_periods: [],
    unlocked: true,
    favourite: false,
    unlocked_at: null,
    availability_updated: false,
    availability_requested_at: null,
    availability_updated_at: '2018-10-09T19:55:02.753366Z',
    can_favourite: false,
    search_percentage: null,
    rating: 3.24,
    bio: null,
    created_at: '2018-10-09T20:55:02Z',
    updated_at: '2018-10-09T20:55:02Z',
    unavailable_weekends: true,
    always_available: false,
    on_hold: false,
    score: 1
  },
  {
    id: 3,
    company: 'Sole Trader',
    job_title: {
      id: 32,
      title: 'Process Improvement Consultant'
    },
    user: {
      id: 931,
      initials: 'SW',
      first_name: 'Matthew',
      last_name: 'Williams',
      avatar: '',
      is_active: true,
      verified_email: true,
      verified_mobile: true,
      email: 'matthew.williams@gmail.com',
      phone: '+44 7911 125548'
    },
    key_skills: [
      {
        id: 63,
        title: 'Coaching',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 77,
        title: 'Lean',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 86,
        title: 'Poke-Yoke',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 87,
        title: 'Process Design',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 100,
        title: 'Target Operating Model (TOM)',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 103,
        title: 'Training',
        experience: 0,
        accepted: true,
        match: false
      }
    ],
    sectors: [
      {
        id: 230,
        title: 'Marketing and Advertising',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 239,
        title: 'Motion Pictures and Film',
        experience: 0,
        accepted: true,
        match: false
      }
    ],
    qualifications: [
      {
        id: 116,
        title: 'Chartered Certified Accountant (ACCA)',
        experience: 0,
        accepted: true,
        match: false
      },
      {
        id: 145,
        title: 'PMP (Project Management Professional)',
        experience: 0,
        accepted: true,
        match: false
      }
    ],
    languages: [
      {
        id: 53,
        title: 'French',
        experience: 0,
        accepted: true,
        match: false
      }
    ],
    locations: [
      {
        id: 5,
        name: 'Beijing'
      },
      {
        id: 2,
        name: 'Moscow'
      },
      {
        id: 1,
        name: 'New York'
      },
      {
        id: 4,
        name: 'Paris'
      }
    ],
    hour_rate: {
      minimum: 6,
      maximum: 12,
      type: 0
    },
    day_rate: {
      minimum: 30,
      maximum: 112,
      type: 1
    },
    unavailable_periods: [],
    unlocked: true,
    favourite: false,
    unlocked_at: null,
    availability_updated: false,
    availability_requested_at: null,
    availability_updated_at: '2018-10-09T19:55:02.753366Z',
    can_favourite: false,
    search_percentage: null,
    rating: 3.24,
    bio: null,
    created_at: '2018-10-09T20:55:02Z',
    updated_at: '2018-10-09T20:55:02Z',
    unavailable_weekends: true,
    always_available: false,
    on_hold: false,
    score: 1
  }
];
