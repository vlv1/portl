import { combineReducers } from 'redux';
import { ADD_PROFILES, UPDATE_PROFILE } from './constants';

const initialState = [];

function profilesReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_PROFILES:
      return action.data.reduce((acc, current, index, arr)=>{
        const profileIndex = state.findIndex(profile=>profile.id === current.id);
        if (profileIndex === -1) return [...acc, current];
        return [
            ...acc.slice(0, profileIndex),
            current,
            ...acc.slice(profileIndex + 1)
        ]
      }, state);
      case UPDATE_PROFILE:
          const profileIndex = state.findIndex(profile=>profile.id === action.data.id);
          if(profileIndex !== -1)
            return [
                ...state.slice(0, profileIndex),
                action.data,
                ...state.slice(profileIndex + 1)
            ];
          return [...state, action.data];
    default:
      return state;
  }
}

export default profilesReducer;
