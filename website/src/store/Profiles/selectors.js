import { createSelector } from 'reselect';

//fp.uniqBy(options, 'id')
export const getAllProfiles = state => state.profiles;
export const getProfileById = (state, id) => getAllProfiles(state).find(profile => profile.id === id);
export const getProfilesById = (state, ids = []) => {
    return getAllProfiles(state).filter(profile => ids.find(id => id === profile.id));
}
