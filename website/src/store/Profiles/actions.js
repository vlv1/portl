import { ADD_PROFILES, UPDATE_PROFILE } from './constants';
import api from "../../utils/api";

export const addProfiles = (users = []) => ({type: ADD_PROFILES, data: users});
export const updateProfile = (user) => ({type: UPDATE_PROFILE, data: user});

export const rateFreelancer = (id, score) => async dispatch => {
        const updatedUser = await api.profile.rate(id, {work_score: score});
        dispatch(updateProfile(updatedUser))

};